module.exports = {
  apps: [
    {
      name: 'notpunch-front',
      script: 'expressServer.js',

      // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
      args: 'one two',
      instances: 1,
      autorestart: true,
      watch: true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production'
      },
      env_production: {
        NODE_ENV: 'production'
      },
      env_qa: {
        NODE_ENV: 'qa'
      }
    }
  ]
};
