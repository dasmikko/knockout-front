'use strict'

self.addEventListener('fetch', ev => {
  ev.respondWith(
    caches
    .match(ev.request)
    .then(response => {
      if (response) return response
      return fetch(ev.request)
    })
  )
});