import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import {
  loadPunchyLabsFromStorageBoolean,
  loadScaleFromStorage,
  loadThemeFromStorage,
  loadWidthFromStorage,
} from './services/theme';

import { updateTheme } from './state/style';

const labsEnabled = loadPunchyLabsFromStorageBoolean();

const AppThemeProvider = ({ children }) => {
  const theme = useSelector((state) => state.style.theme);
  const width = useSelector((state) => state.style.width);
  const loggedIn = useSelector((state) => state.user.loggedIn);
  const dispatch = useDispatch();

  useEffect(() => {
    function handleThemeUpdate(event) {
      if (!loggedIn) {
        const newTheme = event.matches ? 'dark' : 'light';
        dispatch(updateTheme(newTheme));
      }
    }
    window.matchMedia('(prefers-color-scheme: dark)').addListener(handleThemeUpdate);

    if (labsEnabled && !loggedIn) {
      const newTheme = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
      dispatch(updateTheme(newTheme));
    }

    handleThemeUpdate(window.matchMedia('(prefers-color-scheme: dark)'));
    return () =>
      window.matchMedia('(prefers-color-scheme: dark)').removeListener(handleThemeUpdate);
  });

  return (
    <ThemeProvider
      theme={{
        mode: labsEnabled ? theme : loadThemeFromStorage(),
        width: labsEnabled ? width : loadWidthFromStorage(),
        scale: loadScaleFromStorage(),
      }}
    >
      {children}
    </ThemeProvider>
  );
};

export default AppThemeProvider;

AppThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
