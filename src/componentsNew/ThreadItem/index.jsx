import dayjs from 'dayjs';
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import relativeTime from 'dayjs/plugin/relativeTime';
import styled from 'styled-components';
import { getIcon } from '../../services/icons';
import ratingList from '../../utils/ratingList.json';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeHighlightWeaker,
  ThemeFontSizeLarge,
} from '../../utils/ThemeNew';
import UserRoleWrapper from '../UserRoleWrapper';
import Pagination from '../Pagination';
import UserAvatar from '../Avatar';
import MarkUnreadButton from '../MarkUnreadButton';
import { POSTS_PER_PAGE, unreadPostPage } from '../../utils/postsPerPage';

dayjs.extend(relativeTime);

const ThreadItem = ({
  id,
  createdAt,
  deleted,
  iconId,
  lastPost,
  locked,
  pinned,
  postCount,
  title,
  unreadPostCount,
  user,
  showTopRating,
  firstPostTopRating,
  firstUnreadId,
  tags,
  backgroundUrl,
  backgroundType,
  threadIsRead,
  threadOpacity,
  markUnreadAction,
  markUnreadText,
  minimal,
}) => {
  const lastPage = Math.ceil(postCount / POSTS_PER_PAGE);

  const icon = getIcon(iconId);
  const threadTag = tags ? Object.values(tags[0])[0] : null;
  const topRatingUrl = firstPostTopRating && ratingList[firstPostTopRating.rating].url;
  const unreadPostId = firstUnreadId ? `#post-${firstUnreadId}` : '';
  const unreadPostPageNum = unreadPostPage(unreadPostCount, postCount);
  const lastPostAvatar =
    lastPost && lastPost.user && lastPost.user.avatar_url ? lastPost.user.avatar_url : '';

  const formatDate = (date) => {
    if (!minimal) {
      return date.replace('ago', 'old');
    }
    if (date === 'a few seconds') {
      return 'now';
    }
    const dateArray = date.split(' ');
    const termMap = {
      minute: 'min',
      minutes: 'min',
      hour: 'h',
      hours: 'h',
      day: 'd',
      days: 'd',
      month: 'm',
      months: 'm',
      year: 'y',
      years: 'y',
      a: '1',
      an: '1',
    };
    return dateArray
      .map((word) => {
        if (word in termMap) {
          return termMap[word];
        }
        return word;
      })
      .join(' ');
  };
  const lastPostDate = lastPost && dayjs(lastPost.created_at).fromNow();
  const threadDate = formatDate(dayjs(createdAt).fromNow(minimal));

  return (
    <StyledThreadItem threadOpacity={threadOpacity} minimal={minimal} showTopRating={showTopRating}>
      <div className="image thread-icon">
        <Link to={`/thread/${id}`}>
          <div className="thread-icon-inner">
            <img src={icon.url} alt={icon.description} />
          </div>
        </Link>
      </div>
      <div className="content">
        <div className="first-row">
          {locked && (
            <>
              <i className="locked fas fa-lock" />
              &nbsp;
            </>
          )}
          {pinned && (
            <>
              <i className="pinned fas fa-sticky-note" />
              &nbsp;
            </>
          )}
          {deleted && (
            <>
              <i className="deleted fas fa-trash" />
              &nbsp;
            </>
          )}
          <Link to={`/thread/${id}`} className="thread-title">
            {title}
          </Link>
          {threadIsRead && unreadPostCount && unreadPostCount > 0 ? (
            <Link to={`/thread/${id}/${unreadPostPageNum}${unreadPostId}`} className="unread-posts">
              {`${unreadPostCount} new ${unreadPostCount === 1 ? 'post' : 'posts'}`}
            </Link>
          ) : null}
        </div>
        {!minimal && (
          <div className="second-row">
            {threadTag && (
              <>
                <span className="thread-tag">{threadTag}</span>
                <span> • </span>
              </>
            )}
            <span> by </span>
            <Link to={`/user/${user.id}`}>
              <UserRoleWrapper user={user}>{user.username}</UserRoleWrapper>
            </Link>
            {postCount && postCount > POSTS_PER_PAGE ? (
              <>
                <span className="spacer-bead">&nbsp;&nbsp;•</span>
                <Pagination
                  className="thread-item-pagination"
                  pagePath={`/thread/${id}/`}
                  totalPosts={postCount}
                  small
                />
              </>
            ) : null}
            {threadIsRead && markUnreadAction ? (
              <>
                <span className="spacer-bead">&nbsp;&nbsp;•</span>
                <MarkUnreadButton markUnread={markUnreadAction}>{markUnreadText}</MarkUnreadButton>
              </>
            ) : null}
          </div>
        )}
      </div>
      {showTopRating && (
        <div className="thread-ratings">
          {firstPostTopRating && (
            <>
              <img className="top-rating" src={topRatingUrl} alt="top rating" />
              <span> x</span>
              {firstPostTopRating.count}
            </>
          )}
        </div>
      )}
      <Link
        className="info"
        title="Go to latest post"
        to={`/thread/${id}/${lastPage}#post-${lastPost ? lastPost.id : 1}`}
      >
        {!minimal ? (
          <div className="stats-container">
            <span>
              <i className="fas fa-comments" />
              <span> </span>
              {postCount}
              <span> </span>
              {postCount === 1 ? 'post' : 'posts'}
              <span> </span>
            </span>
            <span>
              <i className="fas fa-clock" />
              &nbsp;
              {threadDate}
            </span>
          </div>
        ) : (
          <div className="stats-container-minimal">
            <div className="stats-container-item">
              <i className="fas fa-comments stats-container-icon" />
              <span className="stats-container-text">{postCount}</span>
            </div>
            <div className="stats-container-item">
              <i className="fas fa-clock stats-container-icon" />
              <span className="stats-container-text">{threadDate}</span>
            </div>
          </div>
        )}
        {lastPost && (
          <div className="latest-post">
            <div className="post-info">
              <span>
                <i className="fas fa-angle-double-right" />
                <span> </span>
                {lastPostDate}
              </span>
              <span className="post-author">
                <span>by </span>
                <UserRoleWrapper user={lastPost.user}>{lastPost.user.username}</UserRoleWrapper>
              </span>
            </div>
            <UserAvatar src={lastPostAvatar} />
          </div>
        )}
      </Link>

      {backgroundUrl && (
        <div
          className="background-image"
          style={{
            backgroundImage: `url(${backgroundUrl})`,
            backgroundSize: backgroundType === 'cover' ? 'cover' : null,
            backgroundPosition: 'center center',
            backgroundRepeat: backgroundType === 'tiled' ? 'repeat' : 'no-repeat',
          }}
        />
      )}
    </StyledThreadItem>
  );
};

ThreadItem.propTypes = {
  id: PropTypes.number.isRequired,
  createdAt: PropTypes.string.isRequired,
  user: PropTypes.shape({
    avatarUrl: PropTypes.string,
    username: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
  }).isRequired,
  deleted: PropTypes.bool,
  iconId: PropTypes.number.isRequired,
  lastPost: PropTypes.shape({
    id: PropTypes.number.isRequired,
    created_at: PropTypes.string.isRequired,
    user: PropTypes.shape({
      avatar_url: PropTypes.string,
      username: PropTypes.string.isRequired,
    }).isRequired,
  }),
  locked: PropTypes.bool,
  pinned: PropTypes.bool,
  postCount: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  unreadPostCount: PropTypes.number,
  showTopRating: PropTypes.bool,
  firstPostTopRating: PropTypes.shape({
    count: PropTypes.number.isRequired,
    rating: PropTypes.string.isRequired,
  }),
  firstUnreadId: PropTypes.number,
  tags: PropTypes.arrayOf(PropTypes.object),
  backgroundUrl: PropTypes.string,
  backgroundType: PropTypes.string,
  threadIsRead: PropTypes.bool,
  threadOpacity: PropTypes.string,
  markUnreadAction: PropTypes.func,
  markUnreadText: PropTypes.string,
  minimal: PropTypes.bool,
};

ThreadItem.defaultProps = {
  deleted: false,
  locked: false,
  pinned: false,
  unreadPostCount: 0,
  showTopRating: false,
  firstPostTopRating: null,
  firstUnreadId: 0,
  tags: [{}],
  backgroundUrl: '',
  backgroundType: '',
  threadIsRead: true,
  threadOpacity: undefined,
  markUnreadText: 'Mark unread',
  lastPost: undefined,
  markUnreadAction: undefined,
  minimal: false,
};

export default ThreadItem;

const StyledThreadItem = styled.div`
  background: ${ThemeBackgroundDarker};
  margin-bottom: ${ThemeVerticalPadding};
  font-size: ${ThemeFontSizeMedium};
  opacity: ${(props) => props.threadOpacity};
  position: relative;

  display: grid;
  ${(props) =>
    props.showTopRating
      ? `
      grid-template-columns: 60px 1fr 50px ${props.minimal ? 160 : 280}px;`
      : `
      grid-template-columns: 60px 1fr ${props.minimal ? 160 : 270}px 5px;
  `}

  align-items: center;

  .thread-title {
    ${(props) => props.minimal && `font-size: ${ThemeFontSizeLarge};`}
  }

  .image {
    text-align: center;
    img {
      width: 100%;
    }
  }

  .image,
  .info {
    height: 100%;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    background: ${ThemeBackgroundLighter};
    box-sizing: border-box;
  }

  .thread-icon {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .thread-icon-inner {
    width: 40px;
    height: 40px;
  }

  .content {
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  }

  .locked {
    color: #ffcb00;
  }
  .pinned {
    color: #acff49;
  }
  .deleted {
    color: #ff3535;
  }

  .first-row {
    display: flex;
    align-items: center;
    ${(props) => !props.minimal && `margin-bottom: ${ThemeVerticalPadding(props)};`}
  }

  .unread-posts {
    background: ${ThemeHighlightWeaker};
    padding: 5px;
    line-height: initial;
    margin-left: 8px;
    display: inline-block;
    font-size: ${ThemeFontSizeSmall};
    color: white;
    transition: 0.2s;
    white-space: nowrap;

    &:hover {
      opacity: 1;
    }
  }

  .second-row {
    font-size: ${ThemeFontSizeSmall};

    .thread-tag {
      padding: 1px 2px;
      background: ${ThemeBackgroundLighter};
    }
  }

  .thread-ratings {
    padding: 0;
    font-size: ${ThemeFontSizeSmall};
  }

  .top-rating {
    max-width: 21px;
    height: auto;
    vertical-align: sub;
  }

  .info {
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: ${ThemeFontSizeSmall};
  }

  .stats-container {
    display: flex;
    width: 140px;
    height: 100%;
    flex-direction: column;
    justify-content: space-around;
  }

  .stats-container-minimal {
    display: flex;
    height: 100%;
    align-items: center;
    flex-grow: 1;
    justify-content: space-around;

    .stats-container-item {
      display: flex;
      align-items: center;
    }
    .stats-container-text {
      font-size: ${ThemeFontSizeMedium};
    }

    .stats-container-icon {
      font-size: ${ThemeFontSizeLarge};
      margin-right: 6px;
    }
  }

  .latest-post {
    display: flex;
    height: 100%;
    flex: 1;
    margin: 0 ${ThemeHorizontalPadding};
    justify-content: space-between;
    padding-left: ${ThemeHorizontalPadding};

    img {
      width: 40px;
      height: auto;
      margin: auto;
      margin-right: 0;
      max-height: 40px;
      background: rgba(0, 0, 0, 0.25);
    }

    .post-info {
      display: flex;
      width: 100px;
      height: 100%;
      flex-direction: column;
      justify-content: space-around;
      line-height: initial;
      margin-right: ${ThemeHorizontalPadding};
    }

    .post-author {
      display: flex;
      overflow: hidden;
      width: 80%;
      white-space: pre;
    }
  }

  &:not(.background-image) {
    z-index: 1;
  }

  .background-image {
    width: 100%;
    height: 100%;
    display: block;
    position: absolute;
    z-index: -1;
    opacity: 0.075;
  }

  @media (max-width: 900px) {
    grid-template-columns: 56px 1fr;

    .thread-icon {
      padding: ${ThemeVerticalPadding} 0;
    }

    .thread-ratings,
    .info {
      display: none;
    }

    .spacer-bead {
      display: none;
    }

    .thread-item-pagination {
      display: block;
      margin-top: ${ThemeVerticalPadding};

      .pagination-item:first-child {
        margin-left: 0;
      }

      .pagination-item,
      .pagination-spacer {
        display: inline-block;
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding}!important;
      }
    }
  }
`;

StyledThreadItem.propTypes = {
  threadOpacity: PropTypes.string,
};

StyledThreadItem.defaultProps = {
  threadOpacity: '1.0',
};
