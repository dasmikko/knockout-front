/* eslint-disable no-alert */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import LoggedInOnly from '../../LoggedInOnly';
import UserGroupRestricted, { usergroupCheck } from '../../UserGroupRestricted';
import { ThemeTextColor, ThemeHorizontalPadding } from '../../../utils/ThemeNew';
import { MODERATOR_GROUPS } from '../../../utils/userGroups';
import BanModal from '../../BanModal';
import ReportModal from '../../ReportModal';

const PostControls = ({
  user,
  postId,
  showBBCode,
  toggleEditable,
  replyToPost,
  byCurrentUser,
  threadLocked,
}) => {
  const [banModalOpen, setBanModalOpen] = useState(false);
  const [reportModalOpen, setReportModalOpen] = useState(false);
  const userIsMod = usergroupCheck(MODERATOR_GROUPS);
  return (
    <LoggedInOnly>
      <StyledPostControls>
        <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
          <button
            className="btn-control"
            type="button"
            onClick={() => setBanModalOpen(true)}
            title="Ban User"
          >
            <i className="fas fa-gavel" />
          </button>
          <BanModal
            userId={user.id}
            postId={postId}
            submitFn={() => setBanModalOpen(false)}
            cancelFn={() => setBanModalOpen(false)}
            isOpen={banModalOpen}
          />
        </UserGroupRestricted>

        <ReportModal
          postId={postId}
          isOpen={reportModalOpen}
          close={() => setReportModalOpen(false)}
        />
        <button
          type="button"
          className="btn-control"
          onClick={() => setReportModalOpen(true)}
          title="Report post"
        >
          <i className="fas fa-flag" />
        </button>

        {!byCurrentUser && (
          <button type="button" className="btn-control" onClick={showBBCode} title="Show BBCode">
            <i className="fas fa-code" />
          </button>
        )}
        {(userIsMod || (byCurrentUser && !threadLocked)) && (
          <button type="button" className="btn-control" onClick={toggleEditable} title="Edit">
            <i className="fas fa-pencil-alt" />
          </button>
        )}
        {(userIsMod || !threadLocked) && (
          <button type="button" className="btn-control" onClick={() => replyToPost()} title="Reply">
            <i className="fas fa-reply" />
          </button>
        )}
      </StyledPostControls>
    </LoggedInOnly>
  );
};
export default PostControls;
PostControls.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    isBanned: PropTypes.bool.isRequired,
    avatar_url: PropTypes.string.isRequired,
  }).isRequired,
  postId: PropTypes.number.isRequired,
  showBBCode: PropTypes.func,
  toggleEditable: PropTypes.func,
  replyToPost: PropTypes.func,
  byCurrentUser: PropTypes.bool,
  threadLocked: PropTypes.bool,
};
PostControls.defaultProps = {
  showBBCode: undefined,
  toggleEditable: undefined,
  replyToPost: undefined,
  byCurrentUser: false,
  threadLocked: false,
};

const StyledPostControls = styled.div`
  display: flex;

  .btn-control {
    height: 100%;
    background: transparent;
    color: ${ThemeTextColor};
    opacity: 0.6;
    border: none;
    cursor: pointer;
    padding: 0 calc(${ThemeHorizontalPadding} * 1.5);
    transition: opacity 100ms ease-in-out;
    &:hover {
      opacity: 1;
    }
  }
`;
