import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import config from '../../../config';

const LoginButton = ({ background, color, route, children }) => (
  <StyledLoginButton
    color={color}
    background={background}
    onClick={() => window.open(`${config.apiHost}${route}`, '_blank')}
  >
    {children}
  </StyledLoginButton>
);

LoginButton.propTypes = {
  background: PropTypes.string,
  color: PropTypes.string,
  route: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

LoginButton.defaultProps = {
  background: undefined,
  color: undefined,
};

export const StyledLoginButton = styled.button`
  display: block;
  position: relative;
  background: ${(props) => props.background || 'black'};
  color: ${(props) => props.color || 'white'};
  border: none;
  border-radius: 5px;
  margin: 9px auto;
  padding: 0 0 0 32px;
  font-size: 13px;
  text-align: center;
  width: 230px;
  height: 32px;
  line-height: 32px;
  cursor: pointer;
  overflow: hidden;

  &:active {
    opacity: 0.9;
  }

  i {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 32px;
    line-height: 32px;
    text-align: center;
    background: rgba(0, 0, 0, 0.1);
  }

  span {
    padding: 0 10px;
  }
`;

export default LoginButton;
