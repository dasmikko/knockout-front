import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

/**
 * A styled video block component.
 *
 * @type {Component}
 */
const StyledVideo = styled('video')`
  display: inline-block;
  max-width: 100%;
  max-height: 2000px;
  margin: 15px 0;

  ${(props) => props.selected && `box-shadow: 0px 0px 0 1px #2900ff;`}
`;

/*
 * A function to determine whether a URL has a video extension.
 *
 * @param {String} url
 * @return {Boolean}
 */
export const isVideo = (url) => {
  const videoExtensions = ['mp4', 'webm'];
  const rgx = /(?:\.([^.]+))?$/;

  const extension = rgx.exec(url);

  return !!videoExtensions.includes(extension[1]);
};

const VideoBB = ({ href }) => (
  <StyledVideo
    controls
    preload="metadata"
    poster="https://img.icons8.com/color/384/000000/movie.png"
  >
    <source src={`${href}#t=0.01`} />
  </StyledVideo>
);

VideoBB.propTypes = {
  href: PropTypes.string.isRequired,
};

export default VideoBB;
