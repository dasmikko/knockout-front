import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeBackgroundLighter,
  ThemeFontSizeSmall,
} from '../../../utils/ThemeNew';

const QuoteBB = ({ username, children, threadPage, threadId, postId }) => {
  const [expanded, setExpanded] = useState(false);

  let scrollTimer;

  useEffect(() => {
    return () => clearTimeout(scrollTimer);
  }, [scrollTimer]);

  const scrollToElement = (elemId) => {
    scrollTimer = setTimeout(() => {
      const element = document.getElementById(`post-${elemId}`);
      element.scrollIntoView({ behavior: 'smooth', block: 'center' });
    });
  };

  return (
    <StyledQuote className={`${expanded ? 'expanded' : ''} quotebox`}>
      <Link
        className="title"
        to={`/thread/${threadId}/${threadPage}#post-${postId}`}
        onClick={() => scrollToElement(postId)}
      >
        {username}
        &nbsp; posted:
      </Link>
      <div
        className={`body ${expanded ? 'expanded' : ''}`}
        onClick={!expanded ? () => setExpanded(!expanded) : () => {}}
        onKeyDown={!expanded ? () => setExpanded(!expanded) : () => {}}
        role="button"
        tabIndex="0"
      >
        <div className="content">{children}</div>
      </div>
      <div className="expander-wrapper">
        <div
          className="expander"
          title="Expand The Quote"
          onClick={() => setExpanded(!expanded)}
          onKeyDown={() => setExpanded(!expanded)}
          role="button"
          tabIndex="0"
        >
          <i className="fas fa-chevron-down" />
        </div>
      </div>
    </StyledQuote>
  );
};

QuoteBB.propTypes = {
  username: PropTypes.string,
  children: PropTypes.node.isRequired,
  threadPage: PropTypes.string,
  threadId: PropTypes.string,
  postId: PropTypes.string,
};

QuoteBB.defaultProps = {
  username: '',
  threadPage: '1',
  threadId: '1',
  postId: '1',
};

const StyledQuote = styled.div`
  background-color: ${ThemeBackgroundLighter};
  border: 1px solid rgba(0, 0, 0, 0.4);
  overflow: hidden;
  position: relative;
  margin: ${ThemeVerticalPadding} 0;
  margin-bottom: calc(${ThemeFontSizeSmall} * 1.5);

  .title {
    display: block;
    background-color: rgba(0, 0, 0, 0.2);
    padding: ${ThemeHorizontalPadding} calc(${ThemeHorizontalPadding} * 1.5);
    font-size: ${ThemeFontSizeSmall};
    cursor: pointer;
    transition: background-color 100ms;
    &:hover,
    &:focus {
      background-color: rgba(0, 0, 0, 0.3);
    }
    &:focus {
      outline: none;
    }
  }
  .body {
    margin: calc(${ThemeHorizontalPadding} * 1.5);
    overflow: hidden;
    max-height: 200px;
    position: relative;
    &:focus {
      outline: none;
    }
    &.expanded {
      max-height: unset;
    }
  }
  .expander-wrapper {
    position: absolute;
    display: block;
    text-align: center;
    line-height: 30px;
    cursor: pointer;
    top: 0;
    min-height: 200px;
    height: 100%;
    width: 100%;
    pointer-events: none;
    .expander {
      height: 30px;
      width: 100%;
      background-color: ${ThemeBackgroundLighter};
      position: absolute;
      bottom: 0;
      pointer-events: all;
      &:focus {
        outline: none;
      }
      i {
        position: absolute;
        bottom: 0;
        line-height: 30px;
      }
    }
  }
  &.expanded {
    .expander-wrapper {
      position: relative;
      min-height: 0;
      height: 30px;
    }
    .expander {
      position: relative;
      i::before {
        content: '\f077';
      }
    }
  }
`;

export default QuoteBB;
