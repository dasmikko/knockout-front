/* eslint-disable no-use-before-define */
import React from 'react';
import { ShortcodeTree, ShortcodeNode, TextNode } from 'shortcode-tree';

import Schema from './Schema';

const UNPARSED_TAGS = ['code', 'noparse'];
const NON_BLOCK_TAGS = ['b', 'i', 'u', 's', 'url', 'spoiler', 'noparse', 'img', 'video'];

// Returns if the node should ignore newlines
// directly after the node's closing tag.
function shouldCheckNewLine(node) {
  return node instanceof ShortcodeNode && !NON_BLOCK_TAGS.includes(node.shortcode.name);
}

function concatenate(children) {
  return (
    <span>
      {children.map((node, index) =>
        parseTree(node, index, index > 0 && shouldCheckNewLine(children[index - 1]))
      )}
    </span>
  );
}

function parseTree(node, index, checkNewLine = false) {
  if (node instanceof TextNode || (node instanceof ShortcodeNode && node.shortcode === null)) {
    return (
      <span key={`${node.text}-${index}`}>{checkNewLine ? node.text.trimStart() : node.text}</span>
    );
  }

  if (node instanceof ShortcodeNode) {
    const { properties } = node.shortcode;
    const tag = node.shortcode.name;

    // return just the text contents of tags in UNPARSED_TAGS. prevents breakage
    if (UNPARSED_TAGS.includes(tag)) {
      return (
        <Schema
          tag={tag}
          content={node.text}
          properties={{ ...properties, key: `${tag}-${index}` }}
        />
      );
    }

    if (node.children.length === 0 && node.text) {
      return (
        <Schema
          tag={tag}
          content={node.text}
          properties={{ ...properties, key: `${tag}-${index}` }}
        />
      );
    }

    const content = concatenate(node.children);

    return (
      <Schema tag={tag} content={content} properties={{ ...properties, key: `${tag}-${index}` }} />
    );
  }

  return null;
}

export const bbToTree = (string) => ShortcodeTree.parse(string);

const Parser = ({ content }) => {
  try {
    const shortCodes = ShortcodeTree.parse(content);

    const comps =
      shortCodes.children.length > 0
        ? shortCodes.children.map((node, index) =>
            parseTree(node, index, index > 0 && shouldCheckNewLine(shortCodes.children[index - 1]))
          )
        : parseTree(shortCodes);

    return <div className="bb-post">{comps}</div>;
  } catch (error) {
    return <pre>Could not parse BBcode.</pre>;
  }
};
export default Parser;
