import React from 'react';

import { loadEmbedSettingFromStorage } from '../../utils/thirdPartyEmbedStorage';
import VideoBB from './components/VideoBB';
import LinkBB from './components/LinkBB';
import ImageBB from './components/ImageBB';
import YoutubeBB from './components/YoutubeBB';
import TweetBB from './components/TweetBB';
import StrawPollBB from './components/StrawPollBB';
import SpoilerBB from './components/SpoilerBB';
import QuoteBBLabs from './components/QuoteBB';
import EmbedWarning from '../EmbedWarning';
import CodeBB from './components/CodeBB';
import VimeoBB from './components/VimeoBB';
import StreamableBB from './components/StreamableBB';
import VocarooBB from './components/VocarooBB';

const thirdPartyEmbedsEnabled = loadEmbedSettingFromStorage();

const Schema = ({ tag, content, properties }) => {
  const lowerTag = typeof tag === 'string' ? tag.toLowerCase() : null;
  switch (lowerTag) {
    case 'b': {
      return <b key={properties.key}>{content}</b>;
    }
    case 'i': {
      return <i key={properties.key}>{content}</i>;
    }
    case 'u': {
      return <u key={properties.key}>{content}</u>;
    }
    case 's': {
      return <s key={properties.key}>{content}</s>;
    }
    case 'blockquote': {
      return <blockquote key={properties.key}>{content}</blockquote>;
    }
    case 'spoiler': {
      return <SpoilerBB key={properties.key}>{content}</SpoilerBB>;
    }
    case 'ul': {
      return <ul key={properties.key}>{content}</ul>;
    }
    case 'ol': {
      return <ol key={properties.key}>{content}</ol>;
    }
    case 'li': {
      return <li key={properties.key}>{content}</li>;
    }
    case 'h1': {
      return <h1 key={properties.key}>{content}</h1>;
    }
    case 'h2': {
      return <h2 key={properties.key}>{content}</h2>;
    }
    case 'img': {
      const { href, thumbnail, link } = properties;
      const isThumbnail = typeof thumbnail !== 'undefined';
      const isLink = typeof link !== 'undefined';

      return (
        <ImageBB
          key={properties.key}
          alt="awoo"
          href={href || content}
          thumbnail={isThumbnail}
          link={isLink}
        />
      );
    }
    case 'video': {
      const { href } = properties;
      return <VideoBB key={properties.key} href={href || content} />;
    }
    case 'url': {
      const { href = null, smart } = properties;
      const isSmart = typeof smart !== 'undefined';
      return (
        <LinkBB key={properties.key} isSmart={isSmart} href={href || content}>
          {content || href}
        </LinkBB>
      );
    }
    case 'youtube': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <YoutubeBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning />
      );
    }
    case 'twitter': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <TweetBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning />
      );
    }
    case 'strawpoll': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <StrawPollBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning />
      );
    }
    case 'q':
    case 'quote': {
      const { mentionsUser, postId, threadPage, threadId, username } = properties;
      return (
        <QuoteBBLabs
          key={properties.key}
          mentionsUser={mentionsUser}
          postId={postId}
          threadPage={threadPage}
          threadId={threadId}
          username={username}
        >
          {content}
        </QuoteBBLabs>
      );
    }
    case 'code': {
      const { language } = properties;
      return (
        <CodeBB key={properties.key} language={language}>
          {content}
        </CodeBB>
      );
    }
    case 'vimeo': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <VimeoBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning />
      );
    }
    case 'streamable': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <StreamableBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning />
      );
    }
    case 'vocaroo': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <VocarooBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning />
      );
    }
    case 'noparse': {
      return <span>{`${content}`}</span>;
    }
    default: {
      console.log(`You entered an unsupported tag: ${tag}. This could be a mistake on your end.`);
      return `[${tag}]`;
    }
  }
};

export default Schema;
