/* eslint-disable import/prefer-default-export */
import { submitPost, updatePost } from '../../services/posts';
import { pushSmartNotification } from '../../utils/notification';
import { accountOlderThanWeek } from '../../utils/user';
import { subforumHasAccountAgeCheck } from '../../utils/subforums';

import { clearPostContentsFromStorage } from '../../utils/postEditorAutosave';
import { loadDisplayCountrySettingFromStorage } from '../../utils/postOptionsStorage';

const prePostCheck = (subforumId) => {
  if (!accountOlderThanWeek() && subforumHasAccountAgeCheck(subforumId)) {
    pushSmartNotification({
      error: `Your account is too new to post in this subforum. (ERR2NU-LRKMOAR)`,
    });

    return false;
  }
  return true;
};

export const handleNewPostSubmit = async (
  content,
  subforumId,
  threadId,
  postSubmittedCallback,
  setContent
) => {
  if (prePostCheck(subforumId)) {
    const sendCountryInfo = loadDisplayCountrySettingFromStorage();

    try {
      await submitPost({
        content,
        thread_id: threadId,
        sendCountryInfo,
      });

      clearPostContentsFromStorage(threadId);
      postSubmittedCallback({ goToLatest: true });
      setContent('');
    } catch (error) {
      pushSmartNotification({
        error: `Could not submit post. If all else fails, have you tried logging in again?`,
      });
    }
  }
};

export const handleEditPostSubmit = async (
  content,
  subforumId,
  threadId,
  postId,
  postSubmittedCallback
) => {
  if (prePostCheck(subforumId)) {
    const sendCountryInfo = loadDisplayCountrySettingFromStorage();

    try {
      await updatePost({
        content,
        id: postId,
        thread_id: threadId,
        sendCountryInfo,
      });

      postSubmittedCallback(true);
    } catch (error) {
      pushSmartNotification({
        error: `Could not submit post. If all else fails, have you tried logging in again?`,
      });
    }
  }
};
