import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import config from '../../../config';

export const Avatar = styled.img`
  width: auto;
  height: 100%;
  max-height: 40px;
  background: rgba(0, 0, 0, 0.25);
`;

const UserAvatar = ({ src, alt, className }) => {
  let url = `${config.cdnHost}/image/${src}`;
  if (!src || src.length === 0) {
    url = 'none.webp';
  }
  return <Avatar className={className} src={url} alt={`${alt || 'User'}'s Avatar`} />;
};
UserAvatar.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
  className: PropTypes.string,
};
UserAvatar.defaultProps = {
  alt: undefined,
  className: '',
};
export default UserAvatar;
