import React, { useState } from 'react';
import PropTypes from 'prop-types';
import submitReport from '../../services/reports';
import { pushSmartNotification } from '../../utils/notification';
import Modal from '../Modals/Modal';
import { FieldLabelSmall, TextField } from '../FormControls';

const ReportModal = ({ isOpen, close, postId }) => {
  const [reportReason, setReportReason] = useState('');

  const submit = async () => {
    try {
      await submitReport({ postId, reportReason });
      close();
    } catch (err) {
      pushSmartNotification({ error: err.message });
    }
  };

  return (
    <Modal
      iconUrl="/static/icons/siren.png"
      title="Report post"
      cancelFn={close}
      submitFn={submit}
      isOpen={isOpen}
    >
      <FieldLabelSmall>
        {`Which of the `}
        <a href="/rules" target="_blank">
          site&apos;s rules
        </a>
        {` did this post break?`}
      </FieldLabelSmall>
      <TextField value={reportReason} onChange={(e) => setReportReason(e.target.value)} />
    </Modal>
  );
};

ReportModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  postId: PropTypes.number.isRequired,
};

export default ReportModal;
