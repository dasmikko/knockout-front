import styled from 'styled-components';
import { lighten, transparentize } from 'polished';
import { Link } from 'react-router-dom';
import {
  ThemeTextColor,
  ThemeVerticalPadding,
  ThemeFontSizeSmall,
  ThemeFontSizeMedium,
  ThemeBackgroundLighter,
  ThemeBackgroundDarker,
  ThemeBodyBackgroundColor,
  ThemeHorizontalPadding,
  ThemeHighlightWeaker,
  ThemeFontSizeLarge,
} from '../../utils/ThemeNew';
import {
  buttonHover,
  MobileMediaQuery,
  DesktopMediaQuery,
  buttonHoverBrightness,
} from '../../components/SharedStyles';

export const SubHeaderButton = styled.button`
  position: relative;
  display: block;
  appearance: none;
  color: ${ThemeTextColor};
  background: ${ThemeBackgroundLighter};
  width: 100%;
  padding: 0;
  height: 30px;
  font-size: ${ThemeFontSizeMedium};
  text-align: left;
  text-decoration: none;
  white-space: nowrap;
  border: none;
  outline: none;
  ${(props) => (props.status ? 'text-decoration: unset;' : '')}

  i {
    display: inline-block;
    width: 30px;
    line-height: 30px;
    text-align: center;
    background: ${ThemeBackgroundDarker};
  }

  span {
    display: inline-block;
    padding: 0 calc(${ThemeVerticalPadding} / 2);
  }

  ${buttonHover}

  ${DesktopMediaQuery} {
    margin: 0;
    background: ${ThemeBodyBackgroundColor};

    span {
      display: none;
    }
  }
`;

export const SubheaderLink = styled(Link)`
  display: block;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  font-size: ${ThemeFontSizeSmall};
  background: ${ThemeBackgroundLighter};
  transition: background 1s;

  span {
    margin-left: calc(${ThemeHorizontalPadding} / 2);

    ${MobileMediaQuery} {
      display: none;
    }
  }
`;

export const Button = styled.button`
  display: inline-block;
  appearance: none;
  color: white;
  background: ${(props) => (props.alert ? '#ad1a1a' : ThemeHighlightWeaker)};
  padding: calc(${ThemeHorizontalPadding} * 1.5) calc(${ThemeVerticalPadding} * 2);
  font-size: ${ThemeFontSizeMedium};
  font-family: 'Open Sans', sans-serif;
  font-weight: bold;
  text-align: center;
  text-decoration: none;
  white-space: nowrap;
  border: none;
  outline: none;

  ${buttonHoverBrightness}

  &:disabled {
    pointer-events: none;
    filter: grayscale(70%);
  }
`;

export const TextButton = styled.button`
  background: none;
  border: none;
  color: ${ThemeTextColor};
  font-family: 'Open Sans', sans-serif;
  font-size: ${(props) => (props.small ? ThemeFontSizeSmall : ThemeFontSizeMedium)};
  padding: ${(props) => (props.small ? '0px 5px' : '12px 15px')};
  transition: 0.2s;
  opacity: 0.75;

  &:hover {
    cursor: pointer;
    opacity: 1;
  }
`;

export const OutlineButton = styled.button`
  padding: 10px;
  background: transparent;
  border: 1px solid ${ThemeHighlightWeaker};
  color: ${ThemeTextColor};
  font-size: ${ThemeFontSizeLarge};
  margin-right: 15px;
  margin-bottom: 15px;
  font-family: 'Open Sans', sans-serif;
  box-sizing: border-box;
  cursor: pointer;
  transition: 0.2s;
  opacity: 0.8;

  &:hover {
    background: ${(props) => transparentize(0.6, ThemeHighlightWeaker(props))};
    opacity: 1;
  }
`;

export const Tag = styled.button`
  padding: 9px 10px;
  margin-right: 5px;
  background: ${ThemeHighlightWeaker};
  border: none;
  border-radius: 10px;
  color: white;
  font-size: ${ThemeFontSizeMedium};
  line-height: 10px;
  height: min-content;
  display: inline-block;
  cursor: pointer;
  transition: background 85ms ease-in-out, opacity 0.3s;

  &:hover,
  &:focus {
    background: ${(props) => lighten(0.1, ThemeHighlightWeaker(props))};
  }
`;
