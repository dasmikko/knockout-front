import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import UserAvatar from '../Avatar';
import UserRoleWrapper from '../UserRoleWrapper';

const MiniUserInfo = ({ user }) => (
  <StyledMiniUserInfo to={`/user/${user.id}`}>
    {user.avatarUrl && user.avatarUrl !== 'none.webp' && (
      <UserAvatar className="avatar" src={user.avatarUrl} />
    )}
    <UserRoleWrapper className="username" user={user}>
      {user.username}
    </UserRoleWrapper>
  </StyledMiniUserInfo>
);

const StyledMiniUserInfo = styled(Link)`
  display: flex;
  align-items: center;

  .avatar {
    margin-right: 4px;
    height: 30px;
    background: rgba(0, 0, 0, 0.1);
  }

  .username {
    font-weight: bold;
  }
`;

MiniUserInfo.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number,
    avatarUrl: PropTypes.string,
    username: PropTypes.string,
  }).isRequired,
};

export default MiniUserInfo;
