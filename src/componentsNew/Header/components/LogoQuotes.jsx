import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { ThemeTextColor, ThemeFontSizeSmall, ThemeVerticalPadding } from '../../../utils/ThemeNew';

import { loadQuotesFromStorage, saveQuotesToStorage } from '../../../utils/quotesStorage';

const LogoQuotes = ({ onTopOfPage }) => {
  // pixels per second
  const SCROLL_SPEED = 40;

  const CONTAINER_WIDTH = 150;

  const [quote, setQuote] = useState('');
  const [canScroll, setCanScroll] = useState(false);
  const [scrollTime, setScrollTime] = useState(5);
  const quoteRef = useRef();

  useEffect(() => {
    async function loadQuotes() {
      let quotes = loadQuotesFromStorage();
      if (quotes === null) {
        quotes = await import('../../../Quotes');
        quotes = quotes.default;
        saveQuotesToStorage(quotes);
      }
      const selectedQuote = quotes[Math.floor(Math.random() * quotes.length)];
      setQuote(selectedQuote);
    }

    loadQuotes();
  }, []);

  useEffect(() => {
    const quoteWidth = quoteRef.current.offsetWidth;
    setCanScroll(quoteWidth > CONTAINER_WIDTH);
    const quoteLength = quoteWidth - CONTAINER_WIDTH;
    setScrollTime(quoteLength / SCROLL_SPEED);
  }, [quote]);

  return (
    <StyledLogoQuotes
      className="logo-quotes"
      containerWidth={CONTAINER_WIDTH}
      scrollTime={scrollTime}
      onTopOfPage={onTopOfPage}
    >
      <div ref={quoteRef} className={canScroll ? 'quote scrolling' : 'quote'}>
        {quote}
      </div>
    </StyledLogoQuotes>
  );
};

LogoQuotes.propTypes = {
  onTopOfPage: PropTypes.bool,
};

LogoQuotes.defaultProps = {
  onTopOfPage: false,
};

export default LogoQuotes;

const StyledLogoQuotes = styled.div`
  @media (max-width: 900px) {
    display: none;
  }

  color: ${ThemeTextColor};
  font-size: calc(${ThemeFontSizeSmall} * 0.8);
  text-transform: uppercase;
  height: calc(${ThemeFontSizeSmall} * 0.8);
  margin-bottom: calc(${ThemeVerticalPadding} / 2);
  overflow: hidden;
  width: ${(props) => props.containerWidth}px;
  opacity: ${(props) => Number(props.onTopOfPage)};
  transition: transform 5s ease-in-out, opacity 200ms ease-in-out;
  transform: translateX(calc(-100% + 150px));

  .quote {
    width: max-content;
    position: absolute;
    white-space: nowrap;
    overflow: hidden;
  }

  .quote.scrolling {
    transition: transform ${(props) => Math.min(props.scrollTime, 5)}s ease-in-out;
    transform: translateX(0);
  }

  :hover .quote.scrolling {
    transition: transform ${(props) => props.scrollTime}s linear;
    transform: translateX(calc(-100% + 150px));
  }
`;
