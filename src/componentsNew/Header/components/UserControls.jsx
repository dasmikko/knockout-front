import React from 'react';
import { Link } from 'react-router-dom';

import HeaderUserLoggedIn from './HeaderUserLoggedIn';
import { loadUserFromStorage } from '../../../services/user';
import { Button, TextButton } from '../../Buttons';

class UserControls extends React.Component {
  tagline = '';

  render() {
    const user = loadUserFromStorage();
    const username = user ? user.username : null;

    if (user == null) {
      return (
        <Link to="/login">
          <Button>Log in</Button>
        </Link>
      );
    }
    if (username == null) {
      return (
        <div>
          <Link to="/usersetup">
            <Button>Create your account</Button>
          </Link>
          <Link to="/logout">
            <TextButton>Log out</TextButton>
          </Link>
        </div>
      );
    }
    return <HeaderUserLoggedIn user={{ ...user, avatarUrl: user.avatar_url }} />;
  }
}

export default UserControls;
