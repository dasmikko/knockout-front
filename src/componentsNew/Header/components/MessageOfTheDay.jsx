import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { MobileMediaQuery } from '../../../components/SharedStyles';
import { getLatestMotd } from '../../../services/messageOfTheDay';
import { updateMotdDisplay } from '../../../state/style';
import { MOTD_HEIGHT, MOTD_HEIGHT_MOBILE } from '../../../utils/pageScroll';

import {
  ThemeHighlightWeaker,
  ThemeFontSizeSmall,
  ThemeFontSizeMedium,
} from '../../../utils/ThemeNew';

const MOTD_DISMISSED_KEY = 'motd-dismissed';

const motdDismissed = (id) => {
  return Number(localStorage.getItem(`${MOTD_DISMISSED_KEY}`)) >= id;
};

const dismissMotd = (id) => {
  localStorage.setItem(`${MOTD_DISMISSED_KEY}`, id);
};

const MessageOfTheDay = () => {
  const [motd, setMotd] = useState({});
  const displayMotd = useSelector((state) => state.style.motd);
  const dispatch = useDispatch();

  useEffect(() => {
    const getMotd = async () => {
      try {
        const result = await getLatestMotd();
        setMotd(result);
        dispatch(updateMotdDisplay(!motdDismissed(result.id)));
      } catch (error) {
        console.error(error);
      }
    };
    getMotd();
  }, [dispatch]);

  const isInternalLink = () => {
    try {
      const link = new URL(motd.buttonLink);
      if (link.hostname === window.location.hostname) {
        return true;
      }
    } catch (error) {
      return false;
    }
    return false;
  };

  const getInternalUrl = () => {
    try {
      const link = new URL(motd.buttonLink);
      return link.pathname + link.hash;
    } catch (error) {
      return '#';
    }
  };

  if (!displayMotd) {
    return null;
  }

  if (motd.message === '' || motd.message === undefined) {
    return null;
  }

  return (
    <StyledMessageOfTheDay>
      <div className="motd-content">
        <div className="message">{motd.message}</div>
        {motd.buttonName && motd.buttonLink && (
          <OutlineLink
            data-testid="motd-link"
            to={getInternalUrl()}
            as={!isInternalLink() && 'a'}
            href={motd.buttonLink}
          >
            {motd.buttonName}
          </OutlineLink>
        )}
      </div>
      <button
        type="button"
        title="Dismiss"
        className="dismiss-btn"
        onClick={() => {
          dismissMotd(motd.id);
          dispatch(updateMotdDisplay(false));
        }}
      >
        <i className="fas fa-times" />
      </button>
    </StyledMessageOfTheDay>
  );
};

export default MessageOfTheDay;

const OutlineLink = styled(Link)`
  margin-left: 10px;
  padding: 7px 8px;
  background: transparent;
  border: 1px solid white;
  color: white;
  font-size: ${ThemeFontSizeMedium};
  font-family: 'Open Sans', sans-serif;
  box-sizing: border-box;
  white-space: nowrap;
  cursor: pointer;
  transition: 0.2s;
  opacity: 0.8;

  &:hover {
    opacity: 1;
  }
`;

const StyledMessageOfTheDay = styled.div`
  background: ${ThemeHighlightWeaker};

  display: flex;
  align-items: center;
  position: relative;
  height: ${MOTD_HEIGHT}px;
  font-size: ${ThemeFontSizeMedium};
  color: white;

  ${MobileMediaQuery} {
    height: ${MOTD_HEIGHT_MOBILE}px;
  }

  .motd-content {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-grow: 1;
    font-weight: 600;

    ${MobileMediaQuery} {
      padding-left: 10px;
      padding-right: 32px;
    }
  }

  .message {
    line-height: normal;
  }

  .dismiss-btn {
    position: absolute;
    right: 0;
    height: 100%;
    width: 32px;
    appearance: none;
    cursor: pointer;
    font-size: ${ThemeFontSizeSmall};
    color: white;
    background: none;
    opacity: 0.6;
    border: none;
    outline: none;
    transition: 0.3s;

    &:hover {
      opacity: 1;
    }
  }
`;
