import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom'

const BannedHeaderMessage = ({ banMessage, threadId }) => (
  <BanInfoWrapper>
    <BanInfoIcon
      src="https://img.icons8.com/color/50/000000/police-badge.png"
      alt="Banned!"
      title="Banned!"
    />
    <BanInfoText>
      <div>
        You were banned with reason <b>{banMessage}</b> in&nbsp;
        <BanInfoStrong to={`/thread/${threadId}`}>this</BanInfoStrong> thread.
      </div>
      <div>This message will reappear until you log in successfully.</div>
    </BanInfoText>
  </BanInfoWrapper>
);

export default BannedHeaderMessage;

export const BanInfoWrapper = styled.div`
  width: 100%;
  min-height: 50px;
  background: #dc4035;
  padding: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
`;

export const BanInfoIcon = styled.img`
  width: 50px;
  height: 50px;
  margin-right: 15px;
`;

export const BanInfoText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  width: 100%;
  min-height: 40px;
`;

export const BanInfoStrong = styled(Link)`
  font-weight: bold;
`;
