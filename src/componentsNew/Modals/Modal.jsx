/* eslint-disable react/forbid-prop-types */
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { ThemeSecondaryTextColor, ThemeKnockoutRed } from '../../Theme';
import { SubHeaderButton, Button, TextButton } from '../Buttons';
import { AbsoluteBlackout } from '../../components/SharedStyles';
import { ThemeBackgroundDarker, ThemeTextColor, ThemeFontSizeHuge } from '../../utils/ThemeNew';
import componentOpenAnim from '../../utils/componentOpenAnim';
import { TextField } from '../FormControls';

const Modal = ({
  title,
  cancelText,
  submitText,
  iconUrl,
  submitFn,
  cancelFn,
  children,
  isOpen,
}) => {
  const [isVisible, setIsVisible] = useState(isOpen);
  const [openAnim, setOpenAnim] = useState(false);
  const firstUpdate = useRef(true);

  const animDuration = 300;

  useEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;
      return;
    }
    componentOpenAnim(isVisible, setIsVisible, setOpenAnim, animDuration);
  }, [isOpen]);

  return (
    <>
      {isVisible && (
        <>
          <StyledModalWrapper openAnim={openAnim} transition={animDuration}>
            <div className="modal-section header">
              {iconUrl && <img className="modal-icon" src={iconUrl} alt="Background icon" />}
              <h2 className="modal-title">{title}</h2>
            </div>
            <div className="modal-content">{children}</div>
            <div className="modal-section footer">
              <TextButton type="button" onClick={cancelFn}>
                {cancelText}
              </TextButton>
              <Button type="button" onClick={submitFn}>
                {submitText}
              </Button>
            </div>
          </StyledModalWrapper>
          <AbsoluteBlackout openAnim={openAnim} transition={animDuration} onClick={cancelFn} />
        </>
      )}
    </>
  );
};

export default Modal;

Modal.propTypes = {
  title: PropTypes.string.isRequired,
  cancelText: PropTypes.string,
  submitText: PropTypes.string,
  submitFn: PropTypes.func.isRequired,
  cancelFn: PropTypes.func.isRequired,
  iconUrl: PropTypes.string,
  children: PropTypes.node,
  isOpen: PropTypes.bool,
};

Modal.defaultProps = {
  cancelText: 'Cancel',
  submitText: 'Submit',
  iconUrl: undefined,
  children: undefined,
  isOpen: true,
};

export const StyledModalWrapper = styled.div`
  position: fixed;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  max-width: 100vw;
  width: 500px;
  max-height: 100vh;

  display: flex;
  flex-direction: column;
  justify-content: space-between;

  background: ${ThemeBackgroundDarker};
  color: ${ThemeTextColor};
  overflow: hidden;
  overflow-y: auto;
  opacity: ${(props) => (props.openAnim ? 1 : 0)};
  transition: opacity ${(props) => props.transition / 1000}s;

  ${Button}, ${TextButton} {
    pointer-events: ${(props) => (props.openAnim ? 'inherit' : 'none')};
  }

  z-index: 100;

  .modal-section {
    display: flex;
    background: ${ThemeBackgroundDarker};
    padding: 15px;
    box-sizing: border-box;
    position: relative;
    align-items: center;

    ${SubHeaderButton} {
      &:first-child {
        margin-left: 0;
      }
    }
  }

  ${TextField}:last-child {
    margin-bottom: 0px;
  }

  .footer {
    display: flex;
    justify-content: flex-end;
  }

  .modal-title {
    font-size: ${ThemeFontSizeHuge};
    color: ${ThemeTextColor};
    margin: 10px 0px;
  }

  .modal-content {
    padding: 15px;

    input[type='text'] {
      border: 1px solid ${ThemeSecondaryTextColor};
      color: ${ThemeTextColor};
      padding: 5px;
      background: transparent;
      font-size: 16px;
      border-radius: 5px;
      font-family: 'Open Sans', sans-serif;
      box-sizing: border-box;
      outline: none;

      &:focus {
        border: 1px solid transparent;
        box-shadow: 0 0 0px 2px ${ThemeKnockoutRed};
      }
    }
  }

  .modal-icon {
    max-height: 35px;
    max-width: 35px;
    margin-right: 5px;
  }
`;
