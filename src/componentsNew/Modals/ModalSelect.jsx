import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { ThemeMediumTextSize, ThemeQuaternaryBackgroundColor } from '../../Theme';

const ModalSelect = ({ options, defaultText, selectedOptions, onChange, className }) => {
  const [selected, setSelected] = useState(undefined);

  const handleChange = (e) => {
    if (onChange) {
      onChange(e);
    } else {
      setSelected(e.target.value);
    }
  };

  return (
    <StyledModalSelect
      data-testid="modal-select"
      value={selected}
      name=""
      id=""
      onChange={handleChange}
      className={className}
    >
      {defaultText !== '' && <option>{defaultText}</option>}
      {options.map((option) => (
        <option
          key={option.value}
          value={option.value}
          disabled={selectedOptions.includes(option.value)}
        >
          {option.text || option.value}
        </option>
      ))}
    </StyledModalSelect>
  );
};

export default ModalSelect;

ModalSelect.propTypes = {
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  defaultText: PropTypes.string,
  selectedOptions: PropTypes.arrayOf(PropTypes.number),
  onChange: PropTypes.func,
  className: PropTypes.string,
};
ModalSelect.defaultProps = {
  defaultText: '',
  selectedOptions: [],
  onChange: undefined,
  className: '',
};

export const StyledModalSelect = styled.select`
  border: none;
  width: 100%;
  padding: 5px;
  line-height: 1;
  outline: none;
  font-size: ${ThemeMediumTextSize};
  color: inherit;
  background: ${ThemeQuaternaryBackgroundColor};
  height: 30px;
  margin-bottom: 20px;
`;
