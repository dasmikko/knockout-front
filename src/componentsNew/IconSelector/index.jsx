import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Tooltip from '../Tooltip';
import InputError from '../InputError';

import { listIcons } from '../../services/iconsNew';

import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeFontSizeMedium,
  ThemeBackgroundLighter,
  ThemeBackgroundDarker,
  ThemeTextColor,
} from '../../utils/ThemeNew';
import { Tag } from '../Buttons';

function filterIcons(icons, keyword, category = '') {
  if (keyword === '' && category === '') {
    return icons;
  }

  const loweredAndParsedKeyword = keyword.toLowerCase().replace(/[^\w\s]|_/g, '');
  const filteredIcons = icons.filter((icon) =>
    icon.desc
      .toLowerCase()
      .replace(/[^\w\s]|_/g, '')
      .includes(loweredAndParsedKeyword)
  );

  if (category === '') {
    return filteredIcons;
  }

  return filteredIcons.filter((icon) => icon.category === category);
}

function getCategories(icons) {
  return icons
    .map((icon) => icon.category)
    .filter((category, index, categories) => categories.indexOf(category) === index);
}

const IconSelector = ({ className, selectedId, handleIconChange, error }) => {
  const [icons, setIcons] = useState([]);
  const [filteredIcons, setFilteredIcons] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [categories, setCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState('');

  useEffect(() => {
    const getIcons = async () => {
      const grabbedIcons = await listIcons();
      setIcons(grabbedIcons);
      setFilteredIcons(grabbedIcons);
      setCategories(getCategories(grabbedIcons));
    };

    getIcons();
  }, []);

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
    setFilteredIcons(filterIcons(icons, e.target.value, selectedCategory));
  };

  const handleCategoryChange = (category) => {
    const parsedCategory = category === selectedCategory ? '' : category;
    setSelectedCategory(parsedCategory);
    setFilteredIcons(filterIcons(icons, searchTerm, parsedCategory));
  };

  return (
    <div className={className}>
      <div className="icon-input-title">
        <label>Icon</label>
        <label aria-label="icon-search">
          <span className="screen-reader-only">Icon Search</span>
          <input
            name="icon-search"
            text="string"
            onChange={handleSearchChange}
            value={searchTerm}
            placeholder="Search icons"
          />
        </label>
      </div>
      <div className={`icon-categories ${selectedCategory !== '' ? 'icon-selected' : ''}`}>
        {categories.map((category) => (
          <ClickableCategory
            key={`category_${category}`}
            category={category}
            selected={selectedCategory === category}
            handleClick={() => handleCategoryChange(category)}
          />
        ))}
      </div>
      <div className="icon-area">
        {icons.map((icon) => (
          <ClickableIcon
            key={`icon_${icon.id}`}
            icon={icon}
            selected={icon.id === selectedId}
            handleClick={() => handleIconChange(icon.id)}
            className={filteredIcons.includes(icon) ? '' : 'hidden'}
          />
        ))}
      </div>
      <InputError error={error} />
    </div>
  );
};

IconSelector.propTypes = {
  className: PropTypes.string.isRequired,
  selectedId: PropTypes.number,
  handleIconChange: PropTypes.func.isRequired,
  error: PropTypes.string,
};

IconSelector.defaultProps = {
  selectedId: -1,
  error: undefined,
};

const ClickableIcon = ({ icon, selected, handleClick, className }) => (
  <button
    type="button"
    onClick={handleClick}
    title={`${icon.desc}-${icon.id}`}
    className={className}
  >
    <Tooltip text={icon.desc}>
      <img src={icon.url} alt={icon.desc} className={selected ? 'selected' : ''} />
    </Tooltip>
  </button>
);

ClickableIcon.propTypes = {
  icon: PropTypes.shape({
    id: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
  }).isRequired,
  selected: PropTypes.bool.isRequired,
  handleClick: PropTypes.func.isRequired,
  className: PropTypes.string,
};

ClickableIcon.defaultProps = {
  className: '',
};

const ClickableCategory = ({ category, selected, handleClick }) => (
  <Tag
    type="button"
    onClick={handleClick}
    title={`${category} category`}
    className={selected ? 'selected' : ''}
  >
    {category}
  </Tag>
);

ClickableCategory.propTypes = {
  category: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  handleClick: PropTypes.func.isRequired,
};

export default styled(IconSelector)`
  label {
    span.screen-reader-only {
      display: none;
    }
  }
  .icon-input-title {
    background: ${ThemeBackgroundLighter};
    padding: 0 ${ThemeHorizontalPadding};
    display: flex;
    justify-content: space-between;
    align-items: center;

    input {
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      background: none;
      color: ${ThemeTextColor};
      border: none;
      border-bottom: 2px solid ${ThemeTextColor};
      font-size: ${ThemeFontSizeMedium};
    }
  }

  .icon-categories {
    display: flex;
    flex-wrap: wrap;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    background: ${ThemeBackgroundDarker};

    &.icon-selected {
      ${Tag} {
        opacity: 0.4;

        &.selected {
          opacity: 1;
        }
      }
    }
  }

  .icon-area {
    background: ${ThemeBackgroundDarker};
    min-height: 64px;
    max-height: 400px;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-content: stretch;
    overflow-x: hidden;
    overflow-y: scroll;

    button {
      border: none;
      background: none;
      padding: 0;
      margin: 0;
      cursor: pointer;

      &:hover {
        background: rgba(255, 255, 255, 0.1);
      }

      &.hidden {
        display: none;
      }
    }

    img {
      height: 48px;
      margin: 10px;
      box-sizing: border-box;
      transition: filter 100ms ease-in-out;
      filter: brightness(0.5);

      @media (max-width: 700px) {
        height: 32px;
        margin: 5px;
      }
    }

    button:hover > img {
      filter: brightness(1.5) !important;
    }

    img.selected {
      filter: brightness(1);
      border: 1px solid white;
    }
  }
`;
