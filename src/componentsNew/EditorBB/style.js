/* eslint-disable react/jsx-curly-newline */
import styled from 'styled-components';

import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeFontSizeMedium,
  ThemeFontSizeLarge,
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeTextColor,
} from '../../utils/ThemeNew';

const StyledEditorBB = styled.div`
  position: relative;

  textarea {
    width: 100%;
    height: 120px;
    border: none;
    border-radius: 0;
    background: ${ThemeBackgroundDarker};
    color: ${ThemeTextColor};
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    font-size: ${ThemeFontSizeMedium};
    line-height: calc(${ThemeFontSizeMedium} * 1.2);
    box-sizing: border-box;
    resize: none;

    &:focus {
      outline: 1px solid ${ThemeBackgroundLighter};
    }
  }

  .editor-footer {
    background: ${ThemeBackgroundLighter};
    margin-top: -2px;

    @media (min-width: 800px) {
      display: flex;
      .toolbar {
        flex: 1;
      }
    }

    button,
    a {
      box-sizing: border-box;
      background: none;
      border: none;
      color: ${ThemeTextColor};
      cursor: pointer;
      transition: background 0.3s;
      border-radius: 0;
    }

    .toolbar {
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      button {
        font-size: ${ThemeFontSizeLarge};
      }

      &.disabled {
        opacity: 0.8;
        button {
          cursor: initial;
        }
      }
    }

    .actions {
      display: flex;

      a {
        display: flex;
        align-items: center;
      }
      a,
      button {
        height: 100%;
        flex: 1;
        white-space: nowrap;
        font-size: ${ThemeFontSizeMedium};
        padding: 0.3rem 0.6rem;

        &:hover {
          background: #bf3f3f;
        }

        i {
          margin-right: 0.2rem;
        }
      }
    }
  }

  .options {
    position: relative;
    padding: 1rem 1.2rem;
    background: ${ThemeBackgroundLighter};

    @media (min-width: 800px) {
      font-size: 14px;
      position: absolute;
      bottom: 3.25em;
      right: 0.75em;
      width: 90%;
    }

    .close-button {
      color: ${ThemeTextColor};
      position: absolute;
      top: 1rem;
      right: 1rem;
      background: none;
      border: none;
      cursor: pointer;
    }

    h3 {
      font-size: 1.4rem;
      margin-bottom: 0.8rem;
    }

    input {
      display: none;
    }

    label {
      display: block;
      margin-bottom: 0.4rem;
    }
  }
`;

export default StyledEditorBB;
