/* eslint-disable no-alert */
/* eslint-disable no-restricted-globals */
import replaceBetween from '../../utils/replaceBetween';
import {
  isBoldHotkey,
  isCodeHotkey,
  isItalicHotkey,
  isStrikethroughHotkey,
  isSubmitHotkey,
  isUnderlinedHotkey,
} from './editorHotkeys';

import { isImage } from '../../components/KnockoutBB/components/ImageBB';
import { getStrawPollId } from '../../components/KnockoutBB/components/StrawPollBB';
import { getStreamableId } from '../../components/KnockoutBB/components/StreamableBB';
import { getTweetId } from '../../components/KnockoutBB/components/TweetBB';
import { isVideo } from '../../components/KnockoutBB/components/VideoBB';
import { getVimeoId } from '../../components/KnockoutBB/components/VimeoBB';
import { getYoutubeId } from '../../components/KnockoutBB/components/YoutubeBB';
import { getVocarooId } from '../KnockoutBB/components/VocarooBB';

export const httpRegex = new RegExp(
  '^' +
    '(?:(?:(?:https?|ftp):)?\\/\\/)' +
    '(?:\\S+(?::\\S*)?@)?' +
    '(?:' +
    '(?!(?:10|127)(?:\\.\\d{1,3}){3})' +
    '(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})' +
    '(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})' +
    '(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])' +
    '(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}' +
    '(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))' +
    '|' +
    '(?:' +
    '(?:' +
    '[a-z0-9\\u00a1-\\uffff]' +
    '[a-z0-9\\u00a1-\\uffff_-]{0,62}' +
    ')?' +
    '[a-z0-9\\u00a1-\\uffff]\\.' +
    ')+' +
    '(?:[a-z\\u00a1-\\uffff]{2,}\\.?)' +
    ')' +
    '(?::\\d{2,5})?' +
    '(?:[/?#]\\S*)?' +
    '$',
  'i'
);

function replaceWithin(input, start, end, replacement) {
  return `${input.substring(0, start)}${replacement}${input.substring(end)}`;
}

function modifySelectionRange(input, cursorPosition) {
  setTimeout(() => {
    input.setSelectionRange(cursorPosition, cursorPosition);
    input.focus();
  }, 100);
}

export const handlePaste = (e, setContent, selectionRange) => {
  const clipboardData = e.clipboardData.getData('Text');
  let content;

  if (isImage(clipboardData)) {
    if (confirm('Insert img?')) {
      e.preventDefault();
      content = `[img]${clipboardData}[/img]`;
    }
  } else if (getYoutubeId(clipboardData)) {
    if (confirm('Insert youtube?')) {
      e.preventDefault();
      content = `[youtube]${clipboardData}[/youtube]`;
    }
  } else if (getVimeoId(clipboardData)) {
    if (confirm('Insert Vimeo video?')) {
      e.preventDefault();
      content = `[vimeo]${clipboardData}[/vimeo]`;
    }
  } else if (getStreamableId(clipboardData)) {
    if (confirm('Insert Streamable video?')) {
      e.preventDefault();
      content = `[streamable]${clipboardData}[/streamable]`;
    }
  } else if (getVocarooId(clipboardData)) {
    if (confirm('Insert Vocaroo embed?')) {
      e.preventDefault();
      setContent((prevState) => `${prevState}[vocaroo]${clipboardData}[/vocaroo]`);
    }
  } else if (isVideo(clipboardData)) {
    if (confirm('Insert video?')) {
      e.preventDefault();
      content = `[video]${clipboardData}[/video]`;
    }
  } else if (getStrawPollId(clipboardData)) {
    if (confirm('Insert strawpoll?')) {
      e.preventDefault();
      content = `[strawpoll]${clipboardData}[/strawpoll]`;
    }
  } else if (getTweetId(clipboardData)) {
    if (confirm('Insert twitter?')) {
      e.preventDefault();
      content = `[twitter]${clipboardData}[/twitter]`;
    }
  } else if (httpRegex.test(clipboardData)) {
    if (confirm('Insert url?')) {
      e.preventDefault();
      content = `[url href="${clipboardData}"][/url]`;
    }
  }
  if (content) {
    setContent((prevState) =>
      replaceWithin(prevState, selectionRange[0], selectionRange[1], content)
    );
    modifySelectionRange(e.target, selectionRange[0] + content.length);
  }
};

function selectionRangeValid(selectionRange) {
  return selectionRange[0] !== undefined && selectionRange[1] !== undefined;
}

function selectionRangeIsCursor(selectionRange) {
  return selectionRange[0] === selectionRange[1];
}

export const insertTag = (type, content, setContent, selectionRange, setSelectionRange, input) => {
  if (type === 'li') {
    setContent((prevState) => `${prevState}[ul][li][/li][/ul]`);
    // } else if (type === 'quote') {
    //   const { threadPage, threadId, postId, username, postContent, mentionsUser } = content;
    //   setContent((prevState) =>
    //     `${prevState} [quote mentionsUser="${mentionsUser}" postId="${postId}" threadPage="${threadPage}" threadId="${threadId}" username="${username}"]${postContent}[/quote]`.trim()
    //   );
  } else if (selectionRangeValid(selectionRange)) {
    setContent((prevState) =>
      replaceBetween(prevState, selectionRange[0], selectionRange[1], [`[${type}]`, `[/${type}]`])
    );
    const startTagLength = `[${type}]`.length;
    if (selectionRangeIsCursor(selectionRange)) {
      setSelectionRange([selectionRange[0] + startTagLength, selectionRange[0] + startTagLength]);
      modifySelectionRange(input, selectionRange[0] + startTagLength);
    } else {
      const endTagLength = `[/${type}]`.length;
      setSelectionRange([
        selectionRange[1] + startTagLength + endTagLength,
        selectionRange[1] + startTagLength + endTagLength,
      ]);
      modifySelectionRange(input, selectionRange[1] + startTagLength + endTagLength);
    }
  } else {
    const updatedContent = `${content}[${type}][/${type}]`;
    setContent((prevState) => `${prevState}[${type}][/${type}]`);
    const tagLength = `[/${type}]`.length;
    const contentLength = updatedContent.length;

    setSelectionRange([contentLength - tagLength, contentLength - tagLength]);
    modifySelectionRange(input, contentLength - tagLength);
  }
};

export function checkForHotkeys(
  e,
  content,
  setContent,
  selectionRange,
  setSelectionRange,
  input,
  handleSubmit
) {
  if (isBoldHotkey(e)) {
    e.preventDefault();
    insertTag('b', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isItalicHotkey(e)) {
    e.preventDefault();
    insertTag('i', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isUnderlinedHotkey(e)) {
    e.preventDefault();
    insertTag('u', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isCodeHotkey(e)) {
    e.preventDefault();
    insertTag('code', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isStrikethroughHotkey(e)) {
    e.preventDefault();
    insertTag('s', content, setContent, selectionRange, setSelectionRange, input);
  } else if (handleSubmit !== undefined && isSubmitHotkey(e)) {
    handleSubmit();
  }
}
