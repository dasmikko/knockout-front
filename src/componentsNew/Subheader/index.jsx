import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Pagination from '../Pagination';
import LoggedInOnly from '../LoggedInOnly';
import { buttonHover, MobileMediaQuery, DesktopMediaQuery } from '../../components/SharedStyles';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeBackgroundLighter,
  ThemeFontSizeSmall,
  ThemeTextColor,
} from '../../utils/ThemeNew';

const Subheader = (props) => {
  const {
    returnToUrl,
    returnToText,
    totalPaginationItems,
    currentPage,
    paginationPath,
    pageSize,
    newThreadPath,
  } = props;

  return (
    <StyledSubHeader>
      <span className="back-and-title">
        <div className="left">
          {returnToUrl && returnToText && (
            <Link className="return-btn" to={returnToUrl}>
              <i className="fas fa-angle-left" />
              <span>{returnToText}</span>
            </Link>
          )}
        </div>
      </span>
      <div className="pagination-and-buttons">
        <div className="pagination">
          <Pagination
            showNext
            pagePath={paginationPath}
            totalPosts={totalPaginationItems}
            currentPage={currentPage}
            pageSize={pageSize}
          />
        </div>
        <LoggedInOnly>
          <div className="subheader-menu-wrapper">
            <i className="dropdown-arrow fa fa-angle-down" />
            <div className="subheader-menu-content">
              <div className="subheader-menu-item">
                <Link className="subheader-btn" to={newThreadPath}>
                  <i className="fas fa-dumpster-fire">&nbsp;</i>
                  <span>Create new thread</span>
                </Link>
              </div>
            </div>
          </div>
        </LoggedInOnly>
      </div>
    </StyledSubHeader>
  );
};

Subheader.propTypes = {
  returnToUrl: PropTypes.string.isRequired,
  returnToText: PropTypes.string.isRequired,
  totalPaginationItems: PropTypes.number,
  currentPage: PropTypes.number,
  paginationPath: PropTypes.string,
  pageSize: PropTypes.number,
  newThreadPath: PropTypes.string.isRequired,
};

Subheader.defaultProps = {
  totalPaginationItems: 0,
  currentPage: 1,
  paginationPath: '/',
  pageSize: 40,
};

export default Subheader;

const StyledSubHeader = styled.nav`
  max-width: 100vw;
  padding-top: ${ThemeVerticalPadding};
  padding-bottom: ${ThemeVerticalPadding};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  font-size: ${ThemeFontSizeSmall};
  color: ${ThemeTextColor};

  .return-btn {
    display: block;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    background: ${ThemeBackgroundLighter};

    span {
      margin-left: calc(${ThemeHorizontalPadding} / 2);
      ${MobileMediaQuery} {
        display: none;
      }
    }
  }

  .back-and-title {
    display: flex;
    height: 30px;
    overflow: hidden;
    align-items: stretch;
    margin-right: ${ThemeHorizontalPadding};
    text-decoration: none;
  }

  .pagination-and-buttons {
    display: flex;
    flex-grow: 1;

    .pagination {
      display: flex;
      flex-grow: 1;
      justify-content: flex-end;
    }

    .subheader-menu-wrapper {
      color: ${ThemeTextColor};
      font-size: ${ThemeFontSizeSmall};
      border: none;
      position: relative;
      width: 30px;
      height: 30px;
      margin-left: ${ThemeHorizontalPadding};
      background: ${ThemeBackgroundLighter};
      overflow: hidden;
      z-index: 999;

      &:active,
      &:focus,
      &:hover {
        overflow: visible;
      }

      ${DesktopMediaQuery} {
        width: auto;
        overflow: visible !important;
      }

      .dropdown-arrow {
        position: relative;
        display: block;
        width: 100%;
        height: 100%;
        line-height: 27px;
        text-align: center;
        background: ${ThemeBackgroundLighter};
        cursor: pointer;
        z-index: 41;

        ${DesktopMediaQuery} {
          display: none;
        }
      }

      .subheader-menu-content {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        margin-top: 27px;
        box-sizing: border-box;
        box-shadow: 0 0 0 2000px rgba(0, 0, 0, 0.5);
        z-index: 40;
        background: ${ThemeBackgroundLighter};

        ${DesktopMediaQuery} {
          position: relative;
          width: auto;
          padding: 0;
          margin-top: 0;
          background: transparent;
          box-shadow: none;
          display: flex;
          flex-direction: row;
        }

        .subheader-menu-item {
          position: relative;
          padding: 0;

          span {
            margin-top: calc(${ThemeHorizontalPadding} / 2);
          }

          ${DesktopMediaQuery} {
            span {
              margin: 0;
            }
          }
        }
      }
    }
  }

  .subheader-btn {
    position: relative;
    display: block;
    margin: 0;
    padding: 0;
    padding-left: 30px;
    padding-right: calc(${ThemeHorizontalPadding} / 2);
    text-align: left;
    line-height: 30px;
    white-space: nowrap;
    border: none;
    height: 100%;

    i {
      position: absolute;
      top: 0;
      left: 0;
      padding-left: calc(${ThemeHorizontalPadding} / 2);
      padding-right: calc(${ThemeHorizontalPadding} / 2);
      width: 30px;
      line-height: 30px;
      text-align: center;
    }

    span {
      display: block;
      height: 30px;
    }

    ${buttonHover}

    ${DesktopMediaQuery} {
      padding-right: calc(${ThemeHorizontalPadding} / 2);
    }
  }
`;
