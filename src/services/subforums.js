import axios from 'axios';

import config from '../../config';

import { authPost, authGet } from './common';
import { submitPost } from './posts';

export const getSubforumList = async () => {
  const res = await axios.get(`${config.apiHost}/subforum`);

  const { data } = res;

  return data;
};

export const getSubforumWithThreads = async (subforumid, page = 1) => {
  const user = localStorage.getItem('currentUser');

  // if logged in, get the subforum with auth so
  // subscribed threads are fetched
  if (user) {
    const res = await authGet({ url: `/subforum/${subforumid}/${page}` });

    const { data } = res;

    return data;
  }

  const res = await axios.get(`${config.apiHost}/subforum/${subforumid}/${page}`);

  const { data } = res;

  return data;
};

export const createNewThread = async data => {
  const thread = await authPost({ url: '/thread', data });

  await submitPost({ content: data.content, thread_id: thread.data.id });

  return thread.data;
};
