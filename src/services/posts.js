import axios from 'axios';
import config from '../../config';
import { pushSmartNotification } from '../utils/notification';
import { authGet, authPost, authPut } from './common';

const APP_NAME = 'knockout.chat';

const validateContent = (content) => {
  const contentSerialized = content.trim();
  const contentLength = contentSerialized.length;

  if (contentLength < 1) {
    pushSmartNotification({ error: `Post body too short. ${contentLength}/10` });
    throw new Error('Post body too short.');
  }

  const lineBreakAdjustment = (contentSerialized.match(/\n/g) || '').length * 65;

  if (contentLength + lineBreakAdjustment > 8000) {
    pushSmartNotification({ error: 'Post body too long.' });
    throw new Error('Post body too long.');
  }

  return content.trim();
};

export const submitPost = ({ content, thread_id: threadId, sendCountryInfo }) => {
  const contentStringified = validateContent(content);

  const requestBody = {
    content: contentStringified,
    thread_id: threadId,
    displayCountryInfo: sendCountryInfo,
    appName: APP_NAME,
  };

  return authPost({ url: '/post', data: requestBody });
};

export const getPostList = async () => {
  const res = await axios.get(`${config.apiHost}/post`);

  const { data } = res;

  return data;
};

export const getPost = (postId) => authGet({ url: `/post/${postId}` });

export const updatePost = ({ content, id, threadId }) => {
  const contentStringified = validateContent(content);

  const requestBody = {
    content: contentStringified,
    id,
    threadId,
    appName: APP_NAME,
  };

  return authPut({ url: '/post', data: requestBody });
};
