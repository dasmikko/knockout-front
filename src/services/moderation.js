import dayjs from 'dayjs';

import { authPost, authGet } from './common';
import { pushNotification } from '../utils/notification';

export const getUsernamesByIp = async (ipAddress) => {
  const res = await authPost({ url: `/moderation/usersByIp`, data: { ipAddress } });

  const { data } = res;

  return data;
};

export const getIpsByUsername = async (username) => {
  const res = await authPost({ url: `/moderation/ipsByUser`, data: { username } });

  const { data } = res;

  return data;
};

export const getOpenReports = async () => {
  const res = await authPost({ url: `/moderation/openReports`, data: {} });

  const { data } = res;

  return data;
};

export const countOpenReports = async () => {
  const res = await authPost({ url: `/moderation/countOpenReports`, data: {} });

  const { data } = res;

  return data;
};

export const countOpenReportsRequest = async () =>
  authPost({ url: `/moderation/countOpenReports`, data: {} });

export const closeReport = async (reportId) => {
  const res = await authPost({ url: `/moderation/closeReport`, data: { reportId } });

  const { data } = res;

  pushNotification({ message: 'Report marked as solved.', type: 'success' });

  return data;
};

/**
 * Operate on a Thread's locked and deleted_at fields.
 * @param {integer} threadId - The thread to be deleted.
 * @param {Object}  statuses - object whose properties dictate the operations to be executed.
 * @param {boolean} statuses.delete - if true, sets thread deleted_at to NOW().
 * @param {boolean} statuses.restore - if true, sets thread deleted_at to NULL.
 * @param {boolean} statuses.lock - if true, sets thread locked to true.
 * @param {boolean} statuses.unlock - if true, sets thread locked to false.
 * @returns {Object} response from server
 */
export const changeThreadStatus = async ({ threadId, statuses }) => {
  try {
    const changedData = {};

    if (!threadId || !statuses) {
      throw new Error('Empty request.');
    }

    if (
      !statuses.delete &&
      !statuses.restore &&
      !statuses.lock &&
      !statuses.unlock &&
      !statuses.pinned &&
      !statuses.unpin
    ) {
      throw new Error('Empty request.');
    }

    if (statuses.delete) {
      changedData.deleted_at = dayjs().format('YYYY-MM-DD HH:MM:ss');
    }
    if (statuses.restore) {
      changedData.deleted_at = null;
    }
    if (statuses.lock) {
      changedData.locked = true;
    }
    if (statuses.unlock) {
      changedData.locked = false;
    }
    if (statuses.pinned) {
      changedData.pinned = true;
    }
    if (statuses.unpin) {
      changedData.pinned = false;
    }

    const res = await authPost({
      url: `/moderation/changeThreadStatus`,
      data: { threadId, changedData },
    });

    const resData = res.data;

    pushNotification({ message: 'Thread status updated.', type: 'success' });

    return resData;
  } catch (error) {
    return error;
  }
};

export const getLatestUsers = async () => {
  try {
    const results = await authGet({ url: '/moderation/getLatestUsers', data: {} });

    return results.data;
  } catch (err) {
    return false;
  }
};

export const getDashboardData = async () => {
  try {
    const results = await authGet({ url: '/moderation/getDashboardData', data: {} });

    return results.data;
  } catch (err) {
    return false;
  }
};

export const getFullUserInfo = async (userId) => {
  try {
    const results = await authPost({ url: '/moderation/getFullUserInfo', data: { userId } });

    return results.data;
  } catch (err) {
    return false;
  }
};

export const makeBanInvalid = async (banId, userId) => {
  const results = await authPost({ url: '/moderation/makeBanInvalid', data: { banId, userId } });

  return results.data;
};

export const removeUserImage = async (removalData) => {
  try {
    if (!removalData.userId) {
      throw new Error('No user id provided.');
    }
    if (!removalData.avatar && !removalData.background) {
      throw new Error('No data requested for removal.');
    }

    const results = await authPost({ url: '/moderation/removeUserImage', data: removalData });

    pushNotification({ message: 'User image removed.', type: 'success' });
    return results.data;
  } catch (err) {
    return false;
  }
};

export const removeUserProfile = async (userId) => {
  try {
    if (!userId) {
      throw new Error('No user id provided.');
    }

    const results = await authPost({ url: '/moderation/removeUserProfile', data: { userId } });

    pushNotification({ message: 'User profile removed.', type: 'success' });
    return results.data;
  } catch (err) {
    return false;
  }
};

export const addGoldProduct = async (data) => {
  try {
    if (!data.userId) {
      throw new Error('No user id provided.');
    }

    const results = await authPost({ url: '/moderation/addGoldProduct', data });
    pushNotification({ message: 'Added gold for one month.', type: 'success' });
    return results.data;
  } catch (err) {
    pushNotification({ message: 'Error when giving gold.', type: 'success' });
    return false;
  }
};

export const createTag = async (tagName) => {
  try {
    if (!tagName || !tagName.length || tagName.length <= 3) {
      throw new Error('Invalid tag name.');
    }

    const data = {
      tagName,
    };

    const results = await authPost({ url: '/tag', data });
    pushNotification({ message: 'Tag created.', type: 'success' });
    return results.data;
  } catch (err) {
    pushNotification({ message: 'Could not create tag.', type: 'error' });
    return false;
  }
};
