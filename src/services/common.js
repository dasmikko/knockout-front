/* eslint-disable no-restricted-globals */
import axios from 'axios';

import config from '../../config';

axios.defaults.withCredentials = true;

/**
 * Log user out in case of 401 Unauthorized error
 * @param {import('axios').AxiosError} error
 */
function handleAuthFailure(error) {
  throw error;
}

export const authPut = ({ url, data, headers = {} }) =>
  axios
    .put(`${config.apiHost}${url}`, data, {
      withCredentials: true,
      headers: {
        ...headers,
        'content-format-version': config.contentFormatVersion,
      },
    })
    .catch(handleAuthFailure);

export const authPost = ({ url, data, headers }) =>
  axios
    .post(`${config.apiHost}${url}`, data, {
      withCredentials: true,
      headers: {
        ...headers,
        'content-format-version': config.contentFormatVersion,
      },
    })
    .catch(handleAuthFailure);

export const authGet = ({ url, data, headers }) =>
  axios
    .get(`${config.apiHost}${url}`, {
      data,
      withCredentials: true,
      headers: {
        ...headers,
        'content-format-version': config.contentFormatVersion,
      },
    })
    .catch(handleAuthFailure);

export const authDelete = ({ url, data }) =>
  axios
    .delete(`${config.apiHost}${url}`, {
      data,
      withCredentials: true,
      headers: {
        'content-format-version': config.contentFormatVersion,
      },
    })
    .catch(handleAuthFailure);
