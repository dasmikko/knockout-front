/* eslint-disable react/jsx-curly-newline */
import React from 'react';
import PropTypes from 'prop-types';

import LoggedInOnly from '../LoggedInOnly';
import Tooltip from '../Tooltip';
import UserGroupRestricted, { usergroupCheck } from '../UserGroupRestricted';
import PostDateLink from './PostDateLink';
import {
  PostBanUserButton,
  PostDate,
  PostReplyToButtonIcon,
  PostReplyToButton,
  PostShowBBCodeButton,
  PostShowBBCodeIcon,
  PostButtonContainer,
} from './style';
import ReportButton from './ReportButton';
import submitBan from '../../services/ban';
import { MODERATOR_GROUPS } from '../../utils/userGroups';

const PostHeading = ({
  postId,
  postDate,
  thread,
  threadLocked,
  postPage,
  hideControls,
  user,
  byCurrentUser,
  toggleEditable,
  showBBCode,
  replyToPost,
  countryName,
}) => (
  <PostDate>
    <PostDateLink
      id={postId}
      date={postDate}
      thread={thread}
      postPage={postPage}
      countryName={countryName}
    />
    {!hideControls && (
      <LoggedInOnly>
        <PostButtonContainer>
          <UserGroupRestricted userGroupIds={[3, 4, 5, 6]}>
            <Tooltip text="Ban User" top={false}>
              <PostBanUserButton
                type="submit"
                onClick={() =>
                  submitBan({
                    userId: user.id,
                    postId,
                    banReason: window.prompt("What's the ban reason? (required)"),
                    banLength: window.prompt(
                      'How long should this user be banned for? | 24: 1 day | 72: 3 days | 168: 1 week | 720: 1 month | 0: PERMABAN'
                    ),
                  })
                }
                title="Ban User"
              >
                <PostReplyToButtonIcon className="fas fa-gavel" />
              </PostBanUserButton>
            </Tooltip>
          </UserGroupRestricted>
          <LoggedInOnly>
            <ReportButton postId={postId} />
          </LoggedInOnly>

          {!byCurrentUser && (
            <Tooltip text="Show BBCode" top={false}>
              <PostShowBBCodeButton square onClick={showBBCode} title="Show BBCode">
                <PostShowBBCodeIcon className="fas fa-cog" />
              </PostShowBBCodeButton>
            </Tooltip>
          )}

          {(usergroupCheck(MODERATOR_GROUPS) || byCurrentUser) && (
            <Tooltip text="Edit" top={false}>
              <PostReplyToButton square onClick={toggleEditable} title="Edit">
                <PostReplyToButtonIcon className="fas fa-pencil-alt" />
              </PostReplyToButton>
            </Tooltip>
          )}

          {!threadLocked && (
            <Tooltip text="Reply" top={false}>
              <PostReplyToButton onClick={() => replyToPost()} title="Reply">
                <PostReplyToButtonIcon className="fas fa-reply" />
              </PostReplyToButton>
            </Tooltip>
          )}
        </PostButtonContainer>
      </LoggedInOnly>
    )}
  </PostDate>
);

PostHeading.propTypes = {
  postDate: PropTypes.string.isRequired,
  postId: PropTypes.number.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
  }).isRequired,
  byCurrentUser: PropTypes.bool.isRequired,
  thread: PropTypes.number,
  threadLocked: PropTypes.bool,
  showBBCode: PropTypes.func,
  postPage: PropTypes.string,
  hideControls: PropTypes.bool,
  countryName: PropTypes.string,
  replyToPost: PropTypes.func.isRequired,
  toggleEditable: PropTypes.func.isRequired,
};

PostHeading.defaultProps = {
  postPage: '',
  thread: undefined,
  threadLocked: false,
  showBBCode: undefined,
  countryName: undefined,
  hideControls: false,
};
export default PostHeading;
