/* eslint-disable no-plusplus */
import React from 'react';
import dayjs from 'dayjs';
import PropTypes from 'prop-types';

import Ratings from '../Rating';
import PostBanInformation from './PostBanInformation';
import PostHeading from './PostHeading';
import PostUserInfoWrapper from './PostUserInfoWrapper';
import { PostWrapper, PostBody, PostContent } from './style';

import { updatePost, getPost } from '../../services/posts';
import { loadRatingsXrayFromStorageBoolean } from '../../services/theme';
import PostEditorBB from '../KnockoutBB/PostEditorBB';

class Post extends React.Component {
  editor = null;

  constructor(props) {
    super(props);

    this.state = {
      readOnly: true,
      showBBCode: false,
      ratings: this.props.ratings,
      ratingsXray: loadRatingsXrayFromStorageBoolean(),
    };
  }

  getEditor = (param) => {
    if (!this.editor) this.editor = param;
  };

  replyToPost = () => {
    const {
      threadPage,
      threadId,
      postId,
      username,
      postBody,
      user: { id: targetUserId },
    } = this.props;

    let quotePyramidsRemovedString = `${postBody}`;

    const extractQuotes = () => {
      quotePyramidsRemovedString = quotePyramidsRemovedString.replace(
        /\[quote.*[\s\S]*\[\/quote]/gi,
        ''
      );

      if (/\[quote.*[\s\S]*\[\/quote]/gi.test(quotePyramidsRemovedString)) {
        extractQuotes(quotePyramidsRemovedString);
      }
    };

    try {
      extractQuotes();
    } catch (error) {
      console.error('Could not remove quotes.');

      quotePyramidsRemovedString = postBody;
    }

    this.props.quickReplyEditor('quote', {
      threadPage,
      threadId,
      postId,
      username,
      postContent: quotePyramidsRemovedString.trim(),
      mentionsUser: targetUserId,
    });

    window.scrollTo(0, document.body.scrollHeight);
  };

  updatePost = async (content) => {
    try {
      await updatePost({ content, id: this.props.postId, threadId: this.props.threadId });

      this.setState({ readOnly: true });
    } catch (err) {
      console.log('could not update post', err);

      this.setState({ readOnly: true });
    }
  };

  toggleEditable = () => {
    const currState = this.state.readOnly;
    this.setState({ readOnly: !currState });
  };

  refreshPost = async (postId) => {
    const updatedPost = (await getPost(postId)).data;

    this.setState({ ratings: updatedPost.ratings });
  };

  toggleShowBBCode = () => {
    const { showBBCode } = this.state;
    this.setState({ showBBCode: !showBBCode });
  };

  render() {
    const {
      userJoinDate,
      postBody,
      postDate,
      postId,
      user,
      byCurrentUser,
      bans,
      thread,
      threadLocked,
      postPage,
      isUnread,
      hideUserWrapper,
      hideControls,
      countryName,
      isLinkedPost,
    } = this.props;

    const userJoinDateShort = dayjs(userJoinDate).format('MMM YYYY');
    const userJoinDateLong = dayjs(userJoinDate).format('DD/MM/YYYY');
    const userCakeDay = dayjs(userJoinDate).format('DD/MM') === dayjs(new Date()).format('DD/MM');

    return (
      <PostWrapper id={`post-${postId}`}>
        {!hideUserWrapper && (
          <PostUserInfoWrapper
            user={user}
            isUnread={isUnread}
            byCurrentUser={byCurrentUser}
            userJoinDateLong={userJoinDateLong}
            userJoinDateShort={userJoinDateShort}
            isLinkedPost={isLinkedPost}
            userCakeDay={userCakeDay}
          />
        )}

        <PostBody canRate>
          <PostHeading
            toggleEditable={this.toggleEditable}
            replyToPost={this.replyToPost}
            showBBCode={this.toggleShowBBCode}
            postId={postId}
            postDate={postDate}
            countryName={countryName}
            thread={thread}
            threadLocked={threadLocked}
            postPage={postPage}
            hideControls={hideControls}
            user={user}
            byCurrentUser={byCurrentUser}
          />

          <PostContent className={this.state.readOnly ? '' : 'no-padding'}>
            <PostEditorBB
              getEditor={this.getEditor}
              getContent={this.getEditor}
              readOnly={this.state.readOnly}
              updatePost={this.updatePost}
              editable={!this.state.readOnly}
              showBBCode={this.state.showBBCode}
              contentSource={postBody}
            />
          </PostContent>

          {bans !== undefined && bans.map((ban) => <PostBanInformation key={ban} ban={ban} />)}
          <Ratings
            postId={postId}
            ratings={this.state.ratings}
            userId={user.id}
            refreshPost={this.refreshPost}
            xrayEnabled={this.state.ratingsXray}
            postByCurrentUser={byCurrentUser}
          />
        </PostBody>
      </PostWrapper>
    );
  }
}

export default Post;

Post.propTypes = {
  username: PropTypes.string.isRequired,
  userJoinDate: PropTypes.string.isRequired,
  postBody: PropTypes.string.isRequired,
  postDate: PropTypes.string.isRequired,
  postId: PropTypes.number.isRequired,
  ratings: PropTypes.arrayOf(PropTypes.shape({})),
  user: PropTypes.shape({ id: PropTypes.number.isRequired }).isRequired,
  byCurrentUser: PropTypes.bool.isRequired,
  bans: PropTypes.arrayOf(PropTypes.shape({})),
  threadPage: PropTypes.number.isRequired,
  threadId: PropTypes.number.isRequired,
  threadLocked: PropTypes.bool,
  quickReplyEditor: PropTypes.func.isRequired,
  thread: PropTypes.number,
  postPage: PropTypes.number,
  isUnread: PropTypes.bool.isRequired,
  hideUserWrapper: PropTypes.bool,
  hideControls: PropTypes.bool,
  countryName: PropTypes.string,
  isLinkedPost: PropTypes.bool,
};
Post.defaultProps = {
  bans: undefined,
  ratings: undefined,
  threadLocked: false,
  thread: undefined,
  postPage: undefined,
  hideUserWrapper: false,
  hideControls: false,
  isLinkedPost: false,
  countryName: undefined,
};
