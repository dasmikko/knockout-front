import React from 'react';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import styled from 'styled-components';
import { HashLink as Link } from 'react-router-hash-link';
import PropTypes from 'prop-types';
import { ThemeMediumTextSize, ThemePrimaryTextColor } from '../../Theme';

dayjs.extend(relativeTime);

const actualPostDate = date => {
  const futurePost = dayjs().isBefore(dayjs(date));
  return futurePost ? 'Now' : dayjs().to(dayjs(date));
};

const PostDateLink = ({ className, id, date, thread, postPage, countryName }) => (
  <Link
    className={className}
    to={`${thread ? `/thread/${thread}/${postPage}` : ''}#post-${id}`}
    title={dayjs(date).format('DD/MM/YYYY HH:mm:ss')}
  >
    {actualPostDate(date)} {countryName && `from ${countryName}`}
  </Link>
);

PostDateLink.propTypes = {
  className: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  date: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.instanceOf(Date)])
    .isRequired,
  thread: PropTypes.number,
  postPage: PropTypes.string,
  countryName: PropTypes.string
};

PostDateLink.defaultProps = {
  postPage: '',
  thread: undefined,
  countryName: undefined
};

export default styled(PostDateLink)`
  color: ${ThemePrimaryTextColor};
  font-size: ${ThemeMediumTextSize};
  text-decoration: none;
`;
