/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/prop-types */
import React from 'react';
import humanizeDuration from '../../utils/humanizeDuration';
import { BanInfoWrapper, BanInfoIcon, BanInfoText, BanInfoStrong } from './style';

const PostBanInformation = ({ ban: { banBannedBy, banCreatedAt, banExpiresAt, banReason } }) => (
  <BanInfoWrapper>
    <BanInfoIcon
      src="https://img.icons8.com/color/50/000000/police-badge.png"
      alt="Banned!"
      title="Banned!"
    />
    <BanInfoText>
      <div>
        User was banned by <BanInfoStrong>{banBannedBy}</BanInfoStrong> for this post for
        <BanInfoStrong> {humanizeDuration(banCreatedAt, banExpiresAt)}</BanInfoStrong> with reason:
      </div>
      <div>
        <BanInfoStrong>{banReason}</BanInfoStrong>
      </div>
    </BanInfoText>
  </BanInfoWrapper>
);

export default PostBanInformation;
