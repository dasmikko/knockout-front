/**
 *  Non-visual component to organize non-visual components
 *  such as the notification requests
 * @param { children } Nodes
 */
const FunctionalComponents = ({ children }) => children;

export default FunctionalComponents;
