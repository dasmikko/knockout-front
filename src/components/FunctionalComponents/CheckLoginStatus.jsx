/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import { pushNotification } from '../../utils/notification';
import { isLoggedIn } from '../LoggedInOnly';
import { syncData } from '../../services/user';

class CheckLoginStatus extends React.Component {
  componentDidMount = async () => {
    if (!isLoggedIn()) {
      return;
    }

    // we don't need to sync user data in usersetup yet
    if (this.props.location.pathname === '/usersetup') {
      return;
    }

    const user = await syncData();

    // if we get no response, notify the user that
    // the request failed:
    if (!user || user.error) {
      pushNotification({
        message:
          '⚠️ Could not sync your user data - things might not work properly. Please try again later or try logging out and back in.',
        type: 'error'
      });
      return;
    }

    this.props.updateHeader(user);

    // if the user is banned, log them out.
    // ban local storage info and notification
    // is done in the user.js service
    if (user.isBanned) {
      setTimeout(() => {
        this.props.history.push('/logout');
      }, 3500);
    }
  };

  render() {
    return null;
  }
}
CheckLoginStatus.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  updateHeader: PropTypes.func.isRequired
};

export default withRouter(CheckLoginStatus);
