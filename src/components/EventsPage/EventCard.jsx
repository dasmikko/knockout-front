import React from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import { Link } from 'react-router-dom';
import relativeTime from 'dayjs/plugin/relativeTime';
import { StyledEventCardWrapper, StyledEventDate, StyledEventCardBody } from './style';

dayjs.extend(relativeTime);

const processEventContent = content => {
  const parsedContent = JSON.parse(content).content;

  if (!parsedContent) return <span>Null</span>;

  return parsedContent.map(el => {
    if (typeof el === 'object') {
      if (el.text) {
        return (
          <Link key={el.text} to={el.link}>
            {el.text}
          </Link>
        );
      }
      return <span style={{ color: '#e33' }}>unknown thread</span>;
    }
    if (typeof el === 'string') {
      return <span key={el}>{el}</span>;
    }
    return null;
  });
};

const EventCard = ({ created_at: createdAt, content }) => (
  <StyledEventCardWrapper>
    <StyledEventDate title={`${dayjs(createdAt).format('HH:mm DD/MM/YY')} GMT`}>
      <i className="far fa-calendar-alt" />
      <span>
        &nbsp;
        {dayjs(createdAt).from()}
      </span>
    </StyledEventDate>

    <StyledEventCardBody>{processEventContent(content)}</StyledEventCardBody>
  </StyledEventCardWrapper>
);

export default EventCard;
EventCard.propTypes = {
  created_at: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
};
