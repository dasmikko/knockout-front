import styled from 'styled-components';
import { ThemeSmallTextSize } from '../../Theme';

export const StyledTooltipArrow = styled.div`
  opacity: 0;
  z-index: 99;
  transition: opacity 100ms ease-in-out;
  width: 0;
  height: 0;
  border-style: solid;
  ${(props) =>
    props.top
      ? `
        border-width: 5px 5px 0 5px;
        border-color: #cf2a2a transparent transparent transparent;`
      : `
        border-width: 0 5px 5px 5px;
        border-color: transparent transparent #cf2a2a transparent;`}

  content: '';
  position: absolute;
  ${(props) => (props.top ? `top: -5px;` : `bottom: -5px;`)}
  left: 50%;

  transform: translate(-50%, ${(props) => (props.top ? '-100%' : '100%')});
`;

export const StyledTooltipText = styled.div`
  opacity: 0;
  z-index: 99;
  transition: opacity 100ms ease-in-out;
  pointer-events: none;
  font-size: ${ThemeSmallTextSize};
  position: absolute;

  ${(props) => (props.top ? `top: -10px` : `bottom: -10px`)};
  left: 50%;

  transform: translate(-50%, ${(props) => (props.top ? '-100%' : '100%')});

  padding: 3px;
  background: #cf2a2a;
  color: white;
  border-radius: 3px;
  width: max-content;

  ${(props) =>
    props.widthLimited &&
    `max-width: 100%;
  box-sizing: border-box;
  word-break: break-all;`}
`;

export const StyledTooltip = styled.div`
  position: relative;

  &:hover ${StyledTooltipArrow} {
    opacity: 1;
  }
  &:hover ${StyledTooltipText} {
    opacity: 1;
  }

  ${(props) => props.fullWidth && `width: 100%;`}
`;
