import React from 'react';
import PropTypes from 'prop-types';

import { StyledTooltipArrow, StyledTooltipText, StyledTooltip } from './style';

const Tooltip = ({ text, children, top = true, widthLimited = false, fullWidth = false }) => (
  <StyledTooltip widthLimited={widthLimited} fullWidth={fullWidth}>
    <StyledTooltipArrow top={top} />
    <StyledTooltipText top={top}>{text}</StyledTooltipText>
    {children}
  </StyledTooltip>
);

export default Tooltip;

Tooltip.propTypes = {
  children: PropTypes.node.isRequired,
  text: PropTypes.string.isRequired,
  top: PropTypes.bool,
  widthLimited: PropTypes.bool,
  fullWidth: PropTypes.bool,
};
Tooltip.defaultProps = {
  top: true,
  widthLimited: false,
  fullWidth: false,
};
