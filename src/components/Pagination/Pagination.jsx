/* eslint-disable */
import React from 'react';
import {
  PaginationWrapper,
  PaginationItemSpacer,
  PaginationItem,
  PaginationItemActive
} from './style';

const scrollToTop = () => {
  document.documentElement.scrollTop = 0;
};

const getUnfocusedRange = (totalPages, maxSize) => {
  let output = [];
  let center = Math.ceil(maxSize / 2);

  // count up from page 1 to the center
  for (let i = 1; i <= center; i++) {
    output.push(i);
  }

  // include a deliniator
  output.push(null);

  // count down from the last page to the center
  for (let i = center; i > 0; i--) {
    output.push(totalPages - (i - 1));
  }

  return output;
};

const getFocusedRange = (totalPages, maxSize, currentPage) => {
  let output = [];
  let center = Math.ceil(maxSize / 2);

  // if the user is on a page near the start of the thread
  if (currentPage <= center) {
    for (let i = 1; i <= maxSize; i++) {
      output.push(i);
    }
    output[maxSize - 2] = null;
    output[maxSize - 1] = totalPages;
  }

  // if the user is on a page near the middle of the thread
  if (currentPage > center && currentPage <= totalPages - center) {
    for (let i = center - 1; i > 0; i--) {
      output.push(currentPage - i);
    }
    for (let i = 0; i < center; i++) {
      output.push(parseInt(currentPage) + i);
    }
    output[0] = 1;
    output[1] = null;
    output[maxSize - 2] = null;
    output[maxSize - 1] = totalPages;
  }

  // if the user is on a page near the end of the thread
  if (currentPage > totalPages - center) {
    for (let i = maxSize; i > 0; i--) {
      output.push(totalPages - (i - 1));
    }
    output[0] = 1;
    output[1] = null;
  }

  return output;
};

const getFullRange = (totalPages, maxSize, currentPage) => {
  let output = [];

  // count up to the max Size
  for (let i = 1; i <= totalPages; i++) {
    output.push(i);
  }

  return output;
};

const Pagination = ({
  pagePath,
  totalPosts,
  currentPage,
  pageSize = 20,
  small = false,
  hideFirst = false,
  showNext = false
}) => {
  const totalPages = Math.ceil(totalPosts / pageSize);
  const maxSize = small ? 4 : 9;
  let pages = [];

  if (totalPages > maxSize) {
    // more pages than we have space to show, so lets be a bit more relevent
    if (typeof currentPage == 'undefined') {
      // we dont have a current page set, so lets just show the extremes
      pages = getUnfocusedRange(totalPages, maxSize);
    } else {
      // we do have a current page set, so lets show around it and the extremes
      pages = getFocusedRange(totalPages, maxSize, currentPage);
    }
  } else {
    // less pages than we have space to show, so lets show everything
    pages = getFullRange(totalPages, maxSize, currentPage);
  }

  const onLastPage = parseInt(currentPage, 10) === parseInt(totalPages, 10)
  const nextPage = parseInt(currentPage, 10) + 1

  return (
    <PaginationWrapper small={small || undefined}>
      {pages.map((page, index) => {
        if (page == null) {
          return (
            <PaginationItemSpacer small={small || undefined} key={`page-null-${index}`}>
              ...
            </PaginationItemSpacer>
          );
        }
        if (page === parseInt(currentPage, 10)) {
          return (
            <PaginationItemActive
              small={small || undefined}
              key={`page-${page}-active`}
              to={`${pagePath}${page}`}
              onClick={scrollToTop}
            >
              {page}
            </PaginationItemActive>
          );
        }
        return (
          <PaginationItem
            small={small || undefined}
            key={`page-${page}`}
            to={`${pagePath}${page}`}
            onClick={scrollToTop}
          >
            {page}
          </PaginationItem>
        );
      })}
      {showNext && !onLastPage && pages[0] && (
        <PaginationItem
          small={small || undefined}
          key={`page-next`}
          to={`${pagePath}${nextPage}`}
          onClick={scrollToTop}
        >
          Next <i className="fas fa-angle-double-right"></i>
        </PaginationItem>
      )}
    </PaginationWrapper>
  );
};

export default Pagination;
