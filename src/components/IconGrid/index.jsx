// @ts-check
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Tooltip from '../Tooltip';

function IconGrid({ className, icons, selectedId, onChange }) {
  const makeClickableIcon = icon => (
    <button type="button" onClick={() => onChange(icon)} key={icon.id} title={icon.desc}>
      <Tooltip text={icon.desc}>
        <img src={icon.url} alt={icon.desc} className={icon.id === selectedId ? 'selected' : ''} />
      </Tooltip>
    </button>
  );
  return (
    <div className={className}>
      {icons != null ? icons.map(makeClickableIcon) : 'Loading icons...'}
    </div>
  );
}

IconGrid.propTypes = {
  className: PropTypes.string.isRequired,
  icons: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      url: PropTypes.string.isRequired
    })
  ),
  selectedId: PropTypes.number,
  onChange: PropTypes.func
};

IconGrid.defaultProps = {
  icons: [],
  selectedId: -1,
  onChange: () => null
};

export default styled(IconGrid)`
  min-height: 64px;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-content: stretch;

  button {
    border: none;
    background: none;
    padding: 0;
    margin: 0;
  }

  img {
    height: 48px;
    margin: 10px;
    box-sizing: border-box;
    transition: filter 100ms ease-in-out;
    filter: brightness(0.5);

    @media (max-width: 700px) {
      height: 32px;
      margin: 5px;
    }
  }

  button:hover > img {
    filter: brightness(1.5) !important;
  }

  img.selected {
    filter: brightness(1);
    border: 1px solid white;
  }
`;
