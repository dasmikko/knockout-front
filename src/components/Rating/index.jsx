/* eslint-disable react/forbid-prop-types */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import submitRating from '../../services/ratings';
import { pushNotification } from '../../utils/notification';
import ratingList from './ratingList.json';
import LoggedInOnly from '../LoggedInOnly';
import Tooltip from '../Tooltip';

import {
  StyledRatingsWrapper,
  StyledRatingVoterWrapper,
  StyledRatingVoterWrapperButton,
  StyledRatingVoterMessage,
  HorizontalSpacer,
  StyledRatingsBar,
  StyledRaterList,
  StyledUserListExpanderButton,
} from './style';

const STANDARD_SUBFORUM_RATINGS = [
  'agree',
  'disagree',
  'funny',
  'friendly',
  'kawaii',
  'optimistic',
  'sad',
  'artistic',
  'informative',
  'idea',
  'zing',
  'winner',
  'glasses',
  'late',
  'dumb',
];

class Ratings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      voted: false,
      voterOpen: false,
      userListOpen: false,
    };
  }

  toggleVoter = () => this.setState((prevState) => ({ voterOpen: !prevState.voterOpen }));

  toggleUserList = () => this.setState((prevState) => ({ userListOpen: !prevState.userListOpen }));

  ratePost = async (postId, rating) => {
    if (this.props.postByCurrentUser) {
      return pushNotification({ message: 'Rating yourself is kind of sad.' });
    }

    this.setState({
      voted: true,
      voterOpen: false,
    });

    await submitRating({ postId, rating });

    this.props.refreshPost(postId);

    setTimeout(() => {
      this.setState({ voted: false });
    }, 2000);

    return null;
  };

  render() {
    const { postId, userId, currentUserId, xrayEnabled, loggedIn } = this.props;
    const { ratings } = this.props;
    const { userListOpen, voterOpen, voted } = this.state;

    const allRatings = STANDARD_SUBFORUM_RATINGS;

    return (
      <StyledRatingsBar>
        {ratings && (
          <StyledRatingsWrapper userListOpen={userListOpen}>
            {ratings.map((rating) => (
              <li key={rating.rating} className={voted ? 'voted' : null}>
                <Tooltip text={ratingList[rating.rating].name} top>
                  {loggedIn ? (
                    <img
                      src={ratingList[rating.rating].url}
                      alt={ratingList[rating.rating].name}
                      onClick={() => this.ratePost(postId, rating.rating)}
                    />
                  ) : (
                    <img src={ratingList[rating.rating].url} alt={ratingList[rating.rating].name} />
                  )}
                </Tooltip>
                <span>{rating.count}</span>
                {userListOpen && rating.users && (
                  <StyledRaterList>
                    {rating.users.map((user) => (
                      <li key={`${postId}_${user}`}>{user}</li>
                    ))}
                  </StyledRaterList>
                )}
              </li>
            ))}
            {xrayEnabled && ratings.length > 0 && (
              <StyledUserListExpanderButton>
                <button onClick={this.toggleUserList} type="button">
                  {userListOpen ? <i className="fas fa-eye-slash" /> : <i className="fas fa-eye" />}
                </button>
              </StyledUserListExpanderButton>
            )}
          </StyledRatingsWrapper>
        )}
        <HorizontalSpacer />
        {userId !== currentUserId && (
          <LoggedInOnly>
            {voted ? (
              <StyledRatingVoterMessage>Rated!</StyledRatingVoterMessage>
            ) : (
              <>
                <StyledRatingVoterWrapper className={voterOpen ? 'open' : null}>
                  {allRatings.map((ratingName) => (
                    <li key={ratingList[ratingName].name} className={voted ? 'voted' : null}>
                      <Tooltip text={ratingList[ratingName].name} top>
                        <img
                          src={ratingList[ratingName].url}
                          alt={ratingList[ratingName].name}
                          onClick={() => this.ratePost(postId, ratingName)}
                        />
                      </Tooltip>
                    </li>
                  ))}
                </StyledRatingVoterWrapper>
                <StyledRatingVoterWrapperButton onClick={this.toggleVoter}>
                  {voterOpen ? 'Close' : 'Rate'}
                </StyledRatingVoterWrapperButton>
              </>
            )}
          </LoggedInOnly>
        )}
      </StyledRatingsBar>
    );
  }
}
Ratings.propTypes = {
  postId: PropTypes.number.isRequired,
  userId: PropTypes.number.isRequired,
  currentUserId: PropTypes.number.isRequired,
  refreshPost: PropTypes.func.isRequired,
  xrayEnabled: PropTypes.bool.isRequired,
  loggedIn: PropTypes.bool.isRequired,
  ratings: PropTypes.array,
  postByCurrentUser: PropTypes.bool.isRequired,
};
Ratings.defaultProps = {
  ratings: [],
};

const mapStateToProps = ({ user: { id, isModerator, loggedIn } }) => ({
  currentUserId: id,
  isModerator,
  loggedIn,
});

export default connect(mapStateToProps)(Ratings);
