import styled from 'styled-components';
import {
  ThemeSecondaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeHorizontalHalfPadding,
  ThemeVerticalHalfPadding
} from '../../Theme';
import { MobileMediaQuery } from '../SharedStyles';

export const StyledRatingsWrapper = styled.ul`
  display: flex;
  padding: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};
  background: ${ThemeSecondaryBackgroundColor};
  justify-content: flex-start;
  box-sizing: border-box;
  list-style: none;
  align-items: baseline;

  li {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    padding: 0 ${ThemeHorizontalHalfPadding};
  }

  li.voted {
    cursor: auto;
  }

  li img {
    width: 24px;
    height: 24px;
    cursor: pointer;
  }

  li span {
    margin-top: ${ThemeVerticalHalfPadding};
    font-size: 12px;
    color: ${ThemePrimaryTextColor};
  }

  ${MobileMediaQuery} {
    flex-wrap: ${props => (props.userListOpen ? 'wrap' : 'no-wrap')};
    li {
      flex: ${props => (props.userListOpen ? '0 0 100%' : '1')};
    }
  }
`;

export const StyledRatingVoterWrapper = styled.ul`
  display: flex;
  padding: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};
  opacity: 0.1;
  transition: opacity 100ms ease-in-out;
  width: auto;
  flex-wrap: wrap;
  justify-content: flex;
  list-style: none;

  @media (max-width: 700px) {
    position: absolute;
    display: none;
    top: 0;
    bottom: 0;
    left: 0;
    right: 60px;
    padding: 0 ${ThemeHorizontalHalfPadding};
    background: ${ThemeSecondaryBackgroundColor};
    border-radius: 0 0 0 5px;
    opacity: 0;
    transition: opacity 0.2s ease;
  }

  &.open {
    opacity: 1;
    display: flex;
  }

  &:hover {
    opacity: 1;
  }

  li {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 0 ${ThemeHorizontalHalfPadding};
    cursor: pointer;
  }

  li.voted {
    cursor: auto;
  }

  img:hover {
    filter: drop-shadow(0px 0px 3px #ffcc00);
  }

  li img {
    width: 24px;
    height: 24px;
    cursor: pointer;
  }

  li span {
    margin-top: 5px;
    font-size: 12px;
  }
`;

export const StyledRatingVoterWrapperButton = styled.button`
  position: absolute;
  bottom: 0;
  right: 0;
  display: none;
  border: none;
  width: 60px;
  height: 100%;
  padding: 0;
  outline: none;
  text-align: center;
  background: ${ThemeSecondaryBackgroundColor};
  color: ${ThemePrimaryTextColor};
  z-index: 10;

  @media (max-width: 700px) {
    display: block;
  }
`;

export const StyledRatingVoterMessage = styled.div`
  display: block;
  width: 60px;
  flex-grow: 1;
  text-align: right;
  line-height: 54px;
  padding: 0 15px;
  color: ${ThemePrimaryTextColor};
`;

export const StyledRaterList = styled.ul`
  padding: 0 0.4rem 0.4rem;
  flex: 1;
  max-height: 100px;
  overflow-y: auto;
  scrollbar-width: thin;

  li {
    font-weight: 300;
    font-size: 0.9rem;
    color: ${ThemePrimaryTextColor};
  }
`;

export const StyledUserListExpanderButton = styled.div`
  button {
    background: none;
    border: none;
    color: ${ThemePrimaryTextColor};
    cursor: pointer;

    i {
      opacity: 0.3;
    }
  }
`;

export const HorizontalSpacer = styled.div`
  flex-grow: 1;
`;

export const StyledRatingsBar = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  min-height: 51px;

  ${MobileMediaQuery} {
    min-height: 50px;
    position: relative;
    display: flex;
    flex-direction: row;
  }
`;
