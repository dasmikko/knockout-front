/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import { Link } from 'react-router-dom';

import { HeaderLogo } from '../Header/components/style';
import FooterWrapper from './style';
import LoggedInOnly from '../LoggedInOnly';

import { getLogoPath } from '../../services/theme';

const scrollToTop = () => {
  document.documentElement.scrollTop = 0;
};

const Footer = () => (
  <FooterWrapper>
    <Link to="/">
      <HeaderLogo src={getLogoPath()} alt="Knockout Logo" />
    </Link>

    <div className="linkContainer">
      <span href="#" onClick={scrollToTop}>
        <i className="fas fa-level-up-alt" />
        &nbsp;Top
      </span>

      <a href="https://icons8.com/" target="_blank" rel="noopener">
        <i className="fas fa-external-link-alt" />
        Icons by Icons8
      </a>

      <LoggedInOnly>
        <Link to="/alerts/list">
          <i className="fas fa-newspaper" />
          &nbsp;Subscriptions
        </Link>
      </LoggedInOnly>

      <Link to="/rules">
        <i className="fas fa-atlas" />
        &nbsp;Rules
      </Link>

      <a href="https://discord.gg/wjWpapC" target="_blank" rel="noopener">
        <i className="fab fa-discord" />
        &nbsp;Discord
      </a>

      <a href="mailto:admin@knockout.chat" target="_blank" rel="noopener">
        <i className="fas fa-envelope" />
        &nbsp;Contact
      </a>
    </div>
  </FooterWrapper>
);

export default Footer;
