import styled from 'styled-components';

import {
  ThemeVerticalPadding,
  ThemeVerticalHalfPadding,
  ThemeHorizontalPadding,
  ThemeHorizontalHalfPadding,
  ThemeMediumTextSize,
  ThemeSecondaryBackgroundColor,
  ThemeTertiaryBackgroundColor,
  ThemePrimaryTextColor
} from '../../Theme';

export const PanelTitle = styled.h2`
  position: relative;
  padding-top: ${ThemeVerticalHalfPadding};
  padding-bottom: ${ThemeVerticalHalfPadding};
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalPadding};
  margin-top: 0;
  margin-bottom: ${ThemeVerticalPadding};
  font-size: ${ThemeMediumTextSize};
  font-weight: 100;
  line-height: 1.4;
  text-align: center;
  color: ${ThemePrimaryTextColor};
  background: ${ThemeTertiaryBackgroundColor};
`;

export const PanelParagraph = styled.p`
  line-height: 1.3;
  color: ${ThemePrimaryTextColor};
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;

export const Panel = styled.div`
  position: relative;
  flex-grow: 1;
  flex-shrink: 0;
  min-width: 320px;
  margin: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};
  padding-bottom: ${ThemeVerticalPadding};
  background: ${ThemeSecondaryBackgroundColor};
  border-radius: 5px;
  line-height: 1.5;
  overflow: hidden;

  ${props => props.flexCenter && 'display: flex; justify-content: center; align-items: center;'}
`;
