/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';

import config from '../../../config';

import { UserAvatarWrapper, UserAvatarSource } from './style';

const renderAvatar = ({ user }) => {
  let url = `${config.cdnHost}/image/${user.avatar_url}`;
  let title = `${user.username}'s avatar`;

  if (!user.avatar_url || user.avatar_url.length === 0) {
    url = `${config.cdnHost}/image/none.webp`;
  }
  if (user.isBanned) {
    url = 'https://img.icons8.com/color/80/000000/minus.png';
    title = `${user.username} is banned!`;
  }

  return (
    <UserAvatarWrapper>
      <UserAvatarSource srcSet={url} alt={title} title={title} type="image/webp" />
      <img
        srcSet="https://cdn.knockout.chat/assets/punchy-logo-small.png"
        alt={title}
      />
    </UserAvatarWrapper>
  );
};

const UserAvatar = ({ user }) => <>{renderAvatar({ user })}</>;

UserAvatar.propTypes = {
  user: PropTypes.object.isRequired
};
renderAvatar.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string.isRequired,
    avatar_url: PropTypes.string.isRequired
  }).isRequired
};

export default UserAvatar;
