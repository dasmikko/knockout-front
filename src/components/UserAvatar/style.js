import styled from 'styled-components';
import { ThemeVerticalPadding } from '../../Theme';

export const UserAvatarSource = styled.source`
  max-width: 225px;
  max-height: 100%;
`;

export const UserAvatarWrapper = styled.picture`
  margin-bottom: ${ThemeVerticalPadding};

  img {
    max-width: 225px;
    max-height: 100%;
  }

  @media (max-width: 960px) {
    margin-bottom: 0;
    margin-right: 15px;
  }
`;
