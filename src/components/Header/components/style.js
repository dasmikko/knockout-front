/* eslint-disable no-nested-ternary */
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { darken } from 'polished';
import {
  ThemeHorizontalPadding,
  ThemeMediumTextSize,
  ThemeNavTextColor,
  ThemePrimaryTextColor,
  ThemeKnockoutRed,
  ThemeSecondaryBackgroundColor,
  ThemeBodyWidth,
  ThemeBackgroundDarker,
} from '../../../Theme';

export const HeaderNavItems = styled.div`
  display: flex;
  a {
    color: ${ThemeNavTextColor};
    text-decoration: none;
    transition: opacity 150ms ease-in-out;

    &.link {
      margin-right: 30px;
      font-size: ${ThemeMediumTextSize};

      span {
        @media (max-width: 900px) {
          display: none;
        }
      }
    }

    opacity: 0.75;

    &:hover {
      opacity: 1;
    }
  }

  @media (max-width: 900px) {
    position: fixed;
    left: 0;
    bottom: 0px;
    width: calc(100vw);
    justify-content: space-around;
    display: flex;
    background: ${ThemeSecondaryBackgroundColor};
    padding: 5px 0;
    box-sizing: border-box;
    border-top: 1px solid rgba(236, 55, 55, 0.38);
    box-shadow: 0px -2px 3px rgba(236, 55, 55, 0.058);

    a {
      &.link {
        margin-right: 0;
        padding: 10px;
      }
    }

    span {
      display: none;
    }
  }
`;

export const StyledHeader = styled.header`
  width: 100%;
  top: 0;
  left: 0;
  right: 0;
  position: ${(props) => (props.stickyHeader ? 'fixed' : 'absolute')};
  z-index: 100;
  background: ${ThemeBackgroundDarker};

  border-bottom: 1px solid rgba(236, 55, 55, 0.08);
  box-shadow: 0px 1px 3px rgba(236, 55, 55, 0.058);

  #headerContent {
    max-width: ${ThemeBodyWidth};

    display: flex;
    align-items: center;
    justify-content: center;
    margin: auto;
    box-sizing: border-box;
    padding: 0 ${ThemeHorizontalPadding};
    position: relative;

    transition: max-height 300ms ease-in-out;
    ${(props) =>
      props.stickyHeader &&
      `
      max-height: ${props.topOfPage ? '100px' : '45px'};
    `}
  }

  @media (max-width: 900px) {
    margin-bottom: 50px;

    padding: unset;
    box-sizing: border-box;

    transition: transform 300ms ease-in-out;
    visibility: ${(props) =>
      props.stickyHeader ? (props.topOfPage ? 'visible' : 'hidden') : 'visible'};

    ${HeaderNavItems} {
      ${(props) =>
        props.topOfPage
          ? `visibility: visible;`
          : props.stickyHeader
          ? 'visibility: visible;'
          : 'visibility: hidden;'}
    }
  }

  #nprogress .bar {
    background: linear-gradient(
      90deg,
      ${ThemeKnockoutRed} 0,
      ${(props) => darken(0.06, ThemeKnockoutRed(props))} 100%
    );
  }
`;

export const HeaderLogo = styled.img`
  width: auto;
  height: 60px;
  margin-right: 10px;
  padding: 5px 0;
  position: relative;
  top: 0;
  shape-rendering: geometricprecision;
  transition: transform 300ms ease-in-out;

  ${(props) =>
    !props.topOfPage &&
    `
      transform: scale(0.5) translate(-25px, -3px);
    `}
`;
export const BetaTag = styled.div`
  position: fixed;
  width: 90px;
  text-align: center;
  top: 10px;
  right: -30px;
  transform: rotate(45deg);
  color: white;
  font-size: 10px;
  font-weight: bold;
  z-index: 900;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.28);
  background: linear-gradient(90deg, #28cdff, #a100dd);
`;

export const HeaderLink = styled(Link)`
  position: relative;
`;

export const HeaderLinkNotification = styled.div`
  background: #ec3737;
  padding: 0 3px;
  height: 14px;
  border-radius: 7px;
  position: absolute;
  top: -5px;
  right: 0;
  transform: translateX(100%);

  font-weight: bold;
  font-size: 8px;
  line-height: 14px;
  text-align: center;

  ${(props) =>
    props.left &&
    `
    right: unset;
    left: 0;
    transform: unset;
  `}
`;

export const HeaderLeftWrapper = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: flex-start;
  align-items: center;
  min-width: auto;

  @media (max-width: 700px) {
    font-size: 14px;
  }

  .brand {
    display: flex;
    flex-direction: row;
    align-items: center;
    font-family: 'Arimo', sans-serif;
    margin-right: 30px;
  }

  .title {
    font-family: 'Arimo', sans-serif;

    transition: transform 300ms ease-in-out;

    ${(props) =>
      !props.topOfPage &&
      `
      transform: translateX(-25px);
    `}

    @media (max-width: 900px) {
      display: none;
    }
  }

  .title .line1-text {
    font-size: 27px;
    font-weight: 600;
    color: ${ThemePrimaryTextColor};
    font-style: italic;
    margin-right: 4px;
    opacity: 0.925;
  }

  .title .line1-symbol {
    font-size: 29px;
    font-weight: 600;
    color: ${(props) => props.headerColor || ThemeKnockoutRed};
    margin-left: -3px;
    font-style: italic;
  }

  .title .line2 {
    font-size: 11.5px;
    text-transform: uppercase;
    overflow: hidden;
    width: 150px;
    height: 11.5px;
    position: relative;
    opacity: 1;
    color: ${ThemePrimaryTextColor};
    transition: height 300ms ease-in-out, opacity 300ms ease-in-out;

    ${(props) =>
      !props.topOfPage &&
      `
    height: 0px;
    opacity: 0;
    `}
  }

  .title .line2 .quote {
    width: max-content;
    position: absolute;
    left: 0;
  }

  .brand .title .line2 .quote.scrolling {
    transition: transform 5s ease-in-out;
    transform: translateX(0);
  }

  .brand:hover .title .line2 .quote.scrolling {
    transform: translateX(calc(-100% + 150px));
  }
`;
