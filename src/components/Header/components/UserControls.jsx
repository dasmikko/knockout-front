import React from 'react';
import { Link } from 'react-router-dom';

import HeaderUserLoggedIn from './HeaderUserLoggedIn';
import { loadUserFromStorage } from '../../../services/user';
import { DefaultBlueHollowButton } from '../../SharedStyles';

const LoginButtonStyle = {
  margin: 'auto'
};

class UserControls extends React.Component {
  tagline = '';

  render() {
    const user = loadUserFromStorage();
    const userId = user ? user.id : null;
    const avatarUrl = user ? user.avatar_url : null;
    const username = user ? user.username : null;
    const usergroup = user ? user.usergroup : null;
    const { unreadSubscriptions, mentions, topOfPage } = this.props;

    if (user == null) {
      return (
        <Link to="/login">
          <DefaultBlueHollowButton style={LoginButtonStyle}>Log in</DefaultBlueHollowButton>
        </Link>
      );
    }
    if (username == null) {
      return (
        <div>
          <Link to="/usersetup">
            <DefaultBlueHollowButton>User Setup</DefaultBlueHollowButton>
          </Link>
          <Link to="/logout">
            <DefaultBlueHollowButton>Log out</DefaultBlueHollowButton>
          </Link>
        </div>
      );
    }
    return (
      <HeaderUserLoggedIn
        userId={userId}
        unreadSubscriptions={unreadSubscriptions}
        mentions={mentions}
        avatarUrl={avatarUrl}
        username={username}
        usergroup={usergroup}
        removeMention={this.props.removeMention}
        getMentions={this.props.getMentions}
        topOfPage={topOfPage}
      />
    );
  }
}

export default UserControls;
