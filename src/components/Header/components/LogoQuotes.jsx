import React from 'react';

import { loadQuotesFromStorage, saveQuotesToStorage } from '../../../utils/quotesStorage';

class LogoQuotesComp extends React.Component {
  state = {};

  componentDidMount = async () => {
    let quotes = loadQuotesFromStorage();
    if (quotes === null) {
      quotes = await import('../../../Quotes');
      quotes = quotes.default;
      saveQuotesToStorage(quotes);
    }
    const selectedQuote = quotes[Math.floor(Math.random() * quotes.length)];
    this.setState({ tagline: selectedQuote, taglineScroll: selectedQuote.length > 20 });
  };

  render() {
    return (
      <div className="line2">
        <div className={this.state.taglineScroll ? 'quote scrolling' : 'quote'}>
          {this.state.tagline}
        </div>
      </div>
    );
  }
}

export default LogoQuotesComp;
