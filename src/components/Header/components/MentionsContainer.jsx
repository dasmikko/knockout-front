import React from 'react';
import styled from 'styled-components';
import { HashLink as Link } from 'react-router-hash-link';

import {
  ThemeHorizontalPadding,
  ThemeHorizontalHalfPadding,
  ThemeVerticalPadding,
  ThemeTertiaryBackgroundColor,
  ThemeHoverShadow,
  ThemePrimaryTextColor,
  ThemePrimaryBackgroundColor,
  ThemeQuaternaryBackgroundColor,
  ThemeSecondaryBackgroundColor
} from '../../../Theme';
import { updateMention } from '../../../services/mentions';
import { buttonHoverBrightness } from '../../SharedStyles';

const MentionsContainer = ({ mentions, getMentions, toggleOpen }) => (
  <StyledMentionsContainer mentions={mentions}>
    {(!mentions || mentions.length === 0) && <MentionEmpty>You have no new mentions!</MentionEmpty>}

    {typeof mentions === 'object' &&
      mentions.map(mention => {
        // we're not using anything but a single string at the moment
        const content = JSON.parse(mention.content)[0];
        return (
          <MentionItemWrapper key={mention.mentionId}>
            <MentionItem
              to={`/thread/${mention.threadId}/${mention.threadPage}#post-${mention.postId}`}
              onClick={async () => {
                toggleOpen();
                await updateMention(mention.postId);
                getMentions();
              }}
            >
              {content}
            </MentionItem>
            <DeleteMentionButton
              onClick={async () => {
                await updateMention(mention.postId);
                getMentions();
              }}
            >
              <i className="fas fa-eraser" />
            </DeleteMentionButton>
          </MentionItemWrapper>
        );
      })}
  </StyledMentionsContainer>
);

export default MentionsContainer;

const MentionItemWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  background: ${ThemeTertiaryBackgroundColor};
  box-sizing: border-box;
  color: ${ThemePrimaryTextColor};
  &:hover {
    box-shadow: ${ThemeHoverShadow};
  }
`;

const MentionEmpty = styled.div`
  display: block;
  margin: ${ThemeVerticalPadding} auto;
  background: ${ThemeTertiaryBackgroundColor};
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalHalfPadding};
  box-sizing: border-box;
  color: ${ThemePrimaryTextColor};
`;

const MentionItem = styled(Link)`
  display: block;
  flex-grow: 1;
  line-height: 1.3;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;

const StyledMentionsContainer = styled.div`
  display: flex;
  flex-direction: column;
  max-height: 50vh;
  border-top: solid 1px #666;
  background: ${ThemeTertiaryBackgroundColor};

  scrollbar-width: thin;
  scrollbar-color: ${ThemePrimaryTextColor} ${ThemeQuaternaryBackgroundColor};

  ::-webkit-scrollbar {
    width: 5px;
    border-radius: 2px;
  }

  ::-webkit-scrollbar-thumb {
    background: #ddd;
    border-radius: 2px;
  }

  ::-webkit-scrollbar-track {
    background: #666;
    border-radius: 2px;
  }
`;

const MentionsContainerTitle = styled.div``;

const DeleteMentionButton = styled.button`
  cursor: pointer;
  color: white;
  min-height: 25px;
  background: rgb(187, 37, 37);
  border: none;
  border-radius: 5px;
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  ${buttonHoverBrightness}
`;
