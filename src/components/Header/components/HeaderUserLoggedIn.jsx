import React from 'react';
import { HashLink as Link } from 'react-router-hash-link';
import styled from 'styled-components';

import UserRoleWrapper from '../../UserRoleWrapper';
import config from '../../../../config';

import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeMediumTextSize,
  ThemeSecondaryBackgroundColor,
  ThemeTertiaryBackgroundColor,
  ThemePrimaryTextColor
} from '../../../Theme';
import { AbsoluteBlackout } from '../../SharedStyles';
import { HeaderLinkNotification } from './style';
import MentionsContainer from './MentionsContainer';

const UserAvatar = ({ src, alt }) => {
  if (!src || src === 'none.webp') {
    return null;
  }

  let url = `${config.cdnHost}/image/${src}`;
  if (!src || src.length === 0) {
    url = '/static/default-avatar.png';
  }
  return <Avatar src={url} alt={`${alt}'s Avatar`} />;
};

class HeaderUserLoggedIn extends React.Component {
  state = {
    open: false
  };

  toggleOpen = () => {
    this.setState(prevState => ({
      open: !prevState.open
    }));
  };

  render() {
    const {
      userId,
      avatarUrl,
      username,
      usergroup,
      unreadSubscriptions,
      mentions,
      getMentions,
      topOfPage
    } = this.props;

    return (
      <UserLoggedInButton
        onClick={this.state.open ? null : this.toggleOpen}
        topOfPage={topOfPage}
      >
        {mentions && mentions.length > 0 && (
          <HeaderLinkNotification left>{mentions.length}</HeaderLinkNotification>
        )}
        <UserRoleWrapper user={{ usergroup }}>
          <Username>{username}</Username>
        </UserRoleWrapper>
        <UserAvatar src={avatarUrl} alt={username} />

        {this.state.open && (
          <>
            <OpenUserMenu>
              <UserMenuItem to={'/user/' + userId} onClick={this.toggleOpen}>
                <i className="fas fa-user">&nbsp;</i>
                Profile
              </UserMenuItem>
              <UserMenuItem to="/usersettings" onClick={this.toggleOpen}>
                <i className="fas fa-user-cog">&nbsp;</i>
                Settings
              </UserMenuItem>
              <UserMenuItem to="/alerts/list" onClick={this.toggleOpen}>
                <i className="fas fa-newspaper">&nbsp;</i>
                Subscriptions
                {unreadSubscriptions && <UnseenIndicator>{unreadSubscriptions}</UnseenIndicator>}
              </UserMenuItem>
              <UserMenuItem to="/logout" onClick={this.toggleOpen}>
                <i className="fas fa-sign-out-alt">&nbsp;</i>Log out
              </UserMenuItem>

              <MentionsContainer
                mentions={mentions}
                toggleOpen={this.toggleOpen}
                getMentions={getMentions}
              />
            </OpenUserMenu>

            <AbsoluteBlackout onClick={this.toggleOpen} />
          </>
        )}
      </UserLoggedInButton>
    );
  }
}

const UnseenIndicator = styled.span`
  background: #ec3737;
  padding: 1px 4px;
  margin-left: 3px;
  height: 13px;
  border-radius: 7px;
  font-weight: bold;
  font-size: 10px;
  text-align: center;
  line-height: 13px;
`;

const OpenUserMenu = styled.div`
  top: 45px;
  right: 0;
  z-index: 51;
  color: ${ThemePrimaryTextColor};

  width: 300px;
  max-height: 650px;
  position: absolute;

  margin-top: ${ThemeVerticalPadding};
  background: ${ThemeSecondaryBackgroundColor};
  border-radius: 5px;

  display: flex;
  flex-direction: column;
  overflow: hidden;

  font-size: ${ThemeMediumTextSize};

  @media (max-width: 900px) {
    width: calc(100vw - 45px);
    position: fixed;
    top: 70px;
    left: 50%;
    transform: translateX(-50%);
  }
`;

const UserMenuItem = styled(Link)`
  display: block;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  box-sizing: border-box;
  color: ${ThemePrimaryTextColor};

  i {
    min-width: 25px;
  }

  &:hover {
    background-color: ${ThemeTertiaryBackgroundColor};
  }
`;

const UserLoggedInButton = styled.div`
  display: flex;
  height: 45px;
  padding: 0;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  background: ${ThemeTertiaryBackgroundColor};
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
  position: relative;
  cursor: pointer;

  ${props =>
    !props.topOfPage &&
    `
    border-radius: 0px;
    img { border-radius: 0px; }
    `}

  &:hover {
    background-color: ${ThemeTertiaryBackgroundColor};
  }
  z-index: 51;
`;

const Username = styled.span`
  margin: 0 ${ThemeHorizontalPadding};
  line-height: 1.3;
  font-size: ${ThemeMediumTextSize};

  /* filter: drop-shadow(0px 0px 1px rgba(0, 0, 0, 0.15));
  text-shadow: 1px 1px rgba(0, 0, 0, 0.16); */
`;

const Avatar = styled.img`
  display: block;
  float: right;
  height: 47px;
  margin: 0;
  margin-right: -1px;
  background: rgba(0, 0, 0, 0.25);
  border-radius: 0 5px 5px 0px;
`;

export default HeaderUserLoggedIn;
