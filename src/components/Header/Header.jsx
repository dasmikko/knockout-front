/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import loadable from 'loadable-components';

import config from '../../../config';

import {
  StyledHeader,
  HeaderLogo,
  HeaderLeftWrapper,
  BetaTag,
  HeaderLink,
  HeaderLinkNotification,
  HeaderNavItems,
} from './components/style';

import { getLogoPath } from '../../services/theme';

import UserControls from './components/UserControls';
import BannedHeaderMessage from './components/BannedHeaderMessage';
import LoggedInOnly from '../LoggedInOnly';
import UserGroupRestricted from '../UserGroupRestricted';
import { scrollToTop } from '../../utils/pageScroll';
import { getEventColor, getEventText } from '../../utils/eventDates';
import { MODERATOR_GROUPS } from '../../utils/userGroups';

const LogoQuotes = loadable(() => import('./components/LogoQuotes'));

const getHeaderColor = () => {
  if (config.qa) {
    return '#facf07';
  }

  const eventColor = getEventColor();
  if (eventColor) {
    return eventColor;
  }

  return null;
};

const HeaderComponent = ({
  bannedInformation,
  unreadSubscriptions,
  openReports,
  mentionState,
  getMentions,
  clearSubscriptions,
  clearReports,
  onTopOfPage,
  stickyHeader,
}) => (
  <>
    {bannedInformation && (
      <BannedHeaderMessage
        banMessage={bannedInformation.banMessage}
        threadId={bannedInformation.threadId}
      />
    )}
    <StyledHeader id="knockout-header" topOfPage={onTopOfPage} stickyHeader={stickyHeader}>
      <div id="headerContent">
        <HeaderLeftWrapper topOfPage={onTopOfPage} headerColor={getHeaderColor()}>
          <Link to="/" className="brand">
            <HeaderLogo
              src={getLogoPath()}
              alt="Knockout Logo"
              title="smol punchy glove uwu"
              topOfPage={onTopOfPage}
            />
            <div className="title">
              <div>
                <span className="line1-text">{getEventText()}</span>
                <span className="line1-symbol">!</span>
              </div>
              <div className="line2">
                <LogoQuotes />
              </div>
            </div>
          </Link>

          <HeaderNavItems>
            <Link to="/rules" className="link">
              <i className="fas fa-atlas" /> <span>Rules</span>
            </Link>

            <a href="https://discord.gg/wjWpapC" className="link" target="_blank" rel="noopener">
              <i className="fab fa-discord" /> <span>Discord</span>
            </a>

            <LoggedInOnly>
              <HeaderLink
                to="/alerts/list"
                className="link"
                onClick={() => {
                  clearSubscriptions();
                  scrollToTop();
                }}
              >
                <i className="fas fa-newspaper" /> <span>Subscriptions</span>
                {unreadSubscriptions && (
                  <HeaderLinkNotification>{unreadSubscriptions}</HeaderLinkNotification>
                )}
              </HeaderLink>

              <Link to="/events" className="link" onClick={scrollToTop}>
                <i className="fas fa-bullhorn" /> <span>Events</span>
              </Link>

              <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
                <HeaderLink
                  to="/moderate"
                  className="link"
                  onClick={() => {
                    clearReports();
                    scrollToTop();
                  }}
                >
                  <i className="fas fa-shield-alt" /> <span>Moderation</span>
                  {openReports && <HeaderLinkNotification>{openReports}</HeaderLinkNotification>}
                </HeaderLink>
              </UserGroupRestricted>
            </LoggedInOnly>
          </HeaderNavItems>
        </HeaderLeftWrapper>

        <UserControls
          unreadSubscriptions={unreadSubscriptions}
          mentions={mentionState}
          getMentions={getMentions}
          topOfPage={onTopOfPage}
        />

        <BetaTag>BETA</BetaTag>
      </div>
    </StyledHeader>
  </>
);

HeaderComponent.propTypes = {
  bannedInformation: PropTypes.object,
  clearReports: PropTypes.func.isRequired,
  clearSubscriptions: PropTypes.func.isRequired,
  mentionState: PropTypes.array.isRequired,
  getMentions: PropTypes.func,
  openReports: PropTypes.number,
  unreadSubscriptions: PropTypes.number,
  onTopOfPage: PropTypes.bool.isRequired,
  stickyHeader: PropTypes.bool.isRequired,
};
HeaderComponent.defaultProps = {
  bannedInformation: undefined,
  getMentions: undefined,
  openReports: undefined,
  unreadSubscriptions: undefined,
};

export default HeaderComponent;
