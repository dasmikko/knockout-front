import styled from 'styled-components';
import {
  ThemeSecondaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeMediumTextSize,
  ThemeQuaternaryBackgroundColor,
  ThemeSecondaryTextColor,
  ThemeKnockoutRed
} from '../../Theme';
import { SubHeaderRealButton } from '../SubHeader/style';

export const StyledModalWrapper = styled.div`
  position: fixed;
  left: 50%;
  top: 50%;
  -webkit-font-smoothing: subpixel-antialiased;
  transform: translate(-50%, -50%);
  max-width: 100vw;
  width: 500px;
  height: ${props => props.height || '250px'};
  max-height: 100vh;

  display: flex;
  flex-direction: column;
  justify-content: space-between;

  background: ${ThemeSecondaryBackgroundColor};
  color: ${ThemePrimaryTextColor};
  border-radius: 5px;
  overflow: hidden;
  overflow-y: auto;

  z-index: 100;
`;

export const StyledModalTitle = styled.h2`
  font-size: ${ThemeMediumTextSize};
  color: ${ThemePrimaryTextColor};
`;

export const StyledSectionDelimiter = styled.div`
  display: flex;
  background: ${ThemeKnockoutRed};
  height: 70px;
  padding: 15px;
  box-sizing: border-box;
  position: relative;
  align-items: center;

  ${SubHeaderRealButton} {
    &:first-child {
      margin-left: 0;
    }
  }
`;

export const StyledModalContent = styled.div`
  padding: 15px;

  input[type='text'] {
    border: 1px solid ${ThemeSecondaryTextColor};
    color: ${ThemePrimaryTextColor};
    padding: 5px;
    background: transparent;
    font-size: 16px;
    border-radius: 5px;
    font-family: 'Open Sans', sans-serif;
    box-sizing: border-box;
    outline: none;

    &:focus {
      border: 1px solid transparent;
      box-shadow: 0 0 0px 2px ${ThemeKnockoutRed};
    }
  }

  input[type='radio'] {
    display: none;
    font-size: 16px;

    ~ label * {
      font-size: 16px;
      vertical-align: middle;
    }

    ~ label i {
      margin-right: 5px;
      font-size: 24px;
      margin-bottom: 5px;
    }

    ~ label .labels {
      .checked {
        display: none;
      }
      .unchecked {
        display: inline-block;
      }
    }

    &:checked {
      ~ label .labels {
        font-weight: bold;
        .checked {
          display: inline-block;
        }
        .unchecked {
          display: none;
        }
      }
    }
  }
`;

export const StyledModalSelect = styled.select`
  border: none;
  width: 100%;
  padding: 5px;
  line-height: 1;
  outline: none;
  font-size: ${ThemeMediumTextSize};
  color: inherit;
  border-radius: 0 0 5px 5px;
  background: ${ThemeQuaternaryBackgroundColor};
  height: 30px;
`;

export const StyledModalIcon = styled.img`
  max-height: 50px;
  max-width: 50px;
  margin-right: 5px;
`;
