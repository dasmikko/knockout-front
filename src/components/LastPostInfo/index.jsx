import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React from 'react';
import PropTypes from 'prop-types';
import UserRoleWrapper from '../UserRoleWrapper';
import { ThreadAuthor, ThreadLink, ThreadPageCount, ThreadSubtitle, ThreadTitle } from './style';

dayjs.extend(relativeTime);

const LastPostInfo = ({ post, withTitle, vertical, textAlign }) => {
  if (!post || !post.thread || !post.thread.post_count || !post.thread.id || !post.thread.title) {
    return null;
  }

  // FIXME: Current posts per page value is hardcoded
  const lastPage =
    (post && post.thread && post.thread.post_count && Math.ceil(post.thread.post_count / 20)) || 1;

  return (
    <ThreadLink to={`/thread/${post.thread.id}/${lastPage}#post-${post.id}`}>
      {withTitle ? <ThreadTitle>{post.thread.title}</ThreadTitle> : ''}
      <ThreadSubtitle vertical={vertical} textAlign={textAlign}>
        <ThreadAuthor>
          Last post by&nbsp;
          <UserRoleWrapper user={post.user}>{post.user.username}</UserRoleWrapper>
          &nbsp;
          {dayjs(post.created_at).fromNow()}
        </ThreadAuthor>
        <ThreadPageCount>
          Go to page&nbsp;
          {lastPage}
          &nbsp;➤
        </ThreadPageCount>
      </ThreadSubtitle>
    </ThreadLink>
  );
};

LastPostInfo.propTypes = {
  post: PropTypes.shape({
    thread: PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      post_count: PropTypes.number.isRequired
    })
  }).isRequired,
  withTitle: PropTypes.bool,
  vertical: PropTypes.bool,
  textAlign: PropTypes.string
};

LastPostInfo.defaultProps = {
  withTitle: false,
  vertical: false,
  textAlign: 'initial'
};

export default LastPostInfo;
