import React from 'react';

const messages = [
  'Reticulating Splines...',
  'Node Graph out of date. Rebuilding...',
  'Sending client info...',
  'hl2.exe is not responding...',
  'i see you.'
];

const Loading = () => (
  <pre className="loading">{messages[Math.floor(Math.random() * messages.length)]}</pre>
);

export default Loading;
