import { isKeyHotkey } from 'is-hotkey';

export const isBoldHotkey = e => isKeyHotkey('mod+b', e);
export const isItalicHotkey = e => isKeyHotkey('mod+i', e);
export const isUnderlinedHotkey = e => isKeyHotkey('mod+u', e);
export const isCodeHotkey = e => isKeyHotkey('mod+m', e);
export const isStrikethroughHotkey = e => isKeyHotkey('mod+s', e);

export const isSubmitHotkey = e => isKeyHotkey('mod+Enter', e);
