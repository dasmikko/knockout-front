/* eslint-disable no-restricted-globals */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { submitPost } from '../../services/posts';
import { loadUserFromStorage } from '../../services/user';
import { pushSmartNotification } from '../../utils/notification';
import {
  clearPostContentsFromStorage,
  loadPostContentsFromStorage,
  savePostContentsToStorage,
} from '../../utils/postEditorAutosave';
import {
  loadDisplayCountrySettingFromStorage,
  loadExtraScriptingSettingFromStorage,
  saveDisplayCountryInfoSettingToStorage,
  saveExtraScriptingInfoSettingToStorage,
} from '../../utils/postOptionsStorage';
import replaceBetween from '../../utils/replaceBetween';
import throttle from '../../utils/throttle';
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';
import Tooltip from '../Tooltip';
import { isImage } from './components/ImageBB';
import { getStrawPollId } from './components/StrawPollBB';
import { getStreamableId } from './components/StreamableBB';
import { getTweetId } from './components/TweetBB';
import { isVideo } from './components/VideoBB';
import { getVimeoId } from './components/VimeoBB';
import { getYoutubeId } from './components/YoutubeBB';
import {
  isBoldHotkey,
  isCodeHotkey,
  isItalicHotkey,
  isStrikethroughHotkey,
  isSubmitHotkey,
  isUnderlinedHotkey,
} from './editorHotkeys';
import parseBB from './knockoutBBcodeParser';
import {
  Button,
  PostReplyButtonWrapper,
  StyledBBPostInput,
  StyledPostEditorBB,
  StyledPostOptions,
  StyledQuickReply,
  Toolbar,
} from './style';
import { getVocarooId } from './components/VocarooBB';

const MIN_ACCT_AGE_SUBFORUM_IDS = [5, 6]; // subforumIds where acct has to be at least 3 days old to post

class PostEditorBB extends React.Component {
  inputRef = null;

  selectionRange = [];

  constructor(props) {
    super(props);
    this.state = {
      content: this.props.contentSource || loadPostContentsFromStorage(this.props.threadId) || '',
      canSubmit: true,
      showingOptions: false,
      sendCountryInfo: loadDisplayCountrySettingFromStorage(),
      allowExtraScripting: loadExtraScriptingSettingFromStorage(),
    };
  }

  componentDidMount() {
    if (this.props.getContent) {
      this.props.getContent(this.state.content);
      return;
    }
    if (this.props.getEditor) {
      this.props.getEditor(this.insertTag);
    }
  }

  onKeyUp(e) {
    if (!this.state.allowExtraScripting) {
      return;
    }
    this.selectionRange = [e.target.selectionStart, e.target.selectionEnd];
  }

  togglePostSendCountryInfo = () => {
    saveDisplayCountryInfoSettingToStorage(!this.state.sendCountryInfo);
    this.setState((prevState) => ({ sendCountryInfo: !prevState.sendCountryInfo }));
  };

  toggleAllowExtraScripting = () => {
    saveExtraScriptingInfoSettingToStorage(!this.state.allowExtraScripting);
    this.setState((prevState) => ({ allowExtraScripting: !prevState.allowExtraScripting }));
  };

  toggleOptions = () =>
    this.setState((prevState) => ({
      showingOptions: !prevState.showingOptions,
    }));

  // not ideal. doesn't work 100%
  resizeTextarea = (scrollHeight, elHeight) => {
    if (scrollHeight !== elHeight) {
      this.setState({ textareaHeight: `${elHeight}px` });
    }
  };

  updateContent = (e) => {
    const { target } = e;
    const { scrollHeight } = target;

    if (target.value.length < this.state.content.length) {
      this.setState(
        {
          content: target.value,
          textareaHeight: `${
            // awful hacky workaround
            scrollHeight > 150 && scrollHeight - 16 > 150 ? scrollHeight - 16 : 150
          }px`,
        },
        () => this.resizeTextarea(scrollHeight, this.inputRef.scrollHeight)
      );

      throttle(200, false, savePostContentsToStorage(this.props.threadId, target.value));
    } else {
      this.setState({ content: target.value, textareaHeight: `${scrollHeight}px` }, () =>
        this.resizeTextarea(scrollHeight, this.inputRef.scrollHeight)
      );
    }
  };

  insertTag = (type, content) => {
    if (type === 'li') {
      return this.setState((prevProps) => ({
        content: `${prevProps.content}[ul][li][/li][/ul]`,
      }));
    }

    if (type === 'quote') {
      const { threadPage, threadId, postId, username, postContent, mentionsUser } = content;

      return this.setState((prevState) => ({
        content: `${prevState.content}
[quote mentionsUser="${mentionsUser}" postId="${postId}" threadPage="${threadPage}" threadId="${threadId}" username="${username}"]${postContent}[/quote]`.trim(),
      }));
    }

    if (
      this.selectionRange[0] !== undefined &&
      this.selectionRange[1] !== undefined &&
      this.selectionRange[0] !== this.selectionRange[1]
    ) {
      return this.setState(
        (prevState) => ({
          content: replaceBetween(
            prevState.content,
            this.selectionRange[0],
            this.selectionRange[1],
            [`[${type}]`, `[/${type}]`]
          ),
        }),
        () => {
          this.selectionRange = [this.state.content.length, this.state.content.length];
          this.inputRef.focus();
        }
      );
    }

    return this.setState(
      (prevState) => ({
        content: `${prevState.content}[${type}][/${type}]`,
      }),
      () => {
        this.modifySelectionRange(`[/${type}]`);
      }
    );
  };

  renderButton = (type, icon, tooltipText, iconFamily = 'fas') => (
    <Button onMouseDown={() => this.insertTag(type)}>
      <Tooltip text={tooltipText}>
        <i className={`${iconFamily} fa-${icon}`} />
      </Tooltip>
    </Button>
  );

  submitPost = async () => {
    const { threadId, callbackFn } = this.props;

    const now = new Date();
    const cutoff = new Date(loadUserFromStorage().createdAt);
    cutoff.setDate(cutoff.getDate() + 7);

    const accountIsWeekOld = now.getTime() >= cutoff.getTime();

    if (!accountIsWeekOld && MIN_ACCT_AGE_SUBFORUM_IDS.includes(this.props.subforumId)) {
      return pushSmartNotification({
        error: `Your account is too new to post in this subforum. (ERR2NU-LRKMOAR)`,
      });
    }

    if (!threadId) {
      pushSmartNotification({ error: "No thread ID provided. That's an error!" });
      return undefined;
    }

    this.setState({ canSubmit: false }, async () => {
      try {
        await submitPost({
          content: this.state.content,
          thread_id: threadId,
          sendCountryInfo: this.state.sendCountryInfo,
        });

        this.setState({ content: '', canSubmit: true });

        if (callbackFn) {
          callbackFn({ goToLatest: true });
        }

        clearPostContentsFromStorage(this.props.threadId);
      } catch (error) {
        this.setState({ canSubmit: true }, () => {
          pushSmartNotification({
            error: `Could not submit post. If all else fails, have you tried logging in again?`,
          });
        });
      }
    });

    return undefined;
  };

  updatePost = async () => {
    this.setState({ canSubmit: false }, async () => {
      try {
        await this.props.updatePost(this.state.content);
        this.setState({ canSubmit: true });
      } catch (error) {
        this.setState({ canSubmit: true });

        console.error(error);
      }
    });
  };

  onReply = async () => {
    if (this.props.updatePost) {
      this.updatePost();
    } else {
      this.submitPost();
    }
  };

  onPaste = (e) => {
    if (!this.state.allowExtraScripting) {
      return null;
    }

    const clipboardData = e.clipboardData.getData('Text');

    const httpRegex = new RegExp(
      '^' +
        '(?:(?:(?:https?|ftp):)?\\/\\/)' +
        '(?:\\S+(?::\\S*)?@)?' +
        '(?:' +
        '(?!(?:10|127)(?:\\.\\d{1,3}){3})' +
        '(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})' +
        '(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})' +
        '(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])' +
        '(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}' +
        '(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))' +
        '|' +
        '(?:' +
        '(?:' +
        '[a-z0-9\\u00a1-\\uffff]' +
        '[a-z0-9\\u00a1-\\uffff_-]{0,62}' +
        ')?' +
        '[a-z0-9\\u00a1-\\uffff]\\.' +
        ')+' +
        '(?:[a-z\\u00a1-\\uffff]{2,}\\.?)' +
        ')' +
        '(?::\\d{2,5})?' +
        '(?:[/?#]\\S*)?' +
        '$',
      'i'
    );

    if (isImage(clipboardData)) {
      if (!confirm('Insert img?')) {
        return null;
      }
      e.preventDefault();

      return this.setState((prevState) => ({
        content: `${prevState.content}[img]${clipboardData}[/img]`,
      }));
    }
    if (getYoutubeId(clipboardData)) {
      if (!confirm('Insert youtube?')) {
        return null;
      }
      e.preventDefault();

      return this.setState((prevState) => ({
        content: `${prevState.content}[youtube]${clipboardData}[/youtube]`,
      }));
    }
    if (getVimeoId(clipboardData)) {
      if (!confirm('Insert Vimeo video?')) {
        return null;
      }
      e.preventDefault();

      return this.setState((prevState) => ({
        content: `${prevState.content}[vimeo]${clipboardData}[/vimeo]`,
      }));
    }
    if (getStreamableId(clipboardData)) {
      if (!confirm('Insert Streamable video?')) {
        return null;
      }
      e.preventDefault();

      return this.setState((prevState) => ({
        content: `${prevState.content}[streamable]${clipboardData}[/streamable]`,
      }));
    }
    if (getVocarooId(clipboardData)) {
      if (!confirm('Insert Vocaroo embed?')) {
        return null;
      }
      e.preventDefault();

      return this.setState((prevState) => ({
        content: `${prevState.content}[vocaroo]${clipboardData}[/vocaroo]`,
      }));
    }
    if (isVideo(clipboardData)) {
      if (!confirm('Insert video?')) {
        return null;
      }
      e.preventDefault();

      return this.setState((prevState) => ({
        content: `${prevState.content}[video]${clipboardData}[/video]`,
      }));
    }
    if (getStrawPollId(clipboardData)) {
      if (!confirm('Insert strawpoll?')) {
        return null;
      }
      e.preventDefault();

      return this.setState((prevState) => ({
        content: `${prevState.content}[strawpoll]${clipboardData}[/strawpoll]`,
      }));
    }
    if (getTweetId(clipboardData)) {
      if (!confirm('Insert twitter?')) {
        return null;
      }
      e.preventDefault();

      return this.setState((prevState) => ({
        content: `${prevState.content}[twitter]${clipboardData}[/twitter]`,
      }));
    }
    if (httpRegex.test(clipboardData)) {
      if (!confirm('Insert url?')) {
        return null;
      }
      e.preventDefault();

      return this.setState((prevState) => ({
        content: `${prevState.content}[url href="${clipboardData}"][/url]`,
      }));
    }

    return null;
  };

  modifySelectionRange(insertedString) {
    const { length } = insertedString;
    const contentLength = this.state.content.length;

    this.inputRef.setSelectionRange(contentLength - length, contentLength - length);
    this.selectionRange = [contentLength - length, contentLength - length];

    setTimeout(() => {
      this.inputRef.focus();
    }, 100);
  }

  assignRef(c) {
    this.inputRef = c;
  }

  checkForHotkeys(e) {
    if (!this.state.allowExtraScripting) {
      return;
    }

    if (isBoldHotkey(e)) {
      e.preventDefault();
      this.insertTag('b');
    }
    if (isItalicHotkey(e)) {
      e.preventDefault();
      this.insertTag('i');
    }
    if (isUnderlinedHotkey(e)) {
      e.preventDefault();
      this.insertTag('u');
    }
    if (isCodeHotkey(e)) {
      e.preventDefault();
      this.insertTag('code');
    }
    if (isStrikethroughHotkey(e)) {
      e.preventDefault();
      this.insertTag('s');
    }

    if (isSubmitHotkey(e)) {
      if (this.state.canSubmit) {
        this.onReply();
      }
    }
  }

  handleInputChange(e) {
    const value = { target: e.target };

    if (this.checkForHotkeys(e)) {
      return;
    }
    this.updateContent(value);
  }

  render() {
    const { editable, showBBCode } = this.props;
    const { canSubmit, showingOptions, sendCountryInfo, allowExtraScripting, content } = this.state;

    if (editable || showBBCode) {
      return (
        <StyledPostEditorBB>
          <StyledBBPostInput
            rows={1}
            ref={(ref) => this.assignRef(ref)}
            value={content}
            onChange={this.updateContent}
            onKeyDown={(e) => this.checkForHotkeys(e)}
            onKeyUp={(e) => this.onKeyUp(e)}
            onPaste={(e) => this.onPaste(e)}
            onClick={(e) => this.onKeyUp(e)}
            style={{
              height: this.state.textareaHeight || 'initial',
            }}
          />

          {showingOptions && (
            <div className="post-tools">
              <StyledPostOptions>
                <p>
                  <b>Options:</b>
                </p>

                <div className="option-wrapper">
                  <label htmlFor="toggle-display-country-info">
                    Show country information in post: &nbsp;
                    {sendCountryInfo ? (
                      <i className="options-icon far fa-check-square" />
                    ) : (
                      <i className="options-icon far fa-square" />
                    )}
                  </label>
                  <input
                    id="toggle-display-country-info"
                    type="checkbox"
                    onChange={this.togglePostSendCountryInfo}
                    checked={sendCountryInfo}
                  />
                </div>

                <div className="option-wrapper">
                  <label htmlFor="toggle-allow-extra-scripting">
                    Allow for extra scripting (disable if you have issues on mobile): &nbsp;
                    {allowExtraScripting ? (
                      <i className="options-icon far fa-check-square" />
                    ) : (
                      <i className="options-icon far fa-square" />
                    )}
                  </label>
                  <input
                    id="toggle-allow-extra-scripting"
                    type="checkbox"
                    onChange={this.toggleAllowExtraScripting}
                    checked={allowExtraScripting}
                  />
                </div>
              </StyledPostOptions>
            </div>
          )}

          <div className="post-tools">
            {editable && (
              <Toolbar>
                {allowExtraScripting ? (
                  <>
                    {this.renderButton('b', 'bold', 'Bold')}
                    {this.renderButton('i', 'italic', 'Italic')}
                    {this.renderButton('u', 'underline', 'Underlined')}
                    {this.renderButton('s', 'strikethrough', 'Strikethrough')}
                    {this.renderButton('code', 'code', 'Code')}
                    {this.renderButton('spoiler', 'eye-slash', 'Spoiler')}
                    {this.renderButton('h1', 'heading', 'Very Large Text')}
                    {this.renderButton('h2', 'font', 'Large Text')}
                    {this.renderButton('blockquote', 'quote-right', 'Quote')}
                    {this.renderButton('li', 'list-ul', 'Unordered List')}
                    {this.renderButton('img', 'image', 'Image')}
                    {this.renderButton('youtube', 'youtube', 'Youtube', 'fab')}
                    {this.renderButton('twitter', 'twitter', 'Twitter', 'fab')}
                    {this.renderButton('strawpoll', 'poll', 'Straw Poll')}
                    {this.renderButton('video', 'video', 'Video (webm/mp4)')}
                  </>
                ) : (
                  <Button>
                    <Tooltip text="Extra scripting disabled!">
                      <i className="fas fa-ghost" />
                    </Tooltip>
                  </Button>
                )}
              </Toolbar>
            )}

            {editable && canSubmit && (
              <PostReplyButtonWrapper>
                <Link className="help-link" to="/knockoutbb" target="_blank">
                  <i className="fas fa-question" />
                  &nbsp;Formatting Help
                </Link>
                <StyledQuickReply onClick={this.toggleOptions}>
                  <i className="fas fa-cog" /> Options
                </StyledQuickReply>
                <StyledQuickReply onClick={this.onReply} title="Shortcut: Ctrl+Enter">
                  <i className="fas fa-paper-plane" /> Submit
                </StyledQuickReply>
              </PostReplyButtonWrapper>
            )}
          </div>
        </StyledPostEditorBB>
      );
    }

    return (
      <ErrorBoundary errorMessage="Invalid KnockoutBB code.">{parseBB(content)}</ErrorBoundary>
    );
  }
}

export default PostEditorBB;

PostEditorBB.propTypes = {
  editable: PropTypes.bool.isRequired,
  showBBCode: PropTypes.bool,
  updatePost: PropTypes.func,
  threadId: PropTypes.number.isRequired,
  callbackFn: PropTypes.func,
  subforumId: PropTypes.number.isRequired,
  getEditor: PropTypes.func.isRequired,
  getContent: PropTypes.func,
  contentSource: PropTypes.string,
};

PostEditorBB.defaultProps = {
  showBBCode: false,
  contentSource: '',
  getContent: undefined,
  updatePost: undefined,
  callbackFn: undefined,
};
