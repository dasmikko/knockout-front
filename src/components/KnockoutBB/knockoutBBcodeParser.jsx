/* eslint-disable no-use-before-define */
import React from 'react';
import { ShortcodeTree, ShortcodeNode, TextNode } from 'shortcode-tree';

import renderShortcode from './knockoutBBcodeSchema';

const UNPARSED_TAGS = ['code', 'noparse'];

function concatenate(children) {
  return <span>{children.map(node => parseTree(node))}</span>;
}
function parseTree(node, index) {
  if (node instanceof TextNode || (node instanceof ShortcodeNode && node.shortcode === null)) {
    return <span key={`${node.text}-${index}`}>{node.text}</span>;
  }

  if (node instanceof ShortcodeNode) {
    const { properties } = node.shortcode;
    const tag = node.shortcode.name;

    // return just the text contents of tags in UNPARSED_TAGS. prevents breakage
    if (UNPARSED_TAGS.includes(tag)) {
      return renderShortcode({
        tag,
        content: node.text,
        properties: { ...properties, key: `${tag}-${index}` }
      });
    }

    if (node.children.length === 0 && node.text) {
      return renderShortcode({
        tag,
        content: node.text,
        properties: { ...properties, key: `${tag}-${index}` }
      });
    }

    const content = concatenate(node.children);

    return renderShortcode({ tag, content, properties: { ...properties, key: `${tag}-${index}` } });
  }

  return null;
}

export const bbToTree = string => ShortcodeTree.parse(string);

const parseBB = string => {
  try {
    const shortCodes = ShortcodeTree.parse(string);

    const comps =
      shortCodes.children.length > 0 ? shortCodes.children.map(parseTree) : parseTree(shortCodes);

    return <div className="bb-post">{comps}</div>;
  } catch (error) {
    return <pre>Could not parse BBcode.</pre>;
  }
};
export default parseBB;
