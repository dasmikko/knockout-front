/* eslint-disable react/forbid-prop-types */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { LightAsync as SyntaxHighlighter } from 'react-syntax-highlighter';
import { atomOneDark, atomOneLight } from 'react-syntax-highlighter/dist/cjs/styles/hljs';
import { loadThemeFromStorage } from '../../../services/theme';

const StyledCodeblock = styled(SyntaxHighlighter)`
  display: block;
  max-height: 90vh;
  margin: 15px 0;
  box-sizing: border-box;

  overflow-x: auto;
  overflow-y: auto;
  width: 100%;

  code {
    max-width: 100%;
  }
`;

const colorScheme = loadThemeFromStorage() === 'light' ? atomOneLight : atomOneDark;

const CodeBB = ({ language, children }) => (
  <StyledCodeblock showLineNumbers style={colorScheme} language={language}>
    {children}
  </StyledCodeblock>
);

CodeBB.propTypes = {
  language: PropTypes.string,
  children: PropTypes.any,
};

CodeBB.defaultProps = {
  language: 'javascript',
  children: null,
};

export default CodeBB;
