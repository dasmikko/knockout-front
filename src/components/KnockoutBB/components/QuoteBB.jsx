/* eslint-disable react/state-in-constructor */
/* eslint-disable react/prop-types */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/forbid-prop-types */
import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeMediumTextSize,
  ThemeTertiaryBackgroundColor,
  ThemeQuaternaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeSecondaryTextColor,
} from '../../../Theme';

const StyledQuoteTitle = styled(Link)`
  background: ${ThemeTertiaryBackgroundColor};
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  font-size: ${ThemeMediumTextSize};
  color: ${ThemeSecondaryTextColor};
  display: block;
  text-decoration: none;

  transition: background 100ms ease-in-out;

  &:hover {
    background: rgba(255, 255, 255, 0.2);
  }
`;

const StyledQuoteContents = styled.div`
  color: ${ThemePrimaryTextColor};
  background: ${ThemeQuaternaryBackgroundColor};
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  display: flex;
  flex-direction: column;
  justify-content: flex-end;

  overflow: hidden;

  ${(props) => (props.expanded ? `max-height: unset;` : `max-height: 200px;`)}

  ${(props) =>
    !props.expanded && `mask-image: linear-gradient(180deg,#fff 0px, #fff 170px, transparent);`}
`;

const StyledQuoteWrapper = styled.div`
  margin: ${ThemeVerticalPadding} 0;
  overflow: hidden;
  border-radius: 5px;
  border: 1px solid rgba(62, 62, 62, 0.5);
  border-bottom: 1px solid #4c4242;
`;

class QuoteBB extends React.Component {
  state = { expanded: false };

  expandPost = () => {
    if (this.state.expanded === false) {
      this.setState({ expanded: true });
    }
  };

  render = () => {
    const {
      threadPage = '1',
      threadId = '1',
      postId = '1',
      username = 'User',
      children,
    } = this.props;

    const scrollToElement = (elemId) => {
      setTimeout(() => {
        const element = document.getElementById(`post-${elemId}`);
        element.scrollIntoView();
      }, 1000);
    };

    return (
      <StyledQuoteWrapper>
        <StyledQuoteTitle
          to={`/thread/${threadId}/${threadPage}#post-${postId}`}
          onClick={() => scrollToElement(postId)}
        >
          {username}
          &nbsp; posted:
        </StyledQuoteTitle>
        <StyledQuoteContents expanded={this.state.expanded} onClick={this.expandPost}>
          {children}
        </StyledQuoteContents>
      </StyledQuoteWrapper>
    );
  };
}

QuoteBB.propTypes = {
  postData: PropTypes.shape({
    username: PropTypes.string.isRequired,
    threadPage: PropTypes.string.isRequired,
    threadId: PropTypes.number.isRequired,
    postId: PropTypes.number.isRequired,
  }),
  attributes: PropTypes.any,
  children: PropTypes.any.isRequired,
  editable: PropTypes.bool,
};
QuoteBB.defaultProps = {
  attributes: undefined,
  postData: {
    username: '',
    threadPage: ' ',
    threadId: 1,
    postId: 1,
  },
  editable: false,
};

export default QuoteBB;
