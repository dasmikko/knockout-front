import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

/**
 * A styled image block component.
 *
 * @type {Component}
 */
const StyledImage = styled('img')`
  display: inline-block;
  max-width: 100%;
  max-height: 100vh;
  margin: 15px 0;

  ${props => props.selected && `box-shadow: 0px 0px 0 1px #2900ff;`}
  ${props => (props.isThumbnail || props.isLink) && `border: 1px dotted gray;`}

  ${props =>
    props.isThumbnail &&
    ` max-width: 300px;
      max-height: 300px;
    `}
`;

/*
 * A function to determine whether a URL has an image extension.
 *
 * @param {String} url
 * @return {Boolean}
 */
export const isImage = url => {
  const imageExtensions = ['png', 'jpg', 'jpeg', 'gif', 'webp'];
  const rgx = /(?:\.([^.]+))?$/;

  const extension = rgx.exec(url);

  return !!imageExtensions.includes(extension[1]);
};

class ImageBB extends React.Component {
  state = {
    isThumbnail: this.props.thumbnail,
    isLink: this.props.link
  };

  render() {
    const { href } = this.props;
    const { isLink, isThumbnail } = this.state;

    const imageUrl = href;
    const imageLink = href;

    if (isLink || isThumbnail) {
      return isLink ? (
        <a href={imageLink} target="_blank" rel="ugc noopener">
          <StyledImage src={imageUrl} isThumbnail={isThumbnail} isLink={isLink} />
        </a>
      ) : (
        <a href={imageLink} target="_blank" rel="ugc noopener">
          <StyledImage src={imageUrl} isThumbnail={isThumbnail} />
        </a>
      );
    }

    return <StyledImage src={imageUrl} />;
  }
}

ImageBB.propTypes = {
  src: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  attributes: PropTypes.any
};

ImageBB.defaultProps = {
  attributes: undefined
};

export default ImageBB;
