import React from 'react';
import { TwitterTweetEmbed } from 'react-twitter-embed';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { loadPunchyLabsFromStorageBoolean, loadThemeFromStorage } from '../../../services/theme';

export const getTweetId = (src) => {
  const twitterRegx = /^https?:\/\/(?:mobile\.)?twitter\.com\/(?:#!\/)?(\w+)\/status(es)?\/(\d+)/;

  const tweet = twitterRegx.exec(src);
  if (!tweet) return null;
  const tweetId = tweet[3];
  if (!tweetId) return null;

  return tweetId;
};

const TweetBB = ({ href, children }) => {
  const stateTheme = useSelector((state) => state.style.theme);
  const theme = loadPunchyLabsFromStorageBoolean() ? stateTheme : loadThemeFromStorage();
  try {
    const tweetUrl = href || children.join('');

    const tweetId = getTweetId(tweetUrl);
    return (
      <TwitterTweetEmbed
        tweetId={tweetId}
        title={`tweet-${tweetUrl}`}
        options={{ theme: theme === 'light' ? 'light' : 'dark' }}
      />
    );
  } catch (error) {
    return '[Bad Twitter embed.]';
  }
};

TweetBB.propTypes = {
  src: PropTypes.string,
};
TweetBB.defaultProps = {
  src: undefined,
};

export default TweetBB;
