/* eslint-disable no-useless-escape */
/* eslint-disable prefer-destructuring */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import VisibilitySensor from 'react-visibility-sensor';
import { getYoutubeId } from './YoutubeBB';

/**
 * A styled image block component.
 *
 * @type {Component}
 */
const StyledStreamableWrapper = styled.div`
  display: block;
  margin: 15px 0;

  position: relative;
  width: 100%;
  max-width: 960px;

  &:before {
    content: '';
    width: 1px;
    margin-left: -1px;
    float: left;
    height: 0;
    padding-top: calc(540 / 960 * 100%);
  }
  &:after {
    content: '';
    display: table;
    clear: both;
  }

  ${props => props.selected && `box-shadow: 0px 0px 0 1px #2900ff;`}
`;
const StyledStreamable = styled.iframe`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;
const StyledStreamablePlaceholder = styled.div`
  display: block;
  margin: 15px 0;

  position: relative;
  width: 100%;
  max-width: 960px;

  img {
    width: 100%;
    height: 100%;
    max-height: 540px;
  }

  &:before {
    content: '';
    width: 1px;
    margin-left: -1px;
    float: left;
    height: 0;
    padding-top: calc(540 / 960 * 100%);
  }
  &:after {
    display: table;
    clear: both;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    content: '\f27d';
    font-family: 'Font Awesome 5 Brands', sans-serif;
    font-size: 100px;
  }

  &:hover {
    img {
      filter: brightness(1.1);
    }
    &:after {
      opacity: 0.2;
    }
  }
`;

export const getStreamableId = url => {
  const regex = /(https:\/\/(?:www\.|(?!www))(streamable)\.com\/([a-z0-9]+))/;

  try {
    const result = regex.exec(url);

    if (!result[3]) {
      return null;
    }

    return result[3];
  } catch (error) {
    return null;
  }
};

class StreamableBB extends React.Component {
  state = { visible: false };

  onChange = isVisible => isVisible && this.setState({ visible: isVisible });

  render() {
    try {
      const { content, href } = this.props;
      const streamableUrl = href || `${content}`;
      const streamableId = getStreamableId(streamableUrl);

      return this.state.visible ? (
        <StyledStreamableWrapper>
          <StyledStreamable
            title={streamableId}
            type="text/html"
            width="100%"
            height="auto"
            src={`https://streamable.com/s/${streamableId}`}
            frameBorder="0"
            allowFullScreen="allowfullscreen"
          />
        </StyledStreamableWrapper>
      ) : (
        <VisibilitySensor
          partialVisibility
          minTopValue={150}
          intervalDelay="250"
          onChange={this.onChange}
        >
          <StyledStreamablePlaceholder
            onClick={() => this.onChange(true)}
            title="Play Streamable video"
          >
          <img
              src={`https://img.youtube.com/vi/${getYoutubeId(
                'https://www.youtube.com/watch?v=WLDeuIAFOyo'
              )}/hqdefault.jpg`}
              alt="Thumbnail for Streamable video"
            />
          </StyledStreamablePlaceholder>
        </VisibilitySensor>
      );
    } catch (error) {
      return '[Bad Streamable embed.]';
    }
  }
}
StreamableBB.propTypes = {
  src: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  attributes: PropTypes.any
};

StreamableBB.defaultProps = {
  attributes: undefined
};

export default StreamableBB;
