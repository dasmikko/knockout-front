/* eslint-disable react/forbid-prop-types */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { ThemePrimaryTextColor } from '../../../Theme';

const StyledSpoiler = styled.span`
  background: ${ThemePrimaryTextColor};
  color: ${ThemePrimaryTextColor};

  a {
    color: ${ThemePrimaryTextColor};
  }

  &:hover {
    background: transparent;

    a {
      color: anime;
    }
  }
`;

const SpoilerBB = ({ children }) => <StyledSpoiler>{children}</StyledSpoiler>;

SpoilerBB.propTypes = {
  children: PropTypes.node.isRequired
};

export default SpoilerBB;
