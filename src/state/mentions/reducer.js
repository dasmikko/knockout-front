import { MENTION_REMOVE, MENTION_UPDATE } from './actions';

const initialState = {
  mentions: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case MENTION_UPDATE:
      return { ...state, mentions: action.value };
    case MENTION_REMOVE:
      return {
        ...state,
        mentions: state.mentions.filter((mention) => mention.mentionId !== action.value),
      };
    default:
      break;
  }
  return state;
}
