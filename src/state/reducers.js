import { combineReducers } from 'redux';

import { userReducer } from './user';
import { backgroundReducer } from './background';
import { mentionsReducer } from './mentions';
import { subscriptionsReducer } from './subscriptions';
import { styleReducer } from './style';

export default combineReducers({
  user: userReducer,
  background: backgroundReducer,
  mentions: mentionsReducer,
  subscriptions: subscriptionsReducer,
  style: styleReducer,
});
