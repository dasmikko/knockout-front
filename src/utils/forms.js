import { usergroupCheck } from '../componentsNew/UserGroupRestricted';
import { isImage } from '../components/KnockoutBB/components/ImageBB';

export const checkPresence = (value) => {
  if (value === undefined || value === '') {
    return 'Must have a value';
  }
  return undefined;
};

export const checkBetweenLengths = (value, min, max) => {
  if (value === undefined) {
    return undefined;
  }
  if (value.length < min) {
    return `Must be longer than ${min} characters`;
  }
  if (value.length > max) {
    return `Must be shorter than ${max} characters`;
  }
  return undefined;
};

export const checkLenghGreaterThan = (value, min) => {
  if (value.length < min) {
    return `Must be longer than ${min} characters`;
  }
  return undefined;
};

export const checkIsImage = (value) => {
  if (!isImage(value)) {
    return 'Must be an image';
  }
  return undefined;
};

export const checkUserGroup = (usergroups) => {
  if (!usergroupCheck(usergroups)) {
    return 'USER GROUP INCORRECT';
  }
  return undefined;
};

export const checkValidURL = (value) => {
  const pattern = new RegExp(
    '^(https?:\\/\\/)' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$',
    'i'
  ); // fragment locator
  return pattern.test(value) ? undefined : 'URL is invalid';
};

export const ifPresent = (value, nextFn) => {
  if (value !== undefined && value !== '') {
    return nextFn(value);
  }

  return undefined;
};

export function validate(values, validators) {
  const errors = {};

  Object.entries(validators).forEach(([key, validatorArray]) => {
    validatorArray.some((validator) => {
      const error = validator(values[key]);
      if (error) {
        // Workaround, being able to trap door out of a validator if the
        // user account shouldn't have access in the first place
        if (error !== 'USER GROUP INCORRECT') {
          errors[key] = error;
        }
        return true;
      }
      return false;
    });
  });

  Object.keys(errors).forEach((key) => (errors[key] === undefined ? delete errors[key] : {}));
  return errors;
}
