// Usergroups
// 0: Banned User
// 1: Regular User
// 2: Gold Member
// 3: Moderator
// 4: Admin
// 5: Staff
// 6: Moderator In Training

export const GOLD_MEMBER_GROUPS = [2, 3, 4, 5, 6];
export const MODERATOR_GROUPS = [3, 4, 5, 6];

export const USER_GROUPS = {
  BANNED_USER: 0,
  REGULAR_USER: 1,
  GOLD_MEMBER: 2,
  MODERATOR: 3,
  ADMIN: 4,
  STAFF: 5,
  MODERATOR_IN_TRAINING: 6,
};
