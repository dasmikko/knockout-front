/* eslint-disable no-console */
import theme from 'styled-theming';
import { getEventColor } from './eventDates';

let customTheme = {
  backgroundLighter: '#1f2c39',
  backgroundDarker: '#161d24',
  mainBackgroundColor: '#0b0e11',
  bodyBackgroundColor: '#0d1013',
  knockoutBrandColor: '#db2e2e',
  knockoutLogo: '/static/logo_dark.png',
  textColor: '#ffffff',
  themeFontSizeSmall: '0.79rem',
  themeFontSizeMedium: '0.875rem',
  themeFontSizeLarge: '1.000rem',
  themeFontSizeHuge: '1.500rem',
  themeFontSizeHeadline: '2rem',
  themeGoldMemberColor: 'linear-gradient(375deg, #fcbe20, #fcbe20, #ffe770, #fcbe20, #fcbe20)',
  themeGoldMemberGlow: 'drop-shadow(0px 0px 2px #ffcc4a)',
  themeModeratorColor: '#41ff74',
  themeModeratorInTrainingColor: '#4ccf6f',
  themeBannedUserColor: `#e04545`,
  highlightStrongerColor: '#f07c00',
  highlightWeakerColor: '#4580bf',
  postLineHeight: 160,
};

try {
  if (localStorage.getItem('customTheme')) {
    customTheme = {
      ...customTheme,
      ...JSON.parse(localStorage.getItem('customTheme')),
    };
  }
} catch (error) {
  console.warn('Could not load custom theme from local storage. ', error);
}

const dark = {
  backgroundLighter: '#1f2c39',
  backgroundDarker: '#161d24',
  mainBackgroundColor: '#0b0e11',
  bodyBackgroundColor: '#0d1013',
  knockoutBrandColor: '#db2e2e',
  knockoutLogo: '/static/logo_dark.png',
  textColor: '#ffffff',
  themeFontSizeSmall: '0.79rem',
  themeFontSizeMedium: '0.875rem',
  themeFontSizeLarge: '1.000rem',
  themeFontSizeHuge: '1.500rem',
  themeFontSizeHeadline: '2rem',
  themeGoldMemberColor: 'linear-gradient(375deg, #fcbe20, #fcbe20, #ffe770, #fcbe20, #fcbe20)',
  themeGoldMemberGlow: 'drop-shadow(0px 0px 2px #ffcc4a)',
  themeModeratorColor: '#41ff74',
  themeModeratorInTrainingColor: '#4ccf6f',
  themeBannedUserColor: `#e04545`,
  highlightStrongerColor: '#f07c00',
  highlightWeakerColor: '#4580bf',
  postLineHeight: 160,
};

const classic = {
  backgroundLighter: '#2d2d30',
  backgroundDarker: '#222226',
  mainBackgroundColor: '#1b1b1d',
  bodyBackgroundColor: '#111012',
  knockoutBrandColor: '#db2e2e',
  knockoutLogo: '/static/logo.svg',
  textColor: '#ffffff',
  themeFontSizeSmall: '0.79rem',
  themeFontSizeMedium: '0.875rem',
  themeFontSizeLarge: '1.000rem',
  themeFontSizeHuge: '1.500rem',
  themeFontSizeHeadline: '2rem',
  themeGoldMemberColor: 'linear-gradient(375deg, #fcbe20, #fcbe20, #ffe770, #fcbe20, #fcbe20)',
  themeGoldMemberGlow: 'drop-shadow(0px 0px 2px #ffcc4a)',
  themeModeratorColor: '#41ff74',
  themeModeratorInTrainingColor: '#4ccf6f',
  themeBannedUserColor: `#e04545`,
  highlightStrongerColor: '#f07c00',
  highlightWeakerColor: '#4580bf',
  postLineHeight: 160,
};

const light = {
  backgroundLighter: '#e6e6e6',
  backgroundDarker: '#f0f2f3',
  mainBackgroundColor: '#8b8a8d',
  bodyBackgroundColor: '#b9b9ba',
  knockoutBrandColor: '#db2e2e',
  knockoutLogo: '/static/logo_dark.png',
  textColor: '#222222',
  themeFontSizeSmall: '0.79rem',
  themeFontSizeMedium: '0.875rem',
  themeFontSizeLarge: '1.000rem',
  themeFontSizeHuge: '1.500rem',
  themeFontSizeHeadline: '2rem',
  themeGoldMemberColor: 'linear-gradient(375deg, #fcbe20, #fcbe20, #ffe770, #fcbe20, #fcbe20)',
  themeGoldMemberGlow: 'drop-shadow(0px 0px 2px #ffcc4a)',
  themeModeratorColor: '#1dd626',
  themeModeratorInTrainingColor: '#30b655',
  themeBannedUserColor: `#e04545`,
  highlightStrongerColor: '#f07c00',
  highlightWeakerColor: '#4580bf',
  postLineHeight: 160,
};

// color
export const ThemeKnockoutRed = theme('mode', {
  classic: getEventColor() || classic.knockoutBrandColor,
  light: getEventColor() || light.knockoutBrandColor,
  dark: getEventColor() || dark.knockoutBrandColor,
  custom: customTheme.knockoutBrandColor,
});
export const ThemeBackgroundLighter = theme('mode', {
  classic: classic.backgroundLighter,
  light: light.backgroundLighter,
  dark: dark.backgroundLighter,
  custom: customTheme.backgroundLighter,
});
export const ThemeBackgroundDarker = theme('mode', {
  classic: classic.backgroundDarker,
  light: light.backgroundDarker,
  dark: dark.backgroundDarker,
  custom: customTheme.backgroundDarker,
});
export const ThemeMainBackgroundColor = theme('mode', {
  classic: classic.mainBackgroundColor,
  light: light.mainBackgroundColor,
  dark: dark.mainBackgroundColor,
  custom: customTheme.mainBackgroundColor,
});
export const ThemeBodyBackgroundColor = theme('mode', {
  classic: classic.bodyBackgroundColor,
  light: light.bodyBackgroundColor,
  dark: dark.bodyBackgroundColor,
  custom: customTheme.bodyBackgroundColor,
});
export const ThemeHighlightStronger = theme('mode', {
  classic: classic.highlightStrongerColor,
  light: light.highlightStrongerColor,
  dark: dark.highlightStrongerColor,
  custom: customTheme.highlightStrongerColor,
});
export const ThemeHighlightWeaker = theme('mode', {
  classic: classic.highlightWeakerColor,
  light: light.highlightWeakerColor,
  dark: dark.highlightWeakerColor,
  custom: customTheme.highlightWeakerColor,
});

export const ThemeMainLogo = theme('mode', {
  classic: classic.knockoutLogo,
  light: light.knockoutLogo,
  dark: dark.knockoutLogo,
  custom: customTheme.knockoutLogo,
});
export const ThemeTextColor = theme('mode', {
  classic: classic.textColor,
  light: light.textColor,
  dark: dark.textColor,
  custom: customTheme.textColor,
});

// member colors
export const ThemeGoldMemberColor = theme('mode', {
  classic: classic.themeGoldMemberColor,
  light: light.themeGoldMemberColor,
  dark: dark.themeGoldMemberColor,
  custom: customTheme.themeGoldMemberColor,
});
export const ThemeGoldMemberGlow = theme('mode', {
  classic: classic.themeGoldMemberGlow,
  light: light.themeGoldMemberGlow,
  dark: dark.themeGoldMemberGlow,
  custom: customTheme.themeGoldMemberGlow,
});
export const ThemeModeratorColor = theme('mode', {
  classic: classic.themeModeratorColor,
  light: light.themeModeratorColor,
  dark: dark.themeModeratorColor,
  custom: customTheme.themeModeratorColor,
});
export const ThemeModeratorInTrainingColor = theme('mode', {
  classic: classic.themeModeratorInTrainingColor,
  light: light.themeModeratorInTrainingColor,
  dark: dark.themeModeratorInTrainingColor,
  custom: customTheme.themeModeratorInTrainingColor,
});
export const ThemeBannedUserColor = theme('mode', {
  classic: classic.themeBannedUserColor,
  light: light.themeBannedUserColor,
  dark: dark.themeBannedUserColor,
  custom: customTheme.themeBannedUserColor,
});

// typography
export const ThemeFontSizeSmall = theme('mode', {
  classic: classic.themeFontSizeSmall,
  light: light.themeFontSizeSmall,
  dark: dark.themeFontSizeSmall,
  custom: customTheme.themeFontSizeSmall,
});
export const ThemeFontSizeMedium = theme('mode', {
  classic: classic.themeFontSizeMedium,
  light: light.themeFontSizeMedium,
  dark: dark.themeFontSizeMedium,
  custom: customTheme.themeFontSizeMedium,
});
export const ThemeFontSizeLarge = theme('mode', {
  classic: classic.themeFontSizeLarge,
  light: light.themeFontSizeLarge,
  dark: dark.themeFontSizeLarge,
  custom: customTheme.themeFontSizeLarge,
});
export const ThemeFontSizeHuge = theme('mode', {
  classic: classic.themeFontSizeHuge,
  light: light.themeFontSizeHuge,
  dark: dark.themeFontSizeHuge,
  custom: customTheme.themeFontSizeHuge,
});

export const ThemeFontSizeHeadline = theme('mode', {
  classic: classic.themeFontSizeHeadline,
  light: light.themeFontSizeHeadline,
  dark: dark.themeFontSizeHeadline,
  custom: customTheme.themeFontSizeHeadline,
});

// size
export const ThemeVerticalPadding = theme('scale', {
  large: '9px',
  medium: '8px',
  small: '5px',
});
export const ThemeHorizontalPadding = theme('scale', {
  large: '9px',
  medium: '8px',
  small: '5px',
});

// width
export const ThemeBodyWidth = theme('width', {
  full: 'auto',
  verywide: '1920px',
  wide: '1440px',
  medium: '1280px',
  narrow: '960px',
});

export const ThemePostLineHeight = theme('mode', {
  classic: classic.postLineHeight,
  light: light.postLineHeight,
  dark: dark.postLineHeight,
  custom: customTheme.postLineHeight,
});
