import { setSubscriptions } from '../state/subscriptions';
import { unreadPostPage } from './postsPerPage';

const updateSubscriptions = (dispatch, subscriptions) => {
  const subscriptionState = { threads: {}, count: 0 };
  subscriptions.forEach((thread) => {
    if (thread.unreadPosts) {
      subscriptionState.threads[thread.threadId] = {
        title: thread.threadTitle,
        page: unreadPostPage(thread.unreadPosts, thread.threadPostCount),
        count: thread.unreadPosts,
        postId: thread.firstUnreadId,
        iconId: thread.icon_id,
      };
      subscriptionState.count += thread.unreadPosts;
    }
  });
  dispatch(setSubscriptions(subscriptionState));
};

export default updateSubscriptions;
