export const savePostContentsToStorage = (threadId, value) => {
  const serializedValue = JSON.stringify(value);

  window.localStorage.setItem(`thread-${threadId}-autosave`, serializedValue);
};

export const loadPostContentsFromStorage = (threadId) => {
  const loadedPost = window.localStorage.getItem(`thread-${threadId}-autosave`);

  if (!loadedPost || loadedPost === 'undefined') {
    return null;
  }

  return JSON.parse(loadedPost);
};

export const clearPostContentsFromStorage = (threadId) => {
  window.localStorage.removeItem(`thread-${threadId}-autosave`);
};
