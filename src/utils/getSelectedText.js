const getSelectedText = () => {
  if (window.getSelection) {
    return window.getSelection().toString();
  }
  if (document.selection && document.selection.type !== 'Control') {
    return document.selection.createRange().text;
  }
  return '';
};

export default getSelectedText;
