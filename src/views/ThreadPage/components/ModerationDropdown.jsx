import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { DesktopMediaQuery } from '../../../components/SharedStyles';
import useDropdownMenu, {
  DropdownMenuOpened,
} from '../../../componentsNew/Header/components/DropdownMenu';
import {
  ThemeBackgroundDarker,
  ThemeFontSizeMedium,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';

const ModerationDropdown = ({ children }) => {
  const menuRef = useRef();
  const buttonRef = useRef();
  const [open, setOpen] = useDropdownMenu(menuRef, buttonRef);

  return (
    <ModerationButton
      ref={buttonRef}
      onClick={() => setOpen((value) => !value)}
      status
      type="button"
    >
      <i className="fas fa-shield-alt" />
      <span className="button-name">Moderation</span>
      <i className="fa fa-angle-down" />
      {open && <ModerationMenu ref={menuRef}>{children}</ModerationMenu>}
    </ModerationButton>
  );
};

ModerationDropdown.propTypes = {
  children: PropTypes.node.isRequired,
};

const ModerationButton = styled.button`
  position: relative;
  display: block;
  color: ${ThemeTextColor};
  background: ${ThemeBackgroundDarker};
  margin-left: 12px;
  padding: 0;
  height: 30px;
  font-size: ${ThemeFontSizeMedium};
  border: none;
  outline: none;
  cursor: pointer;

  i {
    display: inline-block;
    width: 30px;
    line-height: 20px;
    text-align: center;
  }

  span {
    display: inline-block;
    padding: 0 calc(${ThemeVerticalPadding} / 2);
  }

  .button-name {
    display: none;
  }

  ${DesktopMediaQuery} {
    .button-name {
      display: inline-block;
    }
  }
`;

const ModerationMenu = styled(DropdownMenuOpened)`
  min-width: 170px;
`;

export default ModerationDropdown;
