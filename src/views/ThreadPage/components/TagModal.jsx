import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { getTags, updateThreadTags } from '../../../services/tags';
import { pushSmartNotification } from '../../../utils/notification';
import Modal from '../../../componentsNew/Modals/Modal';
import ModalSelect from '../../../componentsNew/Modals/ModalSelect';
import { Tag } from '../../../componentsNew/Buttons';
import { FieldLabelSmall } from '../../../componentsNew/FormControls';

const TagModal = ({ thread, isOpen, openFn }) => {
  const [availableTags, setAvailableTags] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);
  const tagsMap = useRef({});

  const defaultText = 'Select a tag...';

  useEffect(() => {
    const retrieveTags = async () => {
      const tags = await getTags();
      setAvailableTags(
        tags.map((tag) => ({
          value: tag.id,
          text: tag.name,
        }))
      );
      tags.forEach((tag) => {
        tagsMap.current[tag.id] = tag.name;
      });
    };

    if (isOpen && availableTags.length === 0) {
      retrieveTags();
    }
  }, [isOpen]);

  const handleTagAdd = (e) => {
    if (selectedTags.length === 3) {
      return;
    }
    const { value } = e.target;
    if (value !== defaultText) {
      setSelectedTags([...selectedTags, Number(value)]);
    }
  };

  const handleTagRemove = (index) => {
    const newTags = [...selectedTags];
    newTags.splice(index, 1);

    setSelectedTags(newTags);
  };

  const handleSubmit = async () => {
    const payload = {
      threadId: thread.id,
      tags: selectedTags,
    };

    try {
      await updateThreadTags(payload);
      pushSmartNotification({ message: 'Tags updated.' });
      openFn(false);
    } catch (error) {
      pushSmartNotification({ error: 'Could not update tags.' });
    }
  };

  return (
    <Modal
      iconUrl="/static/icons/tag.png"
      title="Edit tags"
      cancelFn={() => openFn(false)}
      submitFn={handleSubmit}
      isOpen={isOpen}
    >
      <ModalSelect
        defaultText={defaultText}
        options={availableTags}
        selectedOptions={selectedTags}
        onChange={handleTagAdd}
      />
      <FieldLabelSmall>Selected tags</FieldLabelSmall>
      <div className="selected-tags">
        {selectedTags.map((tag, index) => (
          <Tag
            key={tag}
            type="button"
            className="tag-item"
            data-testid="selected-tag"
            onClick={() => handleTagRemove(index)}
          >
            <i className="fas fa-times-circle" />
            &nbsp;
            {tagsMap.current[tag]}
          </Tag>
        ))}
      </div>
    </Modal>
  );
};

export default TagModal;

TagModal.propTypes = {
  thread: PropTypes.shape({
    id: PropTypes.number,
  }).isRequired,
  isOpen: PropTypes.bool.isRequired,
  openFn: PropTypes.func.isRequired,
};
