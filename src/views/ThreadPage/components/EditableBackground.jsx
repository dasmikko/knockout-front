import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useSelector, useDispatch } from 'react-redux';
import { SubHeaderDropdownListItem } from '../../../components/SubHeader/style';
import LoggedInOnly from '../../../components/LoggedInOnly';
import { updateThread } from '../../../services/threads';
import { pushNotification } from '../../../utils/notification';
import Tooltip from '../../../components/Tooltip';
import { SubHeaderButton } from '../../../componentsNew/Buttons';
import Modal from '../../../componentsNew/Modals/Modal';
import { updateBackgroundRequest } from '../../../state/background';
import { FieldLabelSmall, TextField } from '../../../componentsNew/FormControls';

const EditableBackground = ({ backgroundUrl, threadId, byCurrentUser }) => {
  const [currentBackground, setBackground] = useState('');
  const [newBackground, setNewBackground] = useState('');
  const [modalOpen, setModalOpen] = useState(false);
  const usergroup = useSelector((state) => state.user.usergroup);
  const dispatch = useDispatch();

  useEffect(() => {
    setBackground(backgroundUrl);
  }, [backgroundUrl]);

  const openModal = () => {
    setModalOpen(true);
    setNewBackground(currentBackground);
  };

  const edit = async () => {
    try {
      if (newBackground === null) {
        throw new Error({ error: 'Empty background' });
      }

      await updateThread({ backgroundUrl: newBackground, id: threadId });
      setBackground(newBackground);
      dispatch(updateBackgroundRequest(newBackground, 'cover'));
      pushNotification({ message: 'Thread background updated.' });
      setModalOpen(false);
    } catch (err) {
      console.error(err);
      pushNotification({ message: 'Error updating thread background.', type: 'error' });
    }
  };

  let button = null;

  if ((usergroup >= 3 && usergroup <= 5) || (usergroup === 2 && byCurrentUser)) {
    button = (
      <SubHeaderDropdownListItem>
        <Tooltip text="Change background">
          <SubHeaderButton onClick={openModal} title="Change background">
            <i className="fas fa-image" />
            <span>Change background</span>
          </SubHeaderButton>
        </Tooltip>
      </SubHeaderDropdownListItem>
    );
  } else if (byCurrentUser) {
    button = (
      <SubHeaderDropdownListItem style={{ opacity: 0.5 }}>
        <Tooltip text="Change background (Must be a Gold member)">
          <SubHeaderButton title="Change background (Must be a Gold member)">
            <i className="fas fa-image" />
            <span>Change background (Must be a Gold member)</span>
          </SubHeaderButton>
        </Tooltip>
      </SubHeaderDropdownListItem>
    );
  }
  return (
    <LoggedInOnly>
      {button}
      <Modal
        iconUrl="/static/icons/image-file.png"
        title="Change background"
        cancelFn={() => setModalOpen(false)}
        submitFn={edit}
        isOpen={modalOpen}
      >
        <FieldLabelSmall>Background URL</FieldLabelSmall>
        <TextField value={newBackground} onChange={(e) => setNewBackground(e.target.value)} />
        {/* TODO: Add background type selection */}
      </Modal>
    </LoggedInOnly>
  );
};

EditableBackground.propTypes = {
  backgroundUrl: PropTypes.string,
  byCurrentUser: PropTypes.bool,
  threadId: PropTypes.number,
};
EditableBackground.defaultProps = {
  backgroundUrl: '',
  byCurrentUser: false,
  threadId: 0,
};

export default EditableBackground;
