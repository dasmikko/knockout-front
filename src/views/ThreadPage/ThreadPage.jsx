/* eslint-disable react/forbid-prop-types */
import React, { useState, useRef } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ErrorBoundary from '../../components/ErrorBoundary/ErrorBoundary';
import LoggedInOnly from '../../components/LoggedInOnly';
import Post from '../../componentsNew/Post';
import { PageTitle } from '../../components/SubHeader/style';
import ThreadSubheader from './components/ThreadSubheader';
import PostEditor from '../../componentsNew/PostEditor';
import EditableTitle from './components/EditableTitle';
import { ThemeVerticalPadding, ThemeHorizontalPadding } from '../../utils/ThemeNew';
import Modal from '../../componentsNew/Modals/Modal';
import ModalSelect from '../../componentsNew/Modals/ModalSelect';
import TagModal from './components/TagModal';
import { MODERATOR_GROUPS } from '../../utils/userGroups';
import { usergroupCheck } from '../../componentsNew/UserGroupRestricted';

const ThreadPage = ({
  posts,
  thread,
  params,
  showingMoveModal,
  moveModalOptions,
  currentPage,
  togglePinned,
  toggleLocked,
  toggleDeleted,
  showMoveModal,
  deleteAlert,
  createAlert,
  currentUserId,
  submitThreadMove,
  hideModal,
  refreshPosts,
  goToPost,
  linkedPostId,
}) => {
  const [modalMove, setModalMove] = useState();
  const editorRef = useRef();
  const [tagModal, showTagModal] = useState(false);
  const showNewPostEditor = usergroupCheck(MODERATOR_GROUPS) || (!thread.locked && !thread.deleted);

  return (
    <>
      <Modal
        iconUrl="/static/icons/rearrange.png"
        title="Move thread"
        cancelFn={hideModal}
        submitFn={() => submitThreadMove(modalMove)}
        isOpen={showingMoveModal}
      >
        <ModalSelect
          defaultText="Choose a subforum..."
          options={moveModalOptions}
          onChange={(e) => setModalMove(e.target.value)}
        />
      </Modal>
      <TagModal thread={thread} isOpen={tagModal} openFn={showTagModal} />
      <StyledThreadPage>
        <Helmet defer={false}>
          <title>
            {thread.title ? `${thread.title} - Knockout!` : 'Knockout - Loading thread...'}
          </title>
        </Helmet>
        <PageTitle>
          <EditableTitle
            title={thread.title}
            byCurrentUser={currentUserId === thread.userId}
            threadId={thread.id}
            iconId={thread.iconId}
          />
        </PageTitle>
        <ThreadSubheader
          thread={thread}
          params={params}
          currentPage={currentPage}
          togglePinned={togglePinned}
          toggleDeleted={toggleDeleted}
          toggleLocked={toggleLocked}
          currentUserId={currentUserId}
          showMoveModal={showMoveModal}
          showTagModal={showTagModal}
          createAlert={createAlert}
          deleteAlert={deleteAlert}
        />
        <article className="thread-page-wrapper">
          <ul className="thread-post-list">
            {posts.map((post, index) => {
              const isUnread =
                new Date(thread.subscriptionLastSeen) < new Date(post.createdAt) ||
                new Date(thread.readThreadLastSeen) < new Date(post.createdAt);
              const subforumName = thread.subforumName || 'Subforum';
              const isLinkedPost = linkedPostId && Number(post.id) === Number(linkedPostId);
              const byCurrentUser = currentUserId === post.user.id;

              return (
                <Post
                  key={`${post.id}-${post.updatedAt}`}
                  postDepth={index}
                  totalPosts={posts.length}
                  goToPost={goToPost}
                  username={post.user.username}
                  avatarUrl={post.user.avatar_url}
                  userJoinDate={post.user.createdAt}
                  threadPage={currentPage}
                  threadId={thread.id}
                  threadLocked={thread.locked}
                  postId={post.id}
                  postBody={post.content}
                  postDate={post.createdAt}
                  postThreadPostNumber={post.threadPostNumber}
                  postPage={currentPage}
                  ratings={post.ratings}
                  user={post.user}
                  byCurrentUser={byCurrentUser}
                  bans={post.bans}
                  subforumName={subforumName}
                  isUnread={isUnread}
                  countryName={post.countryName}
                  isLinkedPost={isLinkedPost}
                  subforumId={thread.subforumId}
                  handleReplyClick={(text) => editorRef.current.appendToContent(text)}
                  postSubmitFn={refreshPosts}
                />
              );
            })}
          </ul>
          {showNewPostEditor && (
            <LoggedInOnly>
              <ErrorBoundary errorMessage="The editor has crashed. Your post was saved a few seconds ago (hopefully). Reload the page.">
                <div className="thread-new-post">
                  <PostEditor
                    threadId={parseInt(params.id, 10)}
                    ref={editorRef}
                    postSubmitFn={refreshPosts}
                  />
                </div>
              </ErrorBoundary>
            </LoggedInOnly>
          )}
        </article>
        <ThreadSubheader
          thread={thread}
          params={params}
          currentPage={currentPage}
          togglePinned={togglePinned}
          toggleDeleted={toggleDeleted}
          toggleLocked={toggleLocked}
          currentUserId={currentUserId}
          showMoveModal={showMoveModal}
          showTagModal={showTagModal}
          createAlert={createAlert}
          deleteAlert={deleteAlert}
        />
      </StyledThreadPage>
    </>
  );
};
ThreadPage.propTypes = {
  posts: PropTypes.array.isRequired,
  thread: PropTypes.object.isRequired,
  params: PropTypes.shape({
    id: PropTypes.number,
  }).isRequired,
  showingMoveModal: PropTypes.bool.isRequired,
  moveModalOptions: PropTypes.array.isRequired,
  currentPage: PropTypes.number.isRequired,
  togglePinned: PropTypes.func.isRequired,
  toggleLocked: PropTypes.func.isRequired,
  toggleDeleted: PropTypes.func.isRequired,
  showMoveModal: PropTypes.func.isRequired,
  deleteAlert: PropTypes.func.isRequired,
  createAlert: PropTypes.func.isRequired,
  currentUserId: PropTypes.number,
  submitThreadMove: PropTypes.func.isRequired,
  hideModal: PropTypes.func.isRequired,
  refreshPosts: PropTypes.func.isRequired,
  goToPost: PropTypes.func.isRequired,
  linkedPostId: PropTypes.number,
};

ThreadPage.defaultProps = {
  currentUserId: undefined,
  linkedPostId: undefined,
};

const StyledThreadPage = styled.div`
  .thread-page-wrapper {
    display: block;
    margin-top: ${ThemeVerticalPadding};
    padding-top: 0;
    padding-bottom: ${ThemeVerticalPadding};
    padding-left: ${ThemeHorizontalPadding};
    padding-right: ${ThemeHorizontalPadding};
  }

  .thread-post-list {
    padding: 0;
    margin-top: calc(${ThemeVerticalPadding} / 2);
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
  }
`;
export default ThreadPage;
