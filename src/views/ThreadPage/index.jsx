/* eslint no-console: ["error", { allow: ["error"] }] */
import React, { useEffect, useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation, useRouteMatch } from 'react-router-dom';

import { createAlertRequest, deleteAlertRequest } from '../../services/alerts';
import { createReadThreadRequest } from '../../services/readThreads';
import { getThreadWithPosts, getThread, updateThread } from '../../services/threads';
import { changeThreadStatus } from '../../services/moderation';

import { scrollToBottom, scrollIntoView } from '../../utils/pageScroll';
import { pushNotification, pushSmartNotification } from '../../utils/notification';
import { getSubforumList } from '../../services/subforums';
import { loadAutoSubscribeFromStorageBoolean } from '../../services/theme';
import { updateBackgroundRequest } from '../../state/background';
import { updateMention, getMentions } from '../../services/mentions';
import { updateMentions } from '../../state/mentions';
import {
  updateSubscriptionThread,
  removeSubscriptionThread,
  addSubscriptionThread,
} from '../../state/subscriptions';

import ThreadPageComponent from './ThreadPage';
import { POSTS_PER_PAGE } from '../../utils/postsPerPage';

const defaultDate = new Date('01/01/1980');

const updateAlert = async (
  iconId,
  title,
  posts,
  threadId,
  lastSeen,
  isSubscribed = false,
  totalPosts = 1,
  currentPage = 1,
  dispatch = undefined
) => {
  if (posts.length > 0) {
    const postDates = posts.map((post) => post.createdAt);
    const newestDate = postDates.reduce(
      (acc, val) => (new Date(acc) > new Date(val) ? new Date(acc) : new Date(val)),
      defaultDate
    );
    const result = await createAlertRequest({
      threadId,
      lastSeen: newestDate,
      previousLastSeen: lastSeen,
    });
    if (result !== null && isSubscribed) {
      const totalPages = Math.ceil(totalPosts / POSTS_PER_PAGE);
      let remainingPosts = (totalPages - currentPage) * POSTS_PER_PAGE;
      if (remainingPosts && totalPosts % POSTS_PER_PAGE) {
        remainingPosts += (totalPosts % POSTS_PER_PAGE) - POSTS_PER_PAGE;
      }
      const threadInfo = {
        id: threadId,
        count: remainingPosts,
        title,
        page: currentPage + (remainingPosts > 0 || totalPosts % POSTS_PER_PAGE === 0),
        iconId,
      };
      if (lastSeen === defaultDate && remainingPosts > 0) {
        dispatch(addSubscriptionThread(threadInfo));
      } else if (remainingPosts === 0) {
        dispatch(removeSubscriptionThread(threadId));
      } else {
        dispatch(updateSubscriptionThread(threadInfo));
      }
    }
  }
};

const updateReadThread = async (posts, threadId, lastSeen) => {
  if (posts.length > 0) {
    const postDates = posts.map((post) => post.createdAt);
    const newestDate = postDates.reduce(
      (acc, val) => (new Date(acc) > new Date(val) ? new Date(acc) : new Date(val)),
      defaultDate
    );
    await createReadThreadRequest({ threadId, lastSeen: newestDate, previousLastSeen: lastSeen });
  }
};

const createReadThread = async (posts, threadId) => {
  try {
    // if we're creating a new ReadThread, the previous last seen date can be very old
    updateReadThread(posts, threadId, defaultDate);
  } catch (error) {
    console.error(error);
  }
};

/**
 * @description handles the creation / updating of a ReadThreads entry
 * @param posts thread post array
 * @param threadId number
 * @param lastSeen optional string, if exists updates instead of creating entry
 */
const createOrUpdateReadThreads = async ({ posts, threadId, lastSeen }) => {
  if (lastSeen) {
    updateReadThread(posts, threadId, lastSeen);
  } else {
    createReadThread(posts, threadId);
  }
};

const ThreadPage = () => {
  const dispatch = useDispatch();

  const history = useHistory();
  const location = useLocation();
  const match = useRouteMatch();

  const userState = useSelector((state) => state.user);
  const mentions = useSelector((state) => state.mentions.mentions);

  const [thread, setThread] = useState({
    posts: [],
    user: {
      username: '',
      usergroup: 0,
    },
    id: undefined,
  });
  const [moveModal, setMoveModal] = useState({ show: false, options: [] });
  const [loadingPosts, setLoadingPosts] = useState(false);
  const [postJump, setPostJump] = useState({ goToLatest: undefined, goToPost: undefined });
  const quickReplyEditor = useRef();

  /**
   * @param object single object parameter, contains:
   * @param goToLatest boolean
   * @param goToPost boolean
   */
  const jumpToPost = ({ goToLatest, goToPost }) => {
    if (goToLatest) {
      scrollToBottom(1);
    } else if (goToPost) {
      // scroll to anchor in the url
      scrollIntoView(location.hash);
    }
  };

  const refreshPosts = async ({
    threadId = match.params.id,
    page = match.params.page,
    goToLatest = false,
    goToPost = false,
  } = {}) => {
    try {
      setLoadingPosts(true);
      // this will be used to decide between default param or last page
      let targetPage = page;

      // get the number of the last page
      if (goToLatest) {
        const pureThread = await getThread(threadId);
        const { totalPosts } = pureThread;
        const totalPages = Math.ceil(totalPosts / POSTS_PER_PAGE);
        history.push(`/thread/${threadId}/${totalPages}`);
        targetPage = totalPages;
      }

      // common thread updating logic
      const threadWithPosts = await getThreadWithPosts(threadId, targetPage);

      const newThread = {
        ...threadWithPosts,
        posts: [...threadWithPosts.posts].sort((a, b) => a.id - b.id),
      };

      // handle updating Alerts (subscriptions)
      if ((threadWithPosts.isSubscribedTo || goToLatest) && newThread.subscriptionLastSeen) {
        await updateAlert(
          newThread.iconId,
          newThread.title,
          newThread.posts,
          newThread.id,
          newThread.subscriptionLastSeen,
          threadWithPosts.isSubscribedTo !== undefined,
          newThread.totalPosts,
          newThread.currentPage,
          dispatch
        );
      } else if (goToLatest && loadAutoSubscribeFromStorageBoolean()) {
        await updateAlert(
          newThread.iconId,
          newThread.title,
          newThread.posts,
          newThread.id,
          defaultDate
        );
        await refreshPosts({});
      }

      // handle updating ReadThreads
      createOrUpdateReadThreads({
        posts: newThread.posts,
        threadId: newThread.id,
        lastSeen: newThread.readThreadLastSeen,
      });

      // if we have a thread background, let's set it!
      dispatch(
        updateBackgroundRequest(newThread.threadBackgroundUrl, newThread.threadBackgroundType)
      );

      // sets state with the new thread data
      setThread(newThread);
      setPostJump({ goToLatest, goToPost });
    } catch (error) {
      console.error(error);
    }
    setLoadingPosts(false);
  };

  const createAlert = async (posts, threadId, showNotification = false) => {
    try {
      // if we're creating a new Alert, the previous last seen date can be very old
      await updateAlert(
        thread.iconId,
        thread.title,
        posts,
        threadId,
        defaultDate,
        true,
        thread.totalPosts,
        thread.currentPage,
        dispatch
      );

      refreshPosts({});

      if (showNotification) {
        pushNotification({ message: 'Success! You are now subscribed.', type: 'success' });
      }
    } catch (error) {
      pushNotification({
        message: 'Oops, we could not subscribe you to this thread. Sorry.',
        type: 'warning',
      });
    }
  };

  const getEditor = (param) => {
    if (!quickReplyEditor.current) quickReplyEditor.current = param;
  };

  const deleteAlert = async (threadId) => {
    try {
      await deleteAlertRequest({ threadId });
      pushNotification({ message: 'No longer subscribed to thread.', type: 'success' });
      dispatch(removeSubscriptionThread(threadId));
      refreshPosts({});
    } catch (e) {
      console.error(e);
    }
  };

  const updateThreadStatus = async (status) => {
    try {
      const actions = {};
      if (status.locked) {
        actions.unlock = true;
      }
      if (status.locked === false) {
        actions.lock = true;
      }
      if (status.deleted) {
        actions.restore = true;
      }
      if (status.deleted === false) {
        actions.delete = true;
      }
      if (status.pinned) {
        actions.unpin = true;
      }
      if (status.pinned === false) {
        actions.pinned = true;
      }

      await changeThreadStatus({ threadId: thread.id, statuses: actions });
      await refreshPosts();
    } catch (err) {
      pushNotification({ message: 'Error!', type: 'error' });
    }
  };

  const showMoveModal = async () => {
    const subforums = await getSubforumList();
    const subforumOptions = subforums.list.map((subforum) => ({
      value: subforum.id,
      text: subforum.name,
    }));

    setMoveModal({ show: true, options: subforumOptions });
  };

  const submitThreadMove = async (data) => {
    if (!thread.id) {
      pushSmartNotification({ error: 'Invalid thread.' });
      throw new Error({ error: 'Invalid thread.' });
    }
    await updateThread({ id: thread.id, subforum_id: data });

    setMoveModal({ show: false, options: [] });
    pushNotification({ message: 'Thread moved.', type: 'success' });
  };

  const markMentionsAsRead = async () => {
    const { posts } = thread;
    if (mentions.length > 0) {
      const mentionsInPage = [];

      for (let i = 0; i < mentions.length; i += 1) {
        const mention = mentions[i];

        if (posts.find((post) => post.id === mention.postId)) {
          mentionsInPage.push(mention.postId);
        }
      }

      if (mentionsInPage.length === 0) {
        return;
      }

      await updateMention(mentionsInPage);
      const newMentions = await getMentions();
      dispatch(updateMentions(newMentions));
      pushNotification({
        message: `${mentionsInPage.length} notifications in this page marked as read.`,
      });
    }
  };

  const togglePinned = () => updateThreadStatus({ pinned: thread.pinned });

  const toggleLocked = () => updateThreadStatus({ locked: thread.locked });

  const toggleDeleted = () => updateThreadStatus({ deleted: thread.deleted });

  const deleteAlertFn = () => deleteAlert(thread.id);

  const createAlertFn = () => createAlert(thread.posts, thread.id, true);

  const hideModal = () => setMoveModal({ show: false, options: [] });

  const deferredJumpToPost = () => jumpToPost(postJump);

  useEffect(() => {
    refreshPosts({ threadId: match.params.id, page: match.params.page, goToPost: true });
    return () => {
      dispatch(updateBackgroundRequest(null));
    };
  }, [match.params.id, match.params.page]);

  useEffect(() => {
    setTimeout(() => {
      jumpToPost(postJump);
      markMentionsAsRead();
    }, 500);
  }, [postJump]);

  const { posts } = thread;
  const { hash } = location;

  const postId = hash ? Number(hash.replace('#post-', '')) : undefined;

  const { params } = match;
  const currentPage = params.page ? Number(params.page) : 1;

  return (
    <ThreadPageComponent
      posts={posts}
      thread={thread}
      showingMoveModal={moveModal.show}
      moveModalOptions={moveModal.options}
      loadingPosts={loadingPosts}
      currentPage={currentPage}
      togglePinned={togglePinned}
      toggleLocked={toggleLocked}
      toggleDeleted={toggleDeleted}
      showMoveModal={showMoveModal}
      deleteAlert={deleteAlertFn}
      createAlert={createAlertFn}
      refreshPosts={refreshPosts}
      quickReplyEditor={quickReplyEditor.current}
      currentUserId={userState.id}
      submitThreadMove={submitThreadMove}
      hideModal={hideModal}
      getEditor={getEditor}
      params={params}
      goToPost={deferredJumpToPost}
      linkedPostId={postId}
    />
  );
};

export default ThreadPage;
