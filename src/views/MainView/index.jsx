/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { connect, useSelector } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Slide, ToastContainer } from 'react-toastify';
import Footer from '../../componentsNew/Footer';
import Header from '../../componentsNew/Header';
import routes from '../../routes';
import ScrollOnRouteChange from './components/ScrollOnRouteChange';
import { GlobalStyle, Background, FlexWrapper } from './style';
import PassiveAggressiveness from './components/PassiveAggressiveness';

export const ConnectedGlobalStyle = connect(({ background }) => ({
  backgroundUrl: background.url,
}))(GlobalStyle);

export const ConnectedBackground = connect(({ background }) => ({
  url: background.url,
  bgType: background.type,
}))(Background);

const getPunchyLabsNoticeNotRead = () => {
  return localStorage.getItem('punchyLabsNoticeRead') !== 'true';
};

const MainView = () => {
  const hasMotd = useSelector((state) => state.style.motd);
  return (
    <FlexWrapper hasMotd={hasMotd}>
      <ConnectedGlobalStyle />

      <Header />

      <ScrollOnRouteChange>
        <Switch>
          {routes.map((route) => (
            <Route key={route.name} {...route} />
          ))}
          <Redirect from="*" to="/" />
        </Switch>
      </ScrollOnRouteChange>

      <Footer />

      <ToastContainer transition={Slide} />

      <ConnectedBackground />

      {getPunchyLabsNoticeNotRead() && <PassiveAggressiveness />}
    </FlexWrapper>
  );
};
export default MainView;
