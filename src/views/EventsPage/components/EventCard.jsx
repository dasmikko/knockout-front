import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { Link } from 'react-router-dom';
import { ThemeBackgroundLighter, ThemeVerticalPadding } from '../../../utils/ThemeNew';

dayjs.extend(relativeTime);

const EventCard = ({ content, createdAt, index }) => {
  const readableCreatedAt = dayjs(createdAt).from();
  let parsedContent;

  if (content) {
    try {
      parsedContent = JSON.parse(content).content;
    } catch (e) {
      return (
        <StyledEventCard>
          <div className="icon">
            <span>:(</span>
          </div>
          <div className="content">
            <p className="desc">Somthing Went Wrong</p>
            <p className="time">Uhh ohhh</p>
          </div>
        </StyledEventCard>
      );
    }
  }

  const Icon = () => {
    const emojiRegex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
    const icon = parsedContent[0].match(emojiRegex);
    if (icon === null) {
      return <div className="icon" />;
    }
    return (
      <div className="icon">
        <span>{icon[0]}</span>
      </div>
    );
  };

  const Desc = () => {
    const desc = parsedContent[0].slice(2);
    return <span>{desc}</span>;
  };

  const Extras = () => {
    return parsedContent.map((el, i) => {
      if (i < 1) {
        return null;
      }
      if (typeof el === 'object') {
        if (el.text) {
          return (
            <Link key={el.text} to={el.link} className="event-link">
              {el.text}
            </Link>
          );
        }
        return <span className="unknown">Unknown Thread</span>;
      }
      if (typeof el === 'string') {
        return <span key={el}>{el}</span>;
      }
      return null;
    });
  };

  return (
    <StyledEventCard index={index}>
      {Icon()}
      <div className="content">
        <p className="desc">
          {Desc()}
          {Extras()}
        </p>
        <p className="time">
          {readableCreatedAt !== 'NaN years ago' ? readableCreatedAt : 'Unknown Date'}
        </p>
      </div>
    </StyledEventCard>
  );
};

EventCard.propTypes = {
  createdAt: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
};

export const StyledEventCard = styled.li`
  @keyframes example {
    from {
      opacity: 0;
      transform: translate(200px, 0);
    }
    to {
      opacity: 1;
      transform: translate(0, 0);
    }
  }
  background-color: ${ThemeBackgroundLighter};
  display: flex;
  min-height: 56px;
  margin-bottom: ${ThemeVerticalPadding};
  padding: 12px 10px 28px 56px;
  box-sizing: border-box;
  position: relative;
  opacity: 0;
  animation-delay: ${(props) => `${props.index * 50}ms`};
  animation-name: example;
  animation-duration: 200ms;
  animation-fill-mode: forwards;
  .icon {
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: 0 auto;
    position: absolute;
    width: 56px;
    text-align: center;
    font-size: 24px;
    top: 0;
    left: 0;
    bottom: 0;
  }
  .content {
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    .desc,
    .time {
      margin: 0;
      font-weight: 300;
    }
    .desc {
      line-height: 25px;
    }
    .time {
      position: absolute;
      bottom: -15px;
      font-size: 12px;
      font-style: italic;
      opacity: 0.4;
    }
    a {
      color: #2b8fff;
      transition: color 100ms ease-in-out;
      &:hover {
        color: #66aeff;
      }
    }
  }
  @media (max-width: 700px) {
    .content .desc {
      font-size: 14px;
      line-height: 20px;
    }
  }
`;

export default EventCard;
