import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { transparentize } from 'polished';

import {
  ThemeHorizontalPadding,
  ThemeFontSizeMedium,
  ThemeKnockoutRed,
  ThemeTextColor,
  ThemeBackgroundLighter,
  ThemeHighlightStronger,
} from '../../../utils/ThemeNew';
import { Button } from '../../../componentsNew/Buttons';

const UsernameForm = ({ submitUsername }) => {
  let jiggleTimer;
  const [inputValue, setInputValue] = useState('');
  const [submitEnabled, setSubmitEnabled] = useState(false);
  const [error, setError] = useState('');
  const [warning, setWarning] = useState('');
  const [shouldJiggle, setJiggle] = useState(false);

  useEffect(() => {
    return () => {
      clearTimeout(jiggleTimer);
    };
  }, [jiggleTimer]);

  const isNameCorrectLength = (username) => {
    if (!username || username.length < 3 || username.length > 19) {
      return false;
    }
    return true;
  };

  const jiggle = (warningText) => {
    setWarning(warningText);
    setJiggle(true);
    clearTimeout(jiggleTimer);
    jiggleTimer = setTimeout(() => {
      setJiggle(false);
    }, 100);
  };

  const isInputValid = (username) => {
    if (username === '') {
      jiggle('Usernames can not start with a space.');
      setInputValue('');
      return false;
    }

    if (username.length > 19) {
      jiggle('Usernames can be at max 19 characters.');
      setInputValue(username.substring(0, 19));
      return false;
    }

    return true;
  };

  const onInputChanged = (e) => {
    const username = e.target.value.replace(/^\s+/g, '');

    if (!isInputValid(username)) {
      setSubmitEnabled(false);
      return;
    }

    setInputValue(username);

    if (isNameCorrectLength(username.trim())) {
      setSubmitEnabled(true);
    } else {
      setSubmitEnabled(false);
    }

    setWarning('');
    setError('');
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    const username = inputValue.trim();
    setInputValue(username);

    if (!isNameCorrectLength(username)) {
      jiggle('Username must be at least 3 characters');
      return;
    }

    const usernameStatus = await submitUsername(username);

    if (!usernameStatus) {
      setError('This username is already taken (or the username is invalid)');
    }
  };

  return (
    <>
      <h1 className="underline">Choose Your Username</h1>
      <StyledUsernameForm autoComplete="off" onSubmit={onSubmit}>
        <div className={`input-wrapper ${error ? 'error' : ''} ${warning ? 'warning' : ''}`}>
          {(error || warning) && <p className="error-text">{error || warning}</p>}
          <input
            id="username"
            name="username"
            placeholder="username"
            required
            pattern=".*\S.*"
            onChange={onInputChanged}
            value={inputValue}
            minLength="3"
            className={`${shouldJiggle ? 'jiggle' : ''}`}
          />
          <label htmlFor="username" className={inputValue.length ? 'translated' : ''}>
            Username
          </label>
          <div className="color-bar" />
          <Button type="submit" disabled={!submitEnabled}>
            Submit
          </Button>
        </div>
      </StyledUsernameForm>
    </>
  );
};

UsernameForm.propTypes = {
  submitUsername: PropTypes.func.isRequired,
};

const InputHeight = '50px';
const ExtraPadding = ThemeHorizontalPadding;

const StyledUsernameForm = styled.form`
  @keyframes maxCharJiggle {
    0% {
      transform: translate(2px, 0px);
    }
    25% {
      transform: translate(0px, -2px);
    }
    50% {
      transform: translate(-1px, 0px);
    }
    75% {
      transform: translate(0px, 2px);
    }
    100% {
      transform: translate(1px, 0px);
    }
  }

  display: flex;
  justify-content: center;
  margin: 80px 0;
  .input-wrapper {
    display: flex;
    flex-grow: 1;
    max-width: 500px;
    position: relative;
    font-size: ${ThemeFontSizeMedium};
    letter-spacing: 0.05em;
    height: calc(${InputHeight} + ${ExtraPadding});
    background-color: ${ThemeBackgroundLighter};
    vertical-align: top;
    input {
      /* margin-top: ${ExtraPadding}; */
      background-color: transparent;
      border: 1px solid rgba(0,0,0,.2);
      border-bottom: none;
      border-right:none;
      flex-grow: 1;
      color: ${ThemeTextColor};
      letter-spacing: 0.06em;
      padding: 0 calc(${ThemeHorizontalPadding} * 2);
      padding-top: ${ExtraPadding};
      height: ${InputHeight};
      line-height: ${InputHeight};
      vertical-align: bottom;
      &::placeholder {
        opacity: 0;
      }
      &:focus {
        outline: none;
      }
      &.jiggle {
        animation: maxCharJiggle 100ms linear 1;
      }
    }
    label {
      position: absolute;
      line-height: calc(${InputHeight} + ${ExtraPadding});
      left: calc(${ThemeHorizontalPadding} * 2);
      font-size: 1em;
      top: 0;
      opacity: 0.7;
      transition: bottom 100ms, font-size 100ms, opacity 100ms, line-height 100ms;
      user-select: none;
      pointer-events: none;
    }
    .error-text {
      position: absolute;
      font-size: 0.8em;
      top: 100%;
      color: ${ThemeKnockoutRed};
      letter-spacing: 0.05em;
      margin: 0;
      margin-top: ${ExtraPadding};
      &:before {
        content: '*';
        font-size: 0.8em;
        margin-right: 5px;
      }
    }
    input:valid + label,
    input:focus + label,
    label.translated {
      font-size: 0.85em;
      opacity: 0.5;
      line-height: 2em;
    }
    .color-bar {
      content: ' ';
      position: absolute;
      background-color: ${transparentize(0.4, 'gray')};
      height: 2px;
      width: 100%;
      bottom: 0;
      left: 0;
      right: 0;
      transition: height 200ms ease-in-out, background-color 100ms ease-in-out;
    }
    input:valid ~ .color-bar {
      background-color: green;
      height: 2px;
    }
    input:focus-within {
      &:after {
        height: 2px;
      }
    }
    &.warning {
      .color-bar {
        background-color: ${ThemeHighlightStronger} !important;
      }
      .error-text {
        color: ${ThemeHighlightStronger};
      }
    }
    &.error {
      .color-bar {
        background-color: ${ThemeKnockoutRed} !important;
      }
      .error-text {
        color: ${ThemeKnockoutRed};
      }
    }
  }
`;

export default UsernameForm;
