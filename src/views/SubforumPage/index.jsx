import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { getSubforumWithThreads } from '../../services/subforums';
import SubforumThreadItem from './components/SubforumThreadItem';
import BlankSlate from '../../componentsNew/BlankSlate';
import Subheader from '../../componentsNew/Subheader';
import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeFontSizeHuge,
  ThemeFontSizeSmall,
} from '../../utils/ThemeNew';
import { filterNsfwThreads } from '../../utils/filterNsfwThreads';
import { loadDisplayNsfwFilterSettingFromStorageBoolean } from '../../utils/postOptionsStorage';
import Pagination from '../../componentsNew/Pagination';

const Subforum = (props) => {
  const {
    match: {
      params: { id: subforumId, page = 1 },
    },
  } = props;

  const [subforum, setSubforum] = useState({ threads: [] });

  useEffect(() => {
    const getSubforumThreads = async (id, pageNum) => {
      const subforumResult = await getSubforumWithThreads(id, pageNum);
      const nsfwFilterEnabled = loadDisplayNsfwFilterSettingFromStorageBoolean();

      if (nsfwFilterEnabled) {
        setSubforum({ ...subforumResult, threads: filterNsfwThreads(subforumResult.threads) });
      } else {
        setSubforum(subforumResult);
      }
    };

    if (subforumId) {
      getSubforumThreads(subforumId, page);
    }
  }, [subforumId, page]);

  const { id, threads, currentPage, totalThreads, name } = subforum;

  let subforumContent;
  if (totalThreads === 0) {
    subforumContent = <BlankSlate resourceNamePlural="threads" />;
  } else {
    subforumContent = threads.map((thread) => {
      return (
        <SubforumThreadItem
          key={thread.id}
          id={thread.id}
          createdAt={thread.createdAt}
          deleted={thread.deleted}
          iconId={thread.iconId}
          lastPost={thread.lastPost}
          locked={thread.locked}
          pinned={thread.pinned}
          postCount={thread.postCount}
          read={thread.read}
          readThreadUnreadPosts={thread.readThreadUnreadPosts}
          title={thread.title}
          unreadPostCount={thread.unreadPostCount}
          user={thread.user}
          firstPostTopRating={thread.firstPostTopRating}
          firstUnreadId={thread.firstUnreadId}
          tags={thread.tags}
          backgroundUrl={thread.backgroundUrl}
          backgroundType={thread.backgroundType}
          subforumName={name}
        />
      );
    });
  }

  return (
    <StyledSubforumPage>
      <h2>{name}</h2>

      <Helmet>
        <title>{name}</title>
      </Helmet>

      <Subheader
        returnToUrl="/"
        returnToText="Home"
        totalPaginationItems={totalThreads}
        currentPage={currentPage}
        paginationPath={`/subforum/${id}/`}
        newThreadPath={`/thread/new/${id}`}
        pageSize={40}
      />

      {subforumContent}
      <div className="pagination-and-buttons">
        <div className="pagination">
          <Pagination
            showNext
            pagePath={`/subforum/${id}/`}
            totalPosts={totalThreads}
            currentPage={currentPage}
            pageSize={40}
          />
        </div>
      </div>
    </StyledSubforumPage>
  );
};

Subforum.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({ id: PropTypes.string.isRequired, page: PropTypes.string }).isRequired,
  }).isRequired,
};

export default Subforum;

const StyledSubforumPage = styled.section`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  h2 {
    margin: 0 0 ${ThemeVerticalPadding} 0;
    font-size: ${ThemeFontSizeHuge};
  }

  .pagination-and-buttons {
    display: flex;
    flex-grow: 1;
    font-size: ${ThemeFontSizeSmall};

    .pagination {
      display: flex;
      flex-grow: 1;
      justify-content: flex-end;
    }
  }
`;
