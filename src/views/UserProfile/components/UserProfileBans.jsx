/* eslint-disable react/forbid-prop-types */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import dayjs from 'dayjs';

import { makeBanInvalid } from '../../../services/moderation';
import { pushSmartNotification } from '../../../utils/notification';
import { usergroupCheck } from '../../../components/UserGroupRestricted';
import BanItem from './BanItem';
import { MODERATOR_GROUPS } from '../../../utils/userGroups';
import Modal from '../../../componentsNew/Modals/Modal';

const durationInDays = (a, b) => {
  const date1 = new Date(a);
  const date2 = new Date(b);
  const days = parseInt((date2 - date1) / (1000 * 60 * 60 * 24), 10);

  return `${days} days`;
};

const durationInHours = (a, b) => {
  const date1 = new Date(a);
  const date2 = new Date(b);

  const hours = parseInt((date2 - date1) / (1000 * 60 * 60), 10);

  if (hours > 24) {
    return durationInDays(a, b);
  }

  return `${hours} hours`;
};

const UserProfileBans = ({ user, bans }) => {
  const isMod = usergroupCheck(MODERATOR_GROUPS);

  const [banId, setBanId] = useState(undefined);
  const [username, setUsername] = useState('');
  const [modalOpen, setModalOpen] = useState(false);

  const setBanInvalid = async ({ newBanId, newUsername, banStillValid }) => {
    if (!banStillValid) {
      pushSmartNotification({
        error: 'This ban doesnt seem to be active. Let the devs know and they will try to help.',
      });
      return;
    }

    setBanId(newBanId);
    setUsername(newUsername);
    setModalOpen(true);
  };

  const invalidateBan = async () => {
    try {
      const banResponse = await makeBanInvalid(banId, user.id);
      pushSmartNotification(banResponse);
    } catch (error) {
      pushSmartNotification({ error: 'Unable to invalidate ban.' });
    }
    setModalOpen(false);
  };

  return (
    <BansContainer>
      {bans.map((ban) => {
        const { id, banReason, thread, createdAt, expiresAt, bannedBy, post } = ban;
        const banStillValid = dayjs(expiresAt).isAfter(dayjs());

        return (
          <BanItem
            key={id}
            banReason={banReason}
            createdAt={createdAt}
            expiresAt={expiresAt}
            duration={durationInHours(createdAt, expiresAt)}
            bannedBy={bannedBy}
            thread={thread}
            post={post}
            banStillValid={banStillValid}
            isMod={isMod}
            invalidateBan={() => {
              setBanInvalid({ newBanId: id, newUsername: user.username, banStillValid });
            }}
          />
        );
      })}
      <Modal
        title={`Invalidate ${username}'s ban?`}
        cancelFn={() => setModalOpen(false)}
        submitFn={invalidateBan}
        isOpen={modalOpen}
      />
    </BansContainer>
  );
};

UserProfileBans.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
  }).isRequired,
  bans: PropTypes.array.isRequired,
};

export default UserProfileBans;

const BansContainer = styled.div`
  @media (max-width: 960px) {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
    justify-content: space-evenly;
  }
`;
