import React from 'react';
import PropTypes from 'prop-types';

import Tooltip from '../../../components/Tooltip';

const UserProfileHeading = ({ headingText, personalSite }) => (
  <div
    style={{
      marginBottom: '15px',
    }}
  >
    {headingText && <h1>{headingText}</h1>}
    {personalSite && (
      <div style={{ display: 'inline-block', color: '#3facff' }}>
        <Tooltip text="External link - will take you out of Knockout!">
          <i className="fas fa-link">&nbsp; </i>
          <a href={personalSite} rel="noopener ugc" target="_blank">
            {personalSite}
          </a>
        </Tooltip>
      </div>
    )}
  </div>
);

UserProfileHeading.propTypes = {
  headingText: PropTypes.string,
  personalSite: PropTypes.string,
};
UserProfileHeading.defaultProps = {
  headingText: null,
  personalSite: null,
};
export default UserProfileHeading;
