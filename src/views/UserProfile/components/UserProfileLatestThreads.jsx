/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import Pagination from '../../../componentsNew/Pagination';
import ThreadItem from '../../../componentsNew/ThreadItem';

const UserProfileLatestThreads = ({ threads, user, loading, pageChangeFn }) => {
  if (loading && threads.threads.length === 0) {
    return (
      <div>
        <i className="fas fa-truck-loading" />
        &nbsp;Loading...
      </div>
    );
  }

  return (
    <div>
      <Pagination
        pageChangeFn={pageChangeFn}
        totalPosts={threads.totalThreads}
        currentPage={threads.currentPage}
        pageSize={40}
        marginBottom
        useButtons
      />
      {threads.threads.map((thread) => (
        <ThreadItem
          key={thread.id}
          id={thread.id}
          createdAt={thread.createdAt}
          deleted={thread.deleted}
          iconId={thread.iconId}
          locked={thread.locked}
          pinned={thread.pinned}
          postCount={thread.postCount}
          title={thread.title}
          user={user}
          tags={thread.tags}
          backgroundUrl={thread.backgroundUrl}
          backgroundType={thread.backgroundType}
          subforumName={thread.subforumName}
          minimal
        />
      ))}
      <Pagination
        pageChangeFn={pageChangeFn}
        totalPosts={threads.totalThreads}
        currentPage={threads.currentPage}
        pageSize={40}
        useButtons
      />
    </div>
  );
};

UserProfileLatestThreads.propTypes = {
  threads: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  pageChangeFn: PropTypes.func.isRequired,
};

export default UserProfileLatestThreads;
