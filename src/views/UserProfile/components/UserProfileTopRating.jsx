import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import ratingList from '../../../components/Rating/ratingList.json';
import Tooltip from '../../../components/Tooltip';

const getTopRating = (ratingsArray) =>
  ratingsArray.reduce((acc, val) => {
    if (!acc.name) {
      return val;
    }

    if (val.count > acc.count) {
      return val;
    }

    return acc;
  }, {});

const UserProfileTopRating = ({ topRatings }) => {
  const [ratingInfo, setRatingInfo] = useState(undefined);

  const findTopRating = () => {
    const topRating = getTopRating(topRatings);
    setRatingInfo(ratingList[topRating.name]);
  };

  useEffect(() => {
    findTopRating();
  }, [topRatings]);

  if (!ratingInfo) {
    return null;
  }

  return (
    <UserProfileTopRatingWrapper>
      <Tooltip text={ratingInfo.name} top={false}>
        <img
          src={ratingInfo.url}
          alt={ratingInfo.name}
          data-testid="top-rating"
          style={{
            filter: 'drop-shadow(1px 1px 3px black)',
          }}
        />
      </Tooltip>
    </UserProfileTopRatingWrapper>
  );
};

export default UserProfileTopRating;

UserProfileTopRating.propTypes = {
  topRatings: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export const UserProfileTopRatingWrapper = styled.div`
  margin: 0 0 0 5px;
  @media (min-width: 900px) {
    margin: 15px 0 0 0;
  }
  z-index: 1;
  transition: opacity ease-in-out 500ms;
`;
