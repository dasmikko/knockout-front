import React from 'react';
import PropTypes from 'prop-types';

import Post from '../../../componentsNew/Post';
import Pagination from '../../../componentsNew/Pagination';

const UserProfilePosts = ({ posts, loading, pageChangeFn, showRatings }) => {
  if (loading && posts.posts.length === 0) {
    return (
      <div>
        <i className="fas fa-truck-loading" />
        &nbsp;Loading...
      </div>
    );
  }

  return (
    <div>
      <Pagination
        pageChangeFn={pageChangeFn}
        totalPosts={posts.totalPosts}
        currentPage={posts.currentPage}
        pageSize={40}
        useButtons
        marginBottom
      />
      {posts.posts.map((post) => (
        <Post
          key={post.id}
          hideUserWrapper
          hideControls
          byCurrentUser
          ratings={showRatings ? post.ratings : []}
          threadId={post.thread.id || post.thread}
          threadInfo={post.thread.id && post.thread}
          postId={post.id}
          postBody={post.content}
          postDate={post.createdAt}
          postPage={post.page}
          threadPage={1}
          profileView
        />
      ))}
      <Pagination
        pageChangeFn={pageChangeFn}
        totalPosts={posts.totalPosts}
        currentPage={posts.currentPage}
        pageSize={40}
        useButtons
      />
    </div>
  );
};

UserProfilePosts.propTypes = {
  posts: PropTypes.shape({
    posts: PropTypes.arrayOf(
      PropTypes.shape({
        content: PropTypes.string.isRequired,
        createdAt: PropTypes.string.isRequired,
        id: PropTypes.number.isRequired,
        page: PropTypes.number,
        thread: PropTypes.shape({
          id: PropTypes.number.isRequired,
          title: PropTypes.string.isRequired,
        }),
      })
    ),
    totalPosts: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
  }).isRequired,
  loading: PropTypes.bool.isRequired,
  pageChangeFn: PropTypes.func.isRequired,
  showRatings: PropTypes.bool.isRequired,
};

export default UserProfilePosts;
