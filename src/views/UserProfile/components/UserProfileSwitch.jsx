/* eslint-disable react/forbid-prop-types */
import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import UserProfilePosts from './UserProfilePosts';
import { getUserPosts, getUserThreads } from '../../../services/user';
import { usergroupCheck } from '../../../components/UserGroupRestricted';
import { addGoldProduct } from '../../../services/moderation';
import UserProfileLatestThreads from './UserProfileLatestThreads';
import UserProfileRatings from './UserProfileRatings';
import Tabs from '../../../componentsNew/Tabs';
import { ThemeVerticalPadding } from '../../../utils/ThemeNew';
import UserProfileBans from './UserProfileBans';

const UserProfileSwitch = ({ user, userId, bans, ratings }) => {
  const [threads, setThreads] = useState({ threads: [], currentPage: 0 });
  const [posts, setPosts] = useState({ posts: [], currentPage: 0 });
  const [currentTab, setCurrentTab] = useState(0);
  const [loading, setLoading] = useState(false);

  const fetchData = async (getData, setData, page = 1) => {
    setLoading(true);
    setData(await getData(userId, page));
    setLoading(false);
  };

  useEffect(() => {
    switch (currentTab) {
      case 0:
        fetchData(getUserPosts, setPosts);
        break;
      case 1:
        fetchData(getUserThreads, setThreads);
        break;
      case 2:
      default:
        break;
    }
  }, [currentTab]);

  const tabMap = {
    0: (
      <UserProfilePosts
        posts={posts}
        user={user}
        loading={loading}
        showRatings={ratings.length > 0}
        pageChangeFn={(page) => fetchData(getUserPosts, setPosts, page)}
      />
    ),
    1: (
      <UserProfileLatestThreads
        threads={threads}
        user={user}
        loading={loading}
        pageChangeFn={(page) => fetchData(getUserThreads, setThreads, page)}
      />
    ),
  };

  const renderTab = () => {
    if (currentTab in tabMap) {
      return tabMap[currentTab];
    }
    return usergroupCheck([4]) ? (
      <button type="button" onClick={() => addGoldProduct({ userId })}>
        Add gold for a month
      </button>
    ) : null;
  };

  const { posts: postCount, threads: threadCount } = user;

  const tabs = [
    `Posts ${postCount ? `(${postCount})` : ''}`,
    `Threads ${threadCount ? `(${threadCount})` : ''} `,
  ];

  if (bans && bans.length > 0) {
    tabMap[tabs.length] = <UserProfileBans bans={bans} user={user} />;
    tabs.push(`Bans (${bans.length})`);
  }

  if (ratings && ratings.length > 0) {
    tabMap[tabs.length] = <UserProfileRatings ratings={ratings} />;
    tabs.push(`Ratings`);
  }

  return (
    <div>
      <UserProfileTabContainer>
        <Tabs currentTab={currentTab} tabs={tabs} setTab={setCurrentTab} />
      </UserProfileTabContainer>
      {renderTab()}
    </div>
  );
};

export default UserProfileSwitch;

UserProfileSwitch.propTypes = {
  userId: PropTypes.number.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    posts: PropTypes.number.isRequired,
    threads: PropTypes.number.isRequired,
  }).isRequired,
  bans: PropTypes.array.isRequired,
  posts: PropTypes.string,
  threads: PropTypes.string,
  ratings: PropTypes.array.isRequired,
};

UserProfileSwitch.defaultProps = {
  posts: undefined,
  threads: undefined,
};

export const UserProfileTabContainer = styled.div`
  width: 100;
  border-radius: 5px;
  overflow: hidden;
  display: flex;
  margin-bottom: ${ThemeVerticalPadding};
`;
