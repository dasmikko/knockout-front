import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import RuleCard from './RuleCard';

import { defaultRules, newsRules, politicsRules } from '../../../utils/rules';

const Rules = ({ interactable }) => {
  const selectedId = new URLSearchParams(useLocation().search).get('id');

  return (
    <StyledRules>
      <Helmet>
        <title>Rules - Knockout!</title>
      </Helmet>
      <h1 className="underline">Rules</h1>
      <p className="detail">
        There are times where moderators will use their discretion and common sense to maintain a
        neutral, unbiased and fair stance. Think we’re not doing our job? Problem with a ban? You
        can contact us on the Discord. Remember. Bans are warnings. You are more than welcome to get
        an explanation if you don’t understand the warning.
      </p>
      <ol className="rules-list">
        {defaultRules.map((rule, i) => {
          const identifier = `d-${i + 1}`;
          const selected = identifier === selectedId;
          return (
            <RuleCard
              title={rule.title}
              key={identifier}
              identifier={identifier}
              selected={selected}
              interactable={interactable}
            >
              {rule.content}
            </RuleCard>
          );
        })}
      </ol>
      <h2 className="underline">News Rules</h2>
      <p className="detail">These rules apply only to the News & Politics subforums.</p>
      <ol className="rules-list">
        {newsRules.map((rule, i) => {
          const identifier = `n-${i + 1}`;
          const selected = identifier === selectedId;
          return (
            <RuleCard
              title={rule.title}
              key={identifier}
              identifier={identifier}
              selected={selected}
              interactable={interactable}
            >
              {rule.content}
            </RuleCard>
          );
        })}
      </ol>
      <h2 className="underline">Politics Rules</h2>
      <p className="detail">
        In addition to the above rules, the following apply to the Politics subforum.
      </p>
      <ol className="rules-list">
        {politicsRules.map((rule, i) => {
          const identifier = `p-${i + 1}`;
          const selected = identifier === selectedId;
          return (
            <RuleCard
              title={rule.title}
              key={identifier}
              identifier={identifier}
              selected={selected}
              interactable={interactable}
            >
              {rule.content}
            </RuleCard>
          );
        })}
      </ol>
    </StyledRules>
  );
};

Rules.propTypes = {
  interactable: PropTypes.bool,
};

Rules.defaultProps = {
  interactable: true,
};

const StyledRules = styled.div`
  .rules-list {
    box-sizing: border-box;
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    width: 100%;
    padding: 0;
    list-style: none;
  }
`;
export default Rules;
