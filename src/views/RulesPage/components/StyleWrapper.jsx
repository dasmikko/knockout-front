import styled from 'styled-components';

import {
  ThemeVerticalPadding,
  ThemeFontSizeMedium,
  ThemeFontSizeHuge,
  ThemeHighlightWeaker,
  ThemeTextColor,
} from '../../../utils/ThemeNew';

const StyleWrapper = styled.div`
  h1,
  h2,
  h3 {
    letter-spacing: 0.05em;
    color: ${ThemeTextColor};
  }
  h1 {
    font-size: calc(${ThemeFontSizeHuge} * 2);
    margin-top: 0;
  }
  h2 {
    font-size: calc(${ThemeFontSizeHuge} * 1.7);
    font-weight: 400;
    margin-top: 0;
  }
  h3 {
    margin-top: calc(${ThemeVerticalPadding} * 3);
    padding-bottom: ${ThemeVerticalPadding};
  }
  p {
    font-size: ${ThemeFontSizeMedium};
    line-height: 1.5em;
    filter: brightness(80%);
    color: ${ThemeTextColor};
  }
  section {
    padding: calc(${ThemeVerticalPadding} * 3) 0;
  }

  .underline,
  .underline-small {
    position: relative;
    &::after {
      content: ' ';
      position: absolute;
      height: 2px;
      background-color: ${ThemeHighlightWeaker};
      width: 100%;
      left: 0;
      bottom: -0.2em;
    }
  }
  .underline-small {
    display: inline-block;
  }
`;

export default StyleWrapper;
