import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import UserRoleWrapper from '../../../componentsNew/UserRoleWrapper';
import {
  ThemeBackgroundLighter,
  ThemeBannedUserColor,
  ThemeFontSizeLarge,
  ThemeHorizontalPadding,
} from '../../../utils/ThemeNew';
import { MobileMediaQuery } from '../../../components/SharedStyles';

const ReportSummary = ({ report }) => (
  <StyledReportSummary
    to={{ pathname: `/moderate/reports/${report.id}`, state: { report } }}
    key={report.id}
    $closed={report.solvedBy !== null}
  >
    <i className="fas fa-flag report-icon" />
    <span className="report-reason">{`"${report.reason}"`}</span>
    <span className="report-text">&nbsp;-&nbsp;</span>
    <div className="report-details">
      <UserRoleWrapper user={report.reportedBy}>
        <span className="report-user">
          {report.reportedBy.username}
          &nbsp;
        </span>
      </UserRoleWrapper>
      <span className="report-text">reported&nbsp;</span>
      <UserRoleWrapper user={report.post.user}>
        <span className="report-user">{report.post.user.username}</span>
      </UserRoleWrapper>
      <span className="report-text">&nbsp;in</span>
    </div>
    <div className="report-thread">
      &nbsp;
      {report.post.thread.title}
    </div>
  </StyledReportSummary>
);

ReportSummary.propTypes = {
  report: PropTypes.shape({
    id: PropTypes.number,
    reason: PropTypes.string,
    reportedBy: PropTypes.shape({
      username: PropTypes.string,
    }),
    post: PropTypes.shape({
      user: PropTypes.shape({
        username: PropTypes.string,
      }),
      thread: PropTypes.shape({
        title: PropTypes.string,
      }),
    }),
    solvedBy: PropTypes.object,
  }).isRequired,
};

const StyledReportSummary = styled(Link)`
  background: ${ThemeBackgroundLighter};
  padding: calc(${ThemeHorizontalPadding} * 2);
  display: flex;
  white-space: nowrap;
  line-height: normal;
  align-items: center;
  margin-bottom: 10px;
  ${(props) => props.$closed && 'opacity: 0.6;'}

  ${MobileMediaQuery} {
    .report-thread,
    .report-user,
    .report-text {
      display: none;
    }
  }
  .report-icon {
    color: ${ThemeBannedUserColor};
    margin-right: 10px;
  }

  .report-details {
    ${MobileMediaQuery} {
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }

  .report-reason {
    font-size: ${ThemeFontSizeLarge};
    font-weight: bold;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .report-thread {
    font-weight: bold;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .report-user {
    font-weight: bold;
  }
`;

export default ReportSummary;
