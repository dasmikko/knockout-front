import React, { useState } from 'react';
import {
  Panel,
  PanelHeader,
  PanelSearchField,
  PanelSearchButton,
  PanelBody,
  PanelBodyPlaceholder,
} from './style';
import { getIpsByUsername } from '../../../services/moderation';

const IpByUsernamePanel = () => {
  const [username, setUsername] = useState('');
  const [results, setResults] = useState([]);
  const [searched, setSearched] = useState(false);

  const search = async () => {
    const result = await getIpsByUsername(username);
    setResults(result);
    setSearched(true);
  };

  return (
    <Panel>
      <PanelHeader>
        <PanelSearchField
          type="text"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          onKeyDown={(e) => e.key === 'Enter' && search()}
          placeholder="Search IP by user"
        />
        <PanelSearchButton onClick={search}>Search</PanelSearchButton>
      </PanelHeader>
      {searched && (
        <PanelBody>
          <table>
            <thead>
              <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Address</th>
              </tr>
            </thead>
            <tbody>
              {results.map((item) => (
                <tr key={item}>
                  <td>{item.id}</td>
                  <td>{item.username}</td>
                  <td>
                    {item.ipAddresses.map((ip) => (
                      <span key={ip}>{ip}</span>
                    ))}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </PanelBody>
      )}
      {!searched && (
        <PanelBody>
          <PanelBodyPlaceholder>Please enter a search term above</PanelBodyPlaceholder>
        </PanelBody>
      )}
    </Panel>
  );
};

export default IpByUsernamePanel;
