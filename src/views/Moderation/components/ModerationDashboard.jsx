import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Link, useRouteMatch } from 'react-router-dom';
import { transparentize } from 'polished';
import { MobileMediaQuery } from '../../../components/SharedStyles';
import {
  ThemeFontSizeLarge,
  ThemeFontSizeMedium,
  ThemeHorizontalPadding,
  ThemeTextColor,
} from '../../../utils/ThemeNew';
import { getDashboardData } from '../../../services/moderation';
import { getOpenReports } from '../../../services/reports';
import ReportSummary from './ReportSummary';

const ModerationDashboard = () => {
  const [dashboardData, setDashboardData] = useState({
    threads: 0,
    users: 0,
    posts: 0,
    reports: 0,
    bans: 0,
  });
  const [openReports, setOpenReports] = useState([]);

  const { path } = useRouteMatch();

  useEffect(() => {
    async function getData() {
      setDashboardData(await getDashboardData());
    }
    async function getReports() {
      setOpenReports(await getOpenReports());
    }

    getData();
    getReports();
  }, []);

  const pluralize = (string, stat) => {
    if (stat === 1) return string;
    return `${string}s`;
  };

  const { threads, users, posts, bans } = dashboardData;
  return (
    <div>
      {openReports.length > 0 && (
        <OpenReportsHeader>
          <div className="open-reports-title">Open Reports</div>
          <Link className="reports-all-link" to={`${path}/reports`}>
            See all
          </Link>
        </OpenReportsHeader>
      )}
      {openReports.map((report) => (
        <ReportSummary key={report.id} report={report} />
      ))}
      <StyledModerationDashboard>
        <div className="dashboard-tile users">
          <div className="dashboard-tile-details">
            <div className="dashboard-stat">{users}</div>
            <div className="dashboard-desc">{`new ${pluralize('user', users)}`}</div>
          </div>
          <i className="fas fa-users dashboard-tile-icon" />
        </div>
        <div className="dashboard-tile threads">
          <div className="dashboard-tile-details">
            <div className="dashboard-stat">{threads}</div>
            <div className="dashboard-desc">{`new ${pluralize('thread', threads)}`}</div>
          </div>
          <i className="fas fa-newspaper dashboard-tile-icon" />
        </div>
        <div className="dashboard-tile posts">
          <div className="dashboard-tile-details">
            <div className="dashboard-stat">{posts}</div>
            <div className="dashboard-desc">{`new ${pluralize('post', posts)}`}</div>
          </div>
          <i className="fas fa-comments dashboard-tile-icon" />
        </div>
        <div className="dashboard-tile bans">
          <div className="dashboard-tile-details">
            <div className="dashboard-stat">{bans}</div>
            <div className="dashboard-desc">{pluralize('ban', bans)}</div>
          </div>
          <i className="fas fa-exclamation-triangle dashboard-tile-icon" />
        </div>
      </StyledModerationDashboard>
    </div>
  );
};

const OpenReportsHeader = styled.div`
  display: flex;
  align-items: baseline;
  justify-content: space-between;
  margin-bottom: 20px;

  .open-reports-title {
    font-size: ${ThemeFontSizeLarge};
  }

  .reports-all-link {
    font-size: ${ThemeFontSizeMedium};
    color: ${(props) => transparentize(0.2, ThemeTextColor(props))};
  }
`;
const StyledModerationDashboard = styled.div`
  padding: ${ThemeHorizontalPadding} 0;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;

  ${MobileMediaQuery} {
    justify-content: space-evenly;
  }

  .dashboard-tile {
    height: 150px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    color: white;
    padding: 0 25px;
    flex-basis: 20%;
    margin-bottom: 10px;

    ${MobileMediaQuery} {
      flex-basis: 45%;
    }
  }

  .users {
    background: #55aa48;
  }

  .threads {
    background: #328fbf;
  }

  .posts {
    background: #d76927;
  }

  .bans {
    background: #c23616;
  }

  .dashboard-tile-icon {
    font-size: 70px;

    ${MobileMediaQuery} {
      font-size: 50px;
    }
  }

  .dashboard-stat {
    font-weight: bold;
    font-size: 3rem;
    margin-bottom: 12px;
  }

  .dashboard-desc {
    font-size: ${ThemeFontSizeLarge};
  }
`;
export default ModerationDashboard;
