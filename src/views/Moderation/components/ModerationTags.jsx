import React, { useEffect, useState } from 'react';
import { Panel, PanelHeader, PanelSearchField, PanelSearchButton, TagItem } from './style';
import { createTag } from '../../../services/moderation';
import { pushSmartNotification } from '../../../utils/notification';
import { getTags } from '../../../services/tags';

const ModerationTags = () => {
  const [tagName, setTagName] = useState('');
  const [canSubmit, setCanSubmit] = useState(true);
  const [currentTags, setCurrentTags] = useState([]);

  const submitTag = async () => {
    try {
      if (tagName.length <= 3) {
        throw Error('Tag name too short.');
      }

      setCanSubmit(false);

      await createTag(tagName);

      setCanSubmit(true);
      setTagName('');
    } catch (error) {
      pushSmartNotification({ error: error.msg });
      setCanSubmit(true);
    }
  };

  useEffect(() => {
    async function getAllTags() {
      setCurrentTags(await getTags());
    }
    if (canSubmit) {
      getAllTags();
    }
  }, [canSubmit]);

  return (
    <Panel>
      <PanelHeader>
        <PanelSearchField
          type="text"
          value={tagName}
          onChange={(e) => setTagName(e.target.value)}
          placeholder="Tag name"
        />
        {canSubmit && <PanelSearchButton onClick={submitTag}>Create</PanelSearchButton>}
      </PanelHeader>
      {currentTags.map((tag) => (
        <TagItem key={tag.id}>
          <i className="fas fa-tag tag-icon" />
          {tag.name}
        </TagItem>
      ))}
    </Panel>
  );
};

export default ModerationTags;
