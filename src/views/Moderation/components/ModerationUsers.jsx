import React, { useEffect, useState } from 'react';
import dayjs from 'dayjs';
import { getLatestUsers, getIpsByUsername } from '../../../services/moderation';
import { Panel, PanelBody, PanelHeader, PanelSearchButton, PanelSearchField } from './style';

const ModerationUsers = () => {
  const [query, setQuery] = useState('');
  const [results, setResults] = useState([]);

  useEffect(() => {
    const getUsers = async () => {
      setResults(await getLatestUsers());
    };
    getUsers();
  }, []);

  const search = async () => {
    const users = query.length === 0 ? await getLatestUsers() : await getIpsByUsername(query);
    setResults(users);
  };

  return (
    <Panel>
      <PanelHeader>
        <PanelSearchField
          type="text"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          onKeyDown={(e) => e.key === 'Enter' && search()}
          placeholder="Search user by username"
        />
        <PanelSearchButton onClick={search}>Search</PanelSearchButton>
      </PanelHeader>
      <PanelBody>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Username</th>
              <th>Creation date</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {results.map((item) => (
              <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.username}</td>
                <td>{dayjs(item.created_at).format('DD/MM/YYYY')}</td>
                <td>
                  <span>
                    <a href={`/user/${item.id}`}>Profile</a>
                  </span>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </PanelBody>
    </Panel>
  );
};

export default ModerationUsers;
