import React, { useState } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeFontSizeSmall,
  ThemeFontSizeMedium,
  ThemeFontSizeHuge,
  ThemeBackgroundLighter,
  ThemeTextColor,
  ThemeBodyBackgroundColor,
  ThemeBodyWidth,
} from '../../utils/ThemeNew';
import DeleteAccountWarning from './components/DeleteAccount';
import LoggedInOnly from '../../componentsNew/LoggedInOnly';
import UserExperienceContainer from './components/UserExperienceContainer';
import PatreonContainer from './components/PatreonContainer';
import DeleteAccountContainer from './components/DeleteAccountContainer';
import ThemeConfiguratorContainer from './components/ThemeContainer';
import AvatarBackgroundPreview from './components/AvatarBackgroundPreview';
import { SubheaderLink } from '../../componentsNew/Buttons';

const UserSettingsPage = () => {
  const [showDeleteAccount, setShowDeleteAccount] = useState(false);
  const loggedIn = useSelector((state) => state.user.loggedIn && state.user.username);

  if (!loggedIn) {
    return <Redirect to="/" />;
  }

  return (
    <LoggedInOnly>
      <UserSettingsPageWrapper>
        <Helmet>
          <title>Settings - Knockout!</title>
        </Helmet>
        <h2>User Settings</h2>
        <nav className="subHeader">
          <span className="subHeader-buttons-root">
            <div>
              <SubheaderLink to="/">
                <i className="fas fa-angle-left" />
                <span>Home</span>
              </SubheaderLink>
            </div>
            <div>
              <SubheaderLink to="/logout">
                <i className="fas fa-sign-out-alt" />
                <span>Log me out!</span>
              </SubheaderLink>
            </div>
          </span>
        </nav>
        <ThemeConfiguratorContainer />
        <div className="settings-root">
          <div className="settings-column avatar-background">
            <AvatarBackgroundPreview />
          </div>
          <div className="settings-column">
            <UserExperienceContainer />
            <PatreonContainer />
            <DeleteAccountContainer showDeleteAccount={setShowDeleteAccount} />
          </div>
        </div>
      </UserSettingsPageWrapper>
      {showDeleteAccount && <DeleteAccountWarning />}
    </LoggedInOnly>
  );
};
export default UserSettingsPage;

const UserSettingsPageWrapper = styled.section`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  max-width: ${ThemeBodyWidth};

  h2 {
    margin: 0 0 ${ThemeVerticalPadding} 0;
    font-size: ${ThemeFontSizeHuge};
  }

  .avatar-background {
    margin-right: ${ThemeHorizontalPadding};
    @media (max-width: 900px) {
      margin-right: unset;
    }
  }

  .theme-input {
    background: ${ThemeBackgroundLighter};
    border: none;
    padding: calc(${ThemeHorizontalPadding} / 2);
  }

  .dropdown {
    position: relative;
    display: flex;
    flex-direction: row;

    color: ${ThemeTextColor};
    background: ${ThemeBodyBackgroundColor};
    transition: background 1s;
  }

  .input:last-child {
    margin-bottom: 0;
  }

  .dropdown input,
  .dropdown select {
    border: none;
    padding: 0;
    line-height: 1;
    outline: none;
    font-size: ${ThemeFontSizeMedium};
    color: inherit;
    background: ${ThemeBackgroundLighter};
    transition: background 1s;
  }

  .dropdown label {
    margin-right: ${ThemeHorizontalPadding};
  }

  .panel-desc {
    font-size: ${ThemeFontSizeSmall};
    text-align: center;
    color: ${ThemeTextColor};
  }

  .image-container {
    display: block;
    height: auto;
    margin: auto;
  }

  .input-wrapper {
    margin-top: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    align-items: center;
    background-color: ${ThemeBodyBackgroundColor};
    padding-top: ${ThemeVerticalPadding};
    padding-left: ${ThemeHorizontalPadding};
    padding-right: ${ThemeHorizontalPadding};
    padding-bottom: ${ThemeVerticalPadding};
    transition: background 1s;
  }

  .input-wrapper label {
    margin-left: ${ThemeHorizontalPadding};
    font-size: ${ThemeFontSizeMedium};
  }

  .user-experience-wrapper {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .settings-column {
    display: flex;
    flex-grow: 1;
    flex-basis: 0;
    flex-direction: column;
  }

  .settings-root {
    display: flex;
    flex-direction: row;
    @media (max-width: 900px) {
      flex-direction: column;
    }
  }

  .options-wrapper {
    max-width: 320px;
    margin: 0 auto;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
  }

  .options-wrapper > div {
    flex-grow: 1;
    flex-basis: 0;
  }

  .options-wrapper > div:nth-child(2n) {
    margin-left: ${ThemeHorizontalPadding};
  }

  nav.subHeader {
    max-width: 100vw;
    padding-top: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    font-size: ${ThemeFontSizeSmall};
    color: ${ThemeTextColor};

    .subHeader-buttons-root {
      display: flex;
      height: 26.6px;
      overflow: hidden;
      align-items: stretch;
      justify-content: space-between;
      width: 100%;
      text-decoration: none;
    }
  }

  .input-row {
    margin-bottom: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    align-items: center;
    background-color: ${ThemeBackgroundLighter};

    label {
      margin-left: ${ThemeHorizontalPadding};
      font-size: ${ThemeFontSizeMedium};
      flex: 0 0 250px;
    }

    .input-wrap {
      flex: 1;
      display: flex;
      border: none;
      background-color: ${ThemeBodyBackgroundColor};
      input[type='text'],
      input[type='number'],
      input[type='color'] {
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
        background-color: ${ThemeBodyBackgroundColor};
        color: ${ThemeTextColor};
        border-radius: 0;
      }

      input[type='text'],
      input[type='number'] {
        flex: 1;
        border: none;
        width: 100%;
      }

      input[type='color'] {
        flex: 0 0 40px;
        height: 30px;
      }
    }
  }

  .theme-selection {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    margin-top: 20px;
    margin-bottom: 20px;
  }

  .theme-configuration {
    display: flex;
    flex-wrap: wrap;
    .theme-inputs {
      flex: 0 0 550px;
    }
    .theme-preview {
      flex: 1;
      .browser {
        border: 2px solid #333333;
        background: #1b1b1b;
        .tabs {
          background: #1b1b1b;
          color: white;
          display: flex;
          padding-top: 8px;
          .tab {
            margin: 0 0.4rem;
            flex: 0 0 200px;
            font-size: 14px;
            .inner {
              height: 32px;
              display: flex;
              align-items: center;
              padding: 0 0.8rem;
            }
            .title {
              flex: 1;
            }
            .action {
              flex: 0 0 24px;
              text-align: center;
            }
            &.active {
              .inner {
                background: #333333;
                border-radius: 0.4rem 0.4rem 0 0;
              }
            }
          }
        }
        .address-bar {
          background: #333333;
          color: white;
          padding: 4px;
          .inner {
            background: #2b2b2b;
            padding: 4px 8px;
            span {
              font-size: 14px;
              text-align: left;
            }
          }
        }
      }
    }
    .theme-actions {
      flex: 0 0 100%;
      padding: 0 ${ThemeHorizontalPadding};

      button {
        border: none;
        border-radius: 0;
        color: ${ThemeTextColor};
        background-color: ${ThemeBackgroundLighter};
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
        margin-right: ${ThemeHorizontalPadding};
        cursor: pointer;
        i {
          margin-right: 0.5rem;
        }
      }
    }

    @media (max-width: 960px) {
      .theme-inputs,
      .theme-preview {
        flex: 0 0 100%;
      }
    }

    .theme-inputs {
      padding: 0 ${ThemeHorizontalPadding};
    }
  }
`;
