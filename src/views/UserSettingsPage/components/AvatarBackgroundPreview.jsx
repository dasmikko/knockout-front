import React from 'react';
import styled from 'styled-components';
import { darken, lighten } from 'polished';
import {
  ThemeVerticalPadding,
  ThemeTextColor,
  ThemeBackgroundLighter,
  ThemeKnockoutRed,
} from '../../../utils/ThemeNew';
import AvatarUploadContainer from './AvatarUploadContainer';
import UserGroupRestricted from '../../../componentsNew/UserGroupRestricted';
import BackgroundUploadContainer from './BackgroundUploadContainer';
import { MobileMediaQuery } from '../../../components/SharedStyles';
import { Panel, PanelTitle } from '../../../componentsNew/Panel';

const AvatarBackgroundPreview = () => {
  return (
    <Panel>
      <PanelTitle>Avatar and Background Upload</PanelTitle>
      <BackgroundPreview>
        <div className="preview-container">
          <AvatarUploadContainer />
          <BackgroundUploadContainer />
        </div>
      </BackgroundPreview>
      <div className="panel-desc">
        Select a file to upload
        <br />
        <b>Backgrounds:</b> 230x460 &nbsp;
        <b>Avatars: </b>
        115x115
        <UserGroupRestricted userGroupIds={[1]}>
          <div>
            <br />
            You must be a Gold member to upload animated avatars.
          </div>
        </UserGroupRestricted>
        <UserGroupRestricted userGroupIds={[2]}>
          <br />
          Animated avatars: 115x115 <b>MAX</b>, 125kb <b>MAX</b>, GIFs <b>ONLY</b>. Anything that
          doesn&apos;t match that will be un-animated.
        </UserGroupRestricted>
        <br />
        Resizing before upload recommended
      </div>
    </Panel>
  );
};

const BackgroundPreview = styled.div`
  position: relative;
  display: block;
  margin-top: ${ThemeVerticalPadding};
  margin-bottom: ${ThemeVerticalPadding};
  margin-left: auto;
  margin-right: auto;
  width: 230px;
  height: 460px;

  #upload,
  #bgupload {
    display: none;
  }

  .preview-container {
    display: flex;
    justify-content: start;
    align-items: center;
    flex-direction: column;
    position: relative;
    backdrop-filter: blur(3px);
    height: 100%;

    .avatar-label-container {
      position: relative;
    }

    .avatar-label {
      z-index: 2;
      width: 115px;
      height: 115px;
      text-align: center;
      outline: 2px dashed;
      outline-color: ${(props) => {
        if (props.theme.mode === 'light') {
          return lighten(0.6, ThemeTextColor(props));
        }
        return darken(0.6, ThemeTextColor(props));
      }};
      cursor: pointer;
      position: relative;
      margin-top: 45px;
      transition: 0.4s;

      &:hover,
      &:focus-within {
        outline-color: ${(props) => {
          if (props.theme.mode === 'light') {
            return lighten(0.2, ThemeTextColor(props));
          }
          return darken(0.2, ThemeTextColor(props));
        }};
        .upload-icon,
        .delete-icon {
          transform: scale(1);
        }
      }
    }

    .upload-icon {
      position: absolute;
      font-size: 15px;
      background: ${ThemeTextColor};
      color: ${ThemeBackgroundLighter};
      height: 30px;
      width: 30px;
      line-height: 30px;
      border-radius: 50%;
      right: -10px;
      bottom: -10px;
      transition: 0.4s;
      text-align: center;
      transform: scale(0);
      border: none;
      cursor: pointer;
    }

    .delete-icon {
      position: absolute;
      font-size: 17px;
      background: ${ThemeKnockoutRed};
      color: ${ThemeTextColor};
      height: 30px;
      width: 30px;
      line-height: 28px;
      border-radius: 50%;
      left: -10px;
      bottom: -10px;
      opacity: 0.6;
      transition: 0.4s;
      text-align: center;
      transform: scale(0);
      border: none;
      cursor: pointer;

      &:hover {
        opacity: 1;
      }
    }

    .user-background {
      position: absolute;
      z-index: 1;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      content: '';
      filter: brightness(0.5) contrast(0.925);
      outline: 2px dashed;
      outline-color: ${(props) => {
        if (props.theme.mode === 'light') {
          return lighten(0.6, ThemeTextColor(props));
        }
        return darken(0.6, ThemeTextColor(props));
      }};
      cursor: pointer;
      transition: 0.4s;

      &:hover,
      &:focus-within {
        outline-color: ${(props) => {
          if (props.theme.mode === 'light') {
            return lighten(0.2, ThemeTextColor(props));
          }
          return darken(0.2, ThemeTextColor(props));
        }};
        filter: brightness(0.8) contrast(0.925);
        .upload-icon {
          transform: scale(1);
        }
      }

      .user-background-inner {
        position: relative;
        height: 100%;
      }
    }

    ${MobileMediaQuery} {
      .upload-icon,
      .delete-icon {
        transform: scale(1);
      }
    }
  }
`;

export default AvatarBackgroundPreview;
