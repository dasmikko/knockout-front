import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import config from '../../../../config';
import { pushSmartNotification } from '../../../utils/notification';
import { uploadBackground } from '../../../services/avatar'; // todo: there should be a separate service for backgrounds
import { updateUser } from '../../../services/user';

const BackgroundUploadContainer = () => {
  const currentUser = useSelector((state) => state.user);
  const [backgroundUrl, setBackgroundUrl] = useState(currentUser.background_url || 'none.webp');

  const handleBackgroundUpload = async (file) => {
    try {
      const result = await uploadBackground(file);
      if (result.message) {
        await updateUser({ background_url: result.message });
        const date = new Date();
        setBackgroundUrl(`${result.message}?t=${date.getTime()}`);
        pushSmartNotification({
          message:
            'Your background has been updated successfully! It might take up to 120s to update.',
        });
      } else {
        throw new Error('Unprocessable entity.');
      }
    } catch (err) {
      pushSmartNotification({
        error: 'Upload failed. Are you uploading a JPG/GIF/PNG image? Is the image over 2MB?',
      });
    }
  };

  const openInput = () => {
    document.querySelector('#bgupload').click();
  };

  return (
    <label className="user-background" htmlFor="bgupload">
      <div className="user-background-inner">
        <img alt="User Background" src={`${config.cdnHost}/image/${backgroundUrl}`} />
        <button type="button" className="upload-icon" onClick={openInput}>
          <i className="fas fa-camera" />
        </button>
        <input
          id="bgupload"
          type="file"
          name="background"
          accept="image/*"
          onChange={(e) => handleBackgroundUpload(e.target.files[0])}
        />
      </div>
    </label>
  );
};

export default BackgroundUploadContainer;
