/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { pushSmartNotification } from '../../../utils/notification';
import { uploadAvatar } from '../../../services/avatar';
import { updateUser } from '../../../services/user';
import config from '../../../../config';

const AvatarUploadContainer = () => {
  const currentUser = useSelector((state) => state.user);
  const [avatar, setAvatar] = useState(currentUser.avatar_url || 'none.webp');

  const handleUpload = async (file) => {
    try {
      const result = await uploadAvatar(file);
      if (result.message) {
        await updateUser({ avatar_url: result.message });
        const date = new Date();
        setAvatar(`${result.message}?t=${date.getTime()}`);
        pushSmartNotification({
          message: 'Your avatar has been updated successfully! It might take up to 120s to update.',
        });
      } else {
        throw new Error('Unprocessable entity.');
      }
    } catch (err) {
      pushSmartNotification({
        error: 'Upload failed. Are you uploading a JPG/GIF/PNG image? Is the image over 2MB?',
      });
    }
  };

  const setEmptyAvatar = async (e) => {
    e.preventDefault();
    await updateUser({ avatar_url: 'none.webp' });
    setAvatar('none.webp');
    pushSmartNotification({ message: 'Avatar successfully removed.' });
  };

  const openInput = () => {
    document.querySelector('#upload').click();
  };

  return (
    <label className="avatar-label" htmlFor="upload">
      <img
        className="user-avatar"
        src={`${config.cdnHost}/image/${avatar}`}
        alt={`${currentUser.username}'s Avatar`}
      />
      <button type="button" className="upload-icon" onClick={openInput}>
        <i className="fas fa-camera" />
      </button>
      {avatar !== 'none.webp' && (
        <button type="button" className="delete-icon" onClick={setEmptyAvatar}>
          <i className="fas fa-times" />
        </button>
      )}
      <input
        id="upload"
        type="file"
        name="avatar"
        accept="image/*"
        onChange={(e) => handleUpload(e.target.files[0])}
      />
    </label>
  );
};

export default AvatarUploadContainer;
