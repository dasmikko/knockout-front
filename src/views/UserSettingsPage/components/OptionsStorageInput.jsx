/* eslint-disable jsx-a11y/label-has-for */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Tooltip from '../../../componentsNew/Tooltip';
import { saveValue, loadValue } from '../../../utils/postOptionsStorage';
import { pushSmartNotification } from '../../../utils/notification';

const OptionsStorageInput = ({ storageKey, defaultValue, tooltip, label, onChange }) => {
  const [value, setValue] = useState(loadValue(storageKey, defaultValue));

  const updateValue = (newValue) => {
    setValue(newValue);
    saveValue(storageKey, newValue);
    pushSmartNotification({ message: 'Profile preference saved successfully.' });
  };

  return (
    <Tooltip text={tooltip}>
      <div className="input-wrapper">
        <input
          type="checkbox"
          onChange={(e) => {
            updateValue(e.target.checked);
            if (onChange) {
              onChange(e.target.checked);
            }
          }}
          checked={value}
        />
        <label>{label}</label>
      </div>
    </Tooltip>
  );
};

OptionsStorageInput.propTypes = {
  label: PropTypes.string.isRequired,
  tooltip: PropTypes.string.isRequired,
  storageKey: PropTypes.string,
  defaultValue: PropTypes.bool,
  onChange: PropTypes.func,
};

OptionsStorageInput.defaultProps = {
  storageKey: '',
  defaultValue: false,
  onChange: null,
};

export default OptionsStorageInput;
