import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import { updateTheme } from '../../../state/style';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
  ThemeHighlightWeaker,
  ThemeTextColor,
} from '../../../utils/ThemeNew';
import RadioButton from '../../../componentsNew/RadioButton/RadioButton';
import { MobileMediaQuery } from '../../../components/SharedStyles';
import { setThemeToStorage } from '../../../services/theme';

const ThemeSelect = ({ theme, current }) => {
  const dispatch = useDispatch();

  const setTheme = () => {
    dispatch(updateTheme(theme));
    setThemeToStorage(theme);
  };

  return (
    <StyledThemeSelect theme={{ mode: theme }} selected={current === theme} onClick={setTheme}>
      <RadioButton theme={theme} checked={current === theme} />
      <span className="theme-name">{theme}</span>
    </StyledThemeSelect>
  );
};

const StyledThemeSelect = styled.div`
  display: flex;
  font-weight: bold;
  cursor: pointer;
  margin-right: 20px;

  width: 146px;
  font-size: ${ThemeFontSizeMedium};
  border: 2px solid ${(props) => (props.selected ? ThemeHighlightWeaker : ThemeBackgroundLighter)};
  padding: 14px 15px;
  background: ${ThemeBackgroundDarker};
  color: ${ThemeTextColor};
  text-transform: capitalize;
  align-items: center;
  user-select: none;

  .theme-name {
    flex-grow: 1;
    text-align: center;
  }

  ${MobileMediaQuery} {
    margin-right: 10px;
    margin-bottom: 10px;
    width: 100px;
  }
`;

export default ThemeSelect;

ThemeSelect.propTypes = {
  theme: PropTypes.string.isRequired,
  current: PropTypes.string.isRequired,
};
