import React from 'react';
import PropTypes from 'prop-types';
import { scrollToTop } from '../../../utils/pageScroll';
import { StyledDeleteAccountButton } from '../../../pages/UserSettings/components/DeleteAccount';
import { Panel, PanelTitle } from '../../../componentsNew/Panel';

const DeleteAccountContainer = (props) => {
  return (
    <Panel>
      <PanelTitle title="Delete Account">Delete Account</PanelTitle>
      <StyledDeleteAccountButton
        background="#ad1a1a"
        onClick={() => {
          props.showDeleteAccount(true);
          scrollToTop();
        }}
        type="button"
      >
        <i className="fas fa-bomb" />
        Delete Account
      </StyledDeleteAccountButton>
    </Panel>
  );
};

DeleteAccountContainer.propTypes = {
  showDeleteAccount: PropTypes.func,
};

DeleteAccountContainer.defaultProps = {
  showDeleteAccount: null,
};

export default DeleteAccountContainer;
