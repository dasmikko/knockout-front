import React from 'react';
import LoginButton from '../../../components/LoginButton';
import { Panel, PanelTitle } from '../../../componentsNew/Panel';

const PatreonContainer = () => (
  <Panel>
    <PanelTitle title="Richboi Zone">Patreon Bonuses</PanelTitle>
    <div className="panel-desc">Connects accounts permanently</div>
    <div className="panel-desc">don&apos;t use if you&apos;re not a Patreon supporter!</div>
    <LoginButton color="white" background="#e85b46" route="/link/patreon">
      <i className="fab fa-patreon" />
      <span style={{ color: 'white' }}>Claim Bonuses</span>
    </LoginButton>
  </Panel>
);

export default PatreonContainer;
