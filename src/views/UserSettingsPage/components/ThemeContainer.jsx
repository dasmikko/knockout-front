/* eslint-disable jsx-a11y/label-has-for */ // not working properly
import React, { useState, useRef, useContext } from 'react';
import PropTypes from 'prop-types';

import { ThemeContext } from 'styled-components';
import { pushNotification } from '../../../utils/notification';

import { deleteCustomThemeFromStorage, setCustomThemeToStorage } from '../../../services/theme';

import ThemePreview from './ThemePreview';
import Modal from '../../../componentsNew/Modals/Modal';
import { Panel, PanelTitle } from '../../../componentsNew/Panel';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeBannedUserColor,
  ThemeBodyBackgroundColor,
  ThemeFontSizeHeadline,
  ThemeFontSizeHuge,
  ThemeFontSizeLarge,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeGoldMemberColor,
  ThemeGoldMemberGlow,
  ThemeHighlightStronger,
  ThemeHighlightWeaker,
  ThemeKnockoutRed,
  ThemeMainBackgroundColor,
  ThemeMainLogo,
  ThemeModeratorColor,
  ThemeModeratorInTrainingColor,
  ThemePostLineHeight,
  ThemeTextColor,
} from '../../../utils/ThemeNew';
import ThemeSelect from './ThemeSelect';
import { FieldLabelSmall, TextField } from '../../../componentsNew/FormControls';

const ThemeInput = ({ name, value, onChange, type, label }) => (
  <div className="input-row">
    <label htmlFor={name}>{`${label}: `}</label>
    <div className="input-wrap">
      <input
        className="theme-input"
        type={type}
        id={name}
        name={name}
        value={value}
        onChange={onChange}
      />
      {type === 'color' && (
        <input type="text" id={name} name={name} value={value} onChange={onChange} />
      )}
    </div>
  </div>
);

ThemeInput.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

const ThemeConfiguratorContainer = () => {
  const currentTheme = useContext(ThemeContext);
  const customTheme = {
    backgroundLighter: ThemeBackgroundLighter({ theme: { mode: 'custom' } }),
    backgroundDarker: ThemeBackgroundDarker({ theme: { mode: 'custom' } }),
    mainBackgroundColor: ThemeMainBackgroundColor({ theme: { mode: 'custom' } }),
    bodyBackgroundColor: ThemeBodyBackgroundColor({ theme: { mode: 'custom' } }),
    knockoutBrandColor: ThemeKnockoutRed({ theme: { mode: 'custom' } }),
    knockoutLogo: ThemeMainLogo({ theme: { mode: 'custom' } }),
    textColor: ThemeTextColor({ theme: { mode: 'custom' } }),
    themeFontSizeSmall: ThemeFontSizeSmall({ theme: { mode: 'custom' } }),
    themeFontSizeMedium: ThemeFontSizeMedium({ theme: { mode: 'custom' } }),
    themeFontSizeLarge: ThemeFontSizeLarge({ theme: { mode: 'custom' } }),
    themeFontSizeHuge: ThemeFontSizeHuge({ theme: { mode: 'custom' } }),
    themeFontSizeHeadline: ThemeFontSizeHeadline({ theme: { mode: 'custom' } }),
    themeGoldMemberColor: ThemeGoldMemberColor({ theme: { mode: 'custom' } }),
    themeGoldMemberGlow: ThemeGoldMemberGlow({ theme: { mode: 'custom' } }),
    themeModeratorColor: ThemeModeratorColor({ theme: { mode: 'custom' } }),
    themeModeratorInTrainingColor: ThemeModeratorInTrainingColor({ theme: { mode: 'custom' } }),
    themeBannedUserColor: ThemeBannedUserColor({ theme: { mode: 'custom' } }),
    postLineHeight: ThemePostLineHeight({ theme: { mode: 'custom' } }),
    highlightStrongerColor: ThemeHighlightStronger({ theme: { mode: 'custom' } }),
    highlightWeakerColor: ThemeHighlightWeaker({ theme: { mode: 'custom' } }),
  };

  const [themeValues, setThemeValues] = useState(customTheme);
  const [rerenderPreviewValues, setRerenderPreviewValues] = useState(customTheme);
  const [modalOpen, setModalOpen] = useState(false);
  const [tempTheme, setTempTheme] = useState('');
  const changeTimer = useRef(0);

  const saveTheme = () => {
    setCustomThemeToStorage(JSON.stringify(themeValues));
    window.location.reload();
  };

  const resetTheme = () => {
    deleteCustomThemeFromStorage();
    window.location.reload();
  };

  const handleChange = ({ target: { name, value } }) => {
    const newValues = {
      ...themeValues,
      [name]: value,
    };

    setThemeValues(newValues);
    clearTimeout(changeTimer.current);

    changeTimer.current = setTimeout(() => {
      setRerenderPreviewValues(newValues);
    }, 400);
  };

  const loadNewTheme = (theme) => {
    try {
      const newTheme = JSON.parse(theme);
      setThemeValues(newTheme);
      setRerenderPreviewValues(newTheme);
      pushNotification({ message: 'Theme loaded', type: 'success' });
      setModalOpen(false);
    } catch {
      pushNotification({
        message: 'Unable to read theme. Please make sure it is valid JSON.',
        type: 'error',
      });
    }
  };

  const copyThemeFromClipboard = () => {
    // Check if the browser supports clipboard reading and if we have permission to do so
    navigator.permissions
      .query({ name: 'clipboard-read' })
      .then((result) => {
        // Permissions denied
        if (result.state === 'denied') {
          pushNotification({
            message: 'Unable to read theme. Clipboard permissions denied by browser.',
            type: 'error',
          });
        }

        // Permissions granted or we haven't asked yet
        else if (result.state === 'granted' || result.state === 'prompt') {
          navigator.clipboard.readText().then((theme) => loadNewTheme(theme));
        }
      })
      .catch(() => {
        // Browsers like firefox do not implement clipboard reading publicly, causing an error when asking for it.
        // https://developer.mozilla.org/en-US/docs/Web/API/Clipboard/read
        setTempTheme('');
        setModalOpen(true);
      });
  };

  const copyThemeToClipboard = () => {
    navigator.clipboard
      .writeText(JSON.stringify(themeValues))
      .then(() => {
        pushNotification({ message: 'Theme copied to clipboard', type: 'success' });
      })
      .catch(() => {
        pushNotification({ message: 'Unable to copy theme', type: 'error' });
      });
  };

  const {
    backgroundLighter,
    backgroundDarker,
    mainBackgroundColor,
    bodyBackgroundColor,
    knockoutBrandColor,
    // knockoutLogo,
    textColor,
    themeFontSizeSmall,
    themeFontSizeMedium,
    themeFontSizeLarge,
    themeFontSizeHuge,
    themeFontSizeHeadline,
    themeGoldMemberColor,
    themeGoldMemberGlow,
    themeModeratorColor,
    themeModeratorInTrainingColor,
    themeBannedUserColor,
    postLineHeight,
    highlightStrongerColor,
    highlightWeakerColor,
  } = themeValues;

  return (
    <>
      <Modal
        iconUrl="/static/icons/image-file.png"
        title="Load theme"
        cancelFn={() => setModalOpen(false)}
        submitFn={() => loadNewTheme(tempTheme)}
        isOpen={modalOpen}
      >
        <FieldLabelSmall>Theme data</FieldLabelSmall>
        <TextField value={tempTheme} onChange={(e) => setTempTheme(e.target.value)} />
      </Modal>
      <Panel>
        <PanelTitle>Theme Settings</PanelTitle>
        <div className="theme-selection">
          <ThemeSelect theme="light" current={currentTheme.mode} />
          <ThemeSelect theme="dark" current={currentTheme.mode} />
          <ThemeSelect theme="classic" current={currentTheme.mode} />
          <ThemeSelect theme="custom" current={currentTheme.mode} />
        </div>
        {currentTheme.mode === 'custom' && (
          <div className="theme-configuration">
            <div className="theme-inputs">
              <ThemeInput
                type="color"
                name="backgroundLighter"
                value={backgroundLighter}
                onChange={handleChange}
                label="Lighter Background Color"
              />
              <ThemeInput
                type="color"
                name="backgroundDarker"
                value={backgroundDarker}
                onChange={handleChange}
                label="Darker Background Color"
              />
              <ThemeInput
                type="color"
                name="highlightStrongerColor"
                value={highlightStrongerColor}
                onChange={handleChange}
                label="Stronger Highlight Color"
              />
              <ThemeInput
                type="color"
                name="highlightWeakerColor"
                value={highlightWeakerColor}
                onChange={handleChange}
                label="Weaker Highlight Color"
              />
              <ThemeInput
                type="color"
                name="mainBackgroundColor"
                value={mainBackgroundColor}
                onChange={handleChange}
                label="Page Background Color"
              />
              <ThemeInput
                type="color"
                name="bodyBackgroundColor"
                value={bodyBackgroundColor}
                onChange={handleChange}
                label="Page Content Background Color"
              />
              <ThemeInput
                type="color"
                name="textColor"
                value={textColor}
                onChange={handleChange}
                label="Text Color"
              />
              <ThemeInput
                type="color"
                name="knockoutBrandColor"
                value={knockoutBrandColor}
                onChange={handleChange}
                label="Knockout Brand Color"
              />
              <ThemeInput
                type="text"
                name="themeGoldMemberColor"
                value={themeGoldMemberColor}
                onChange={handleChange}
                label="Gold Member Color"
              />
              <ThemeInput
                type="text"
                name="themeGoldMemberGlow"
                value={themeGoldMemberGlow}
                onChange={handleChange}
                label="Gold Member Glow"
              />
              <ThemeInput
                type="color"
                name="themeModeratorColor"
                value={themeModeratorColor}
                onChange={handleChange}
                label="Moderator Color"
              />
              <ThemeInput
                type="color"
                name="themeModeratorInTrainingColor"
                value={themeModeratorInTrainingColor}
                onChange={handleChange}
                label="Moderator in Training Color"
              />
              <ThemeInput
                type="color"
                name="themeBannedUserColor"
                value={themeBannedUserColor}
                onChange={handleChange}
                label="Banned User Color"
              />
              <ThemeInput
                type="text"
                name="themeFontSizeSmall"
                value={themeFontSizeSmall}
                onChange={handleChange}
                label="Font Size Small"
              />
              <ThemeInput
                type="text"
                name="themeFontSizeMedium"
                value={themeFontSizeMedium}
                onChange={handleChange}
                label="Font Size Medium"
              />
              <ThemeInput
                type="text"
                name="themeFontSizeLarge"
                value={themeFontSizeLarge}
                onChange={handleChange}
                label="Font Size Large"
              />
              <ThemeInput
                type="text"
                name="themeFontSizeHuge"
                value={themeFontSizeHuge}
                onChange={handleChange}
                label="Font Size Huge"
              />
              <ThemeInput
                type="text"
                name="themeFontSizeHeadline"
                value={themeFontSizeHeadline}
                onChange={handleChange}
                label="Font Size Headline"
              />
              <ThemeInput
                type="number"
                name="postLineHeight"
                value={postLineHeight}
                onChange={handleChange}
                label="Post content line height (%)"
              />
            </div>
            <div className="theme-preview">
              <ThemePreview theme={rerenderPreviewValues} />
            </div>
            <div className="theme-actions">
              <button type="button" onClick={saveTheme}>
                <i className="fas fa-save" />
                Save
              </button>
              <button type="button" onClick={resetTheme}>
                <i className="fas fa-redo" />
                Reset
              </button>
              <button type="button" onClick={copyThemeFromClipboard}>
                <i className="fas fa-file-import" />
                Load theme from clipboard
              </button>
              <button type="button" onClick={copyThemeToClipboard}>
                <i className="fas fa-file-export" />
                Copy theme to clipboard
              </button>
            </div>
          </div>
        )}
      </Panel>
    </>
  );
};

export default ThemeConfiguratorContainer;
