import React from 'react';

const ThreadCreationNotice = () => (
  <div className="thread-creation-notice">
    <h2>You&apos;re about to get banned!</h2>
    <p className="lead">Watch out! Looks like you&apos;re going to create a thread.</p>
    <p>
      Make sure you&apos;ve read and understood the rules and privacy policy before making a mistake
      you&apos;ll regret forever. Bans on Knockout are warnings, and you&nbsp;
      <em>will</em>
      &nbsp;be banned if you break rules or don&apos;t adhere to the standards of the community.
    </p>
  </div>
);

export default ThreadCreationNotice;
