/* eslint-disable import/prefer-default-export */
import { createNewThread } from '../../services/threads';
import { pushSmartNotification } from '../../utils/notification';
import { createAlertRequest } from '../../services/alerts';
import { loadAutoSubscribeFromStorageBoolean } from '../../services/theme';
import { accountOlderThanWeek } from '../../utils/user';
import { subforumHasAccountAgeCheck } from '../../utils/subforums';

const getTagId = (str) => str.split('|')[0];

export const submitThread = async (
  title,
  content,
  selectedIconId,
  backgroundUrl,
  backgroundType,
  selectedTags,
  subforumId,
  history
) => {
  try {
    if (!accountOlderThanWeek() && subforumHasAccountAgeCheck(subforumId)) {
      pushSmartNotification({
        error: `Your account is too new to post in this subforum. (ERR2NU-LRKMOAR)`,
      });
    }

    const tagIds = selectedTags.map((tagString) => getTagId(tagString));

    let backgroundOptions = {};
    if (backgroundUrl) {
      backgroundOptions = { backgroundUrl, backgroundType };
    }

    const thread = await createNewThread({
      title,
      icon_id: selectedIconId,
      subforum_id: subforumId,
      content,
      tagIds,
      ...backgroundOptions,
    });

    if (!thread || !thread.id) {
      pushSmartNotification({ error: 'Thread creation failed' });
      throw new Error('Could not create thread');
    }

    if (loadAutoSubscribeFromStorageBoolean()) {
      await createAlertRequest({
        threadId: thread.id,
        lastSeen: new Date(),
        previousLastSeen: new Date('01/01/1980'),
      });
    }

    history.push(`/thread/${thread.id}`);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error('Error creating thread:', err);
    return false;
  }

  return true;
};
