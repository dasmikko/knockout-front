import React from 'react';
import { Helmet } from 'react-helmet';

import { EventList, EventDescription } from '../components/EventsPage/style';
import EventCard from '../components/EventsPage/EventCard';
import getEventsList from '../services/events';

import {
  SubHeader,
  SubHeaderBackAndTitle,
  SubHeaderTitle,
  SubHeaderBack
} from '../components/SubHeader/style';

class EventsPage extends React.Component {
  state = {
    events: []
  };

  componentDidMount = async () => {
    const events = await getEventsList();

    this.setState({ events });
  };

  render() {
    return (
      <div>
        <SubHeader>
          <SubHeaderBackAndTitle>
            <SubHeaderBack to="/">&#8249;</SubHeaderBack>
            <SubHeaderTitle>Event Log</SubHeaderTitle>
          </SubHeaderBackAndTitle>
        </SubHeader>

        <Helmet>
          <title>Event Log - Knockout!</title>
        </Helmet>

        <EventList>
          <EventDescription>
            This page shows the latest actions done by moderators and, in some cases, users.
          </EventDescription>
          {this.state.events.map(event => (
            <EventCard key={event.id} {...event} />
          ))}
        </EventList>
      </div>
    );
  }
}

export default EventsPage;
