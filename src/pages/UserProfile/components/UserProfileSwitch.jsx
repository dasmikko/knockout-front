/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';

import UserProfileBans from './UserProfileBans';
import UserProfilePosts from './UserProfilePosts';
import { getUserPosts, getUserThreads } from '../../../services/user';
import UserProfileLatestThreads from './UserProfileLatestThreads';
import { UserProfileTabContainer, UserProfileTab } from './style';
import { usergroupCheck } from '../../../components/UserGroupRestricted';
import { addGoldProduct } from '../../../services/moderation';
import UserProfileRatings from './UserProfileRatings';

class UserProfileSwitch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      threads: { threads: [], currentPage: 0 },
      posts: { posts: [], currentPage: 0 },
      currentTab: 0,
      loading: false,
    };
  }

  componentDidMount() {
    // Since we dafult to the 'posts' tab, get posts on load
    this.getPosts();
  }

  getPosts = async (page = 1) => {
    this.setState({ loading: true });

    const posts = await getUserPosts(this.props.userId, page);

    this.setState({ loading: false, posts });
  };

  getThreads = async (page = 1) => {
    this.setState({ loading: true });

    const threads = await getUserThreads(this.props.userId, page);

    this.setState({ loading: false, threads });
  };

  handleTabClick = (tabIndex) =>
    this.setState({ currentTab: tabIndex }, () => {
      switch (tabIndex) {
        case 0:
          this.getPosts();
          break;
        case 1:
          this.getThreads();
          break;
        case 2:
          // noop
          break;
        default:
          break;
      }
    });

  renderTab = () => {
    const { threads, posts, currentTab, loading } = this.state;
    const { user, bans, ratings } = this.props;

    switch (currentTab) {
      case 0:
        return (
          <UserProfilePosts
            posts={posts}
            user={user}
            loading={loading}
            pageChangeFn={this.getPosts}
          />
        );
      case 1:
        return (
          <UserProfileLatestThreads
            threads={threads}
            user={user}
            loading={loading}
            pageChangeFn={this.getThreads}
          />
        );
      case 2:
        return <UserProfileBans bans={bans} user={user} />;
      case 3:
        return <UserProfileRatings ratings={ratings} />;

      default:
        return usergroupCheck([4]) ? (
          <button type="button" onClick={() => addGoldProduct({ userId: this.props.userId })}>
            Add gold for a month
          </button>
        ) : null;
    }
  };

  render() {
    const { currentTab } = this.state;
    const { user, bans, ratings } = this.props;
    const { posts, threads } = user;

    return (
      <div>
        <UserProfileTabContainer>
          <UserProfileTab
            type="button"
            onClick={() => this.handleTabClick(0)}
            isSelected={currentTab === 0}
          >
            <i className="fas fa-comment-dots" />
            &nbsp;Posts ({posts})
          </UserProfileTab>
          <UserProfileTab
            type="button"
            onClick={() => this.handleTabClick(1)}
            isSelected={currentTab === 1}
          >
            <i className="fas fa-file-alt" />
            &nbsp;Threads ({threads})
          </UserProfileTab>
          {bans && bans.length > 0 && (
            <UserProfileTab
              type="button"
              onClick={() => this.handleTabClick(2)}
              isSelected={currentTab === 2}
            >
              <i className="fas fa-gavel" />
              &nbsp;Bans ({bans.length})
            </UserProfileTab>
          )}
          {ratings && ratings.length > 0 && (
            <UserProfileTab
              type="button"
              onClick={() => this.handleTabClick(3)}
              isSelected={currentTab === 2}
            >
              <i className="fas fa-box-open" />
              &nbsp;Ratings
            </UserProfileTab>
          )}
        </UserProfileTabContainer>
        {this.renderTab()}
      </div>
    );
  }
}

export default UserProfileSwitch;

UserProfileSwitch.propTypes = {
  userId: PropTypes.number.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    posts: PropTypes.number.isRequired,
    threads: PropTypes.number.isRequired,
  }).isRequired,
  bans: PropTypes.array.isRequired,
  posts: PropTypes.string,
  threads: PropTypes.string,
  ratings: PropTypes.array.isRequired,
};

UserProfileSwitch.defaultProps = {
  posts: undefined,
  threads: undefined,
};
