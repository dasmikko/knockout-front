/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Post from '../../../components/Post';
import {
  ThemeQuaternaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeLargeTextSize,
  ThemeVerticalPadding,
  ThemeHorizontalHalfPadding
} from '../../../Theme';
import { buttonHoverGray } from '../../../components/SharedStyles';

class UserProfileLatestPosts extends React.Component {
  state = { open: false };

  handleToggleContent = key => {
    this.setState(prevState => ({ [key]: !prevState[key] }));
  };

  render() {
    const { posts, user } = this.props;

    if (!posts[0]) return null;

    return (
      <StyledLatestPosts>
        <PostsWrapperHeading onClick={() => this.handleToggleContent('open')}>
          Latest Created Posts:
        </PostsWrapperHeading>

        <PostsWrapper open={this.state.open}>
          {posts.map(post => (
            <Post
              key={post.id}
              hideUserWrapper
              hideControls
              user={user}
              postId={post.id}
              postBody={post.content}
              postDate={post.createdAt}
              username={user.username}
              userJoinDate={user.userJoinDate}
              byCurrentUser
              threadPage={1}
            />
          ))}
        </PostsWrapper>
      </StyledLatestPosts>
    );
  }
}

UserProfileLatestPosts.propTypes = {
  posts: PropTypes.array.isRequired,
  user: PropTypes.array.isRequired
};

export default UserProfileLatestPosts;

const StyledLatestPosts = styled.div``;
const PostsWrapper = styled.div`
  overflow: hidden;
  max-height: ${props => (props.open ? `unset` : `0px`)};
`;
const PostsWrapperHeading = styled.button`
  font-size: ${ThemeLargeTextSize};
  font-weight: bold;
  background: ${ThemeQuaternaryBackgroundColor};
  border: none;
  color: ${ThemePrimaryTextColor};
  width: 100%;
  display: block;
  text-align: left;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalHalfPadding};
  ${buttonHoverGray}
  margin-bottom: 10px;
`;
