import React from 'react';
import dayjs from 'dayjs';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { ThemeVerticalPadding, ThemeHorizontalPadding } from '../../../Theme';

const toDate = (date) => dayjs(date).format('DD/MM/YYYY');

const BanItem = ({
  banReason,
  createdAt,
  banIcon,
  duration,
  bannedByName,
  threadTitle,
  banStillValid,
  isMod,
  invalidateBan,
  thread,
  post,
}) => (
  <StyledBan>
    <div className="date">
      Banned on{toDate(createdAt)}
      .
</div>
    <img src={banIcon} alt="" />
    <div>
      Banned for: <b>{banReason}</b>
      {post &&
        thread &&
        <span>&nbsp;in thread&nbsp;
          <Link to={`/thread/${thread.id}/${post.page}#post-${post.id}`}>
            <b>{threadTitle}</b>
          </Link>
          </span>}
      .
    </div>
    <div>
      Banned by <b>{bannedByName}</b> for <b>{duration}</b>
.
</div>
    <div>
      Ban is <b>{banStillValid ? `still active` : `no longer active`}</b>
.
</div>
    {banStillValid && isMod && (
      <button type="button" onClick={invalidateBan}>
        Unban
      </button>
    )}
  </StyledBan>
);

export default BanItem;

BanItem.propTypes = {
  banReason: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  banIcon: PropTypes.string.isRequired,
  duration: PropTypes.string.isRequired,
  bannedByName: PropTypes.string.isRequired,
  threadTitle: PropTypes.string.isRequired,
  banStillValid: PropTypes.bool.isRequired,
  isMod: PropTypes.bool.isRequired,
  invalidateBan: PropTypes.func.isRequired,
  thread: PropTypes.string,
  post: PropTypes.string,
};

BanItem.defaultProps = {
  thread: undefined,
  post: undefined,
};

const StyledBan = styled.div`
  background: #f44336;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  border-radius: 5px;
  text-align: center;
  transition: background 200ms ease-in-out;

  display: inline-block;
  vertical-align: top;
  width: calc(25% - 39px);
  height: 350px;
  margin: 0 10px 10px 10px;

  position: relative;
  overflow: hidden;

  &:nth-child(4n) {
    margin-right: 0;
  }
  &:nth-child(4n - 7) {
    margin-left: 0;
  }

  div {
    margin: 10px auto;
    transition: filter 200ms ease-in-out;
  }

  button {
    position: absolute;
    bottom: 0;
    width: 100%;
    height: 10%;
    background: #b53329;
    color: #ff9a92;
    border: none;
    left: 0;
    right: 0;

    &:hover {
      cursor: pointer;
      filter: brightness(1.1);
    }
  }

  a {
    text-decoration: underline;
  }

  .date {
    position: absolute;
    top: -40px;
    left: 0;
    height: 30px;
    width: 100%;
    text-align: center;
    background: #f44336;
    border-bottom: 2px solid #b53329;
    z-index: 2;
    margin-top: 0;
    line-height: 30px;
    transition: transform 200ms ease-in-out;
  }

  &:hover {
    background: #322a29;

    .date {
      transform: translateY(40px);
    }

    div:not(:first-child),
    img {
      filter: grayscale(1);
    }
  }

  @media (max-width: 960px) {
    width: 100%;
    max-width: 240px;
    box-sizing: border-box;

    margin: 0;
    margin-bottom: 10px;

    &:nth-child(4n) {
      margin-right: unset;
    }
    &:nth-child(4n - 7) {
      margin-left: unset;
    }
  }
`;
