/* eslint-disable react/forbid-prop-types */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Post from '../../../components/Post';
import PaginationNoLink from '../../../components/Pagination/PaginationNoLink';

class UserProfilePosts extends React.Component {
  state = {};

  render() {
    const { posts, user, loading, pageChangeFn } = this.props;

    if (loading && posts.posts.length === 0) {
      return (
        <StyledLatestPosts>
          <i className="fas fa-truck-loading" />
          &nbsp;Loading...
        </StyledLatestPosts>
      );
    }

    return (
      <StyledLatestPosts>
        <PaginationNoLink
          pageChangeFn={pageChangeFn}
          totalPosts={posts.totalPosts}
          currentPage={posts.currentPage}
          pageSize={40}
          marginBottom
        />
        {posts.posts.map(post => (
          <Post
            key={post.id}
            hideUserWrapper
            hideControls
            user={user}
            thread={post.thread.id || post.thread}
            postId={post.id}
            postBody={post.content}
            postDate={post.createdAt}
            postPage={post.page}
            username={user.username}
            userJoinDate={user.userJoinDate}
            byCurrentUser
            threadPage={1}
          />
        ))}
        <PaginationNoLink
          pageChangeFn={pageChangeFn}
          totalPosts={posts.totalPosts}
          currentPage={posts.currentPage}
          pageSize={40}
        />
      </StyledLatestPosts>
    );
  }
}

UserProfilePosts.propTypes = {
  posts: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  pageChangeFn: PropTypes.func.isRequired
};

export default UserProfilePosts;

const StyledLatestPosts = styled.div``;
