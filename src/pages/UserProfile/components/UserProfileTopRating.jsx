import React from 'react';

import ratingList from '../../../components/Rating/ratingList.json';
import Tooltip from '../../../components/Tooltip';
import { UserProfileTopRatingWrapper } from './style';

const getTopRating = ratingsArray =>
  ratingsArray.reduce((acc, val) => {
    if (!acc.name) {
      return val;
    }

    if (val.count > acc.count) {
      return val;
    }

    return acc;
  }, {});

const UserProfileTopRating = ({ topRatings }) => {
  if (!topRatings || topRatings.length === 0) {
    return null;
  }

  try {
    const topRating = getTopRating(topRatings);
    const ratingInfo = ratingList[topRating.name];

    return (
      <UserProfileTopRatingWrapper>
        <Tooltip text={ratingInfo.name} top={false}>
          <img
            src={ratingInfo.url}
            alt={ratingInfo.name}
            style={{
              filter: 'drop-shadow(1px 1px 3px black)'
            }}
          />
        </Tooltip>
      </UserProfileTopRatingWrapper>
    );
  } catch (error) {
    return null;
  }
};

export default UserProfileTopRating;
