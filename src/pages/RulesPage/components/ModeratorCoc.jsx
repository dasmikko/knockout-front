import React from 'react';

const ModeratorCoc = () => (
  <article>
    <h2>Moderator Code of Conduct</h2>
    <i>
      This is a copy of the guide new/existing moderators are sent to serve as their guidelines. It
      is shared publicly to help people understand what rules mods operate under.
    </i>

    <div>
      <strike>Sorry for</strike>
      &nbsp;congratulations on your appointment as a Moderator! Thank you for dedicating your free
      time to make the forums better.
    </div>

    <br />

    <img
      src="https://cdn.knockout.chat/assets/moderator-guide.jpg"
      alt="Moderator Guide"
      style={{ maxWidth: '100%', maxHeight: '600px' }}
    />

    <h3>The Vision</h3>
    <i>In which Inacio tries to explain what the forums should be like</i>

    <p>
      As moderators of a semi-active, not-comically-small online community, and considering how we
      have moderators of radically different backgrounds and life stories, it’s safe to say that at
      some point you will see someone post something that you disagree with / offends you
      (non-personally) / etc.
    </p>

    <p>
      I feel that it’s important to say that my vision for Knockout does not include moderators
      acting as thought-police. As long as people are discussing on-topic, respectfully and without
      breaking any other rules, they should be free to share their dumb, shitty opinions. Users will
      naturally rate them dumb, reply to them saying they’re idiots and so on if that is the case.
      If all mods ban people because their opinions are not in line with their own we’ll soon have
      to ban everyone, since we all have radically different views. I don’t want Knockout to turn
      into radical lefty gaming forums or radical right wing racist nazi incel imageboard boards.
      Echo chambers don’t make for good discussion environments, in my opinion.
    </p>

    <p>
      Users should always be respectful, never attack other users, not incite violence, but should
      attempt to win an argument through rational and objective arguments instead of resorting to
      name calling. If you want to say gamerwords there are other places. As a certified Enlightened
      Radical Centrist™ community we won’t accept any of that shit.
    </p>

    <h3>Duties and Responsibilities</h3>
    <i>
      All Moderators are expected to join the Official Knockout Discord server. This is a vital part
      of being able to discuss concerns or issues with the Moderation Team.
    </i>

    <p>Moderators must respect the hierarchy:</p>

    <ol>
      <li>Inacio</li>
      <ul>
        <li>Inacio</li>
      </ul>

      <li>Lead Moderator</li>
      <ul>
        <li>UncleJimmema </li>
      </ul>
    </ol>

    <p>
      Moderators in their probationary period will be assigned to a Mentor. When the Mentor is not
      available, the probationary Moderator should seek advice from other moderators. Failing that,
      they should discuss the matter in the Moderator Discord.
    </p>

    <p>
      Moderators are expected to check reports on a regular basis, ensuring that they do not stack
      and that they are dealt with on a fair and consistent basis.
    </p>

    <p>
      Moderators are expected to communicate with other mods on the mod channel on Discord
      frequently and discuss possible bans and other mod actions with them. Moderators are required
      to notify UncleJimmema and other fellow moderators if they are due to go on an extended leave
      of absence.
    </p>

    <h3>Honesty and Integrity</h3>

    <p>
      Moderators must act with honesty and integrity at all times. They should be sincere and
      truthful, showing courage in doing what they believe to be right.
    </p>

    <p>
      Moderators should not solicit or readily accept the offer of any gift or gratuity that could
      compromise their impartiality. In the interests of transparency, any gifts given must be
      declared to a Senior Moderator.
    </p>

    <p>
      Moderators are accountable to the community. All actions undertaken by Moderators are recorded
      in the Event Log. Moderators should not shy away from criticism, but rather explain their
      rationale when challenged.
    </p>

    <h3>Standards of Behaviour</h3>

    <p>
      Moderators must lead by example in their posts and threads. Users take their cues from how the
      Moderators post.
    </p>
    <p>
      The tone of Moderators’ posts must be suitable for the given situation. Good humour is
      expected, but must also be used appropriately. Remember that whatever post you makes is
      representative of all the mod team, and by extension KO.
    </p>
    <p>
      Bans must always be justifiable and proportionate. Moderators must take into account both the
      member (time on the forum, number of bans, overall quality of posts) and the content of the
      bannable material (are they flaming with 1 vulgar word or are they flaming with multiple
      vulgarities and deragitories?)
    </p>
    <p>
      If you take an issue with a Moderator’s behaviour, please discuss it with them in a
      constructive manner on the moderator discord channel. If you feel the behaviour is beyond
      this, please contact UncleJimmema or Taliyah with your concerns.
    </p>
    <p>
      In regard to situations that expand beyond the confines of the forum, moderators should stop
      engaging within the situation and discuss how to handle it best amongst the other moderators.
      If a definitive answer can not be drawn from this conversation then a senior moderator will
      make the final call as to how to best handle it. This includes dm’s on
      discord/steam/twitter/other social media.
    </p>
    <p>
      While we cannot control what members do outside of the forum we can control what we do.
      Remember that even outside of the forum while running your forum username you are acting as a
      representative of the KO community. While operating under your forum username it is expected,
      though not required, for you to maintain similar conduct as you would on the forums. If
      actions are taken that may damage the reputation of both the mod team and/or KO a discussion
      will be had in the moderator chat about the best course of action in handling such an event.
    </p>
    <p>
      Moderators are expected to treat users with respect, both in public and private conversation.
      Moderators will not talk about other users in any derogatory fashion, and this will be enforced by the Lead Moderator.
    </p>
    <p>
      Remember, we are all a team. We must all work together and be on the same page as much as
      possible. When in doubt, ask questions!
    </p>
  </article>
);

export default ModeratorCoc;
