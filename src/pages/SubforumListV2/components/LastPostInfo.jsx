import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import {
  ThemeHorizontalHalfPadding,
  ThemeMediumTextSize,
  ThemePrimaryTextColor,
  ThemeSmallTextSize,
  ThemeSecondaryTextColor
} from '../../../Theme';
import { TabletMediaQuery } from '../../../components/SharedStyles';
import UserRoleWrapper from '../../../components/UserRoleWrapper';

dayjs.extend(relativeTime);

const LastPostInfo = ({ post, withTitle, vertical, textAlign }) => {
  if (!post || !post.thread || !post.thread.post_count || !post.thread.id || !post.thread.title) {
    return null;
  }

  // FIXME: Current posts per page value is hardcoded
  const lastPage =
    (post && post.thread && post.thread.post_count && Math.ceil(post.thread.post_count / 20)) || 1;

  return (
    <ThreadLink to={`/thread/${post.thread.id}/${lastPage}#post-${post.id}`}>
      {withTitle ? <h3 className="lastpost-title">{post.thread.title}</h3> : ''}
      <div className="lastpost-subtitle" vertical={vertical} textAlign={textAlign}>
        <span className="lastpost-author">
          Last post by&nbsp;
          <UserRoleWrapper user={post.user}>{post.user.username}</UserRoleWrapper>
          &nbsp;
          {dayjs(post.created_at).fromNow()}
        </span>
        <span className="lastpost-author lastpost-pagecount">
          Go to page&nbsp;
          {lastPage}
          &nbsp;➤
        </span>
      </div>
    </ThreadLink>
  );
};

LastPostInfo.propTypes = {
  post: PropTypes.shape({
    thread: PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      post_count: PropTypes.number.isRequired
    })
  }).isRequired,
  withTitle: PropTypes.bool,
  vertical: PropTypes.bool,
  textAlign: PropTypes.string
};

LastPostInfo.defaultProps = {
  withTitle: false,
  vertical: false,
  textAlign: 'initial'
};

export default LastPostInfo;

export const ThreadLink = styled(Link)`
  flex-grow: 1;

  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }

  .lastpost-title {
    color: ${ThemePrimaryTextColor};
    font-size: ${ThemeMediumTextSize};
    line-height: 1.4;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    padding: 0;
  }

  .lastpost-author {
    color: ${ThemeSecondaryTextColor};
    font-size: ${ThemeSmallTextSize};
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    flex-shrink: 1;
    line-height: 1.4;
  }

  .lastpost-subtitle {
    display: flex;
    justify-content: space-between;
    flex-direction: ${props => (props.vertical ? 'column' : 'row')};
    text-align: ${props => props.textAlign || 'left'};
  }

  .lastpost-pagecount {
    font-size: ${ThemeSmallTextSize};
    padding-left: ${ThemeHorizontalHalfPadding};
    flex-shrink: 0;
    z-index: 1;
    color: ${ThemePrimaryTextColor};
  }

  ${TabletMediaQuery} {
    margin-left: ${ThemeHorizontalHalfPadding};
    margin-right: ${ThemeHorizontalHalfPadding};
    margin-bottom: 0;
  }
`;
