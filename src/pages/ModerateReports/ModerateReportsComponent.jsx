/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';

import { DefaultBlueHollowButton } from '../../components/SharedStyles';
import UserGroupRestricted from '../../components/UserGroupRestricted';
import NavbarResponsive from '../Moderation/components/NavbarResponsive';
import Reports from './components/Reports';
import { NoReports, PageWrapper } from './components/style';
import { MODERATOR_GROUPS } from '../../utils/userGroups';

const ModerateReportsComponent = ({
  reports,
  currentReport,
  currReport,
  hasReports,
  navItems,
  goToPreviousReport,
  goToNextReport,
  getOpenReports
}) => {
  const currentReportNumber = currentReport + 1;
  const reportsLength = reports.length;

  if (!hasReports) {
    return (
      <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
        <PageWrapper>
          <NavbarResponsive items={navItems} />
          <NoReports>
            No reports!&nbsp;
            <span role="img" aria-label="emoji">
              💃🎺🎉
            </span>
          </NoReports>
        </PageWrapper>
      </UserGroupRestricted>
    );
  }

  return (
    <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
      <PageWrapper>
        <NavbarResponsive items={navItems} />
        <p>
          Report {currentReportNumber} of {reportsLength}
        </p>
        <DefaultBlueHollowButton onClick={goToPreviousReport}>Previous</DefaultBlueHollowButton>
        <DefaultBlueHollowButton onClick={goToNextReport}>Next</DefaultBlueHollowButton>
        {currReport && (
          <Reports reports={[reports[currentReport]]} getOpenReports={getOpenReports} />
        )}
      </PageWrapper>
    </UserGroupRestricted>
  );
};
ModerateReportsComponent.propTypes = {
  reports: PropTypes.array.isRequired,
  currentReport: PropTypes.number.isRequired,
  currReport: PropTypes.object,
  hasReports: PropTypes.bool.isRequired,
  navItems: PropTypes.array.isRequired,
  goToPreviousReport: PropTypes.func.isRequired,
  goToNextReport: PropTypes.func.isRequired,
  getOpenReports: PropTypes.func.isRequired
};
ModerateReportsComponent.defaultProps = {
  currReport: undefined
};

export default ModerateReportsComponent;
