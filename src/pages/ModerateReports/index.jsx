import React from 'react';

import { getOpenReports } from '../../services/moderation';

import { navItems } from '../Moderation/components/NavItems';
import ModerateReportsComponent from './ModerateReportsComponent';

class ModeratePageReports extends React.Component {
  state = {
    reports: [],
    currentReport: 0
  };

  async componentDidMount() {
    await this.getOpenReports();
  }

  getOpenReports = async () => {
    const reports = await getOpenReports();

    this.setState({ reports, currentReport: 0 });
  };

  goToPreviousReport = () => this.goToReport(-1);

  goToNextReport = () => this.goToReport(1);

  goToReport(num) {
    const { reports, currentReport } = this.state;

    if (reports[currentReport + num]) {
      this.setState({
        currentReport: currentReport + num
      });
    }
  }

  render() {
    const { reports, currentReport } = this.state;

    const currReport = reports && Array.isArray(reports) && reports[currentReport];
    const hasReports = reports && Array.isArray(reports) && reports.length > 0;

    return (
      <ModerateReportsComponent
        reports={reports}
        currentReport={currentReport}
        currReport={currReport}
        hasReports={hasReports}
        navItems={navItems}
        goToNextReport={this.goToNextReport}
        goToPreviousReport={this.goToPreviousReport}
        getOpenReports={getOpenReports}
      />
    );
  }
}

export default ModeratePageReports;
