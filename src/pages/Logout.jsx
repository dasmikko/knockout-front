import React, { Fragment } from 'react';

import { authPost } from '../services/common';
import { removeUserFromStorage } from '../services/user';

class Logout extends React.Component {
  componentDidMount() {
    return authPost({ url: '/auth/logout' }).then(() => {
      removeUserFromStorage();
      window.location.href = '/';
    });
  }

  render() {
    return <Fragment />;
  }
}

export default Logout;
