/* eslint-disable jsx-a11y/media-has-caption */
import React, { useLayoutEffect, useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  ResponsiveContainer,
  LineChart,
  Line,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
} from 'recharts';
import VisibilitySensor from 'react-visibility-sensor';

import usersMonth from './usersMonth.json';
import postsMonth from './postsMonth.json';
import bansMonth from './bansMonth.json';
import moderatorBans from './moderatorBans.json';

import { updateBackgroundRequest } from '../../state/background';
import { scrollToTop } from '../../utils/pageScroll';
import SplashScreen from './SplashScreen';
import { StyledYearReviewWrapper, StyledEndImage } from './styles';
import { wew, CustomizedAxisTick } from './graphComponents';

function useWindowSize() {
  const [size, setSize] = useState([0, 0]);
  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth, window.innerHeight]);
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, []);
  return size;
}

const EndImage = () => {
  const [width] = useWindowSize();
  return <StyledEndImage width={width} src="static/other/2019review/endoffacepunch.png" alt="" />;
};

class Review2019 extends React.Component {
  audioRef = new Audio(
    'https://cdn.knockout.chat/misc/kommsussertod.mp3'
  );

  constructor(props) {
    super(props);
    this.state = { clicked: false };
  }

  componentDidMount() {
    this.props.updateBackground('static/other/2019review/black.png', 'tiled');

    this.audioRef.play();
    this.audioRef.pause();
  }

  componentWillUnmount() {
    this.props.updateBackground(null);
    this.audioRef.pause();
  }

  render() {
    return (
      <StyledYearReviewWrapper>
        {this.state.clicked ? (
          <div>
            <div className="title-card-container">
              <img
                className="title-card"
                src="static/other/2019review/2019reviewTitle.svg"
                alt=""
              />
              <img className="flare1" src="static/other/2019review/flare.svg" alt="" />
              <img className="flare2" src="static/other/2019review/flare.svg" alt="" />
              <img className="flare3" src="static/other/2019review/flare.svg" alt="" />
            </div>

            <div className="half-page" />

            <VisibilitySensor partialVisibility intervalDelay="250">
              {({ isVisible }) => (
                <div className="content">
                  {isVisible ? <img src="static/other/2019review/sinceMarch.svg" alt="" /> : null}
                </div>
              )}
            </VisibilitySensor>

            <div className="half-page" />

            <VisibilitySensor partialVisibility intervalDelay="250">
              {({ isVisible }) => (
                <div className="content">
                  {isVisible ? (
                    <img className="section-img" src="static/other/2019review/threads.svg" alt="" />
                  ) : null}
                </div>
              )}
            </VisibilitySensor>

            <VisibilitySensor partialVisibility intervalDelay="250">
              {({ isVisible }) => (
                <div className="content">
                  {isVisible ? (
                    <ResponsiveContainer width="100%" height={350}>
                      <LineChart
                        width={730}
                        height={250}
                        data={usersMonth}
                        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                      >
                        <XAxis stroke="#ffa700" dataKey="name" />
                        <YAxis stroke="#ffa700" dataKey="value" />
                        <Line type="monotone" dataKey="value" stroke="#82ca9d" />
                      </LineChart>
                    </ResponsiveContainer>
                  ) : null}
                </div>
              )}
            </VisibilitySensor>

            <div className="half-page" />

            <VisibilitySensor partialVisibility intervalDelay="250">
              {({ isVisible }) => (
                <div className="content">
                  {isVisible ? (
                    <img className="section-img" src="static/other/2019review/posts.svg" alt="" />
                  ) : null}
                </div>
              )}
            </VisibilitySensor>
            <VisibilitySensor partialVisibility intervalDelay="250">
              {({ isVisible }) => (
                <div className="content">
                  {isVisible ? (
                    <ResponsiveContainer width="100%" height={350}>
                      <LineChart
                        width={730}
                        height={250}
                        data={postsMonth}
                        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                      >
                        <XAxis stroke="#ffa700" dataKey="name" />
                        <YAxis stroke="#ffa700" dataKey="value" />
                        <Line type="monotone" dataKey="value" stroke="#82ca9d" />
                      </LineChart>
                    </ResponsiveContainer>
                  ) : null}
                </div>
              )}
            </VisibilitySensor>

            <div className="half-page" />

            <VisibilitySensor partialVisibility intervalDelay="250">
              {({ isVisible }) => (
                <div className="content">
                  {isVisible ? (
                    <img className="section-img" src="static/other/2019review/bans.svg" alt="" />
                  ) : null}
                </div>
              )}
            </VisibilitySensor>

            <VisibilitySensor partialVisibility intervalDelay="250">
              {({ isVisible }) => (
                <div className="content">
                  {isVisible ? (
                    <ResponsiveContainer width="100%" height={350}>
                      <LineChart
                        width={730}
                        height={250}
                        data={bansMonth}
                        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                      >
                        <XAxis stroke="#ffa700" dataKey="name" />
                        <YAxis stroke="#ffa700" dataKey="value" />
                        <Line type="monotone" dataKey="value" stroke="#82ca9d" />
                      </LineChart>
                    </ResponsiveContainer>
                  ) : null}
                </div>
              )}
            </VisibilitySensor>

            <div className="half-page" />
            <VisibilitySensor partialVisibility intervalDelay="250">
              {({ isVisible }) => (
                <div className="content">
                  {isVisible ? (
                    <img
                      className="section-img"
                      src="static/other/2019review/killcounts.svg"
                      alt=""
                    />
                  ) : null}
                </div>
              )}
            </VisibilitySensor>

            <VisibilitySensor partialVisibility intervalDelay="250">
              {({ isVisible }) => (
                <div className="content">
                  {isVisible ? (
                    <ResponsiveContainer width="100%" height={350}>
                      <BarChart
                        layout="horizontal"
                        width={730}
                        height={250}
                        data={moderatorBans}
                        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                      >
                        <XAxis
                          stroke="#ffa700"
                          dataKey="name"
                          tick={CustomizedAxisTick}
                          minTickGap={-100}
                        />
                        <YAxis stroke="#ffa700" dataKey="value" />
                        <Tooltip content={wew} />
                        <Bar type="monotone" dataKey="value" stroke="#82ca9d" />
                      </BarChart>
                    </ResponsiveContainer>
                  ) : null}
                </div>
              )}
            </VisibilitySensor>

            <div className="half-page" />
            <div className="half-page" />

            <div className="title-card-container">
              <EndImage />
            </div>
          </div>
        ) : (
          <SplashScreen
            onClick={() => {
              this.setState({ clicked: true });
              this.audioRef.play();
              scrollToTop();
            }}
          />
        )}
      </StyledYearReviewWrapper>
    );
  }
}

Review2019.propTypes = {
  updateBackground: PropTypes.func.isRequired,
};

const mapStateToProps = ({ user, background, mentions }) => ({
  userState: user,
  background,
  mentions: mentions.mentions,
});
const mapDispatchToProps = (dispatch) => ({
  updateBackground: (value, bgType) => dispatch(updateBackgroundRequest(value, bgType)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Review2019);
