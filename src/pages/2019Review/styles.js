import styled, { keyframes } from 'styled-components';

const flareAnim = keyframes`
0%{
  opacity: 0;
}
40%{
  opacity: 0;
}
50% {
  opacity: 1;
}
60%{
  opacity: 0;
}
100%{
  opacity: 0;
}
`;

const titleAnim = keyframes`
0%{
  opacity: 1;
  filter: drop-shadow(0px 0px 30px rgba(255, 255, 255, 1)) drop-shadow(0px -2px 2px rgba(221, 33, 23, 0.7)) drop-shadow(0px 1px 1px #1ca88edb)
      blur(5px);
}
100%{
  opacity: 1;
  filter: drop-shadow(0px -2px 2px rgba(221, 33, 23, 0.7)) drop-shadow(0px 1px 1px #1ca88edb)
      blur(0.6px);
}
`;

export const StyledYearReviewWrapper = styled.article`
  background: black;
  color: white;
  position: relative;

  .recharts-surface {
    overflow: visible;
  }
  .recharts-tooltip-cursor {
    fill: black;
    stroke: #ffa700;
    fill: linear-gradient(
      45deg,
      #bbbbbb 25%,
      #b5b5b5 25%,
      #b5b5b5 50%,
      #bbbbbb 50%,
      #bbbbbb 75%,
      #b5b5b5 75%,
      #b5b5b5 100%
    );
  }

  &:after {
    content: '';
    position: fixed;
    width: 100vw;
    height: 100vh;
    background: black;
    display: block;
    pointer-events: none;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: -1;
  }

  img {
    position: relative;
    width: 95vw;
    filter: drop-shadow(0px -2px 2px rgba(221, 33, 23, 0.7)) drop-shadow(0px 1px 1px #1ca88edb)
      blur(0.6px);
  }

  .nerv-logo {
    width: 50%;
    max-width: 600px;
    margin: 0 auto;
    position: unset;
    filter: unset;
    margin-top: 50px;
    display: block;
  }
  .sound-warning {
    color: #db1616;
    font-family: monospace;
  }
  .begin-btn {
    width: 150px;
    height: 50px;
    background: none;
    border: 2px solid #eaa82a;
    position: relative;
    color: #eaa82a;
    transition: border;
    margin: 0 auto;
    display: block;
    cursor: pointer;
    margin-top: 20px;
    padding: 10px;
    font-family: monospace;

    &:after {
      color: #db1616;
      line-height: 47px;
      content: 'BEGIN';
      display: block;
      position: absolute;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      z-index: 0;
      width: 100%;
      height: 100%;
      opacity: 0;
      font-family: monospace;
    }

    &:hover {
      border: 2px solid #db1616;
      &:after {
        opacity: 1;
      }
    }
  }

  .content {
    position: relative;
    min-height: 300px;

    .section-img {
      position: absolute;
      max-width: 100%;

      top: 0;
      right: 0;

      &:nth-child(2n) {
        right: unset;
        left: 0;
      }
    }
  }

  .title-card-container {
    width: 100%;
    height: 90vh;
    position: relative;
    .title-card {
      max-height: 90vh;
      position: absolute;
      top: 0;
      left: 50%;
      opacity: 0;
      transform: translateX(-50%);
      animation: ${titleAnim} 700ms;
      animation-fill-mode: forwards;
      animation-delay: 400ms;
    }

    .flare1,
    .flare2,
    .flare3 {
      transform: rotate(45deg) scale(2.5);
      position: absolute;
      opacity: 0;

      filter: drop-shadow(0px 0px 15px rgba(255, 255, 255, 0.7))
        drop-shadow(0px -2px 2px rgba(221, 33, 23, 0.3)) drop-shadow(0px 1px 1px #1ca88edb)
        blur(0.6px);

      radialGradient {
        display: none;
      }
    }
  }
  .flare1 {
    top: -30%;
    right: 0%;
    animation: ${flareAnim} 350ms;
    animation-iteration-count: 2;
  }
  .flare2 {
    bottom: -50%;
    left: -60%;
    animation: ${flareAnim} 350ms;
    animation-iteration-count: 2;
    animation-delay: 50ms;
  }
  .flare3 {
    top: -50%;
    left: -60%;
    animation: ${flareAnim} 350ms;
    animation-iteration-count: 2;
    animation-delay: 50ms;
  }

  div.half-page {
    width: 100%;
    height: 50vh;
  }
`;

export const StyledEndImage = styled.img`
  width: ${props => props.width}px!important;
  filter: unset !important;
  left: 50%;
  transform: translateX(calc(-${props => props.width}px / 2));
`;
