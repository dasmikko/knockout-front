/* eslint-disable react/forbid-prop-types */
import React from 'react';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';

import {
  PageTitle,
  SubHeader,
  SubHeaderBackAndTitle,
  SubHeaderBack,
  SubHeaderBackText,
  SubHeaderPaginationAndButtons,
  SubHeaderPagination,
  SubHeaderDropdown,
  SubHeaderDropdownArrow,
  SubHeaderDropdownList,
  SubHeaderDropdownListItem,
  SubHeaderButton,
} from '../../components/SubHeader/style';
import {
  ThreadPageWrapper,
  ThreadPageItem,
  ThreadPageBottomControls,
  ThreadList,
  UnreadPostCount,
  LockedIndicator,
  PinnedIndicator,
  ForumIconWrapper,
  ForumIconAlign,
  ThreadInfoWrapper,
  ThreadTitle,
  ThreadPagination,
  ThreadMetaWrapper,
  ThreadMetaColumn,
} from './components/style';
import Pagination from '../../components/Pagination/Pagination';
import ForumIcon from '../../components/ForumIcon';
import UserRoleWrapper from '../../components/UserRoleWrapper';
import LoggedInOnly from '../../components/LoggedInOnly';
import UserAvatar from '../../components/Avatar';
import ThreadItemPlaceholder from './components/ThreadItemPlaceholder';
import { scrollToTop } from '../../utils/pageScroll';
import ratingList from '../../components/Rating/ratingList.json';
import ThreadTag from './components/ThreadTag';

dayjs.extend(relativeTime);

const SubforumPageComponent = ({ name, threads, totalThreads, currentPage, id, threadsHidden }) => (
  <ThreadPageWrapper>
    <PageTitle>{name}</PageTitle>
    <SubHeader>
      <SubHeaderBackAndTitle>
        <SubHeaderBack to="/" onClick={scrollToTop}>
          &#8249;
          <SubHeaderBackText>Home</SubHeaderBackText>
        </SubHeaderBack>
      </SubHeaderBackAndTitle>
      <SubHeaderPaginationAndButtons>
        <SubHeaderPagination>
          <Pagination
            showNext
            pagePath={`/subforum/${id}/`}
            totalPosts={totalThreads}
            currentPage={currentPage}
            pageSize={40}
          />
        </SubHeaderPagination>
        <LoggedInOnly>
          <SubHeaderDropdown>
            <SubHeaderDropdownArrow className="fa fa-angle-down" />
            <SubHeaderDropdownList>
              <SubHeaderDropdownListItem>
                <SubHeaderButton to={`/thread/new/${id}`}>
                  <i className="fas fa-dumpster-fire">&nbsp;</i>
                  <span>Create new thread</span>
                </SubHeaderButton>
              </SubHeaderDropdownListItem>
            </SubHeaderDropdownList>
          </SubHeaderDropdown>
        </LoggedInOnly>
      </SubHeaderPaginationAndButtons>
    </SubHeader>

    <Helmet>
      <title>{name ? `${name} - Knockout!` : 'Knockout!'}</title>
    </Helmet>

    <ThreadList>
      {threads.map((thread) => {
        // Displays while loading
        if (thread.placeholder) return <ThreadItemPlaceholder thread={thread} />;

        const lastPost = thread.lastPost || { user: {}, thread: { post_count: 1, id: 0 } };
        const lastPage = Math.ceil(thread.postCount / 20);

        // unreadPostCount are subs. readThreadUnreadPosts are reads.
        const unreadPosts = thread.unreadPostCount || thread.readThreadUnreadPosts;

        const topRating = thread.firstPostTopRating;
        const topRatingUrl = topRating && ratingList[topRating.rating].url;

        const threadTag = thread.tags ? Object.values(thread.tags[0])[0] : null;

        return (
          <ThreadPageItem
            key={thread.id + thread.title}
            locked={!!thread.locked}
            pinned={!!thread.pinned}
            deleted={!!thread.deleted_at}
            hasSeenNoNewPosts={thread.hasSeenNoNewPosts}
          >
            <ForumIconWrapper>
              <Link to={`/thread/${thread.id}/1`}>
                <ForumIconAlign>
                  <ForumIcon iconId={thread.icon_id} />
                </ForumIconAlign>
              </Link>
            </ForumIconWrapper>
            <ThreadInfoWrapper>
              <ThreadTitle to={`/thread/${thread.id}/1`}>
                {!!thread.locked && <LockedIndicator title="Locked!" className="fas fa-lock" />}
                {!!thread.pinned && (
                  <PinnedIndicator title="Pinned" className="fas fa-sticky-note" />
                )}
                <span>{thread.title}</span>
                {threadTag && <ThreadTag>{threadTag}</ThreadTag>}
                {unreadPosts > 0 && (
                  <UnreadPostCount
                    unreadType={thread.unreadType}
                    to={`/thread/${thread.id}/${Math.ceil(
                      (thread.postCount - (unreadPosts - 1)) / 20
                    )}${thread.firstUnreadId ? `#post-${thread.firstUnreadId}` : ''}`}
                  >
                    {unreadPosts}
                    &nbsp;
                    {`new ${unreadPosts === 1 ? 'post' : 'posts'}`}
                  </UnreadPostCount>
                )}
              </ThreadTitle>

              <ThreadPagination>
                <UserRoleWrapper user={thread.user}>{thread.user.username}</UserRoleWrapper>
                {thread.postCount > 0 && (
                  <Pagination
                    small
                    pagePath={`/thread/${thread.id}/`}
                    totalPosts={thread.postCount}
                  />
                )}
              </ThreadPagination>
              {topRating && (
                <div className="thread-top-rating">
                  <img src={topRatingUrl} alt="awoo" />
                  <span>x{topRating.count}</span>
                </div>
              )}
            </ThreadInfoWrapper>
            <ThreadMetaWrapper
              title="Go to latest post"
              to={`/thread/${thread.id}/${lastPage}#post-${lastPost.id}`}
            >
              <ThreadMetaColumn>
                <span>
                  <i className="fas fa-comments" />
                  {thread.postCount}
                  <span> posts</span>
                </span>
                <span>
                  <i className="fas fa-clock" />

                  {dayjs(thread.created_at).fromNow().replace('ago', 'old')}
                </span>
              </ThreadMetaColumn>
              <ThreadMetaColumn>
                <span>
                  <UserRoleWrapper user={lastPost.user}>{lastPost.user.username}</UserRoleWrapper>
                </span>
                <span>
                  <i className="fas fa-angle-double-right" />
                  {dayjs(lastPost.created_at).fromNow()}
                </span>
              </ThreadMetaColumn>
              <ThreadMetaColumn>
                <UserAvatar src={thread.lastPost && thread.lastPost.user.avatar_url} />
              </ThreadMetaColumn>
            </ThreadMetaWrapper>
          </ThreadPageItem>
        );
      })}
      {threadsHidden && (
        <p className="threads-hidden-message">
          Some threads were hidden due to your filter settings.
        </p>
      )}
    </ThreadList>

    <ThreadPageBottomControls>
      <Pagination
        showNext
        pagePath={`/subforum/${id}/`}
        totalPosts={totalThreads}
        currentPage={currentPage}
        pageSize={40}
      />
    </ThreadPageBottomControls>
  </ThreadPageWrapper>
);

SubforumPageComponent.propTypes = {
  name: PropTypes.string,
  threads: PropTypes.array,
  totalThreads: PropTypes.number,
  currentPage: PropTypes.number,
  id: PropTypes.number,
  threadsHidden: PropTypes.bool,
};

SubforumPageComponent.defaultProps = {
  name: 'Loading subforum...',
  threads: [],
  totalThreads: 0,
  currentPage: 1,
  id: 1,
  threadsHidden: false,
};

export default SubforumPageComponent;
