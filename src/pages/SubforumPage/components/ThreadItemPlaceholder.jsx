import React from 'react';
import PropTypes from 'prop-types';
import {
  ThreadPageItem,
  ForumIconWrapper,
  ForumIconAlign,
  ThreadInfoWrapper,
  ThreadTitlePlaceholder,
  ThreadUsernamePlaceholder,
  ThreadUserAvatarPlaceholder,
  ThreadPagination,
  ThreadMetaWrapperPlaceholder,
  ThreadMetaColumn
} from './style';
import ForumIcon from '../../../components/ForumIcon';

const ThreadItemPlaceholder = props => (
  <ThreadPageItem key={props.thread.id}>
    <ForumIconWrapper>
      <ForumIconAlign>
        <ForumIcon iconId={props.thread.icon_id} />
      </ForumIconAlign>
    </ForumIconWrapper>
    <ThreadInfoWrapper>
      <ThreadTitlePlaceholder />
      <ThreadPagination>
        <ThreadUsernamePlaceholder />
      </ThreadPagination>
    </ThreadInfoWrapper>
    <ThreadMetaWrapperPlaceholder>
      <ThreadMetaColumn>
        <span>
          <i className="fas fa-comments" />
          <span>0 posts</span>
        </span>
        <span>
          <i className="fas fa-clock" />
          soon
        </span>
      </ThreadMetaColumn>
      <ThreadMetaColumn>
        <ThreadUsernamePlaceholder />
        <span>
          <i className="fas fa-angle-double-right" />
          soon
        </span>
      </ThreadMetaColumn>
      <ThreadMetaColumn>
        <ThreadUserAvatarPlaceholder />
      </ThreadMetaColumn>
    </ThreadMetaWrapperPlaceholder>
  </ThreadPageItem>
);

ThreadItemPlaceholder.propTypes = {
  thread: PropTypes.shape({
    id: PropTypes.number.isRequired,
    icon_id: PropTypes.number.isRequired
  }).isRequired
};

export default ThreadItemPlaceholder;
