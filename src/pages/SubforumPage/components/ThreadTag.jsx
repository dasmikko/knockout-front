import React from 'react';
import styled from 'styled-components';
import { ThemeKnockoutRed } from '../../../Theme';

export const StyledThreadTag = styled.span`
  padding: 3px 6px;
  margin: 0 5px;
  background: ${ThemeKnockoutRed};
  border: none;
  border-radius: 10px;
  color: white;
  font-size: 10px;
  line-height: 10px;
  height: min-content;
  display: inline-block;

  &:hover {
    filter: brightness(1.25);
  }
`;

const ThreadTag = ({ children }) => <StyledThreadTag>{children}</StyledThreadTag>;

export default ThreadTag;
