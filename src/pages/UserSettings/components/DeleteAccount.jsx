import React, { useState } from 'react';
import styled from 'styled-components';
import { pushSmartNotification } from '../../../utils/notification';
import { deleteOwnAccount } from '../../../services/user';

const DeleteAccountWarning = () => {
  const [countdown, setCountdown] = useState({ count: 1, lastClick: null });

  const updateCountdown = () => {
    const currDate = new Date();

    if (!countdown.lastClick || currDate.getTime() - countdown.lastClick > 1000) {
      setCountdown({ count: countdown.count - 1, lastClick: currDate.getTime() });
    } else if (currDate.getTime() - countdown.lastClick < 1000) {
      pushSmartNotification({ error: 'You are going too fast. Please wait.' });
    }
  };

  return (
    <StyledDeleteAccountModal>
      <div>
        <img src="static/icons/mushroom-cloud.png" alt="nuclear bomb goes off" />
        <h2>You are about to delete your account. This action is irreversible.</h2>

        <h4>
          Note that as per our Privacy Policy / Terms of Service, some data required for the proper
          functioning of this site will be retained. Feel free to review the Privacy Policy / Terms
          of Service you agreed to.
        </h4>

        <h4>
          You might also find yourself unable to make new accounts in the future. This is also
          required to prevent abuse of the account deletion system.
        </h4>

        <p>
          Please also consider the negative effects account deletion has on the preservation of
          internet history. You can always log out and never log back in again. Only Redditors
          delete their accounts.
        </p>

        <p>
          In order to give you sufficient time to ponder this carefully and prevent any rash
          decisions, please press the button below at your own pace, but not faster than once a
          second.
        </p>

        {countdown.count > 0 && (
          <>
            <h1>{countdown.count}</h1>

            <StyledDeleteAccountButton background="#ff3d00" onClick={() => updateCountdown()}>
              <i className="fas fa-exclamation-triangle" />
              Yes, I understand the consequences.
            </StyledDeleteAccountButton>
          </>
        )}

        {countdown.count <= 0 && (
          <StyledDeleteAccountButton
            background="#ad1a1a"
            onClick={async () => {
              const result = await deleteOwnAccount();
              pushSmartNotification(result);

              window.location = '/logout';
            }}
            type="button"
          >
            <i className="fas fa-bomb" />
            Delete Account
          </StyledDeleteAccountButton>
        )}
      </div>
    </StyledDeleteAccountModal>
  );
};

const StyledDeleteAccountModal = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: black;
  color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 15px 20%;
  max-width: 100vw;
  box-sizing: border-box;
  line-height: 1.3;

  h2 {
    font-size: 24px;
  }

  h4 {
    margin: 5px 0;
    font-size: 18px;
  }

  img {
    margin: 0 -5px;
  }

  button {
    width: 100%;
    margin: 0 auto;
  }

  @media (max-width: 960px) {
    padding: 15px 25px;

    img {
      display: block;
    }
    h2 {
      display: block;
    }

    button {
      width: 100%;
      max-width: unset;
    }
  }
`;

export const StyledDeleteAccountButton = styled.button`
  display: block;
  position: relative;
  background: ${(props) => props.background || 'black'};
  color: ${(props) => props.color || 'white'};
  border: none;
  border-radius: 5px;
  margin: 9px auto;
  padding: 0 0 0 32px;
  font-size: 13px;
  text-align: center;
  width: 230px;
  height: 32px;
  line-height: 32px;
  cursor: pointer;
  overflow: hidden;

  &:active {
    opacity: 0.9;
  }

  i {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 32px;
    line-height: 32px;
    text-align: center;
    background: rgba(0, 0, 0, 0.1);
  }

  span {
    padding: 0 10px;
  }
`;

export default DeleteAccountWarning;
