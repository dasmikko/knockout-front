/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react';
import styled from 'styled-components';
import {
  ThemePrimaryTextColor,
  ThemeSecondaryTextColor,
  ThemeBackgroundDarker,
  ThemeBackgroundLighter
} from '../../Theme';
import LinkBB from '../../components/KnockoutBB/components/LinkBB';
import QuoteBB from '../../components/KnockoutBB/components/QuoteBB';
import CodeBB from '../../components/KnockoutBB/components/CodeBB';
import VideoBB from '../../components/KnockoutBB/components/VideoBB';
import VimeoBB from '../../components/KnockoutBB/components/VimeoBB';
import YoutubeBB from '../../components/KnockoutBB/components/YoutubeBB';
import TweetBB from '../../components/KnockoutBB/components/TweetBB';
import StrawPollBB from '../../components/KnockoutBB/components/StrawPollBB';
import ImageBB from '../../components/KnockoutBB/components/ImageBB';

const KnockoutBBSyntaxPage = () => (
  <PageWrapper>
    <h2>KnockoutBB: Syntax and how to use</h2>
    <p>
      Knockout utilises a form of BBCode (Bulletin Board Code) used to format posts.
      <br />
      This page will provide examples of each of the BBCode tags that Knockout supports and how to
      use them.
    </p>

    <table>
      <tbody>
        <tr>
          <td>
            <b>
              <strong>Tag type</strong>
            </b>
          </td>
          <td>
            <strong>KnockoutBB Example</strong>
          </td>
          <td>
            <strong>KnockoutBB Output</strong>
          </td>
        </tr>
        <tr>
          <td>
            <b>Bold text</b>
          </td>
          <td>[b]Bold text[/b]</td>
          <td>
            <strong>Bold text</strong>
          </td>
        </tr>
        <tr>
          <td>
            <b>Italic text</b>
          </td>
          <td>[i]Italic text[/i]</td>
          <td>
            <i>Italic text</i>
          </td>
        </tr>
        <tr>
          <td>
            <b>Underlined text</b>
          </td>
          <td>[u]Underlined text[/u]</td>
          <td>
            <u>Underlined text</u>
          </td>
        </tr>
        <tr>
          <td>
            <b>Striked through text</b>
          </td>
          <td>[s]Striked through text[/s]</td>
          <td>
            <s>Striked through text</s>
          </td>
        </tr>
        <tr>
          <td>
            <b>Spoiler</b>
          </td>
          <td>[spoiler]Hello[/spoiler]</td>
          <td>
            <span className="spoiler-example">Hello</span>
          </td>
        </tr>
        <tr>
          <td>
            <b>Heading 1</b>
            <hr />
            Very large text
          </td>
          <td>[h1]Very large text[/h1]</td>
          <td>
            <h1>Very large text</h1>
          </td>
        </tr>
        <tr>
          <td>
            <b>Heading 2</b>
            <hr />
            Large text
          </td>
          <td>[h2]Large text[/h2]</td>
          <td>
            <h2>Large text</h2>
          </td>
        </tr>
        <tr>
          <td>
            <b>URL aka Hyperlink</b>
            <hr />
            This tag has multiple attributes
            <hr />
            href: allows you to set the url and change the links text
            <br />
            Smart: turns the url into a smart link
          </td>
          <td>
            [url]https://knockout.chat[/url]
            <hr />
            [url href=&quot;https://knockout.chat&quot;]This is a link[/url]
            <hr />
            [url smart]https://knockout.chat[/url]
          </td>
          <td>
            <LinkBB href="https://knockout.chat/" />
            <hr />
            <LinkBB href="https://knockout.chat/">This is a link</LinkBB>
            <hr />
            <LinkBB isSmart href="https://knockout.chat/" />
          </td>
        </tr>
        <tr>
          <td>
            <b>Quote</b>
            <hr />
            For quoting other users posts
            <br />
            Use the reply button to automatically add to your post
          </td>
          <td>[quote]Created when replying to another user[/quote]</td>
          <td>
            <QuoteBB mentionsUser="1" postId="1" threadPage="1" threadId="1" username="Inacio">
              It is live!
            </QuoteBB>
          </td>
        </tr>
        <tr>
          <td>
            <b>Blockquote</b>
            <hr />
            For quoting external sources
          </td>
          <td>
            [blockquote]Quote block for citing information from an external source[/blockquote]
          </td>
          <td>
            <blockquote>Quote block for citing information from an external source</blockquote>
          </td>
        </tr>
        <tr>
          <td>
            <b>Code</b>
            <hr />
            Used for formatting programming code
            <br />
            Use language variable to define
            <br />
            Defaults to javascript
          </td>
          <td>
            {`[code language="cpp"]#include <iostream>
            int main() {
                std::cout << "Hello, world!\n";
          }[/code]`}
          </td>
          <td>
            <CodeBB language="cpp">
              {`#include <iostream>int main() {
              std::cout << "Hello, world! ";
            }`}
            </CodeBB>
          </td>
        </tr>
        <tr>
          <td>
            <b>Lists</b>
            <hr />
            This tag has multiple attributes
            <hr />
            ol: ordered lists
            <br />
            ul: unordered lists
            <br />
            li: list items
          </td>
          <td>
            [ol][li]First[/li][li]Second[/li][li]Third[/li][/ol]
            <hr />
            [ul][li]First[/li][li]Second[/li][li]Third[/li][/ul]
          </td>
          <td>
            <ol>
              <li>First</li>
              <li>Second</li>
              <li>Third</li>
            </ol>
            <hr />
            <ul>
              <li>First</li>
              <li>Second</li>
              <li>Third</li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            <b>Image</b>
            <hr />
            This tag has multiple attributes
            <hr />
            Thumbnail: thumbnails the image
            <hr />
            Link: Turns the image into a link to the image
          </td>
          <td>
            [img]https://knockout.chat/uwupunchy.png[/img]
            <hr />
            [img thumbnail]https://knockout.chat/uwupunchy.png[/img]
            <hr />
            [img link]https://knockout.chat/uwupunchy.png[/img]
          </td>
          <td>
            <details>
              <ImageBB href="static/logo.svg" />
            </details>
            <hr />
            <details>
              <ImageBB href="static/logo.svg" thumbnail />
            </details>
            <hr />
            <details>
              <ImageBB href="static/logo.svg" link />
            </details>
          </td>
        </tr>
        <tr>
          <td>
            <b>Video embed</b>
            <hr />
            Only for Webm &amp; MP4, must be directly linked
          </td>
          <td>
            [video]https://test-videos.co.uk/vids/bigbuckbunny/webm/vp8/360/Big_Buck_Bunny_360_10s_1MB.webm[/video]
          </td>
          <td>
            <details>
              <VideoBB href="https://test-videos.co.uk/vids/bigbuckbunny/webm/vp8/360/Big_Buck_Bunny_360_10s_1MB.webm" />
            </details>
          </td>
        </tr>
        <tr>
          <td>
            <b>Video services</b>
            <hr />
            Applies to only Youtube and Vimeo
          </td>
          <td>
            [youtube]https://www.youtube.com/watch?v=onzV-wmrYoA[/youtube]
            <hr />
            [vimeo]https://vimeo.com/1084537[/vimeo]
          </td>
          <td>
            <details>
              <YoutubeBB href="https://www.youtube.com/watch?v=gVEdQJ7qtJw" />
            </details>
            <br />
            <details>
              <VimeoBB href="https://vimeo.com/1084537" />
            </details>
          </td>
        </tr>
        <tr>
          <td>
            <b>Twitter</b>
          </td>
          <td>
            [twitter]https://mobile.twitter.com/Moristiko/status/1047567156886392833[/twitter]
          </td>
          <td>
            <details>
              <TweetBB href="https://twitter.com/Moristiko/status/1047567156886392833" />
            </details>
          </td>
        </tr>
        <tr>
          <td>
            <b>Strawpoll</b>
          </td>
          <td>[strawpoll]https://www.strawpoll.me/19289198[/strawpoll]</td>
          <td>
            <details>
              <StrawPollBB href="https://www.strawpoll.me/19289198" />
            </details>
          </td>
        </tr>
      </tbody>
    </table>
  </PageWrapper>
);

export default KnockoutBBSyntaxPage;

const PageWrapper = styled.section`
  padding: 0 15px;
  border-radius: 0 0 5px 5px;
  margin: 0 auto;
  color: ${ThemePrimaryTextColor};

  line-height: 1.3;

  p {
    margin-bottom: 15px;
    line-height: 1.5;
  }

  ul,
  ol {
    padding-left: 20px;
  }

  li {
    margin-bottom: 5px;
  }

  li a {
    color: #3facff;
  }

  h2 {
    font-weight: 100;
    font-size: 28px;
    margin-top: 25px;
    margin-bottom: 10px;
    color: ${ThemeSecondaryTextColor};

    &:nth-child(1) {
      margin-top: 0;
    }
  }

  h3 {
    font-weight: 100;
    font-size: 22px;
    margin-top: 25px;
    margin-bottom: 10px;
    color: ${ThemeSecondaryTextColor};
  }

  tr {
    padding: 5px;
    background: ${ThemeBackgroundDarker} none repeat scroll 0% 0%;
    vertical-align: middle;
  }
  tr:first-of-type {
    background: ${ThemeBackgroundLighter} none repeat scroll 0% 0%;
  }
  tr:first-of-type td {
    padding: 10px;
    text-align: center;
  }
  tr td {
    border: 1px solid ${ThemeBackgroundLighter};
    vertical-align: middle;
    padding: 10px 10px;

    border-bottom-color: #3e3e3e;

    overflow: auto;
    overflow-wrap: break-word;
  }
  tr td hr {
    border: none;
    border-bottom: 1px solid rgba(255, 255, 255, 0.05);
  }
  .spoiler-example {
    background: rgb(255, 255, 255) none repeat scroll 0% 0%;
    color: rgb(255, 255, 255);
  }
  .spoiler-example:hover {
    background: transparent;
  }

  table,
  tbody {
    width: 100%;
  }

  @media (max-width: 960px) {
    tr {
      display: flex;
      flex-direction: column;
      max-width: 100vw;
      box-sizing: border-box;
      margin-bottom: 10px;
      border-radius: 5px;

      td {
        border-color: transparent transparent rgba(255, 255, 255, 0.05) transparent;
        &:last-child {
          border-bottom-color: transparent;
        }
      }
    }
  }
`;
