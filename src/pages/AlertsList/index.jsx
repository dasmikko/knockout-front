import React from 'react';
import dayjs from 'dayjs';
import { Helmet } from 'react-helmet';
import relativeTime from 'dayjs/plugin/relativeTime';
import { HashLink as Link } from 'react-router-hash-link';

import {
  ThreadPageItem,
  ThreadList,
  UnreadPostCount,
  LockedIndicator,
  ForumIconWrapper,
  ForumIconAlign,
  ThreadInfoWrapper,
  ThreadTitle,
  ThreadPagination,
  ThreadMetaWrapper,
  ThreadMetaColumn,
} from '../SubforumPage/components/style';

import {
  SubHeader,
  SubHeaderBackAndTitle,
  SubHeaderTitle,
  SubHeaderBack,
} from '../../components/SubHeader/style';

import Pagination from '../../components/Pagination/Pagination';
import ForumIcon from '../../components/ForumIcon';
import UserRoleWrapper from '../../components/UserRoleWrapper';
import { DeleteAlertButton, ThreadItemWrapper } from './components/style';

import { getAlerts, deleteAlertRequest } from '../../services/alerts';
import { isLoggedIn } from '../../components/LoggedInOnly';
import UserAvatar from '../../components/Avatar';

dayjs.extend(relativeTime);

// TODO: Refactor this entire thing. It works (sort of)
// but is absolutely disgusting
class AlertsList extends React.Component {
  constructor() {
    super();
    this.state = {
      alerts: [],
    };
  }

  componentDidMount = async () => {
    await this.fetchAlerts();
  };

  fetchAlerts = async () => {
    const alerts = await getAlerts();

    if (alerts) {
      this.setState({ alerts });
    }
  };

  render() {
    if (!isLoggedIn()) {
      return null;
    }

    return (
      <div>
        <Helmet>
          <title>Subscriptions - Knockout!</title>
        </Helmet>

        <SubHeader>
          <SubHeaderBackAndTitle>
            <SubHeaderBack to="/">&#8249;</SubHeaderBack>
            <SubHeaderTitle>Subscriptions</SubHeaderTitle>
          </SubHeaderBackAndTitle>
        </SubHeader>

        <ThreadList>
          {this.state.alerts.map((thread) => {
            const lastPost = thread.lastPost || { thread: { post_count: 1, id: 0 } };
            const lastPage =
              (lastPost.thread &&
                lastPost.thread.post_count &&
                Math.ceil(lastPost.thread.post_count / 20)) ||
              1;

            return (
              <ThreadItemWrapper key={thread.thread_id}>
                <ThreadPageItem
                  to={`/thread/${thread.threadId}/1`}
                  key={thread.threadId + thread.threadTitle}
                  locked={thread.threadLocked === 1}
                  deleted={!!thread.deleted}
                >
                  <ForumIconWrapper>
                    <Link to={`/thread/${thread.threadId}/1`}>
                      <ForumIconAlign>
                        <ForumIcon iconId={thread.icon_id} />
                      </ForumIconAlign>
                    </Link>
                  </ForumIconWrapper>
                  <ThreadInfoWrapper>
                    <ThreadTitle to={`/thread/${thread.threadId}/1`}>
                      {!!thread.locked && (
                        <LockedIndicator title="Locked!" className="fas fa-lock" />
                      )}
                      <span>{thread.threadTitle}</span>
                      {thread.unreadPosts > 0 && (
                        <UnreadPostCount
                          to={`/thread/${thread.threadId}/${Math.ceil(
                            (thread.threadPostCount - (thread.unreadPosts - 1)) / 20
                          )}${thread.firstUnreadId ? `#post-${thread.firstUnreadId}` : ''}`}
                        >
                          {thread.unreadPosts}
                          &nbsp;
                          {`new ${thread.unreadPosts === 1 ? 'post' : 'posts'}`}
                        </UnreadPostCount>
                      )}
                    </ThreadTitle>

                    <ThreadPagination>
                      {thread.threadPostCount > 0 && (
                        <Pagination
                          small
                          pagePath={`/thread/${thread.threadId}/`}
                          totalPosts={thread.threadPostCount}
                        />
                      )}
                    </ThreadPagination>
                  </ThreadInfoWrapper>
                  <ThreadMetaWrapper
                    title="Go to latest post"
                    to={`/thread/${lastPost.thread.id}/${lastPage}#post-${lastPost.id}`}
                  >
                    <ThreadMetaColumn>
                      <span>
                        <i className="fas fa-comments" />
                        {thread.threadPostCount}
                        <span> replies</span>
                      </span>
                      <span>
                        <i className="fas fa-clock" />

                        {dayjs(thread.threadCreatedAt).fromNow().replace('ago', 'old')}
                      </span>
                    </ThreadMetaColumn>
                    <ThreadMetaColumn>
                      <span>
                        <UserRoleWrapper user={lastPost.user}>
                          {lastPost.user.username}
                        </UserRoleWrapper>
                      </span>
                      <span>
                        <i className="fas fa-angle-double-right" />
                        {dayjs(lastPost.created_at).fromNow()}
                      </span>
                    </ThreadMetaColumn>
                    <ThreadMetaColumn>
                      <UserAvatar src={thread.lastPost && thread.lastPost.user.avatar_url} />
                    </ThreadMetaColumn>
                  </ThreadMetaWrapper>
                </ThreadPageItem>
                <DeleteAlertButton
                  onClick={async () => {
                    await deleteAlertRequest({ threadId: thread.thread_id });
                    await this.fetchAlerts();
                  }}
                >
                  <i className="fas fa-eraser" />
                </DeleteAlertButton>
              </ThreadItemWrapper>
            );
          })}
        </ThreadList>
      </div>
    );
  }
}

export default AlertsList;
