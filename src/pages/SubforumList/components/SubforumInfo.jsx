import React from 'react';
import PropTypes from 'prop-types';
import LastPostInfo from '../../../components/LastPostInfo';
import { SubforumCenteredMessage, StyledSubforumInfo } from './style';
import SubforumStats from './SubforumStats';

const SubforumInfo = ({ subforum, color }) => {
  if (subforum.lastPost) {
    return (
      <StyledSubforumInfo>
        <LastPostInfo post={subforum.lastPost} withTitle />
        <SubforumStats subforum={subforum} color={color} />
      </StyledSubforumInfo>
    );
  }
  return (
    <SubforumCenteredMessage>
      No posts found
      <span role="img" aria-label="sad face">
        😞
      </span>
    </SubforumCenteredMessage>
  );
};

SubforumInfo.propTypes = {
  subforum: PropTypes.shape({
    lastPost: PropTypes.shape({
      user: PropTypes.shape({
        username: PropTypes.string.isRequired,
        usergroup: PropTypes.number.isRequired
      }).isRequired
    }),
    name: PropTypes.string.isRequired,
    totalThreads: PropTypes.number.isRequired,
    totalPosts: PropTypes.number.isRequired
  }).isRequired,
  color: PropTypes.string.isRequired
};

export default SubforumInfo;
