import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { ThemeMediumTextSize, ThemeSmallTextSize, ThemeSecondaryTextColor } from '../../../Theme';
import LinkBox from '../../../components/LinkBox';

const getThreadCount = subforum => {
  if (subforum.totalThreads > 999) {
    return `${Math.floor(subforum.totalThreads / 1000)}k`;
  }
  return subforum.totalThreads;
};

const getPostCount = subforum => {
  if (subforum.totalPosts > 999) {
    return `${Math.floor(subforum.totalPosts / 1000)}k`;
  }
  return subforum.totalPosts;
};

const StatValue = styled.span`
  line-height: 1.4;
  padding: 0;
  color: ${props => props.color || '#ccc'};
  font-size: ${ThemeMediumTextSize};
  text-align: center;
  font-weight: bold;
`;

const StatLabel = styled.span`
  line-height: 1.4;
  font-size: ${ThemeSmallTextSize};
  color: ${ThemeSecondaryTextColor};
  text-align: center;
`;

const StyledSubforumStats = styled.span`
  border-left: 1px solid #636363;
  height: 38px;
  display: flex;
  flex-direction: column;
  margin-top: auto;
  margin-bottom: auto;
  width: 83px;
`;

const SubforumStats = ({ subforum, color }) => (
  <StyledSubforumStats>
    <LinkBox to={`/subforum/${subforum.id}`}>
      <StatValue color={color}>{getThreadCount(subforum)}</StatValue>&nbsp;
      <StatLabel>{subforum.totalThreads === 1 ? 'thread' : 'threads'}</StatLabel>
    </LinkBox>
    <LinkBox to={`/subforum/${subforum.id}`}>
      <StatValue color={color}>{getPostCount(subforum)}</StatValue>&nbsp;
      <StatLabel>{subforum.totalPosts === 1 ? 'post' : 'posts'}</StatLabel>
    </LinkBox>
  </StyledSubforumStats>
);

SubforumStats.propTypes = {
  subforum: PropTypes.shape({
    lastPost: PropTypes.shape({
      user: PropTypes.shape({
        username: PropTypes.string.isRequired,
        usergroup: PropTypes.number.isRequired
      }).isRequired
    }),
    name: PropTypes.string.isRequired,
    totalThreads: PropTypes.number.isRequired,
    totalPosts: PropTypes.number.isRequired
  }).isRequired,
  color: PropTypes.string.isRequired
};

export default SubforumStats;
