import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { buttonHoverBrightness, TabletMediaQuery, SmallDesktopMediaQuery } from '../../../components/SharedStyles';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeHorizontalHalfPadding,
  ThemeVerticalHalfPadding,
  ThemeLargeTextSize,
  ThemeMediumTextSize,
  ThemeSecondaryBackgroundColor,
  ThemeTertiaryBackgroundColor,
  ThemeSecondaryTextColor,
  ThemeTertiaryTextColor
} from '../../../Theme';

export const DowntimeNotice = styled.div`
  background: ${ThemeSecondaryBackgroundColor};
  color: ${ThemeTertiaryTextColor};
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  padding: 0;
  border-radius: 5px;
  overflow: hidden;

  i {
    display: inline-block;
    color: red;
    width: 100%;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    background: ${ThemeTertiaryBackgroundColor};
  }

  p {
    display: inline-block;
    margin: 0;
    line-height: 1.4;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  }
`;

export const SubforumListTitle = styled.h2`
  font-weight: bold;
  margin-bottom: 15px;
  font-size: 26px;
  display: block;
  width: 100%;
`;

export const SubforumListWrapper = styled.section`
  display: flex;
  flex-direction: row-reverse;
  width: 100%;
  margin-top: 5px;

  ${TabletMediaQuery} {
    flex-direction: column;
  }
`;

export const SubforumItem = styled.div`
  width: calc(50% - 7.5px);
  min-height: 106px;
  padding: 0;
  margin-bottom: 15px;
  box-sizing: border-box;
  font-size: 18px;
  color: white;
  text-decoration: none;
  position: relative;
  transition: background 0.15s ease-in-out;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
  display: flex;
  flex-direction: column;
  flex-grow: 0;
  align-self: flex-start;
  border-radius: 5px;
  overflow: hidden;

  &:nth-child(2n - 1) {
    margin-right: 15px;
  }

  ${SmallDesktopMediaQuery} {
    width: 100%;
    &:nth-child(2n - 1) {
      margin-right: 0;
    }
  }
`;

export const SubforumItemWrapper = styled.div`
  padding-top: 0;
  padding-bottom: 0;
  padding-left: ${ThemeHorizontalHalfPadding};
  padding-right: ${ThemeHorizontalPadding};
  display: flex;
  flex: 1 1 auto;
  flex-wrap: wrap;
  width: 0;
  height: 100%;

  ${TabletMediaQuery} {
    padding-left: ${ThemeHorizontalPadding};
  }

  ${TabletMediaQuery} {
    width: unset;
  }
`;

export const SubforumItemHeader = styled(Link)`
  display: flex;
  margin: 0;

  padding-top: ${ThemeVerticalPadding};
  padding-left: ${ThemeHorizontalPadding};
  padding-right: 125px;
  padding-bottom: ${ThemeVerticalPadding};

  background: ${ThemeTertiaryBackgroundColor};
  box-shadow: inset 0 1px rgba(0, 0, 0, 0.03), 0 1px rgba(0, 0, 0, 0.03);
  border-bottom: solid 1px ${props => props.color || '#666'};
  color: white;
  text-decoration: none;
  width: 100%;
  box-sizing: border-box;
  overflow: hidden;
  position: relative;
  flex-direction: column;

  &:after {
    content: '';
    width: 100px;
    height: 150%;
    background-image: url(${props => props['background-image']});
    background-repeat: no-repeat;
    background-position: right;
    background-size: contain;
    position: absolute;
    right: ${ThemeHorizontalPadding};
    top: -25%;
    opacity: 0.2;
  }

  ${buttonHoverBrightness}
`;

export const SubforumTitle = styled.h2`
  margin: 0;
  padding: 0;
  color: ${ThemeTertiaryTextColor};
  line-height: 1.4;
  font-size: ${ThemeLargeTextSize};
  font-weight: bold;
  white-space: nowrap;
`;

export const SubforumDescription = styled.h3`
  display: inline-block;
  margin: 0;
  padding: 0;
  line-height: 1.4;
  font-size: ${ThemeMediumTextSize};
  font-weight: 100;
  color: ${ThemeSecondaryTextColor};
  flex-grow: 1;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`;

export const SubforumCenteredMessage = styled.div`
  color: gray;
  text-align: center;
  align-self: center;
  line-height: 1.25;
  font-size: ${ThemeMediumTextSize};
  width: 100%;
  padding: 17px 0;
  background: ${ThemeSecondaryBackgroundColor};

  span {
    padding-left: ${ThemeHorizontalHalfPadding};
  }
`;

export const StyledSubforumInfo = styled.summary`
  flex-grow: 1;
  display: flex;
  flex-direction: row;
  align-items: stretch;
  font-size: 11pt;
  box-sizing: border-box;
  overflow: hidden;
  border-radius: 0 0 5px 5px;
  align-content: stretch;
  padding: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};
  background: ${ThemeSecondaryBackgroundColor};
`;
