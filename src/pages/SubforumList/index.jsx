import React from 'react';

import SubforumListComponent from './SubforumListComponent';

import { getSubforumList } from '../../services/subforums';

const defaultColors = ['#623dd2', '#29d44e', '#f5aa20', '#ef2424', '#61d9fa', '#ef8665'];
const defaultColor = index => defaultColors[index % defaultColors.length];

class SubforumList extends React.Component {
  defaultIcon = 'https://img.icons8.com/color/96/000000/filled-chat.png';

  placeholderSubforum = {
    id: 0,
    name: 'Loading subforums...',
    description: 'Patience is a virtue 🙏'
  };

  placeholderSubforumList = [
    { ...this.placeholderSubforum, id: 1 },
    { ...this.placeholderSubforum, id: 2 },
    { ...this.placeholderSubforum, id: 3 },
    { ...this.placeholderSubforum, id: 4 },
    { ...this.placeholderSubforum, id: 5 },
    { ...this.placeholderSubforum, id: 6 },
    { ...this.placeholderSubforum, id: 7 },
    { ...this.placeholderSubforum, id: 8 },
    { ...this.placeholderSubforum, id: 9 },
    { ...this.placeholderSubforum, id: 10 },
    { ...this.placeholderSubforum, id: 11 },
    { ...this.placeholderSubforum, id: 13 },
    { ...this.placeholderSubforum, id: 14 }
  ];

  constructor(props) {
    super(props);
    this.state = {
      subforums: this.placeholderSubforumList
    };
  }

  async componentDidMount() {
    const subforums = await getSubforumList();
    this.setState({
      subforums: subforums.list
    });
  }

  render() {
    const { subforums } = this.state;

    return (
      <SubforumListComponent
        subforums={subforums}
        defaultColor={defaultColor}
        defaultIcon={this.defaultIcon}
      />
    );
  }
}

export default SubforumList;
