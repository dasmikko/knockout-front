import React from 'react';
import UserGroupRestricted from '../../../components/UserGroupRestricted';
import NavbarResponsive from './NavbarResponsive';
import { Grid, Tile, TileHeader, TileBody, TileBodyCount } from './Dashboard/style';
import { navItems, navItemsSecondRow } from './NavItems';
import { PageWrapper } from './style';
import { getDashboardData } from '../../../services/moderation';
import { MODERATOR_GROUPS } from '../../../utils/userGroups';

class ModeratePage extends React.Component {
  state = {
    dashboardData: {
      threads: 0,
      users: 0,
      posts: 0,
      reports: 0,
      bans: 0
    }
  };

  async componentDidMount() {
    const dashboardData = await getDashboardData();

    this.setState({ dashboardData });
  }

  render() {
    const { threads, users, posts, reports, bans } = this.state.dashboardData;

    return (
      <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
        <PageWrapper>
          <NavbarResponsive items={navItems} />
          <NavbarResponsive items={navItemsSecondRow} />
          <Grid>
            <Tile>
              <TileHeader>New users</TileHeader>
              <TileBody>
                <TileBodyCount>{users}</TileBodyCount>
              </TileBody>
            </Tile>
            <Tile>
              <TileHeader>New threads</TileHeader>
              <TileBody>
                <TileBodyCount>{threads}</TileBodyCount>
              </TileBody>
            </Tile>
            <Tile>
              <TileHeader>New posts</TileHeader>
              <TileBody>
                <TileBodyCount>{posts}</TileBodyCount>
              </TileBody>
            </Tile>
            <Tile>
              <TileHeader>Pending reports</TileHeader>
              <TileBody>
                <TileBodyCount>{reports}</TileBodyCount>
              </TileBody>
            </Tile>
            <Tile>
              <TileHeader>Bans issued</TileHeader>
              <TileBody>
                <TileBodyCount>{bans}</TileBodyCount>
              </TileBody>
            </Tile>
          </Grid>
        </PageWrapper>
      </UserGroupRestricted>
    );
  }
}

export default ModeratePage;
