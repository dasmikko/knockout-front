import styled from 'styled-components';
import { ThemeHorizontalPadding, ThemeVerticalPadding } from '../../../Theme';

export const PageWrapper = styled.div`
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;