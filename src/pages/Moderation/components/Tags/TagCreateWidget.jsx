import React from 'react';
import {
  Panel,
  PanelHeader,
  PanelSearchField,
  PanelSearchButton,
  // PanelBody,
  // PanelBodyPlaceholder
} from '../IpLookup/style';
import { createTag } from '../../../../services/moderation';
import { pushSmartNotification } from '../../../../utils/notification';

class TagCreateWidget extends React.Component {
  state = {
    tagName: '',
    canSubmit: true
  };

  setTagName = e => this.setState({ tagName: e.target.value });

  submitTag = async () => {
    try {
      alert('wew')
      if (this.state.tagName.length <= 3) {
        return pushSmartNotification({ error: 'Tag name too short.' });
      }

      this.setState({ canSubmit: false });

      await createTag(this.state.tagName);

      this.setState({ canSubmit: true, tagName: '' });
    } catch (error) {
      this.setState({ canSubmit: true });
    }
  };

  render() {
    const { canSubmit } = this.state;

    return (
      <Panel>
        <PanelHeader>
          <PanelSearchField
            type="text"
            value={this.state.tagName}
            onChange={this.setTagName}
            placeholder="Tag name"
          />
          {canSubmit && <PanelSearchButton onClick={this.submitTag}>Create</PanelSearchButton>}
        </PanelHeader>
      </Panel>
    );
  }
}

export default TagCreateWidget;
