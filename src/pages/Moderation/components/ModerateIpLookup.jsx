import React from 'react';
import UserGroupRestricted from '../../../components/UserGroupRestricted';
import NavbarResponsive from './NavbarResponsive';
import { Panels } from './IpLookup/style';
import IpLookupPanel from './IpLookup/IpLookupPanel';
import UsernameLookupPanel from './IpLookup/UsernameLookupPanel';
import { navItems } from './NavItems';
import { PageWrapper } from './style';
import { MODERATOR_GROUPS } from '../../../utils/userGroups';

const ModerateIpLookup = () => (
  <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
    <PageWrapper>
      <NavbarResponsive items={navItems} />
      <Panels>
        <IpLookupPanel />
        <UsernameLookupPanel />
      </Panels>
    </PageWrapper>
  </UserGroupRestricted>
);

export default ModerateIpLookup;
