import React from 'react';
import dayjs from 'dayjs';
import { Panel, PanelHeader, PanelSearchField, PanelSearchButton, PanelBody } from './style';
import { getLatestUsers, getIpsByUsername } from '../../../../services/moderation';

const formatDate = date => dayjs(date).format('DD/MM/YYYY');

const Results = ({ results }) => (
  <tbody>
    {results.map(item => (
      <tr key={item.id}>
        <td>{item.id}</td>
        <td>{item.username}</td>
        <td>{formatDate(item.created_at)}</td>
        <td>
          <span>
            <a href={`/user/${item.id}`}>Profile</a>
          </span>
        </td>
      </tr>
    ))}
  </tbody>
);

class UserLookupPanel extends React.Component {
  state = {
    query: '',
    results: []
  };

  async componentDidMount() {
    const users = await getLatestUsers();
    this.setState({
      results: users
    });
  }

  setQuery = e => this.setState({ query: e.target.value });

  search = async () => {
    const query = this.state.query;
    const users = query.length === 0 ? await getLatestUsers() : await getIpsByUsername(query);
    this.setState({
      results: users
    });
  };

  render() {
    return (
      <Panel>
        <PanelHeader>
          <PanelSearchField
            type="text"
            value={this.state.query}
            onChange={this.setQuery}
            placeholder="Search user by username"
          />
          <PanelSearchButton onClick={this.search}>Search</PanelSearchButton>
        </PanelHeader>
        <PanelBody>
          <table>
            <thead>
              <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Creation date</th>
                <th>Actions</th>
              </tr>
            </thead>
            <Results results={this.state.results} />
          </table>
        </PanelBody>
      </Panel>
    );
  }
}

export default UserLookupPanel;
