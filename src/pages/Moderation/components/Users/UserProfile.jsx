/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/button-has-type */
import React from 'react';
import styled from 'styled-components';
import dayjs from 'dayjs';

import {
  UserInfo,
  UserInfoWrapper,
  UserName,
  UserJoinDate,
  UserInfoBackgroundImage,
  BackgroundBottomOverlay,
  BackgroundTopOverlay,
  UserAvatarWrapper,
  UserAvatar
} from '../../Post/style';
import UserRoleWrapper from '../../UserRoleWrapper';
import config from '../../../../config';
import { ThemeBodyWidth, ThemeHorizontalHalfPadding } from '../../../Theme';
import { makeBanInvalid, removeUserImage, addGoldProduct } from '../../../services/moderation';
import { pushSmartNotification } from '../../../utils/notification';
import UserGroupRestricted from '../../UserGroupRestricted';

const renderAvatar = ({ avatarUrl, backgroundUrl, username }) => {
  const url = `${config.apiHost}/image/${avatarUrl}`;
  const title = `${username}'s avatar`;

  return (
    <UserAvatarWrapper>
      <UserAvatar srcSet={url} alt={title} title={title} type="image/webp" />
      <img srcSet="https://i.imgur.com/0smjpkB.jpg" alt={title} />
    </UserAvatarWrapper>
  );
};

const durationInDays = (a, b) => {
  const date1 = new Date(a);
  const date2 = new Date(b);
  return parseInt((date2 - date1) / (1000 * 60 * 60 * 24), 10);
};

const setBanInvalid = async ({ banId, username, banStillValid }) => {
  if (!banStillValid) {
    alert('This ban doesnt seem to be active. Let the devs know and they will try to help.');
    return;
  }

  if (
    window.confirm(
      `Are you sure you want to invalidate ${username}'s ban by setting its year to 2001?`
    )
  ) {
    try {
      const banResponse = await makeBanInvalid(banId);

      pushSmartNotification(banResponse);
    } catch (error) {
      console.error(error);
    }
  }
};

const removeImage = async ({ avatar, background, userId }) => {
  if (!avatar && !background) {
    return;
  }
  try {
    const imageRemovalResponse = await removeUserImage({ avatar, background, userId });

    pushSmartNotification(imageRemovalResponse);
  } catch (error) {
    console.error(error);
  }
};

const UserProfile = ({ id, username, avatar_url, background_url, created_at, usergroup, bans }) => {
  const userJoinDateShort = dayjs(created_at).format('MMM YYYY');
  const userJoinDateLong = dayjs(created_at).format('DD/MM/YYYY');

  return (
    <UserProfileWrapper>
      <UserInfoWrapper style={{ height: '460px' }}>
        <UserInfo>
          {renderAvatar({ avatarUrl: avatar_url, background_url, username })}

          <UserRoleWrapper user={{ usergroup }}>
            <UserName>{username}</UserName>
          </UserRoleWrapper>

          <UserJoinDate title={`Joined ${userJoinDateLong}`}>
            {userJoinDateShort}

            {userJoinDateLong === dayjs().format('DD/MM/YYYY') && ' 🍰'}
          </UserJoinDate>
        </UserInfo>
        <UserInfoBackgroundImage backgroundUrl={`${config.apiHost}/image/${background_url}`}>
          <BackgroundTopOverlay />
          <BackgroundBottomOverlay />
        </UserInfoBackgroundImage>
      </UserInfoWrapper>
      <UserProfileContent>
        <BasicInfoWrapper>
          <BigInfo>{username}</BigInfo>
          <div>
            Joined&nbsp;
            {dayjs(created_at).format('DD/MM/YYYY HH:mm:ss')}&nbsp;({dayjs(created_at).from()})
            <pre>ID: {id}</pre>
          </div>
        </BasicInfoWrapper>

      </UserProfileContent>
    </UserProfileWrapper>
  );
};

export default UserProfile;

const UserProfileWrapper = styled.div`
  width: 100%;
  max-width: ${ThemeBodyWidth};
  margin: 0 auto;
  display: flex;
`;
const UserProfileContent = styled.div`
  display: block;
  padding: 0 ${ThemeHorizontalHalfPadding};
`;
