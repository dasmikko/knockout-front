export const navItems = [
  {
    url: '/moderate',
    title: 'Dashboard',
    icon: 'fas fa-columns'
  },
  {
    url: '/moderate/users',
    title: 'Users',
    icon: 'fas fa-user'
  },
  {
    url: '/moderate/iplookup',
    title: 'Ip lookup',
    icon: 'fas fa-network-wired'
  },
  {
    url: '/moderate/reports',
    title: 'Reports',
    icon: 'fas fa-flag'
  },
  {
    url: '/moderate/tags',
    title: 'Manage Tags',
    icon: 'fas fa-tags'
  }
];

export const navItemsSecondRow = [
];