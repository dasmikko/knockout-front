import React from 'react';
import UserGroupRestricted from '../../../components/UserGroupRestricted';
import NavbarResponsive from './NavbarResponsive';
import { Panels } from './UserLookup/style';
import UserLookupPanel from './UserLookup/UserLookupPanel';
import { navItems } from './NavItems';
import { PageWrapper } from './style';
import { MODERATOR_GROUPS } from '../../../utils/userGroups';

const ModerateUserLookup = () => (
  <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
    <PageWrapper>
      <NavbarResponsive items={navItems} />
      <Panels>
        <UserLookupPanel />
      </Panels>
    </PageWrapper>
  </UserGroupRestricted>
);

export default ModerateUserLookup;
