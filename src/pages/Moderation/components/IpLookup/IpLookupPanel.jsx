import React from 'react';
import {
  Panel,
  PanelHeader,
  PanelSearchField,
  PanelSearchButton,
  PanelBody,
  PanelBodyPlaceholder
} from './style';
import { getUsernamesByIp } from '../../../../services/moderation';

const Results = ({ results }) => (
  <tbody>
    {results.map(item => (
      <tr key={item}>
        <td>{item.id}</td>
        <td>{item.username}</td>
        <td>
          <span>{item.ip_address}</span>
        </td>
      </tr>
    ))}
  </tbody>
);

class IpLookupPanel extends React.Component {
  state = {
    ip: '',
    results: [],
    searched: false
  };

  setIp = e => this.setState({ ip: e.target.value });

  search = async () => {
    const result = await getUsernamesByIp(this.state.ip);
    this.setState({
      results: result,
      searched: true
    });
  };

  render() {
    return (
      <Panel>
        <PanelHeader>
          <PanelSearchField
            type="text"
            value={this.state.ip}
            onChange={this.setIp}
            placeholder="Search user by IP"
          />
          <PanelSearchButton onClick={this.search}>Search</PanelSearchButton>
        </PanelHeader>
        {this.state.searched && (
          <PanelBody>
            <table>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Username</th>
                  <th>Address</th>
                </tr>
              </thead>
              <Results results={this.state.results} />
            </table>
          </PanelBody>
        )}
        {!this.state.searched && (
          <PanelBody>
            <PanelBodyPlaceholder>Please enter a search term above</PanelBodyPlaceholder>
          </PanelBody>
        )}
      </Panel>
    );
  }
}

export default IpLookupPanel;
