import styled from 'styled-components';
import { Link } from 'react-router-dom';
import {
  ThemeVerticalPadding,
  ThemeVerticalHalfPadding,
  ThemeHorizontalPadding,
  ThemeHorizontalHalfPadding,
  ThemeTertiaryBackgroundColor,
  ThemePrimaryTextColor
} from '../../../../Theme';
import { MobileMediaQuery } from '../../../../components/SharedStyles';

export const Wrapper = styled.div`
  position: relative;
  color: ${ThemePrimaryTextColor};
  width: 100%;
`;

export const List = styled.ul`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  list-style: none;
  padding: 0;
  margin: 0;
`;

export const ListItem = styled.li`
  position: relative;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  margin: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};
  background: ${ThemeTertiaryBackgroundColor};
  border-radius: 5px;

  &.active {
    border-bottom: 1px solid white;
  }

  ${MobileMediaQuery} {
    flex-grow: 1;
    flex-basis: 30%;
  }
`;

export const ListItemLink = styled(Link)`
  position: relative;

  span {
    padding-left: ${ThemeHorizontalHalfPadding};
  }
`;
