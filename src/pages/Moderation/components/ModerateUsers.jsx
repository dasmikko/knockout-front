import React from 'react';

import UserGroupRestricted from '../../../components/UserGroupRestricted';
import { getFullUserInfo, getIpsByUsername, getLatestUsers } from '../../../services/moderation';
import { PanelHeader, Panels, PanelSearchButton, PanelSearchField } from './IpLookup/style';
import NavbarResponsive from './NavbarResponsive';
import { navItems } from './NavItems';
import { PageWrapper } from './style';
import UserList from './Users/UserList';
import UserProfile from './Users/UserProfile';
import { MODERATOR_GROUPS } from '../../../utils/userGroups';

class ModerateUsersPage extends React.Component {
  state = {
    users: [],
    userProfile: null
  };

  async componentDidMount() {
    const users = await getLatestUsers();

    this.setState({
      users
    });
  }

  getFullUserInfo = async userId => {
    const userProfile = await getFullUserInfo(userId);
    this.setState({ userProfile });
  };

  getUsersByUsername = async username => {
    const users = await getIpsByUsername(username);
    this.setState({ users });
  };

  render() {
    const { users, userProfile } = this.state;

    return (
      <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
        <PageWrapper>
          <NavbarResponsive items={navItems} />
          <Panels>
            <PanelHeader centerWide>
              <PanelSearchField
                type="text"
                value={this.state.ip}
                onChange={e => this.setState({ userSearch: e.target.value })}
                placeholder="Search user by username"
              />
              <PanelSearchButton onClick={() => this.getUsersByUsername(this.state.userSearch)}>
                Search
              </PanelSearchButton>
            </PanelHeader>
          </Panels>
          {!userProfile && (
            <Panels>
              {users[0] && <UserList users={users} getFullUserInfo={this.getFullUserInfo} />}
            </Panels>
          )}
          {userProfile && <UserProfile {...userProfile} />}
        </PageWrapper>
      </UserGroupRestricted>
    );
  }
}

export default ModerateUsersPage;
