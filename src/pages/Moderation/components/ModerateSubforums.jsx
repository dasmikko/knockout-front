import React from 'react';
import UserGroupRestricted from '../../../components/UserGroupRestricted';
import NavbarResponsive from './NavbarResponsive';
import { navItems } from './NavItems';
import { PageWrapper } from './style';
import { MODERATOR_GROUPS } from '../../../utils/userGroups';

const ModerateSubforums = () => (
  <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
    <PageWrapper>
      <NavbarResponsive items={navItems} />
    </PageWrapper>
  </UserGroupRestricted>
);

export default ModerateSubforums;
