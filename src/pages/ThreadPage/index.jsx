/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { createAlertRequest, deleteAlertRequest } from '../../services/alerts';
import { createReadThreadRequest } from '../../services/readThreads';
import { getThreadWithPosts, getThread, updateThread } from '../../services/threads';
import { changeThreadStatus } from '../../services/moderation';

import { scrollToBottom, scrollIntoView } from '../../utils/pageScroll';
import { pushNotification, pushSmartNotification } from '../../utils/notification';
import { getSubforumList } from '../../services/subforums';
import { loadAutoSubscribeFromStorageBoolean } from '../../services/theme';
import { updateBackgroundRequest } from '../../state/background';
import { updateMention, getMentions } from '../../services/mentions';
import { updateMentions } from '../../state/mentions';

import ThreadPageComponent from './ThreadPage';

class ThreadPage extends React.Component {
  defaultDate = new Date('01/01/1980');

  constructor(props) {
    super(props);

    const { match } = this.props;

    this.state = {
      thread: {
        posts: [],
        user: {
          username: '',
          usergroup: 0,
        },
      },
      showingMoveModal: false,
      moveModalOptions: [],
      loadingPosts: false,
    };

    this.refreshPosts = this.refreshPosts.bind(this);

    this.threadId = match.params.id;
  }

  componentDidMount = async () => {
    this.refreshPosts({ goToPost: true });
  };

  componentWillReceiveProps(nextProps) {
    const { match } = this.props;

    // thread id or page has changed. refetch thread
    if (
      nextProps.match.params.id !== match.params.id ||
      nextProps.match.params.page !== match.params.page
    ) {
      this.refreshPosts({ threadId: nextProps.match.params.id, page: nextProps.match.params.page });
    }

    if (nextProps.match.params.totalPosts !== match.params.totalPosts) {
      pushNotification({
        message: 'Tell the devs this has appeared. threadpage line 66',
        type: 'error',
      });
      // this block is probably unused
      this.refreshPosts({ goToLatest: false });
    }
  }

  componentWillUnmount() {
    const { updateBackground: updateBg } = this.props;

    updateBg(null);
  }

  refreshPosts = async ({
    threadId = this.threadId,
    page = this.props.match.params.page,
    goToLatest = false,
    goToPost = false,
  } = {}) => {
    try {
      this.setState({ loadingPosts: true });
      // this will be used to decide between default param or last page
      let targetPage = page;

      // get the number of the last page
      if (goToLatest) {
        const pureThread = await getThread(threadId);
        const { totalPosts } = pureThread;
        const totalPages = Math.ceil(totalPosts / 20);
        this.props.history.push(`/thread/${threadId}/${totalPages}`);
        targetPage = totalPages;
      }

      // common thread updating logic
      const thread = await getThreadWithPosts(threadId, targetPage);

      const newThread = {
        ...thread,
        posts: [...thread.posts].sort((a, b) => a.id - b.id),
      };

      // handle updating Alerts (subscriptions)
      this.createOrUpdateAlert({
        posts: newThread.posts,
        threadId: newThread.id,
        lastSeen: newThread.subscriptionLastSeen,
        isSubscribedTo: thread.isSubscribedTo,
        goToLatest,
      });

      // handle updating ReadThreads
      this.createOrUpdateReadThreads({
        posts: newThread.posts,
        threadId: newThread.id,
        lastSeen: newThread.readThreadLastSeen,
      });

      // if we have a thread background, let's set it!
      this.props.updateBackground(newThread.threadBackgroundUrl, newThread.threadBackgroundType);

      // sets state with the new thread data
      // calls jumpToPost method
      return this.setState({ thread: newThread, loadingPosts: false, goToLatest, goToPost }, () => {
        setTimeout(() => {
          this.jumpToPost({ goToLatest, goToPost });
          this.markMentionsAsRead();
        }, 300);
      });
    } catch (error) {
      return this.setState({ loadingPosts: false });
    }
  };

  /**
   * @param object single object parameter, contains:
   * @param goToLatest boolean
   * @param goToPost boolean
   */
  jumpToPost = ({ goToLatest, goToPost }) => {
    if (goToLatest) {
      scrollToBottom(1);
    } else if (goToPost) {
      // scroll to anchor in the url
      scrollIntoView(this.props.location.hash);
    }
  };

  getEditor = (param) => {
    if (!this.quickReplyEditor) this.quickReplyEditor = param;
  };

  /**
   * @description handles the creation / updating of an Alerts (subscription) entry
   * @param posts thread post array
   * @param threadId number
   * @param lastSeen optional string, if exists updates instead of creating entry
   * @param goToLatest optional boolean, if exists tries to create a new Alerts entry, depending on the AutoSub user setting
   * @param isSubscribedTo optional boolean, if exists allows for the updating of an existing Alerts entry. (potentially unnecessary?)
   */
  createOrUpdateAlert = async ({ posts, threadId, lastSeen, goToLatest, isSubscribedTo }) => {
    if (!isSubscribedTo && !goToLatest) {
      return;
    }

    if (lastSeen) {
      await this.updateAlert(posts, threadId, lastSeen);
    } else if (goToLatest && loadAutoSubscribeFromStorageBoolean()) {
      await this.createAlert(posts, threadId);
    }
  };

  updateAlert = async (posts, threadId, lastSeen) => {
    if (posts.length > 0) {
      const postDates = posts.map((post) => post.createdAt);
      const newestDate = postDates.reduce(
        (acc, val) => (new Date(acc) > new Date(val) ? new Date(acc) : new Date(val)),
        this.defaultDate
      );
      await createAlertRequest({ threadId, lastSeen: newestDate, previousLastSeen: lastSeen });
    }
  };

  createAlert = async (posts, threadId, showNotification = false) => {
    try {
      // if we're creating a new Alert, the previous last seen date can be very old
      this.updateAlert(posts, threadId, this.defaultDate);

      this.refreshPosts({});

      if (showNotification) {
        pushNotification({ message: 'Success! You are now subscribed.', type: 'success' });
      }
    } catch (error) {
      pushNotification({
        message: 'Oops, we could not subscribe you to this thread. Sorry.',
        type: 'warning',
      });
    }
  };

  /**
   * @description handles the creation / updating of a ReadThreads entry
   * @param posts thread post array
   * @param threadId number
   * @param lastSeen optional string, if exists updates instead of creating entry
   */
  createOrUpdateReadThreads = async ({ posts, threadId, lastSeen }) => {
    if (lastSeen) {
      this.updateReadThread(posts, threadId, lastSeen);
    } else {
      this.createReadThread(posts, threadId);
    }
  };

  updateReadThread = async (posts, threadId, lastSeen) => {
    if (posts.length > 0) {
      const postDates = posts.map((post) => post.createdAt);
      const newestDate = postDates.reduce(
        (acc, val) => (new Date(acc) > new Date(val) ? new Date(acc) : new Date(val)),
        this.defaultDate
      );
      await createReadThreadRequest({ threadId, lastSeen: newestDate, previousLastSeen: lastSeen });
    }
  };

  createReadThread = async (posts, threadId) => {
    try {
      // if we're creating a new ReadThread, the previous last seen date can be very old
      this.updateReadThread(posts, threadId, this.defaultDate);
    } catch (error) {
      console.error('Error creating a read thread entry.');
    }
  };

  deleteAlert = async (threadId) => {
    try {
      await deleteAlertRequest({ threadId });
      pushNotification({ message: 'No longer subscribed to thread.', type: 'success' });

      this.refreshPosts({});
    } catch (e) {
      console.error(e);
    }
  };

  updateThreadStatus = async (threadId, status) => {
    try {
      const actions = {};
      if (status.locked) {
        actions.unlock = true;
      }
      if (status.locked === false) {
        actions.lock = true;
      }
      if (status.deleted) {
        actions.restore = true;
      }
      if (status.deleted === false) {
        actions.delete = true;
      }
      if (status.pinned) {
        actions.unpin = true;
      }
      if (status.pinned === false) {
        actions.pinned = true;
      }

      await changeThreadStatus({ threadId, statuses: actions });
      await this.refreshPosts();
    } catch (err) {
      pushNotification({ message: 'Error!', type: 'error' });
    }
  };

  showMoveModal = async () => {
    const subforums = await getSubforumList();
    const subforumOptions = subforums.list.map((subforum) => ({
      value: subforum.id,
      text: subforum.name,
    }));

    this.setState({ showingMoveModal: true, moveModalOptions: subforumOptions });
  };

  submitThreadMove = async (data) => {
    if (!this.state.thread.id) {
      pushSmartNotification({ error: 'Invalid thread.' });
      throw new Error({ error: 'Invalid thread.' });
    }
    await updateThread({ id: this.state.thread.id, subforum_id: data });

    this.setState(
      { showingMoveModal: false },
      pushNotification({ message: 'Thread moved.', type: 'success' })
    );
  };

  markMentionsAsRead = async () => {
    const { mentions } = this.props;
    const {
      thread: { posts },
    } = this.state;
    if (mentions.length > 0) {
      const mentionsInPage = [];

      for (let i = 0; i < mentions.length; i += 1) {
        const mention = mentions[i];

        if (posts.find((post) => post.id === mention.postId)) {
          mentionsInPage.push(mention.postId);
        }
      }

      if (mentionsInPage.length === 0) {
        return;
      }

      await updateMention(mentionsInPage);
      const newMentions = await getMentions();
      this.props.updateMentionState(newMentions);
      pushNotification({
        message: `${mentionsInPage.length} notifications in this page marked as read.`,
      });
    }
  };

  togglePinned = () =>
    this.updateThreadStatus(this.state.thread.id, { pinned: this.state.thread.pinned });

  toggleLocked = () =>
    this.updateThreadStatus(this.state.thread.id, { locked: this.state.thread.locked });

  toggleDeleted = () =>
    this.updateThreadStatus(this.state.thread.id, { deleted: this.state.thread.deleted });

  deleteAlertFn = () => this.deleteAlert(this.state.thread.id);

  createAlertFn = () => this.createAlert(this.state.thread.posts, this.state.thread.id, true);

  hideModal = () => this.setState({ showingMoveModal: false });

  deferredJumpToPost = () =>
    this.jumpToPost({ goToLatest: this.state.goToLatest, goToPost: this.state.goToPost });

  render() {
    const { thread, showingMoveModal, moveModalOptions, loadingPosts } = this.state;
    const { posts } = thread;
    const {
      location: { hash },
    } = this.props;

    const postId = hash ? hash.replace('#post-', '') : undefined;

    const {
      match: { params },
      userState,
    } = this.props;
    const currentPage = params.page ? params.page : 1;

    return (
      <ThreadPageComponent
        posts={posts}
        thread={thread}
        showingMoveModal={showingMoveModal}
        moveModalOptions={moveModalOptions}
        loadingPosts={loadingPosts}
        currentPage={currentPage}
        togglePinned={this.togglePinned}
        toggleLocked={this.toggleLocked}
        toggleDeleted={this.toggleDeleted}
        showMoveModal={this.showMoveModal}
        deleteAlert={this.deleteAlertFn}
        createAlert={this.createAlertFn}
        refreshPosts={this.refreshPosts}
        quickReplyEditor={this.quickReplyEditor}
        currentUserId={userState.id}
        submitThreadMove={this.submitThreadMove}
        hideModal={this.hideModal}
        getEditor={this.getEditor}
        params={params}
        goToPost={this.deferredJumpToPost}
        linkedPostId={postId}
      />
    );
  }
}

ThreadPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
      page: PropTypes.string,
      totalPosts: PropTypes.string,
    }).isRequired,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
  userState: PropTypes.shape({
    id: PropTypes.number,
  }),
  location: PropTypes.shape({
    hash: PropTypes.string,
  }),
  updateMentionState: PropTypes.func.isRequired,
  updateBackground: PropTypes.func.isRequired,
  mentions: PropTypes.array.isRequired,
};
ThreadPage.defaultProps = {
  userState: { id: undefined },
  location: {
    hash: undefined,
  },
};

const mapStateToProps = ({ user, background, mentions }) => ({
  userState: user,
  background,
  mentions: mentions.mentions,
});
const mapDispatchToProps = (dispatch) => ({
  updateBackground: (value, bgType) => dispatch(updateBackgroundRequest(value, bgType)),
  updateMentionState: (val) => dispatch(updateMentions(val)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ThreadPage));
