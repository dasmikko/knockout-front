import React from 'react';
import PropTypes from 'prop-types';

import {
  SubHeaderDropdownListItem,
  SubHeaderRealButton
} from '../../../components/SubHeader/style';
import LoggedInOnly from '../../../components/LoggedInOnly';
import { usergroupCheck } from '../../../components/UserGroupRestricted';
import { updateThread } from '../../../services/threads';
import { pushNotification } from '../../../utils/notification';
import Tooltip from '../../../components/Tooltip';
import { MODERATOR_GROUPS } from '../../../utils/userGroups';

class EditableBackground extends React.Component {
  state = { backgroundUrl: '' };

  static getDerivedStateFromProps(props, state) {
    return { ...state, backgroundUrl: props.backgroundUrl };
  }

  edit = async () => {
    const newBackground = prompt(
      'Please enter your background url',
      this.state.backgroundUrl || ''
    );
    const { threadId } = this.props;

    try {
      if (newBackground === null) {
        throw new Error({ error: 'Prompt cancelled' });
      }

      await updateThread({ backgroundUrl: newBackground, id: threadId });

      pushNotification({ message: 'Thread background updated. Refresh to see it!' });
    } catch (err) {
      this.setState({ editing: true });
    }
  };

  render() {
    const { byCurrentUser } = this.props;

    let button = null;

    if (usergroupCheck(MODERATOR_GROUPS) || (usergroupCheck([2]) && byCurrentUser)) {
      button = (
        <SubHeaderDropdownListItem>
          <Tooltip text="Change background">
            <SubHeaderRealButton onClick={() => this.edit()}>
              <i className="fas fa-image" />
              <span>Change background</span>
            </SubHeaderRealButton>
          </Tooltip>
        </SubHeaderDropdownListItem>
      );
    } else if (byCurrentUser) {
      button = (
        <SubHeaderDropdownListItem style={{ opacity: 0.5 }}>
          <Tooltip text="Change background (Must be a Gold member)">
            <SubHeaderRealButton>
              <i className="fas fa-image" />
              <span>Change background (Must be a Gold member)</span>
            </SubHeaderRealButton>
          </Tooltip>
        </SubHeaderDropdownListItem>
      );
    }

    return <LoggedInOnly>{button}</LoggedInOnly>;
  }
}

EditableBackground.propTypes = {
  byCurrentUser: PropTypes.bool,
  threadId: PropTypes.number
};
EditableBackground.defaultProps = {
  byCurrentUser: false,
  threadId: 0
};

export default EditableBackground;
