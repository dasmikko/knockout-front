import React from 'react';
import PropTypes from 'prop-types';

import {
  SubHeaderDropdownListItem,
  SubHeaderRealButton
} from '../../../components/SubHeader/style';
import LoggedInOnly from '../../../components/LoggedInOnly';
import { pushNotification } from '../../../utils/notification';
import Tooltip from '../../../components/Tooltip';
import { deleteReadThread } from '../../../services/readThreads';

class MarkUnreadButton extends React.Component {
  edit = async () => {
    try {
      await deleteReadThread(this.props.threadId);

      pushNotification({ message: 'Thread marked as unread.' });
    } catch (err) {
      pushNotification({ message: 'Could not mark thread as unread.', type: 'error' });
    }
  };

  render() {
    return (
      <LoggedInOnly>
        <SubHeaderDropdownListItem>
          <Tooltip text="Mark unread">
            <SubHeaderRealButton onClick={() => this.edit()}>
              <i className="fas fa-broom" />
              <span>Mark unread</span>
            </SubHeaderRealButton>
          </Tooltip>
        </SubHeaderDropdownListItem>
      </LoggedInOnly>
    );
  }
}

MarkUnreadButton.propTypes = {
  threadId: PropTypes.number
};
MarkUnreadButton.defaultProps = {
  threadId: 0
};

export default MarkUnreadButton;
