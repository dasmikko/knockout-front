import styled from 'styled-components';

import {
  ThemeVerticalPadding,
  ThemeVerticalHalfPadding,
  ThemeHorizontalPadding,
  ThemeSecondaryBackgroundColor,
} from '../../../Theme';

import { SubHeaderRealButton } from '../../../components/SubHeader/style';

export const ThreadUser = styled.span`
  opacity: 0.9;
  margin-left: 15px;
`;

export const ThreadPageWrapper = styled.article`
  display: block;
  margin-top: ${ThemeVerticalPadding};
  padding-top: 0;
  padding-bottom: ${ThemeVerticalPadding};
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalPadding};
`;

export const ThreadPostList = styled.ul`
  padding: 0;
  margin-top: ${ThemeVerticalHalfPadding};
  margin-bottom: 0;
  margin-left: 0;
  margin-right: 0;
`;

export const ThreadNewPost = styled.div`
  background: ${ThemeSecondaryBackgroundColor};
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
  border-radius: 5px;
`;

export const ThreadStatusAction = styled(SubHeaderRealButton)`
  text-decoration: unset;
`;

export const ThreadPageTopControls = styled.div`
  display: block;
  text-align: right;
  margin-top: ${ThemeVerticalHalfPadding};
  padding: 0;
`;

export const ThreadPageBottomControls = styled.div`
  text-align: right;
  padding: 0;

  display: flex;
  flex-grow: 1;
  margin-top: ${ThemeVerticalPadding};
  justify-content: flex-end;
`;
