import { loadProgressBar } from 'axios-progress-bar';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import MainView from './MainView';
import MainViewNew from './views/MainView';
import { loadPunchyLabsFromStorageBoolean } from './services/theme';
import store from './state/configureStore';
import AppThemeProvider from './AppThemeProvider';

const labsEnabled = loadPunchyLabsFromStorageBoolean();

const MainViewComponent = labsEnabled ? MainViewNew : MainView;

// adds the axios progress bar to the global axios instance
loadProgressBar({
  parent: '#knockout-header',
});
toast.configure();

const App = () => {
  return (
    <Provider store={store}>
      <AppThemeProvider>
        <BrowserRouter basename="/">
          <MainViewComponent />
        </BrowserRouter>
      </AppThemeProvider>
    </Provider>
  );
};

export default App;

ReactDOM.render(<App />, document.getElementById('app'));
