import React from 'react';
import { Switch, Route } from 'react-router-dom';
import styled, { createGlobalStyle, keyframes, css } from 'styled-components';
import { Slide, ToastContainer } from 'react-toastify';
import { connect } from 'react-redux';

import Header from './components/Header/index';
import Footer from './components/Footer';
import NotificationStyles from './components/NotificationStyles/style';

import {
  ThemePrimaryBackgroundImage,
  ThemePrimaryBackgroundColor,
  ThemeBodyWidth,
  ThemeBodyColor,
  ThemeBodyColorWithBg,
} from './Theme';

import routes from './routes';
import ScrollOnRouteChange from './components/ScrollOnRouteChange';

const GlobalStyle = createGlobalStyle`
  html {
    background-color: ${ThemePrimaryBackgroundColor};
  }
  body {
    max-width: ${ThemeBodyWidth};
    background-color: ${(props) => (props.backgroundUrl ? ThemeBodyColorWithBg : ThemeBodyColor)};
    transition: background-color 1s ease-in-out;
  }
  ::selection {
    background-color: rgba(218, 43, 43, 0.94);
    color: #ffffff;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.42);
  }

  ${NotificationStyles}
`;
const ConnectedGlobalStyle = connect(({ background }) => ({
  backgroundUrl: background.url,
}))(GlobalStyle);

const FlexWrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  padding-top: 80px;
`;

const FlexExpander = styled.div`
  flex-grow: 1;
`;

const bgAnim = keyframes`
  from {
    filter: opacity(0)
  }
  to {
    filter: opacity(1)
  }
`;

const Background = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-image: ${(props) => (props.url ? `url("${props.url}")` : ThemePrimaryBackgroundImage)};
  background-position: top center;
  background-size: ${(props) => (props.bgType === 'tiled' ? 'unset' : 'cover')};
  background-repeat: ${(props) => (props.bgType === 'tiled' ? 'repeat' : 'unset')};
  z-index: -1000;
  opacity: 0.5;
  animation: ${(props) =>
    props.url
      ? css`
          ${bgAnim} 1s linear
        `
      : ''};
`;

const ConnectedBackground = connect(({ background }) => ({
  url: background.url,
  bgType: background.type,
}))(Background);

const MainView = () => (
  <FlexWrapper>
    <ConnectedGlobalStyle />

    <Header />

    <ScrollOnRouteChange>
      <Switch>
        {routes.map((route) => (
          <Route
            key={route.name}
            path={route.path}
            component={route.component}
            name={route.name}
            exact={route.exact}
          />
        ))}
      </Switch>
    </ScrollOnRouteChange>

    <FlexExpander />

    <Footer />

    <ToastContainer transition={Slide} />

    <ConnectedBackground />
  </FlexWrapper>
);
export default MainView;
