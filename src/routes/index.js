import * as Routes from './routes';
import { loadPunchyLabsFromStorageBoolean } from '../services/theme';

const labsEnabled = loadPunchyLabsFromStorageBoolean();

const routes = [
  {
    path: '/',
    name: 'HomePage',
    component: labsEnabled ? Routes.HomePageNew : Routes.HomePage,
    exact: true,
  },
  // user "control panel":
  {
    path: '/usersetup',
    name: 'UserSetup',
    component: labsEnabled ? Routes.UserSetupNew : Routes.UserSetup,
    exact: true,
  },
  // login page:
  {
    path: '/login',
    name: 'LoginPage',
    component: labsEnabled ? Routes.LoginPageNew : Routes.LoginPage,
    exact: true,
  },
  // user profile edit
  {
    path: '/usersettings',
    name: 'UserSettings',
    component: labsEnabled ? Routes.UserSettingsPageNew : Routes.UserSettings,
    exact: true,
  },
  // year review 2019
  {
    path: '/2019-review',
    name: 'Review2019',
    component: Routes.Review2019,
    exact: true,
  },
  // user profile
  {
    path: '/user/:id',
    name: 'UserProfile',
    component: labsEnabled ? Routes.UserProfileNew : Routes.UserProfile,
    exact: true,
  },
  // alerts list
  {
    path: '/alerts/list',
    name: 'AlertsList',
    component: labsEnabled ? Routes.AlertsListNew : Routes.AlertsList,
    exact: true,
  },
  // logout
  {
    path: '/logout',
    name: 'Logout',
    component: Routes.Logout,
    exact: true,
  },
  // static rules page:
  {
    path: '/rules',
    name: 'Rules',
    component: labsEnabled ? Routes.RulesNew : Routes.Rules,
    exact: !labsEnabled,
  },
  // privacy policy page:
  {
    path: '/privacy-policy',
    name: 'PrivacyPolicy',
    component: Routes.PrivacyPolicyPage,
    exact: true,
  },
  // static KnockoutBB syntax page:
  {
    path: '/knockoutbb',
    name: 'KnockoutBBSyntaxPage',
    component: labsEnabled ? Routes.KnockoutBBSyntaxPageNew : Routes.KnockoutBBSyntaxPage,
    exact: true,
  },
  // subforum page:
  {
    path: '/subforum/:id/:page?',
    name: 'SubforumPage',
    component: labsEnabled ? Routes.SubforumPageNew : Routes.SubforumPage,
    exact: true,
  },
  // thread creation:
  {
    path: '/thread/new/:id',
    name: 'ThreadCreationPage',
    component: labsEnabled ? Routes.ThreadCreationPageNew : Routes.ThreadCreationPage,
    exact: true,
  },
  // thread page:
  {
    path: '/thread/:id/:page?',
    name: 'ThreadPage',
    component: labsEnabled ? Routes.ThreadPageNew : Routes.ThreadPage,
    exact: true,
  },
  // moderation page:
  {
    path: '/moderate',
    name: 'ModerateDashboard',
    component: labsEnabled ? Routes.ModerateNew : Routes.ModerateDashboard,
    exact: !labsEnabled,
  },
  {
    path: '/moderate/iplookup/:ip?',
    name: 'ModeratePageIpLookup',
    component: Routes.ModerateIpLookup,
    exact: true,
  },
  {
    path: '/moderate/reports',
    name: 'ModeratePageReports',
    component: Routes.ModerateReports,
    exact: true,
  },
  {
    path: '/moderate/subforums',
    name: 'ModerateSubforums',
    component: Routes.ModerateSubforums,
    exact: true,
  },
  {
    path: '/moderate/users',
    name: 'ModerateUsers',
    component: Routes.ModerateUsers,
    exact: true,
  },
  {
    path: '/moderate/tags',
    name: 'ModerateTags',
    component: Routes.ModerateTags,
    exact: true,
  },
  // events page:
  {
    path: '/events',
    name: 'EventsPage',
    component: labsEnabled ? Routes.EventsPageNew : Routes.EventsPage,
    exact: true,
  },
  {
    path: '/search',
    name: 'SearchPage',
    component: Routes.SearchPage,
    exact: true,
  },
  // /////////////////////////////////// //
  // easter eggs only after this comment //
  // easter eggs only after this comment //
  // easter eggs only after this comment //
  // easter eggs only after this comment //
  // easter eggs only after this comment //
  // easter eggs only after this comment //
  // easter eggs only after this comment //
  // /////////////////////////////////// //
  // love lain:
  {
    path: '/lovelain',
    name: 'LoveHer',
    component: Routes.LoveHer,
    exact: true,
  },
];

export default routes;
