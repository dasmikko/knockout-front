/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import axios from 'axios';
import { waitFor } from '@testing-library/react';
import config from '../../../../config';
import UserProfileSwitch from '../../../../src/views/UserProfile/components/UserProfileSwitch';
import { customRender } from '../../../custom_renderer';

jest.mock('axios');

describe('UserProfileSwitch component', () => {
  const defaultProps = {
    user: {
      id: 0,
      posts: 100,
      threads: 35,
      username: 'Bob',
    },
    userId: 0,
    bans: [
      {
        id: 2,
        ban_reason: 'stuff',
        banReason: 'stuff',
        expires_at: '2020-09-04T01:25:06.000Z',
        expiresAt: '2020-09-04T01:25:06.000Z',
        created_at: '2020-09-04T00:25:06.000Z',
        createdAt: '2020-09-04T00:25:06.000Z',
        updated_at: '2020-09-04T00:25:06.000Z',
        updatedAt: '2020-09-04T00:25:06.000Z',
        post_id: null,
        postId: null,
        post: null,
        user: {
          id: 4323,
          username: 'Jarod4',
          usergroup: 1,
          avatarUrl: '',
          avatar_url: '',
          backgroundUrl: '',
          posts: 252,
          threads: 1,
          createdAt: '2019-09-16T04:02:01.000Z',
          updatedAt: '2019-09-14T16:31:08.000Z',
          banned: true,
          isBanned: true,
        },
        bannedBy: {
          id: 7,
          username: 'Wowza',
          usergroup: 3,
          avatarUrl: '7.webp',
          avatar_url: '7.webp',
          backgroundUrl: '7-bg.webp',
          posts: 229,
          threads: 2,
          createdAt: '2020-06-16T01:28:16.000Z',
          updatedAt: '2020-06-16T01:28:16.000Z',
          banned: false,
          isBanned: false,
        },
        thread: null,
      },
    ],
    ratings: [{ name: 'agree', count: 20 }],
  };

  beforeEach(() => {
    axios.get.mockImplementation((path) => {
      if (path === `${config.apiHost}/user/0/posts/1`) {
        return Promise.resolve({
          data: {
            posts: [
              {
                id: 2301382,
                thread: 9259,
                page: 2,
                content: 'Sit earum adipisci',
                created_at: '2020-04-07T14:59:48.000Z',
                createdAt: '2020-04-07T14:59:48.000Z',
                updated_at: '2020-06-06T17:41:54.000Z',
                updatedAt: '2020-06-06T17:41:54.000Z',
                ratings: [],
                bans: [],
              },
            ],
            currentPage: 1,
          },
        });
      }
      return Promise.resolve({ data: { threads: [{ title: 'Things' }], currentPage: 1 } });
    });
  });

  it('displays tabs', () => {
    const { queryByText, findByText } = customRender(<UserProfileSwitch {...defaultProps} />);
    waitFor(() => findByText('Posts (100)'));
    expect(queryByText('Posts (100)')).not.toBeNull();
    expect(queryByText('Threads (35)')).not.toBeNull();
  });

  it('displays posts', async () => {
    const { queryByText, findByText } = customRender(<UserProfileSwitch {...defaultProps} />);
    const posts = queryByText('Posts (100)');
    expect(posts).not.toBeNull();
    posts.click();
    expect(await findByText('Sit earum adipisci')).toBeTruthy();
  });

  it('displays threads', async () => {
    const { queryByText, findByText } = customRender(<UserProfileSwitch {...defaultProps} />);
    const threads = queryByText('Threads (35)');
    expect(threads).not.toBeNull();
    threads.click();
    expect(await findByText('Things')).toBeTruthy();
  });

  it('displays bans', () => {
    const { queryByText, findByText } = customRender(<UserProfileSwitch {...defaultProps} />);
    waitFor(() => findByText('Posts'));
    const bans = queryByText('Bans (1)');
    expect(bans).not.toBeNull();
    bans.click();
    expect(queryByText('"stuff"')).not.toBeNull();
  });

  it('displays ratings', () => {
    const { queryByText, findByText } = customRender(<UserProfileSwitch {...defaultProps} />);
    waitFor(() => findByText('Posts'));
    const ratings = queryByText('Ratings');
    expect(ratings).not.toBeNull();
    ratings.click();
    expect(queryByText('Agree')).not.toBeNull();
  });
});
