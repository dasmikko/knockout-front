import React from 'react';
import axios from 'axios';
import { customRender } from '../../custom_renderer';
import UserProfile from '../../../src/views/UserProfile';
import config from '../../../config';

jest.mock('axios');

describe('UserProfile component', () => {
  const customRenderOptions = {
    path: '/user/:id',
    route: '/user/0',
  };

  beforeEach(() => {
    axios.get.mockImplementation((path) => {
      if (path === `${config.apiHost}/user/0/posts/1`) {
        return Promise.resolve({ data: { posts: [] } });
      }
      if (path === `${config.apiHost}/user/0/topRatings`) {
        return Promise.resolve({ data: [] });
      }
      return Promise.resolve({ data: { username: 'Rando', id: 0 } });
    });
  });

  describe('as a non-logged in user', () => {
    it('hides moderator controls', async () => {
      const { findByText } = customRender(<UserProfile />, customRenderOptions);
      await expect(findByText('Remove Avatar', {}, { timeout: 100 })).rejects.toThrow();
    });
    it('cannot edit the profile', async () => {
      const { findByText } = customRender(<UserProfile />, customRenderOptions);
      await expect(findByText('Edit profile', {}, { timeout: 100 })).rejects.toThrow();
    });
  });

  describe('as a logged in user', () => {
    const loggedInState = {
      user: {
        loggedIn: true,
        id: 123,
        username: 'TestUser',
        usergroup: 1,
        avatar_url: 'avatar.png',
      },
    };

    beforeEach(() => {
      customRenderOptions.initialState = loggedInState;
    });

    it('hides moderator controls', async () => {
      const { findByText } = customRender(<UserProfile />, customRenderOptions);
      await expect(findByText('Remove Avatar', {}, { timeout: 100 })).rejects.toThrow();
    });

    it('cannot edit the profile', async () => {
      const { findByText } = customRender(<UserProfile />, customRenderOptions);
      await expect(findByText('Edit profile', {}, { timeout: 100 })).rejects.toThrow();
    });

    describe('who is a moderator', () => {
      const modState = {
        user: {
          loggedIn: true,
          username: 'TestUser',
          id: 123,
          usergroup: 3,
        },
      };

      beforeEach(() => {
        customRenderOptions.initialState = modState;
      });

      it('shows moderator controls', async () => {
        const { findByText } = customRender(<UserProfile />, customRenderOptions);
        expect(await findByText('Remove Avatar')).toBeTruthy();
      });

      it('cannot edit the profile', async () => {
        const { findByText } = customRender(<UserProfile />, customRenderOptions);
        await expect(findByText('Edit profile', {}, { timeout: 100 })).rejects.toThrow();
      });
    });

    describe('viewing their own profile', () => {
      const ownUserState = {
        user: {
          loggedIn: true,
          id: 0,
          username: 'Rando',
          usergroup: 1,
          avatar_url: 'avatar.png',
        },
      };

      beforeEach(() => {
        customRenderOptions.initialState = ownUserState;
      });

      it('cannot edit the profile', async () => {
        const { findByText } = customRender(<UserProfile />, customRenderOptions);
        await expect(findByText('Edit profile', {}, { timeout: 100 })).rejects.toThrow();
      });

      describe('as a gold member', () => {
        const goldState = {
          user: {
            loggedIn: true,
            id: 0,
            username: 'Rando',
            usergroup: 2,
            avatar_url: 'avatar.png',
          },
        };
        beforeEach(() => {
          customRenderOptions.initialState = goldState;
        });

        it('can edit the profile', async () => {
          const { findByText } = customRender(<UserProfile />, customRenderOptions);
          expect(await findByText('Edit profile')).toBeTruthy();
        });
      });
    });
  });
});
