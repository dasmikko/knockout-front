import React from 'react';

import EventCard from '../../../../src/views/EventsPage/components/EventCard';
import { customRender } from '../../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';

describe('EventCard Component', () => {
  const correctDateFormat = '2020-06-16T05:58:34.000Z';
  const wrongDateFormat = 'Borgor';
  const content = JSON.stringify({
    content: ['? Wowza renamed ', 'Thing to ', { text: 'Things', link: '/thread/42' }, '.'],
  });
  const wrongContent = 'Hello I am wrong';

  describe('The correct date format is provided', () => {
    it('does not display Unknown Date', () => {
      const { queryByText } = customRender(
        <EventCard createdAt={correctDateFormat} content={content} index={0} />
      );
      expect(queryByText('Unknown Data')).not.toBeInTheDocument();
    });
  });

  describe('The incorrect date format is provided', () => {
    it('displays Unknown Date', () => {
      const { queryByText } = customRender(
        <EventCard createdAt={wrongDateFormat} content={content} index={0} />
      );
      expect(queryByText('Unknown Date')).toBeInTheDocument();
    });
  });

  describe('The correct content is provided', () => {
    it('event message is displayed', () => {
      const { queryByText } = customRender(
        <EventCard createdAt={correctDateFormat} content={content} index={0} />
      );
      expect(queryByText('Wowza renamed')).toBeInTheDocument();
    });
  });

  describe('The incorrect content is provided', () => {
    it('event message is displayed', () => {
      const { queryByText } = customRender(
        <EventCard createdAt={correctDateFormat} content={wrongContent} index={0} />
      );
      expect(queryByText('Somthing Went Wrong')).toBeInTheDocument();
    });
  });
});
