/* eslint-disable no-underscore-dangle */
import React from 'react';

import EditableBackground from '../../../../src/views/ThreadPage/components/EditableBackground';
import { customRender } from '../../../custom_renderer';

describe('EditableBackground component', () => {
  describe('as a non-logged in user', () => {
    it('does not display the button', () => {
      const { queryByText } = customRender(<EditableBackground />);
      expect(queryByText('Change background')).toBeNull();
    });
  });

  describe('as a logged in user', () => {
    let loggedInState = {};

    beforeEach(() => {
      loggedInState = {
        user: {
          loggedIn: true,
          username: 'TestUser',
          usergroup: 1,
        },
      };
    });

    describe('who is not the thread creator', () => {
      it('does not display the button', () => {
        const { queryByText } = customRender(<EditableBackground />, {
          initialState: loggedInState,
        });
        expect(queryByText('Change background')).toBeNull();
      });
    });

    describe('who is the thread creator', () => {
      describe('and a gold member', () => {
        beforeEach(() => {
          loggedInState.user.usergroup = 2;
        });

        it('allows the user to edit the background', () => {
          const { queryByTitle } = customRender(<EditableBackground byCurrentUser />, {
            initialState: loggedInState,
          });
          expect(queryByTitle('Change background')).not.toBeNull();
        });
      });

      it('does not allow the user to edit the background', () => {
        const { queryByTitle } = customRender(<EditableBackground byCurrentUser />, {
          initialState: loggedInState,
        });
        expect(queryByTitle('Change background (Must be a Gold member)')).not.toBeNull();
      });
    });

    describe('who is an admin', () => {
      beforeEach(() => {
        loggedInState.user.usergroup = 3;
      });
      it('allows the user to edit the background', () => {
        const { queryByTitle } = customRender(<EditableBackground />, {
          initialState: loggedInState,
        });
        expect(queryByTitle('Change background')).not.toBeNull();
      });
    });
  });
});
