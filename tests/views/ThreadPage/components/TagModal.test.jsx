import React from 'react';

import { fireEvent, act } from '@testing-library/react';
import axios from 'axios';
import TagModal from '../../../../src/views/ThreadPage/components/TagModal';
import { customRender } from '../../../custom_renderer';

jest.mock('axios');

describe('TagModal component', () => {
  const tags = [
    { id: 1, name: 'NSFW' },
    { id: 2, name: 'Memes' },
    { id: 3, name: 'Politics' },
  ];
  const thread = { id: 0 };
  axios.get.mockResolvedValue({ data: tags });

  const openFn = jest.fn();

  it('does not display the modal on render', () => {
    const { queryByText } = customRender(
      <TagModal thread={thread} isOpen={false} openFn={openFn} />
    );
    expect(queryByText('Cancel')).toBeNull();
  });

  it('displays the modal', async () => {
    const { queryByText } = customRender(<TagModal thread={thread} isOpen openFn={openFn} />);
    const fieldTitle = queryByText('Selected tags');
    expect(fieldTitle).not.toBeNull();

    expect(queryByText('Cancel')).not.toBeNull();
  });

  it('correctly displays selected tags', async () => {
    const { queryByText, queryAllByTestId, getByTestId } = customRender(
      <TagModal thread={thread} isOpen openFn={openFn} />
    );
    const fieldTitle = queryByText('Selected tags');
    expect(fieldTitle).not.toBeNull();

    const modalSelect = getByTestId('modal-select');
    await act(async () => {
      fireEvent.focus(modalSelect);
      fireEvent.keyDown(modalSelect, { key: 'ArrowDown', code: 40 });
      fireEvent.change(modalSelect, { target: { value: tags[0].id } });
    });

    let selectedTags = queryAllByTestId('selected-tag');
    expect(selectedTags).toHaveLength(1);
    await act(async () => {
      fireEvent.click(selectedTags[0]);
    });
    selectedTags = queryAllByTestId('selected-tag');
    expect(selectedTags).toHaveLength(0);
  });
});
