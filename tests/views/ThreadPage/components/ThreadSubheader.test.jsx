import { fireEvent } from '@testing-library/react';
/* eslint-disable no-underscore-dangle */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import ThreadSubheader from '../../../../src/views/ThreadPage/components/ThreadSubheader';
import { customRender } from '../../../custom_renderer';

describe('ThreadSubheader component', () => {
  let defaultProps = {};

  beforeEach(() => {
    localStorage.clear();
    defaultProps = {
      thread: {
        title: 'New thread',
        userId: 0,
        iconId: 0,
        id: 0,
        locked: false,
        deleted: false,
        subforumName: '',
        subforumId: 0,
        subscriptionLastSeen: new Date(),
        readThreadLastSeen: new Date(),
        totalPosts: 0,
      },
      params: {
        id: 0,
      },
      currentPage: 0,
      currentUserId: 0,
      togglePinned: jest.fn(),
      toggleLocked: jest.fn(),
      toggleDeleted: jest.fn(),
      showMoveModal: jest.fn(),
      showTagModal: jest.fn(),
      deleteAlert: jest.fn(),
      createAlert: jest.fn(),
    };
  });

  it('displays the subforum name', () => {
    defaultProps.thread.subforumName = 'General';
    const { queryByText } = customRender(<ThreadSubheader {...defaultProps} />);
    expect(queryByText('General')).not.toBeNull();
  });

  describe('as a non-logged in user', () => {
    it('hides the thread controls', () => {
      const { queryByText } = customRender(<ThreadSubheader {...defaultProps} />);
      expect(queryByText('Subscribe')).toBeNull();
    });
  });

  describe('as a logged in user', () => {
    const loggedInState = {
      user: {
        loggedIn: true,
        username: 'TestUser',
      },
    };

    const userLocalStorageDetails = {
      id: 123,
      username: 'TestUser',
      usergroup: 1,
      avatar_url: 'avatar.png',
    };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
    });

    it('shows the thread controls', () => {
      const { queryAllByText } = customRender(<ThreadSubheader {...defaultProps} />, {
        initialState: loggedInState,
      });
      expect(queryAllByText('Subscribe')).toHaveLength(2);
    });

    it('hides moderator controls', () => {
      const { queryAllByText } = customRender(<ThreadSubheader {...defaultProps} />, {
        initialState: loggedInState,
      });
      expect(queryAllByText('Moderation')).toHaveLength(0);
    });

    describe('who is a moderator', () => {
      const moderatorState = {
        user: {
          loggedIn: true,
          username: 'TestUser',
          usergroup: 3,
        },
      };

      it('displays moderator controls', () => {
        const { queryByText } = customRender(<ThreadSubheader {...defaultProps} />, {
          initialState: moderatorState,
        });
        const modButton = queryByText('Moderation');
        expect(modButton).not.toBeNull();
        fireEvent.click(modButton);

        expect(queryByText('Move thread')).not.toBeNull();
        expect(queryByText('Pin Thread')).not.toBeNull();
        expect(queryByText('Lock Thread')).not.toBeNull();
        expect(queryByText('Delete Thread')).not.toBeNull();
      });
    });
  });
});
