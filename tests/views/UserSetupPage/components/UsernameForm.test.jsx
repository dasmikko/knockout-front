import React from 'react';
import { waitFor, fireEvent, act } from '@testing-library/react';
import { customRender } from '../../../custom_renderer';
import UsernameForm from '../../../../src/views/UserSetupPage/components/UsernameForm';
import '@testing-library/jest-dom/extend-expect';

describe('The Username Form component', () => {
  describe('Is empty, no text is in the form', () => {
    it('has no value in the input', () => {
      const { getByPlaceholderText } = customRender(<UsernameForm submitUsername={() => {}} />);
      expect(getByPlaceholderText('username').value).toEqual('');
    });
    it('has a disabled button', () => {
      const { getByText } = customRender(<UsernameForm submitUsername={() => {}} />);
      expect(getByText('Submit').closest('button')).toHaveAttribute('disabled');
    });
  });
  describe('Is populated with less than 3 characters', () => {
    it('has a disabled button', () => {
      const { getByPlaceholderText, getByText } = customRender(
        <UsernameForm submitUsername={() => {}} />
      );
      const input = getByPlaceholderText('username');

      expect(getByPlaceholderText('username').value).toEqual('');

      fireEvent.change(input, { target: { value: 'aa' } });

      expect(getByPlaceholderText('username').value).toEqual('aa');
      expect(getByText('Submit').closest('button')).toHaveAttribute('disabled');
    });
  });
  describe('Is populated with more than 3 characters and less than 20', () => {
    it('has an enabled submit button', () => {
      const { getByPlaceholderText, getByText } = customRender(
        <UsernameForm submitUsername={() => {}} />
      );
      const input = getByPlaceholderText('username');

      fireEvent.change(input, { target: { value: 'More Than 3' } });

      expect(input.value).toEqual('More Than 3');
      expect(getByText('Submit').closest('button')).not.toHaveAttribute('disabled');
    });
  });
  describe('has leading and trailing spaces', () => {
    it('trims leading space, and displays warning', () => {
      const { getByPlaceholderText, getByText } = customRender(
        <UsernameForm submitUsername={() => {}} />
      );
      const input = getByPlaceholderText('username');

      fireEvent.change(input, { target: { value: '   ' } });

      expect(getByPlaceholderText('username').value).toEqual('');
      expect(getByText('Usernames can not start with a space.'));
    });
    it('trims trailing space on submit', async () => {
      const { getByPlaceholderText, getByText } = customRender(
        <UsernameForm submitUsername={async () => {}} />
      );
      const input = getByPlaceholderText('username');
      const button = getByText('Submit');

      await act(async () => {
        fireEvent.change(input, { target: { value: ' Trailing   ' } });
        await waitFor(() => button.click());
      });

      expect(input.value).toEqual('Trailing');
    });
  });
  describe('Is populated with more than 19 characters', () => {
    it('has a disabled submit button, and displays warning', () => {
      const { getByPlaceholderText, getByText } = customRender(
        <UsernameForm submitUsername={() => {}} />
      );
      const input = getByPlaceholderText('username');

      fireEvent.change(input, { target: { value: 'Hello I Am A Very Long Username' } });
      expect(input.value).toEqual('Hello I Am A Very L');
      expect(getByText('Usernames can be at max 19 characters.'));
    });
  });
});
