/* eslint-disable no-unused-vars */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import UserSettingsPage from '../../../src/views/UserSettingsPage/index';
import { customRender, screen } from '../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';

describe('Avatar Upload Container Component', () => {
  beforeEach(() => {
    localStorage.clear();
  });

  describe('as a logged in user', () => {
    const loggedInState = {
      user: {
        loggedIn: true,
        username: 'TestUser',
        avatar_url: 'avatar.png',
      },
    };

    const userLocalStorageDetails = {
      id: 123,
      username: 'TestUser',
      usergroup: 1,
      avatar_url: 'avatar.png',
    };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
    });

    it('displays user settings page', () => {
      customRender(<UserSettingsPage currentUser={userLocalStorageDetails} />, {
        initialState: loggedInState,
      });

      expect(screen.queryByText('User Settings')).toBeDefined();
    });
  });
});
