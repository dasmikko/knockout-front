import {
  httpRegex,
  handlePaste,
  insertTag,
  checkForHotkeys,
} from '../../../src/componentsNew/EditorBB/helpers';
import * as Hotkeys from '../../../src/componentsNew/EditorBB/editorHotkeys';

describe('EditorBB Helpers', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('httpRegex', () => {
    it('matches a URL with only https://', () => {
      const url = 'https://twitter.com';
      expect(httpRegex.test(url)).toBeTruthy();
    });

    it('matches a URL with both https:// and www.', () => {
      const url = 'https://www.bbc.co.uk';
      expect(httpRegex.test(url)).toBeTruthy();
    });

    it('matches a URL with a complex path', () => {
      const url =
        'https://www.bbc.co.uk/news/av/world-us-canada-53452080/why-i-fell-in-love-with-irish-dancing';
      expect(httpRegex.test(url)).toBeTruthy();
    });

    it('matches a URL with a subdomain', () => {
      const url = 'https://store.google.com';
      expect(httpRegex.test(url)).toBeTruthy();
    });

    it('does not match a URL with nothing before the domain name', () => {
      const url = 'twitter.com/LiveCutCorner';
      expect(httpRegex.test(url)).toBeFalsy();
    });

    it('does not match a parameterised string', () => {
      const notUrl = 'this_is_not_a_url';
      expect(httpRegex.test(notUrl)).toBeFalsy();
    });
  });

  describe('handlePaste', () => {
    let originalContent;
    let setContentMock;
    let selectionRange;

    beforeEach(() => {
      originalContent = 'Some content!';
      setContentMock = jest.fn().mockImplementation((func) => {
        return func(originalContent);
      });
      selectionRange = [0, 0];
      window.confirm = jest.fn().mockImplementation(() => true);
    });

    describe('when pasting an image', () => {
      const pastedContent = 'https://bbc.co.uk/example.png';

      it('asks for confirmation, and if yes appends the content wrapped with img tags', () => {
        handlePaste(
          { clipboardData: { getData: () => pastedContent }, preventDefault: () => {} },
          setContentMock,
          selectionRange
        );
        expect(window.confirm).toHaveBeenCalled();
        expect(setContentMock).toHaveLastReturnedWith(
          `[img]${pastedContent}[/img]${originalContent}`
        );
      });
    });

    describe('when pasting a youtube link', () => {
      const pastedContent = 'https://www.youtube.com/watch?v=bm0S4cn_rfw';

      it('asks for confirmation, and if yes appends the content wrapped with youtube tags', () => {
        handlePaste(
          { clipboardData: { getData: () => pastedContent }, preventDefault: () => {} },
          setContentMock,
          selectionRange
        );
        expect(window.confirm).toHaveBeenCalled();
        expect(setContentMock).toHaveLastReturnedWith(
          `[youtube]${pastedContent}[/youtube]${originalContent}`
        );
      });
    });

    describe('when pasting a vimeo link', () => {
      const pastedContent = 'https://vimeo.com/435789152';

      it('asks for confirmation, and if yes appends the content wrapped with vimeo tags', () => {
        handlePaste(
          { clipboardData: { getData: () => pastedContent }, preventDefault: () => {} },
          setContentMock,
          selectionRange
        );
        expect(window.confirm).toHaveBeenCalled();
        expect(setContentMock).toHaveLastReturnedWith(
          `[vimeo]${pastedContent}[/vimeo]${originalContent}`
        );
      });
    });

    describe('when pasting a streamable link', () => {
      const pastedContent = 'https://streamable.com/mz4mhn';

      it('asks for confirmation, and if yes appends the content wrapped with streamable tags', () => {
        handlePaste(
          { clipboardData: { getData: () => pastedContent }, preventDefault: () => {} },
          setContentMock,
          selectionRange
        );
        expect(window.confirm).toHaveBeenCalled();
        expect(setContentMock).toHaveLastReturnedWith(
          `[streamable]${pastedContent}[/streamable]${originalContent}`
        );
      });
    });

    describe('when pasting a video link', () => {
      const pastedContent = 'https://i.imgur.com/IdA0Olj.mp4';

      it('asks for confirmation, and if yes appends the content wrapped with video tags', () => {
        handlePaste(
          { clipboardData: { getData: () => pastedContent }, preventDefault: () => {} },
          setContentMock,
          selectionRange
        );
        expect(window.confirm).toHaveBeenCalled();
        expect(setContentMock).toHaveLastReturnedWith(
          `[video]${pastedContent}[/video]${originalContent}`
        );
      });
    });

    describe('when pasting a strawpoll link', () => {
      const pastedContent = 'https://www.strawpoll.me/23';

      it('asks for confirmation, and if yes appends the content wrapped with strawpoll tags', () => {
        handlePaste(
          { clipboardData: { getData: () => pastedContent }, preventDefault: () => {} },
          setContentMock,
          selectionRange
        );
        expect(window.confirm).toHaveBeenCalled();
        expect(setContentMock).toHaveLastReturnedWith(
          `[strawpoll]${pastedContent}[/strawpoll]${originalContent}`
        );
      });
    });

    describe('when pasting a twitter post link', () => {
      const pastedContent = 'https://twitter.com/simpscreens/status/1284654197707767811';

      it('asks for confirmation, and if yes appends the content wrapped with twitter tags', () => {
        handlePaste(
          { clipboardData: { getData: () => pastedContent }, preventDefault: () => {} },
          setContentMock,
          selectionRange
        );
        expect(window.confirm).toHaveBeenCalled();
        expect(setContentMock).toHaveLastReturnedWith(
          `[twitter]${pastedContent}[/twitter]${originalContent}`
        );
      });
    });

    describe('when pasting any other URL link', () => {
      const pastedContent = 'https://gitlab.com/notpunch/nashipunch-front';

      it('asks for confirmation, and if yes appends the content wrapped with url tags', () => {
        handlePaste(
          { clipboardData: { getData: () => pastedContent }, preventDefault: () => {} },
          setContentMock,
          selectionRange
        );
        expect(window.confirm).toHaveBeenCalled();
        expect(setContentMock).toHaveLastReturnedWith(
          `[url href="${pastedContent}"][/url]${originalContent}`
        );
      });
    });
  });

  describe('insertTag', () => {
    let originalContent;
    let setContentMock;
    let selectionRange;
    let setSelectionRange;
    let input;

    beforeEach(() => {
      originalContent = 'Some content!';
      setContentMock = jest.fn().mockImplementation((func) => {
        return func(originalContent);
      });
      selectionRange = [];
      setSelectionRange = jest.fn();
      input = document.createElement('input');
      input.value = originalContent;
    });

    describe('when inserted in "li" tag', () => {
      it('appends the list syntax to the end of the content', () => {
        insertTag('li', originalContent, setContentMock, selectionRange, setSelectionRange, input);
        expect(setContentMock).toHaveLastReturnedWith(`Some content![ul][li][/li][/ul]`);
      });
    });

    describe('With some content selected', () => {
      beforeEach(() => {
        selectionRange = [5, 12];
      });

      it('wraps the selected content with the type of tag passed in', () => {
        insertTag('b', originalContent, setContentMock, selectionRange, setSelectionRange, input);
        expect(setContentMock).toHaveLastReturnedWith(`Some [b]content[/b]!`);
      });
    });

    it('otherwise just appends the tags to the already existing content, and sets the cursor to the center of the new tags', () => {
      insertTag('b', originalContent, setContentMock, selectionRange, setSelectionRange, input);
      expect(setContentMock).toHaveLastReturnedWith(`Some content![b][/b]`);
      expect(setSelectionRange).toHaveBeenCalledWith([16, 16]);
    });
  });

  describe('checkForHotkeys', () => {
    let originalContent;
    let setContentMock;
    let selectionRange;
    let setSelectionRange;
    let input;
    let handleSubmitMock;

    beforeEach(() => {
      originalContent = 'Some content!';
      setContentMock = jest.fn().mockImplementation((func) => {
        return func(originalContent);
      });
      selectionRange = [0, 0];
      setSelectionRange = jest.fn();
      input = document.createElement('input');
      input.value = originalContent;
      handleSubmitMock = jest.fn();
    });

    describe('pressing the hotkey for the bold tags', () => {
      let isBoldHotkeyMock;

      beforeEach(() => {
        isBoldHotkeyMock = jest.spyOn(Hotkeys, 'isBoldHotkey').mockImplementation(() => true);
      });

      it('inserts a bold tag', () => {
        checkForHotkeys(
          { preventDefault: () => {} },
          originalContent,
          setContentMock,
          selectionRange,
          setSelectionRange,
          input,
          handleSubmitMock
        );

        expect(isBoldHotkeyMock).toBeCalled();
        expect(setContentMock).toHaveLastReturnedWith(`[b][/b]${originalContent}`);
      });
    });

    describe('pressing the hotkey for the italics tags', () => {
      let isItalicHotkeyMock;

      beforeEach(() => {
        jest.spyOn(Hotkeys, 'isBoldHotkey').mockImplementation(() => false);
        isItalicHotkeyMock = jest.spyOn(Hotkeys, 'isItalicHotkey').mockImplementation(() => true);
      });

      it('inserts a italics tag', () => {
        checkForHotkeys(
          { preventDefault: () => {} },
          originalContent,
          setContentMock,
          selectionRange,
          setSelectionRange,
          input,
          handleSubmitMock
        );

        expect(isItalicHotkeyMock).toBeCalled();
        expect(setContentMock).toHaveLastReturnedWith(`[i][/i]${originalContent}`);
      });
    });

    describe('pressing the hotkey for the italics tags', () => {
      let isUnderlinedHotkeyMock;

      beforeEach(() => {
        jest.spyOn(Hotkeys, 'isBoldHotkey').mockImplementation(() => false);
        jest.spyOn(Hotkeys, 'isItalicHotkey').mockImplementation(() => false);
        isUnderlinedHotkeyMock = jest
          .spyOn(Hotkeys, 'isUnderlinedHotkey')
          .mockImplementation(() => true);
      });

      it('inserts a italics tag', () => {
        checkForHotkeys(
          { preventDefault: () => {} },
          originalContent,
          setContentMock,
          selectionRange,
          setSelectionRange,
          input,
          handleSubmitMock
        );

        expect(isUnderlinedHotkeyMock).toBeCalled();
        expect(setContentMock).toHaveLastReturnedWith(`[u][/u]${originalContent}`);
      });
    });

    describe('pressing the hotkey for the italics tags', () => {
      let isCodeHotkeyMock;

      beforeEach(() => {
        jest.spyOn(Hotkeys, 'isBoldHotkey').mockImplementation(() => false);
        jest.spyOn(Hotkeys, 'isItalicHotkey').mockImplementation(() => false);
        jest.spyOn(Hotkeys, 'isUnderlinedHotkey').mockImplementation(() => false);
        isCodeHotkeyMock = jest.spyOn(Hotkeys, 'isCodeHotkey').mockImplementation(() => true);
      });

      it('inserts a italics tag', () => {
        checkForHotkeys(
          { preventDefault: () => {} },
          originalContent,
          setContentMock,
          selectionRange,
          setSelectionRange,
          input,
          handleSubmitMock
        );

        expect(isCodeHotkeyMock).toBeCalled();
        expect(setContentMock).toHaveLastReturnedWith(`[code][/code]${originalContent}`);
      });
    });

    describe('pressing the hotkey for the italics tags', () => {
      let isStrikethroughHotkeyMock;

      beforeEach(() => {
        jest.spyOn(Hotkeys, 'isBoldHotkey').mockImplementation(() => false);
        jest.spyOn(Hotkeys, 'isItalicHotkey').mockImplementation(() => false);
        jest.spyOn(Hotkeys, 'isUnderlinedHotkey').mockImplementation(() => false);
        jest.spyOn(Hotkeys, 'isCodeHotkey').mockImplementation(() => false);
        isStrikethroughHotkeyMock = jest
          .spyOn(Hotkeys, 'isStrikethroughHotkey')
          .mockImplementation(() => true);
      });

      it('inserts a italics tag', () => {
        checkForHotkeys(
          { preventDefault: () => {} },
          originalContent,
          setContentMock,
          selectionRange,
          setSelectionRange,
          input,
          handleSubmitMock
        );

        expect(isStrikethroughHotkeyMock).toBeCalled();
        expect(setContentMock).toHaveLastReturnedWith(`[s][/s]${originalContent}`);
      });
    });

    describe('pressing the hotkey for the italics tags', () => {
      let isSubmitHotkeyMock;

      beforeEach(() => {
        jest.spyOn(Hotkeys, 'isBoldHotkey').mockImplementation(() => false);
        jest.spyOn(Hotkeys, 'isItalicHotkey').mockImplementation(() => false);
        jest.spyOn(Hotkeys, 'isUnderlinedHotkey').mockImplementation(() => false);
        jest.spyOn(Hotkeys, 'isCodeHotkey').mockImplementation(() => false);
        jest.spyOn(Hotkeys, 'isStrikethroughHotkey').mockImplementation(() => false);
        isSubmitHotkeyMock = jest.spyOn(Hotkeys, 'isSubmitHotkey').mockImplementation(() => true);
      });

      it('inserts a italics tag', () => {
        checkForHotkeys(
          { preventDefault: () => {} },
          originalContent,
          setContentMock,
          selectionRange,
          setSelectionRange,
          input,
          handleSubmitMock
        );

        expect(isSubmitHotkeyMock).toBeCalled();
        expect(handleSubmitMock).toBeCalled();
      });
    });
  });
});
