/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { customRender } from '../custom_renderer';
import ThreadItem from '../../src/componentsNew/ThreadItem';

dayjs.extend(relativeTime);

describe('ThreadItem component', () => {
  let defaultProps = {};

  beforeEach(() => {
    defaultProps = {
      id: 1,
      createdAt: '2020-06-16T05:58:34.000Z',
      deleted: false,
      iconId: 1,
      user: {
        avatarUrl: '',
        username: 'Test User',
        usergroup: 1,
      },
      lastPost: {
        id: 2,
        created_at: '2020-06-17T05:58:34.000Z',
        user: {
          avatar_url: '',
          username: 'Test User Two',
          usergroup: 1,
        },
      },
      locked: false,
      postCount: 2,
      unreadPostCount: 1,
      title: 'Test Title',
      firstUnreadId: 2,
      backgroundUrl: '',
      backgroundType: '',
      markUnreadAction: jest.fn(),
    };
  });

  it('displays the thread title', () => {
    const { queryByText } = customRender(<ThreadItem {...defaultProps} />);
    expect(queryByText('Test Title')).not.toBeNull();
  });

  it('displays the user username', () => {
    const { queryByText } = customRender(<ThreadItem {...defaultProps} />);
    expect(queryByText('Test User')).not.toBeNull();
  });

  it('displays the last post user username', () => {
    const { queryByText } = customRender(<ThreadItem {...defaultProps} />);
    expect(queryByText('Test User Two')).not.toBeNull();
  });

  it('displays the number of unread posts', () => {
    const { queryByText } = customRender(<ThreadItem {...defaultProps} />);
    expect(queryByText('1 new post')).not.toBeNull();
  });

  it('displays the thread created date', () => {
    const { queryByText } = customRender(<ThreadItem {...defaultProps} />);
    const threadDate = dayjs('2020-06-16T05:58:34.000Z').fromNow().replace('ago', 'old');
    expect(queryByText(threadDate)).not.toBeNull();
  });
});
