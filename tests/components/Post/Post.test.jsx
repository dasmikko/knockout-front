/* eslint-disable no-underscore-dangle */
import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import Post from '../../../src/componentsNew/Post';
import store from '../../../src/state/configureStore';
import { customRender, screen } from '../../custom_renderer';

jest.mock('../../../config', () => {
  return require('../../../appConfig'); // eslint-disable-line global-require
});

describe('Post', () => {
  const userObject = {
    id: 1,
    username: 'Donglord',
    usergroup: 1,
    avatar_url: '1.webp',
    backgroundUrl: '1-bg.webp',
    createdAt: '2019-09-08T23:54:59.000Z',
    isBanned: false,
    loggedIn: true,
  };

  const userObjectNoBackground = {
    id: 1,
    username: 'Donglord',
    usergroup: 1,
    avatar_url: '',
    backgroundUrl: '',
    createdAt: '2019-09-08T23:54:59.000Z',
    isBanned: false,
    loggedIn: true,
  };

  beforeEach(() => {
    localStorage.clear();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('as a non-logged-in user', () => {
    it("renders no action buttons in the post's toolbar or clickable ratings", () => {
      customRender(
        <Post
          postDepth={1}
          totalPosts={10}
          username="Donglord"
          avatarUrl="1.webp"
          userJoinDate="2019-09-08T23:54:59.000Z"
          threadPage={1}
          threadId={1}
          threadLocked={false}
          postId={31333901}
          postBody="\nknockout!\nRules\nDiscord\nSubscriptions\nEvents\nDonglordDonglord's Avatar\nLABS\nHamburguerFarmers taking a hard hit as pandemic goes on; Beef & dairy sales plummet while some forced to mow down crops due to restaurant sales ending\nEdit title\n‹General Discussion\n1\n...\n16\n17\n18\n19\n20\n21\n22\n\n    Change background (Must be a Gold member)\n\nEdit title\nMark unread\nSubscribe\n\n    DonglordDonglord's avatarUser BackgroundSep 2019\n    a month ago\n\nAAAAAAAAAAAAAAAA\n\nDonglordDonglord's avatarUser BackgroundSep ..."
          postDate="2020-07-15T01:48:18.000Z"
          ratings={[]}
          user={userObject}
          byCurrentUser={false}
          bans={[]}
          subforumName="General Discussion"
          isUnread={false}
          countryName={null}
          isLinkedPost={false}
          postPage={1}
          hideUserWrapper={false}
          hideControls={false}
          subforumId={1}
          quickReplyEditor={() => {}}
        />,
        { initialState: { user: { loggedIn: false } } }
      );

      expect(screen.queryByTitle('Ban User')).toBeNull();
      expect(screen.queryByTitle('Report post')).toBeNull();
      expect(screen.queryByTitle('Show BBCode')).toBeNull();
      expect(screen.queryByTitle('Edit')).toBeNull();
      expect(screen.queryByTitle('Reply')).toBeNull();

      expect(screen.queryByLabelText('Rate Agree')).toBeNull();
      expect(screen.queryByLabelText('Rate Funny')).toBeNull();
    });
  });

  describe("as a logged in blue user, on someone else's post", () => {
    const loggedInState = { user: { username: 'Animefan2002', loggedIn: true, usergroup: 1 } };

    const userLocalStorageDetails = {
      id: 123,
      username: 'TestUser',
      usergroup: 1,
      avatar_url: 'avatar.png',
    };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);

      jest.spyOn(store, 'getState').mockImplementation(() => loggedInState);
    });

    it("renders action buttons in the post's toolbar and clickable ratings", () => {
      customRender(
        <Post
          postDepth={1}
          totalPosts={10}
          username="Donglord"
          avatarUrl="1.webp"
          userJoinDate="2019-09-08T23:54:59.000Z"
          threadPage={1}
          threadId={1}
          threadLocked={false}
          postId={31333901}
          postBody="\nknockout!\nRules\nDiscord\nSubscriptions\nEvents\nDonglordDonglord's Avatar\nLABS\nHamburguerFarmers taking a hard hit as pandemic goes on; Beef & dairy sales plummet while some forced to mow down crops due to restaurant sales ending\nEdit title\n‹General Discussion\n1\n...\n16\n17\n18\n19\n20\n21\n22\n\n    Change background (Must be a Gold member)\n\nEdit title\nMark unread\nSubscribe\n\n    DonglordDonglord's avatarUser BackgroundSep 2019\n    a month ago\n\nAAAAAAAAAAAAAAAA\n\nDonglordDonglord's avatarUser BackgroundSep ..."
          postDate="2020-07-15T01:48:18.000Z"
          ratings={[]}
          user={userObject}
          byCurrentUser={false}
          bans={[]}
          subforumName="General Discussion"
          isUnread={false}
          countryName={null}
          isLinkedPost={false}
          postPage={1}
          hideUserWrapper={false}
          hideControls={false}
          subforumId={1}
          quickReplyEditor={() => {}}
        />,
        { initialState: loggedInState }
      );

      expect(screen.queryByTitle('Ban User')).toBeNull();
      expect(screen.queryByTitle('Report post')).not.toBeNull();
      expect(screen.queryByTitle('Show BBCode')).not.toBeNull();
      expect(screen.queryByTitle('Reply')).not.toBeNull();
      expect(screen.queryByTitle('Edit')).toBeNull();

      expect(screen.queryByTitle('Rate Agree')).not.toBeNull();
      expect(screen.queryByTitle('Rate Funny')).not.toBeNull();
      expect(screen.queryByTitle('Citation Needed')).toBeNull();
    });
  });

  describe("as a logged in moderator user, on someone else's post", () => {
    const loggedInState = {
      user: {
        id: 3,
        username: 'Animefan2002',
        loggedIn: true,
        usergroup: 3,
        avatar_url: 'avatar.png',
      },
    };

    const userLocalStorageDetails = loggedInState.user;

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);

      jest.spyOn(store, 'getState').mockImplementation(() => loggedInState);
    });

    it("renders action buttons in the post's toolbar and clickable ratings", () => {
      customRender(
        <Post
          postDepth={1}
          totalPosts={10}
          username="Donglord"
          avatarUrl="1.webp"
          userJoinDate="2019-09-08T23:54:59.000Z"
          threadPage={1}
          threadId={1}
          threadLocked={false}
          postId={31333901}
          postBody="\nknockout!\nRules\nDiscord\nSubscriptions\nEvents\nDonglordDonglord's Avatar\nLABS\nHamburguerFarmers taking a hard hit as pandemic goes on; Beef & dairy sales plummet while some forced to mow down crops due to restaurant sales ending\nEdit title\n‹General Discussion\n1\n...\n16\n17\n18\n19\n20\n21\n22\n\n    Change background (Must be a Gold member)\n\nEdit title\nMark unread\nSubscribe\n\n    DonglordDonglord's avatarUser BackgroundSep 2019\n    a month ago\n\nAAAAAAAAAAAAAAAA\n\nDonglordDonglord's avatarUser BackgroundSep ..."
          postDate="2020-07-15T01:48:18.000Z"
          ratings={[]}
          user={userObject}
          byCurrentUser={false}
          bans={[]}
          subforumName="General Discussion"
          isUnread={false}
          countryName={null}
          isLinkedPost={false}
          postPage={1}
          hideUserWrapper={false}
          hideControls={false}
          subforumId={1}
          quickReplyEditor={() => {}}
        />,
        { initialState: loggedInState }
      );

      expect(screen.queryByTitle('Ban User')).not.toBeNull();
      expect(screen.queryByTitle('Report post')).not.toBeNull();
      expect(screen.queryByTitle('Show BBCode')).not.toBeNull();
      expect(screen.queryByTitle('Edit')).not.toBeNull();
      expect(screen.queryByTitle('Reply')).not.toBeNull();

      expect(screen.queryByTitle('Rate Agree')).not.toBeNull();
      expect(screen.queryByTitle('Rate Funny')).not.toBeNull();
      expect(screen.queryByTitle('Citation Needed')).toBeNull();
    });

    it("renders action buttons in the post's toolbar when the thread is locked", () => {
      customRender(
        <Post
          postDepth={1}
          totalPosts={10}
          username="Donglord"
          avatarUrl="1.webp"
          userJoinDate="2019-09-08T23:54:59.000Z"
          threadPage={1}
          threadId={1}
          threadLocked
          postId={31333901}
          postBody="\nknockout!\nRules\nDiscord\nSubscriptions\nEvents\nDonglordDonglord's Avatar\nLABS\nHamburguerFarmers taking a hard hit as pandemic goes on; Beef & dairy sales plummet while some forced to mow down crops due to restaurant sales ending\nEdit title\n‹General Discussion\n1\n...\n16\n17\n18\n19\n20\n21\n22\n\n    Change background (Must be a Gold member)\n\nEdit title\nMark unread\nSubscribe\n\n    DonglordDonglord's avatarUser BackgroundSep 2019\n    a month ago\n\nAAAAAAAAAAAAAAAA\n\nDonglordDonglord's avatarUser BackgroundSep ..."
          postDate="2020-07-15T01:48:18.000Z"
          ratings={[]}
          user={userObject}
          byCurrentUser={false}
          bans={[]}
          subforumName="General Discussion"
          isUnread={false}
          countryName={null}
          isLinkedPost={false}
          postPage={1}
          hideUserWrapper={false}
          hideControls={false}
          subforumId={1}
          quickReplyEditor={() => {}}
        />,
        { initialState: loggedInState }
      );

      expect(screen.queryByTitle('Ban User')).not.toBeNull();
      expect(screen.queryByTitle('Report post')).not.toBeNull();
      expect(screen.queryByTitle('Show BBCode')).not.toBeNull();
      expect(screen.queryByTitle('Edit')).not.toBeNull();
      expect(screen.queryByTitle('Reply')).not.toBeNull();
    });
  });

  describe('as a logged in blue user, on your own post', () => {
    const loggedInState = {
      user: userObject,
    };
    const userLocalStorageDetails = userObject;

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);

      jest.spyOn(store, 'getState').mockImplementation(() => loggedInState);
    });

    it("renders action buttons in the post's toolbar and clickable ratings", () => {
      customRender(
        <Post
          postDepth={1}
          totalPosts={10}
          username="Donglord"
          avatarUrl="1.webp"
          userJoinDate="2019-09-08T23:54:59.000Z"
          threadPage={1}
          threadId={1}
          threadLocked={false}
          postId={31333901}
          postBody="asdf"
          postDate="2020-07-15T01:48:18.000Z"
          ratings={[]}
          user={userObject}
          byCurrentUser
          bans={[]}
          subforumName="General Discussion"
          isUnread={false}
          countryName={null}
          isLinkedPost={false}
          postPage={1}
          hideUserWrapper={false}
          hideControls={false}
          subforumId={1}
          quickReplyEditor={() => {}}
        />,
        { initialState: loggedInState }
      );

      expect(screen.queryByTitle('Ban User')).toBeNull();
      expect(screen.queryByTitle('Report post')).not.toBeNull();
      expect(screen.queryByTitle('Show BBCode')).toBeNull();
      expect(screen.queryByTitle('Reply')).not.toBeNull();
      expect(screen.queryByTitle('Edit')).not.toBeNull();

      expect(screen.queryByTitle('Rate Agree')).toBeNull();
      expect(screen.queryByTitle('Rate Funny')).toBeNull();
      expect(screen.queryByTitle('Citation Needed')).toBeNull();
    });
  });

  describe('general rendering', () => {
    it('renders ban messages appropriately', () => {
      const { getByText } = customRender(
        <Post
          postDepth={1}
          totalPosts={10}
          username="Donglord"
          avatarUrl="1.webp"
          userJoinDate="2019-09-08T23:54:59.000Z"
          threadPage={1}
          threadId={1}
          threadLocked={false}
          postId={31333901}
          postBody="bans"
          postDate="2020-07-15T01:48:18.000Z"
          ratings={[]}
          user={userObject}
          byCurrentUser={false}
          bans={[
            {
              banReason: '111',
              banCreatedAt: '2030-07-13T13:08:53.000Z',
              banExpiresAt: '2030-06-13T14:08:53.000Z',
              banPostId: 31333899,
              banBannedBy: 'Donglord',
            },
            {
              banReason: 'tidde',
              banCreatedAt: '2030-07-13T13:58:21.000Z',
              banExpiresAt: '2030-06-13T14:58:21.000Z',
              banPostId: 31333899,
              banBannedBy: 'Donglord',
            },
          ]}
          subforumName="General Discussion"
          isUnread={false}
          countryName={null}
          isLinkedPost={false}
          postPage={1}
          hideUserWrapper={false}
          hideControls={false}
          subforumId={1}
          quickReplyEditor={() => {}}
        />,
        { initialState: { user: { loggedIn: false } } }
      );

      expect(
        getByText(
          (content, element) =>
            element.innerHTML ===
            `User was banned for this post by Donglord&nbsp;with reason "<strong>111</strong>" for&nbsp;-719 hours`
        )
      );

      expect(
        getByText(
          (content, element) =>
            element.innerHTML ===
            `User was banned for this post by Donglord&nbsp;with reason "<strong>tidde</strong>" for&nbsp;-719 hours`
        )
      );
    });

    it('renders avatar and background, with profile link', () => {
      customRender(
        <Post
          postDepth={1}
          totalPosts={10}
          username="Donglord"
          avatarUrl="1.webp"
          userJoinDate="2019-09-08T23:54:59.000Z"
          threadPage={1}
          threadId={1}
          threadLocked={false}
          postId={31333901}
          postBody="bans"
          postDate="2020-07-15T01:48:18.000Z"
          ratings={[]}
          user={userObject}
          byCurrentUser={false}
          bans={[]}
          subforumName="General Discussion"
          isUnread={false}
          countryName={null}
          isLinkedPost={false}
          postPage={1}
          hideUserWrapper={false}
          hideControls={false}
          subforumId={1}
          quickReplyEditor={() => {}}
        />,
        { initialState: { user: { loggedIn: false } } }
      );

      expect(screen.getByAltText(`Donglord's avatar`));
      expect(screen.getByAltText(`User Background`));
      expect(screen.getByTitle(`Donglord's profile`)).toHaveAttribute('href', '/user/1');
    });

    it('does not render a background, and renders a default avatar if not set', () => {
      customRender(
        <Post
          postDepth={1}
          totalPosts={10}
          username="Donglord"
          avatarUrl="1.webp"
          userJoinDate="2019-09-08T23:54:59.000Z"
          threadPage={1}
          threadId={1}
          threadLocked={false}
          postId={31333901}
          postBody="bans"
          postDate="2020-07-15T01:48:18.000Z"
          ratings={[]}
          user={userObjectNoBackground}
          byCurrentUser={false}
          bans={[]}
          subforumName="General Discussion"
          isUnread={false}
          countryName={null}
          isLinkedPost={false}
          postPage={1}
          hideUserWrapper={false}
          hideControls={false}
          subforumId={1}
          quickReplyEditor={() => {}}
        />,
        { initialState: { user: { loggedIn: false } } }
      );
      expect(screen.queryByAltText(`User Background`)).toBe(null);
      expect(screen.getByAltText(`Donglord's avatar`)).toHaveAttribute(
        'src',
        expect.stringContaining('none.webp')
      );
    });
  });
});
