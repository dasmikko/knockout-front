/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import dayjs from 'dayjs';
import { USER_GROUPS } from '../../../src/utils/userGroups';
import UserInfo from '../../../src/componentsNew/Post/components/UserInfo';
import { customRender } from '../../custom_renderer';

describe('UserInfo component', () => {
  let defaultProps = {};
  const createDate = dayjs().subtract(5, 'day').toISOString();

  beforeEach(() => {
    localStorage.clear();
    defaultProps = {
      user: {
        username: 'Bob',
        avatar_url: '',
        backgroundUrl: '',
        createdAt: createDate,
        id: 1,
        usergroup: USER_GROUPS.REGULAR_USER,
      },
      profileView: false,
      topRatings: [],
    };
  });

  it('does not display an empty background', () => {
    const { queryByAltText } = customRender(<UserInfo {...defaultProps} />);
    expect(queryByAltText('User Background')).toBeNull();
  });

  it('displays a different avatar for banned users', () => {
    defaultProps.user.isBanned = true;
    const { queryByAltText } = customRender(<UserInfo {...defaultProps} />);
    expect(queryByAltText('Bob is banned!')).not.toBeNull();
  });

  it('displays a title for moderators in training', () => {
    defaultProps.user.usergroup = USER_GROUPS.MODERATOR_IN_TRAINING;
    const { queryByText } = customRender(<UserInfo {...defaultProps} />);
    expect(queryByText('Moderator in training')).not.toBeNull();
  });

  it('hides the top rating of the user', () => {
    const { queryByTestId } = customRender(<UserInfo {...defaultProps} />);
    expect(queryByTestId('top-rating')).toBeNull();
  });

  it('displays the top rating on the user profile', () => {
    defaultProps.profileView = true;
    defaultProps.topRatings = [{ name: 'agree', count: 1 }];
    const { queryByTestId } = customRender(<UserInfo {...defaultProps} />);
    expect(queryByTestId('top-rating')).not.toBeNull();
  });

  it('displays a join date', () => {
    const { queryByText } = customRender(<UserInfo {...defaultProps} />);
    expect(queryByText(dayjs(createDate).format('MMM YYYY'))).not.toBeNull();
  });

  describe('with a deleted user', () => {
    beforeEach(() => {
      defaultProps.user.username = 'DELETED_USER';
    });

    it('does not display a background', () => {
      const { queryByAltText } = customRender(<UserInfo {...defaultProps} />);
      expect(queryByAltText('User Background')).toBeNull();
    });

    it('does not display a join date', () => {
      const { queryByText } = customRender(<UserInfo {...defaultProps} />);
      expect(queryByText(dayjs(createDate).format('MMM YYYY'))).toBeNull();
    });
  });
});
