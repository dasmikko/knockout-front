import React from 'react';
import { customRender } from '../custom_renderer';
import '@testing-library/jest-dom/extend-expect';

import BanModal from '../../src/componentsNew/BanModal';

describe('BanModal component', () => {
  it('displays if isOpen is true', () => {
    const { queryByText } = customRender(
      <BanModal userId={1} postId={1} submitFn={() => {}} cancelFn={() => {}} isOpen />
    );
    expect(queryByText('Ban User')).not.toBeNull();
  });

  it('does not display if isOpen is false', () => {
    const { queryByText } = customRender(
      <BanModal userId={1} postId={1} submitFn={() => {}} cancelFn={() => {}} isOpen={false} />
    );
    expect(queryByText('Ban User')).toBeNull();
  });

  it('displays "hours" in the period dropdown by default', () => {
    const { queryByText } = customRender(
      <BanModal userId={1} postId={1} submitFn={() => {}} cancelFn={() => {}} isOpen />
    );
    expect(queryByText('Hours')).not.toBeNull();
  });
});
