const path = require('path');
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const appConfig = require('./appConfig');

/**
 * @type {import('webpack').Configuration}
 */
module.exports = {
  resolve: {
    extensions: ['.jsx', '.js'],
  },
  entry: './src/App.jsx',
  devtool: 'source-map',
  output: {
    filename: 'static/js/[name].[contenthash:4].js',
    chunkFilename: 'static/js/[chunkhash:4][name].[contenthash:4].js',
    publicPath: '/',
    path: path.resolve(__dirname, './dist/client'),
  },
  devServer: {
    historyApiFallback: true,
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        test: /\.(js|jsx)$/,
        sourceMap: false,
        terserOptions: {
          mangle: true,
          output: { comments: false },
        },
        extractComments: false,
      }),
    ],
    splitChunks: {
      chunks: 'all',
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    ],
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: './static/template.ejs',
      filename: './index.html',
      templateParameters: { icon: appConfig.favicon },
    }),
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    new webpack.DefinePlugin({
      'process.appConfig': JSON.stringify(appConfig),
    }),
  ],
};
